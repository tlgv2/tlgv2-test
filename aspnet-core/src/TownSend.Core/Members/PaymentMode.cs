﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Members
{
    [Table("PaymentMode", Schema = "tlgv2")]
    public class PaymentMode : Entity
    {

        public string Name { get; set; }
        public string Alias { get; set; }

    }
}
