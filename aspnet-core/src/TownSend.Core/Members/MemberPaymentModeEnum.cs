﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members
{
    public enum MemberPaymentModeEnum
    {
        PayInFull=1
        , Deposit12EqualPayments
        , CustomPlanWithDeposit
        , CustomPlanWithOutDeposit
    }
}
