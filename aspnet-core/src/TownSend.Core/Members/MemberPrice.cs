﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Members
{
    [Table("MemberPrices", Schema = "tlgv2")]
    public class MemberPrice : FullAuditedEntity, ISoftDelete
    {
   
        public decimal BasePrice { get; set; }
        public decimal Discount { get; set; }
        public decimal Scholarship_Amount { get; set; }
        public decimal Membership_Amount { get; set; }
        public decimal Deposit { get; set; }
        public long MemberId { get; set; }
        public Boolean Concordia_Discount { get; set; }
        public Boolean Ministry_Discount { get; set; }

        public int PayInFullDiscount { get; set; }
        public DateTime CustomStartDate { get; set; }

    }
}
