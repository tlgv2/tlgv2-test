﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Members
{
    [Table("MemberInviteMapping", Schema = "tlgv2")]
    public class MemberInviteMapping : Entity
    {
        public long MemberId { get; set; }
        public long InviteId { get; set; }
    }
}
