﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Members
{
    [Table("Members", Schema = "tlgv2")]
    public class Member : FullAuditedEntity, ISoftDelete
    {
   
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MiddleName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int State { get; set; }
        public int ZipCode { get; set; }
        public long PhoneNumber { get; set; }
        public int TeamId { get; set; }

        public long UserId { get; set; }

        public MemberStatusEnum MemberStatusId { get; set; }
        public DateTime NotificationDate { get; set; }
        public string AboutMe { get; set; }
        public string FullName 
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
