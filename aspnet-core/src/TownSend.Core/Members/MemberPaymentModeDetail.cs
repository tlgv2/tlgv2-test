﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Members
{
    [Table("MemberPaymentModeDetails", Schema = "tlgv2")]
    public class MemberPaymentModeDetail : FullAuditedEntity, ISoftDelete
    {
        public long MemberPaymentModeMappingId { get; set; }
        public long MemberId { get; set; }
        public DateTime PayDate { get; set; }
        public decimal PayAmount { get; set; }
    }
}
