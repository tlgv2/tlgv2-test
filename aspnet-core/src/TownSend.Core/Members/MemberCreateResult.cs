﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members
{
    public class MemberCreateResult
    {
        public MemberCreateResult()
        {
            ErrorMessage = new List<string>();
        }
        public bool IsSuccess { get; set; }
        public List<string> ErrorMessage { get; set; }
    }
}
