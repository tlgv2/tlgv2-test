﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Members
{
    [Table("MemberPaymentModeMapping", Schema = "tlgv2")]
    public class MemberPaymentModeMapping : FullAuditedEntity, ISoftDelete
    {
        public long MemberId { get; set; }
        public MemberPaymentModeEnum PaymentMode { get; set; }
    }
}
