﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members
{
    public enum MemberStatusEnum
    {
        InviteSentNeverOpened=1
        ,InviteOpenedPendingContractSignature
        ,InviteOpenedPendingPaymentSelection
        ,PaymentMade
        ,RegistrationCompletePendingCheck
        ,RegistrationComplete
        ,InviteCancelled
    }
}
