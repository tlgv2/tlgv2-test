﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Members
{
    [Table("MemberTeamMapping", Schema = "tlgv2")]
    public class MemberTeamMapping : Entity
    {
        public long MemberId { get; set; }
        public long TeamId { get; set; }
    }
}
