﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend
{
    public static class Constant
    {
        public static string Success = "success";
        public static string Error = "error";
        public static string Warning = "warning";

        public const string SomethingwentWrong = "Something went wrong. Please try after sometime.";
        public const string ModelstateNotValid = "Please check validation errors.";

        public const string PartnerPercentageValidation = "Partner percentages must always equal to 100%";

        public static string noteAdded = "Note Added Successfully";
        public static string noteRemoved = "Note Removed Successfully";
        public static string linkAdded = "Link Added Successfully";
        public static string linkRemoved = "Link Removed Successfully";
        public static string documentAdded = "Document Added Successfully";
        public static string documentRemoved = "Document Removed Successfully";
        public static string documentUpdate = "Document Updated Successfully";
        public static string documentDownloaded = "Document Download Successfully";
        public static string deletePartner = "Partner Removed Successfully";
        public static string updateTeamPartner = "Partner Updated Successfully In Team";
        public static string addPartner = "Partner Added Successfully";
        public static string updatePartner = "Partner Updated Successfully";
        public static string addTeam = "Team Added Successfully";
        public static string updateTeam = "Team Updated Successfully";
        public static string resetPasswordmail = "Email Sent Successfully";
        public static string changePasswordSuccess = "Password Changed Successfully";
        public static string siteKey = "6LcNPNoUAAAAAAwgFvsf38yik9vXhPLuEKAnASKB";
        public static string secretKey = "6LcNPNoUAAAAAH9db7Bo2mRkMMw-H14NWfJBZmKn";


        public const string ScholarshipIvitationSent = "Scholarship submitted for approval.";

        public const string RecordsUpdatedMessage = "Record updated successfully";

        public const string ScholarshipApprovedMessage = "Approved";
        public const string ScholarshipDeniedMessage = "Denied";
        public const string ScholarshipCancelMessage = "Cancelled";
        public const string ScholarshipResendMessage = "Questionnaire resend successfully.";

        public const string memberIvitationSent = "Member submitted for approval.";
        public static string addMember = "Member Added Successfully";
        public static string updateMember = "Member Updated Successfully";

        public static class MvcFormats
        {
            public const string Date = "{0:MM/dd/yyyy}";
            //public const string DateNonFormated = "MM/dd/yyyy";
            public const string ShortDate = "{0:mm/dd/yyyy}";
            public const string Currency = "{0:C}";
            public const string Percentage = "%{0:F1}";
            public const string DateTimeAMPM = "{0:MM/dd/yyyy hh:mmtt}";
            //public const string SimpleDateTimeAMPM = "MM/dd/yyyy hh:mm tt";
            public const string Time = "{hh:mm:ss}";
        }
    }

    public enum EntityType
    {
        Partners=1,
        Teams=2,
        Members=3
    }

    
}
