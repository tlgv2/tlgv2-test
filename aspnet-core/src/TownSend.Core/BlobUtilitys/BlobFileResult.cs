﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using System.Text;

namespace TownSend.BlobUtilitys
{
    public class BlobFileResult : IFileResult
    {
        public BlobFileResult(CloudBlockBlob blockBlob, MemoryStream stream)
        {
            ContentType = blockBlob.Properties.ContentType;
            FileName = blockBlob.Name;
            Stream = stream;
        }
        public string ContentType { get; }
        public string FileName { get; set; }
        public Stream Stream { get; set; }
    }
}
