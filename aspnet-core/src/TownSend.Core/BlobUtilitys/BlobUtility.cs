﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TownSend.BlobUtilitys
{
    public class BlobUtility
    {
        public static string blobConnectionString= "DefaultEndpointsProtocol=https;AccountName=tlgv2data;AccountKey=8hzsm9vAwE0TsagzlrOqai+J+uLosPLiYccMzyxXLwm/r+FmBxbpc8HKomz4ruzJd9qnkzwNZwjxq8QHMcBhFg==;EndpointSuffix=core.windows.net";
        public async static Task<bool> CopyToBlob(string filePath, string container, string fileName, MemoryStream memory)
        {
            var copySuccessful = false;

            try
            {
                var blobConnexion = blobConnectionString;
                var storageAccount = CloudStorageAccount.Parse(blobConnexion);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var blobcontainer = blobClient.GetContainerReference(container);
                var blockBlob = blobcontainer.GetBlockBlobReference(filePath);
                await blockBlob.UploadFromStreamAsync(memory);

                copySuccessful = true;
            }
            catch (Exception e)
            {

            }

            return copySuccessful;
        }
        public async static Task<bool> FileInBlob(string filePath, string container, string fileName)
        {
            var fileFound = false;

            try
            {
                var blobConnexion = blobConnectionString;
                var storageAccount = CloudStorageAccount.Parse(blobConnexion);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var blobcontainer = blobClient.GetContainerReference(container);
                var blockBlob = blobcontainer.GetBlockBlobReference(filePath);
                await blockBlob.FetchAttributesAsync();

                fileFound = true;
            }
            catch (Exception e)
            {

            }

            return fileFound;
        }
        public async static Task<MemoryStream> CopyFromBlob(string filePath, string container, string fileName)
        {
            try
            {
                var blobConnexion =blobConnectionString;
                var storageAccount = CloudStorageAccount.Parse(blobConnexion);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var blobcontainer = blobClient.GetContainerReference(container);
                var blockBlob = blobcontainer.GetBlockBlobReference(filePath);
                var memory = new MemoryStream();
                await blockBlob.DownloadToStreamAsync(memory);

                return memory;
            }
            catch
            {
                return null;
            }
        }
        public async static Task<BlobFileResult> StreamFromBlob(string filePath, string container, string fileName)
        {
            try
            {
                var blobConnexion =blobConnectionString;
                var storageAccount = CloudStorageAccount.Parse(blobConnexion);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var blobcontainer = blobClient.GetContainerReference(container);
                var blockBlob = blobcontainer.GetBlockBlobReference(filePath);
                var memory = new MemoryStream();
                await blockBlob.DownloadToStreamAsync(memory);

                var result = new BlobFileResult(blockBlob, memory);
                return result;
            }
            catch
            {
                return null;
            }
        }
        public async static Task<bool> UploadProfile(string filePath, string container, string fileName, MemoryStream memory)
        {
            var copySuccessful = false;

            try
            {
                var blobConnexion = blobConnectionString;
                var storageAccount = CloudStorageAccount.Parse(blobConnexion);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var blobcontainer = blobClient.GetContainerReference(container);
                var blockBlob = blobcontainer.GetBlockBlobReference(filePath);
                blockBlob.Properties.ContentType = "image/jpg";
                await blockBlob.UploadFromStreamAsync(memory);

                copySuccessful = true;
            }
            catch (Exception e)
            {

            }

            return copySuccessful;
        }

        public async static Task<CloudBlockBlob> GetProfileFromBlob(string filePath, string container)
        {
            try
            {
                var blobConnexion = blobConnectionString;
                var storageAccount = CloudStorageAccount.Parse(blobConnexion);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var blobcontainer = blobClient.GetContainerReference(container);
                var blockBlob = blobcontainer.GetBlockBlobReference(filePath);                
                var memory = new MemoryStream();
                blockBlob.Properties.ContentType = "image/jpg";
                await blockBlob.SetPropertiesAsync();
                return blockBlob;
                //await blockBlob.DownloadToStreamAsync(memory);

                //var result = new BlobFileResult(blockBlob, memory);
                //return result;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
