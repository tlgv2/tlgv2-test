﻿    using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TownSend.BlobUtilitys
{
    public interface IFileResult
    {
        string ContentType { get; }
        string FileName { get; set; }
        Stream Stream { get; set; }
    }
}
