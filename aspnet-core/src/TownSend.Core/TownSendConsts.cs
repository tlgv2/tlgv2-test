﻿namespace TownSend
{
    public class TownSendConsts
    {
        public const string LocalizationSourceName = "TownSend";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = false;

        public const int QuestionnaireTextAreaStringLimit = 10000;
        public const int QuestionnaireTextAreaStringServerLimit = 20000;

        public class EmailTemplateAliasConsts
        {
            public const string ScholarshipCreatedToAdmin = "ScholarshipCreatedToAdmin";
            public const string ScholarshipQuestionnaireToMember = "ScholarshipQuestionnaireToMember";
            public const string ScholarshipApprovedToMember = "ScholarshipApprovedToMember";
            public const string ScholarshipApprovedToPartner = "ScholarshipApprovedToPartner";
            public const string ScholarshipDeniedToMember = "ScholarshipDeniedToMember";
            public const string ScholarshipDeniedToPartner = "ScholarshipDeniedToPartner";
            public const string InvitationToMember = "InvitationToMember";
            public const string UsersResetPassword = "UsersChangePassword";
        }

        public class EmailSettings
        {
            public const string SupportFromEmailAddress = "support@hubonesystems.com";
            public const string AddPartnerEmailSubject = "Join us as a new Partner!";
            public const string AddMemberEmailSubject = "Join us as a new Member!";
            //public const string ScholarshipApproverEmail = "psells@sellsgroup.com";
            public const string ScholarshipApproverEmail = "hardik@softvan.in";
            public const string ScholarshipApproverFullName = "Patrick";
            //public const string DefaultFromEmailAddress = "noreply@hubonesystems.com";
            public const string DefaultFromEmailAddress = "hardik@softvan.in";
            public const string DefaultFromDisplayName = "HubOne Support";
            public const string ChangePasswordByAdminEmailSubject = "Change Password By Admin";
            public const string ChangePasswordEmailSuject = "Password Reset";
            public const string ChangePasswordByAdminTitleSubject = "Password Change";
            public const string ChangePasswordTitleSubject = "Password Reset";
            public const string ChangePasswordDisplayName = "Password Reset";
            public const string ResetPasswordEmailBody = "<strong>Please reset your password  by clicking the link provided: </strong><a href=\"[link]\">Reset Password</a>";


        }
        public class EmailSubject
        {    
            public const string ScholarshipApproverEmail = "nkidwell@hubonesystems.com";
            public const string ScholarshipApproverFullName = "Patrick";            
            public const string DefaultFromEmailAddress = "noreply@townsendleadership.com";
            public const string DefaultFromDisplayName = "Townsend Leadership";
        }

        public class PaymentConsts
        {
            public class QuickBooksDetails
            {
                public const string ClientId = "ABP5rP0DzL5RVTxVSG9bTiyvwXAAV1uGf09yY4awxTJIu5NARD";
                public const string ClientSecret = "3BYsRiQAu2fzAuNTKHRxaCGvD0ZxQ6muVJty60k0";
                public const string CompanyId = "4620816365041110580";
                
                public const string Environment = "sandbox";
                public const string BaseURL = "sandbox-quickbooks.api.intuit.com";
            }

            public class QuickBooksURLs
            {
                public const string RedirectURL = "https://developer.intuit.com/v2/OAuth2Playground/RedirectUrl";

                //sandbox urls
                public const string PaymentBaseSandboxURL = "https://sandbox.api.intuit.com";
                public const string AccountingBaseSandboxURL = "https://sandbox-quickbooks.api.intuit.com";

                //production urls
                public const string PaymentBaseProdURL = "https://sandbox.api.intuit.com";
                public const string AccountingBaseProdURL = "https://sandbox-quickbooks.api.intuit.com";
            }

            public class QbiBooksTokens
            {
                public const string AccessToken = "";
                public const string RefreshToken = "AB11593351187y08gNEEVdqb6NJl6fPTMHSbUUiq4yYIIRO6JE";

            }
        }
    }
}
