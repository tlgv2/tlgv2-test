﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Scholarships
{
    [Table("ScholarshipStatus", Schema ="tlgv2")]
    public class ScholarshipStatus : Entity
    {
        public string Status { get; set; }

        public string Alias { get; set; }
    }
}
