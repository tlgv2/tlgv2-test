﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Scholarships
{
    [Table("Scholarship", Schema = "tlgv2")]
    public class Scholarship : Entity, ISoftDelete, ICreationAudited
    {
        public int MemberId { get; set; }
        public int TeamId { get; set; }
        public decimal ScholarshipAmount { get; set; }
        public bool? IsApproved { get; set; }
        public long ApprovedBy { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public bool IsDeleted { get; set; }
        public bool FirstTimeApply { get; set; }
        public bool FirstTimeinTLP { get; set; }
        public string GrowthDetails { get; set; }
        public string AchivementsDetails { get; set; }
        public string LifeExperienceDetails { get; set; }
        public string FutureGoalDetails { get; set; }
        public DateTime CompletionDate { get; set; }
        public DateTime NotificationDate { get; set; }

        public ScholarshipStatusEnum ScholarshipStatus { get; set; }
        public DateTime CreationTime { get ; set ; }
        public long? CreatorUserId { get; set; }
    }
}
