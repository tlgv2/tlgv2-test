﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Scholarships
{
    public class ScholarshipConsts
    {
        public class Heading
        {
            public const string ApprovedSendInvite = "Questionnaire Completed";
            public const string Cancelled = "Scholarship Cancelled";
            public const string DeniedSendInvite = "Scholarship Denied";
            public const string PendingApproval = "Questionnaire Completed";
        }

        public class Message
        {
            public const string ApprovedSendInvite = "You've already completed the questionnaire and the scholarship has been approved.";
            public const string Cancelled = "This questionnaire is no longer active. Please contact your Partner for more information.";
            public const string DeniedSendInvite = "You've already completed the questionnaire. However, the scholarship was denied.";
            public const string PendingApproval = "You've already completed the questionnaire and the scholarship is pending approval.";
        }

    }
}
