﻿using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TownSend.Scholarships
{
    public class ScholarshipManager : TownSendDomainServiceBase
    {
        private readonly IRepository<Scholarship> _scholarshipRepository;
        public ScholarshipManager(IRepository<Scholarship> scholarshipRepository)
        {

            _scholarshipRepository = scholarshipRepository;
        }

        public bool IsScholarshipDuplicate()
        {   
            return false;
        }

        public ScholarshipValidityResult ValidateSchorlarshipById(int id)
        {
            var scholarshipValidity = new ScholarshipValidityResult
            {
                IsValid = false
            };

            var scholarship = _scholarshipRepository.GetAll().Where(x => x.Id == id).FirstOrDefault();
            if (scholarship != null)
            {
                if(scholarship.ScholarshipStatus == ScholarshipStatusEnum.QuestionnaireSent)
                {
                    scholarshipValidity.IsValid = true;
                    scholarshipValidity.ScholarshipId = scholarship.Id;
                    scholarshipValidity.CurrentStatus = scholarship.ScholarshipStatus;
                }
                else
                {
                    scholarshipValidity.IsValid = false;
                    scholarshipValidity.ScholarshipId = scholarship.Id;
                    scholarshipValidity.CurrentStatus = scholarship.ScholarshipStatus;
                }                
            }

            return scholarshipValidity;
        }

        public class ScholarshipValidityResult
        {
            public int ScholarshipId { get; set; }
            public ScholarshipStatusEnum CurrentStatus { get; set; }
            public bool IsValid { get; set; }
        }
    }
}
