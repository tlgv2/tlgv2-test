﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Scholarships
{
    [Table("ScholarshipInviteMapping", Schema = "tlgv2")]
    public class ScholarshipInviteMapping : Entity
    {
        public int ScholarshipId { get; set; }
        public long InviteId { get; set; }
    }
}
