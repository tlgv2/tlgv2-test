﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Scholarships
{
    public class ScholarshipRegisterResult
    {
        public ScholarshipRegisterResult()
        {
            ErrorMessage = new List<string>();
        }
        public bool IsSuccess { get; set; }
        public List<string> ErrorMessage { get; set; }
    }
}
