﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Scholarships
{
    public enum ScholarshipStatusEnum
    {
        QuestionnaireSent = 1
        ,PendingApproval 
        ,ApprovedSendInvite
        ,DeniedSendInvite
        ,Cancelled
    }
}
