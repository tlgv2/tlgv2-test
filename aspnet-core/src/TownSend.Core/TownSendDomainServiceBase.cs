﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend
{
    public abstract class TownSendDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected TownSendDomainServiceBase()
        {
            LocalizationSourceName = TownSendConsts.LocalizationSourceName;
        }
    }
}
