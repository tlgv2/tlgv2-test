﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public enum CardType
    {
        VISA,
        MC,
        AMEX,
        DISC,
        DINERS,
        JCB
    }
}
