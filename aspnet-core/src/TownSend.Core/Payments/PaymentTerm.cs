﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public enum PaymentTerm
    {
        PeriodicInstallment,
        SingleInstallment,
        LumpSum,
        AdminManual
    }
}
