﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public class HubOneCustomer
    {
        public string HubOneCustomerID { get; set; }

        //ID used by external payment API
        public string APIReferenceID { get; set; }

        public string DisplayName { get; set; }
        public string Title { get; set; }
        public string GivenName { get; set; }
        public string MiddleName { get; set; } = "";
        public string Suffix { get; set; } = "";
        public string FamilyName { get; set; }
        public string PrimaryEmailAddress { get; set; }
        public string FullyQualifiedName { get; set; }
        public string PrimaryPhone { get; set; }
        public string Notes { get; set; }


        public string CompanyName { get; set; } = "";

        /*Customer Address information*/
        public string BillingAddress { get; set; }
        public string CountrySubDivisionCode { get; set; } //It's a state
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string StreetName { get; set; }
        public string CivicNumber { get; set; }
        public string AppartmentOrSuite { get; set; }
        public string Country { get; set; } = "USA";


        #region UnusedFields
        /* these fields I can see a use for maybe later on, but for now, we
         don't use them. */

        //public string AlternateNumber { get; set; }
        //public string MobileNumber { get; set; }
        #endregion

        public string PrintOutCustomerInformation()
        {
            return string.Concat(
                  "Give Name - ", GivenName, Environment.NewLine
                , "Family Name - ", FamilyName, Environment.NewLine
                , "Middle Name - ", MiddleName, Environment.NewLine
                , "Display Name - ", DisplayName, Environment.NewLine
                , "Title - ", Title, Environment.NewLine
                , "HubCusID - ", HubOneCustomerID, Environment.NewLine
                , "Email - ", PrimaryEmailAddress, Environment.NewLine
                , "City - ", City, Environment.NewLine
                , "PostalCode - ", PostalCode, Environment.NewLine
                , "Streetname - ", StreetName, Environment.NewLine
                , "Civic # - ", CivicNumber, Environment.NewLine
                , "Country - ", Country, Environment.NewLine
                , "State - ", CountrySubDivisionCode, Environment.NewLine);
        }

    }
}
