﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public enum HubOnePaymentDisposition
    {
        COACH_Payment_Scheduled,
        COACH_Payment_Manual,
        COACH_Payment_Cancelled,
        COACH_Payment_Processing,
        COACH_Payment_Completed,
        COACH_Payment_Declined_Expired,
        COACH_Payment_Declined_Compromised,
        COACH_Payment_Declined_NoFund,
        COACH_Payment_Declined_Unallowed,
        COACH_Payment_Declined_UnAuthorised,
        COACH_Payment_Declined_Other,
        COACH_Payment_Deferred_Next,
        COACH_Payment_Deferred_Remaining,
        COACH_Payment_Deferred_Additional,
        COACH_Credit_Processing,
        COACH_Credit_Completed,
        COACH_Credit_Declined_Other,
        COACH_Credit_Cancelled,
        COACH_Credit_Manual,
        COACH_Credit_Scheduled
    }
}
