﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public class ExistingPaymentMethodRead
    {
        public int PaymentMethodId { get; set; }
        public int CustomerID { get; set; }
        public string CardNumber { get; set; }
        public string ExpMonth { get; set; }
        public string ExpYear { get; set; }
        public string CVC { get; set; }

        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
    }
}
