﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public enum InvoiceType
    {
        COACH_LumpSum,
        COACH_Deposit,
        COACH_Periodic
    }
}
