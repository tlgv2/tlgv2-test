﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public class PaymentMethodTypeRead
    {
        public string PaymentMethodTypeH1Name { get; set; }
        public int PaymentMethodTypeID { get; set; }
    }
}
