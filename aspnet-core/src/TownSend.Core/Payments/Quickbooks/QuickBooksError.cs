﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Quickbooks
{
    public class QuickBooksError
    {
        public string code { get; set; }
        public string type { get; set; }
        public string message { get; set; }
        public string detail { get; set; }
        public string moreInfo { get; set; }
        public string infoLink { get; set; }
    }
}
