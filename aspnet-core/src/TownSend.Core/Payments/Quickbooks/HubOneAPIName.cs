﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Quickbooks
{
    public enum HubOneAPIName
    {
        QuickbooksPayment,
        QuickbooksAccounting
    }
}
