﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Quickbooks
{
    public class Result
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }

    public class Result<T>
    {
        public T Value { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
