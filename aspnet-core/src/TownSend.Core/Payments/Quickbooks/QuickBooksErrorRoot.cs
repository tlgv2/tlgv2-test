﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Quickbooks
{
    public class QuickBooksErrorRoot
    {
        public List<QuickBooksError> errors { get; set; }
    }
}
