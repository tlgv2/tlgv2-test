﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public enum PaymentChargeStatus
    {
        Authorized,
        Cancelled,
        Captured,
        Declined,
        Failed,
        Refunded,
        Settled,
        Issued,
        RefundInProgress, //fake status

        //Echeck status
        EcheckPending,
        EcheckSucceeded,
        EcheckDeclined,
        EcheckVoided,
        EcheckRefunded,
        EcheckFailed,
        EcheckRefundInProgress
    }
}
