﻿using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Payments.Quickbooks;

namespace TownSend.Payments
{
    public class HubOneCustomerAPIInformation
    {
        public int CustomerID { get; set; }//HubOneCustomerID
        public HubOneAPIName APIName { get; set; }

    }
}
