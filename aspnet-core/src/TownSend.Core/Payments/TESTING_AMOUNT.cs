﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public enum TESTING_AMOUNT
    {
        PENDING,
        DECLINED,
        SUCCEEDED,
        VOIDED,
        REFUNDED
    }
}
