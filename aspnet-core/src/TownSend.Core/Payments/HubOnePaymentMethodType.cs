﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public enum HubOnePaymentMethodType
    {
        COACH_BankAccount,
        COACH_Check,
        COACH_CreditCard,
        COACH_Manual
    }
}
