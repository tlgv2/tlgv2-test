﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments
{
    public class PaymentToProcess
    {
        public int PaymentID { get; set; }
        public string TeamName { get; set; }
        public string PaymentMethodType { get; set; }
        public string QuickbooksPaymentMethodId { get; set; }
        public decimal PaymentTotal { get; set; }
        public string HubOnePaymentCode { get; set; }

        public string PrintPaymentInformation()
        {
            return string.Concat(
                  PaymentID
                , " - "
                , TeamName
                , " - "
                , PaymentMethodType
                , " - "
                , QuickbooksPaymentMethodId
                , " - "
                , PaymentTotal
                , " - "
                , HubOnePaymentCode);

        }
    }
}
