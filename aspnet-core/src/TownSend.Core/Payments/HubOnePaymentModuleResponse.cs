﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TownSend.Payments
{
    public class HubOnePaymentModuleResponse
    {

        public string CallInternalCode { get; set; }

        public string ResponseMessage { get; set; }

        public bool IsSuccessful { get; set; }

        //Business oriented data gets bounds here
        public IDictionary<string, string> ResponseValues { get; set; }

        public string ResponseValueType { get; set; }

        //Raw API Data gets bounds here
        public IDictionary<string, string> APIData { get; set; }

        public List<string> ErrorList { get; set; }

        public HubOnePaymentModuleResponse()
        {
        }

        public HubOnePaymentModuleResponse
        (

            string callInternalCode,
            string responseMessage,
            bool isSuccessful,
            Dictionary<string, string> responseValue,
            string responseValueType,
            Dictionary<string, string> apiData,
            List<string> errorList


        )
        {

            CallInternalCode = callInternalCode;
            ResponseMessage = responseMessage;
            IsSuccessful = isSuccessful;
            ResponseValues = responseValue;
            ResponseValueType = responseValueType;
            APIData = apiData;
            ErrorList = errorList;

        }

        public Dictionary<string, string> GetAPIDataAndResponses()
        {

            if (ResponseValues == null)
                ResponseValues = new Dictionary<string, string>();

            if (APIData == null)
                APIData = new Dictionary<string, string>();


            return APIData.Union(ResponseValues).ToDictionary(d => d.Key, d => d.Value);
        }

        public string ToString(bool full, string preText = null)
        {
            return ToString(full);
        }

        private string ToString(bool full)
        {
            if (full)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("IsSuccessful: {0};", IsSuccessful);
                sb.AppendFormat("ResponseMessage: {0};", ResponseMessage);

                if (!string.IsNullOrEmpty(CallInternalCode))
                    sb.AppendFormat("CallInternalCode: {0};", CallInternalCode);

                if (!string.IsNullOrEmpty(ResponseValueType))
                    sb.AppendFormat("ResponseValueType: {0};", ResponseValueType);

                if (ResponseValues != null)
                    foreach (var item in ResponseValues)
                    {
                        sb.AppendFormat("{0}: {1};", item.Key, item.Value);
                    }

                if (APIData != null)
                    foreach (var item in APIData)
                    {
                        sb.AppendFormat("{0}: {1};", item.Key, item.Value);
                    }

                return sb.ToString();
            }
            else
            {
                return ResponseMessage;
            }
        }

    }
}
