﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TownSend.Payments
{
    public enum HubOneBankAccountType
    {
        [Description("Personal Checking")]
        PERSONAL_CHECKING,
        [Description("Personal Savings")]
        PERSONAL_SAVINGS,
        [Description("Business Checking")]
        BUSINESS_CHECKING,
        [Description("Business Savings")]
        BUSINESS_SAVINGS
    }
}
