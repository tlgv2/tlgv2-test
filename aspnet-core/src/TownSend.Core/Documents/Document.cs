﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Documents
{
    [Table("Documents", Schema = "tlgv2")]
    public class Document : FullAuditedEntity<long>
    {        
        public string  Name { get; set; }
        public string Description { get; set; }

        public string DocumentPath { get; set; }
        
        public DateTime? VisibleDateFrom { get; set; }
        
        public DateTime? VisibleDateTo { get; set; }
        public Decimal Size { get; set; }
    }
}
