﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace TownSend.Documents
{
    public class DocumentManager : TownSendDomainServiceBase
    {
        private IRepository<Document, long> _documentRepository;
        private IRepository<DocumentPartnerMapping, long> _documentPartnerRepository;
        private IRepository<DocumentTeamMapping, long> _documentTeamRepository;
        private IRepository<DocumentMemberMapping, long> _documentMemberRepository;

        public DocumentManager(IRepository<Document,long> documentRepository, 
                                IRepository<DocumentPartnerMapping, long> documentPartnerRepository,
                                IRepository<DocumentTeamMapping,long> documentTeamRepository,
                                IRepository<DocumentMemberMapping, long> documentMemberRepository)
        {
            _documentRepository = documentRepository;
            _documentPartnerRepository = documentPartnerRepository;
            _documentTeamRepository = documentTeamRepository;
            _documentMemberRepository = documentMemberRepository;
        }


        public async Task<List<Document>> GetDocumentForPartnerAsync(long mapId)
        {
            var query = await(from doc in _documentRepository.GetAll()
                         join partnerMap in _documentPartnerRepository.GetAll() on doc.Id equals partnerMap.DocumentId
                         where partnerMap.PartnerId==mapId
                         select doc).ToListAsync();
            return query;
        }

        public async Task<List<Document>> GetDocumentForTeamAsync(long mapId)
        {
            var query = await (from doc in _documentRepository.GetAll()
                               join teamMap in _documentTeamRepository.GetAll() on doc.Id equals teamMap.DocumentId
                               where teamMap.TeamId==mapId
                               select doc).ToListAsync();
            return query;
        }

        public async Task<List<Document>> GetDocumentForMemberAsync(long mapId)
        {
            var query = await (from doc in _documentRepository.GetAll()
                               join memberMap in _documentMemberRepository.GetAll() on doc.Id equals memberMap.DocumentId
                               where memberMap.MemberId == mapId
                               select doc).ToListAsync();
            return query;
        }
    }
}
