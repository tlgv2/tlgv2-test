﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Documents
{
    [Table("DocumentPartnerMappings", Schema = "tlgv2")]
    public class DocumentPartnerMapping : FullAuditedEntity<long>
    {
        public long DocumentId { get; set; }
        public long PartnerId { get; set; }
    }
}
