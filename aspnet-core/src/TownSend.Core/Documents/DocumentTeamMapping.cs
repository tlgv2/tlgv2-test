﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Documents
{
    [Table("DocumentTeamMapping", Schema = "tlgv2")]
    public class DocumentTeamMapping : FullAuditedEntity<long>
    {
        public long DocumentId { get; set; } 
        public int TeamId { get; set; }
    }
}
