﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Documents
{
    [Table("DocumentMemberMapping", Schema = "tlgv2")]
    public class DocumentMemberMapping : FullAuditedEntity<long> 
    {
        public long DocumentId { get; set; }
        public int MemberId { get; set; }
    }
}
