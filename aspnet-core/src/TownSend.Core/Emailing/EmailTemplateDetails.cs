﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Emailing
{
    public class EmailTemplateDetails
    {
        public string Body { get; set; }
        public string Subject { get; set; }
        public string FromEmail { get; set; }
        public string Alias { get; set; }
    }
}
