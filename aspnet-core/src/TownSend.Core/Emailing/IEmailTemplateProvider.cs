﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Emailing
{
    public interface IEmailTemplateProvider
    {
        string GetDefaultTemplate(int? tenantId);

        string GetMemberInviteTemplate(int? tenantId);
    }
}
