﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Blob
{
    public class BlobInfo
    {
        public BlobInfo()
        {

        }
        public BlobInfo(string fileNameAndFilePath, string container)
        {
            FileName = fileNameAndFilePath;
            Container = container;
            FilePath = fileNameAndFilePath;
        }

        public BlobInfo(string fileName, string filePath, string container)
        {
            FileName = fileName;
            Container = container;
            FilePath = filePath;
        }

        public string FileName { get; set; }    // Name of File
        public string Container { get; set; }   // Blob Container
        public string FilePath { get; set; }    // Blob Path
    }
}
