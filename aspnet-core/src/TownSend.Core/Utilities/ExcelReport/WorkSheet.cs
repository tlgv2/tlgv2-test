﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TownSend.Blob;
using TownSend.BlobUtilitys;

namespace TownSend.Utilities.ExcelReport
{
    public class WorkSheet
    {
        public async static void ExcelGeneratorExport(BlobInfo info, DataTable data)
        {
            bool copySuccessful;

            if (info == null)
                throw new ArgumentNullException("info");

            MemoryStream memory = BuildFile(info.FileName, data);

            memory.Position = 0;

            if (memory != null && memory.CanRead)
            {
                copySuccessful = await BlobUtility.CopyToBlob(info.FilePath, info.Container, info.FileName, memory);

                if (!copySuccessful)
                    throw new InvalidDataException("PDF file cannot be loaded to blob.");
            }
            else
            {
                throw new InvalidDataException("Excel file cannot be loaded from stream.");
            }
        }

        public async static Task<MemoryStream> ExcelExport(BlobInfo info, DataTable data)
        {
            bool copySuccessful;

            if (info == null)
                throw new ArgumentNullException("info");

            MemoryStream memory = BuildFile(info.FileName, data);

            memory.Position = 0;

            if (memory != null && memory.CanRead)
            {
                copySuccessful = await BlobUtility.CopyToBlob(info.FilePath, info.Container, info.FileName, memory);

                if (!copySuccessful)
                    throw new InvalidDataException("PDF file cannot be loaded to blob.");
            }
            else
            {
                throw new InvalidDataException("Excel file cannot be loaded from stream.");
            }

            return memory;
        }

        public static MemoryStream BuildFile(string fileName, DataTable data)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            var row = 1;
            var column = 1;

            IDictionary<int, string> headers = new Dictionary<int, string>();

            for (int i = 0; i < data.Columns.Count; i++)
            {
                headers.Add(i, data.Columns[i].ColumnName);
            }
            

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var memory = new MemoryStream();
            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(fileName);

                for (int i = 0; i < headers.Count; i++)
                {
                    column = i + 1;

                    worksheet.Cells[row, column].Value = headers[i];
                }

                for (int i = 1; i < data.Rows.Count + 1; i++)
                {
                    row = 1 + i;

                    for (int j = 0; j < headers.Count; j++)
                    {
                        column = 1 + j;

                        string Type = data.Columns[j].DataType.ToString().ToLower();

                        if (Type.Contains("int"))
                        {
                            worksheet.Cells[row, column].Value = Convert.ToInt32(data.Rows[i - 1][headers[j]]);
                        }
                        else if (Type.Contains("decimal"))
                        {
                            worksheet.Cells[row, column].Value = Convert.ToDecimal(data.Rows[i - 1][headers[j]]);
                        }
                        else if (Type.Contains("double"))
                        {
                            worksheet.Cells[row, column].Value = Convert.ToDecimal(data.Rows[i - 1][headers[j]]);
                        }
                        else
                        {
                            worksheet.Cells[row, column].Value = data.Rows[i - 1][headers[j]].ToString();
                        }


                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                 memory = new MemoryStream(package.GetAsByteArray());
            }                                  
            return memory;
        }
    }
}
