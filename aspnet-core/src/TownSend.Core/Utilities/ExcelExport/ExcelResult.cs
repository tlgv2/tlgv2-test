﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TownSend.BlobUtilitys;

namespace TownSend.Utilities.ExcelExport
{
    public class ExcelResult : IFileResult
    {
        public ExcelResult(Stream stream, string filename)
        {
            Stream = stream;
            FileName = filename;
        }

        //public string ContentType => Data.Constants.ContentTypes.OpenXml;
        public string ContentType => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public string FileName { get; set; }
        public Stream Stream { get; set; }
    }
}
