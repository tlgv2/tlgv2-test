﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.AuditHistory
{
    [Table("TeamPartnerAuditHistoryMapping", Schema = "tlgv2")]
    public class TeamPartnerAuditHistoryMapping : FullAuditedEntity
    {
        public string AuditAction { get; set; }
        public int TeamPartnerMappingId { get; set; }
        public int TeamId { get; set; }

        public int PartnerId { get; set; }
        public DateTime AuditStartDate { get; set; }
        public DateTime AuditEndDate { get; set; }
    }
}
