﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using Abp.Authorization.Users;
using TownSend.Authorization.Users;
using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace TownSend.AuditHistory
{
    [Table("PartnerAuditHistory", Schema = "tlgv2")] 
    public class PartnerAuditHistory : FullAuditedEntity ,ISoftDelete
    {
        public string AuditAction { get; set; }
        public int PartnerId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public long PhoneNumber { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public int ZipCode { get; set; }

        public string AboutMe { get; set; }

        public PartnerStatus PartnerStatus { get; set; }
        public long UserId { get; set; }
        public string Country { get; set; }
        public string ProfilePath { get; set; }
        public DateTime AuditStartDate { get; set; }
        public DateTime AuditEndDate { get; set; }

    }
    public enum PartnerStatus
    {
        PartnerInvited = 1,
        PartnerRegistered = 2,
    }
}
