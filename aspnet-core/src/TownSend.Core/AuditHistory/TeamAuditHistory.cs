﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.AuditHistory
{
    [Table("TeamsAuditHistory", Schema = "tlgv2")]
    public class TeamAuditHistory : FullAuditedEntity
    {
        public string AuditAction { get; set; }
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public string Description { get; set; }
        public int TeamPrice { get; set; }
        public int Deposit { get; set; }
        public bool IsPaymentRecieved { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime AuditStartDate { get; set; }
        public DateTime AuditEndDate { get; set; }
    }
}
