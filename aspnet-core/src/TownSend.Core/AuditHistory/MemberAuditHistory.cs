﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.AuditHistory
{
    public class MemberAuditHistory : FullAuditedEntity
    {
        public string AuditAction { get; set; }
        public int MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string TeamId { get; set; }

        public long UserId { get; set; }
    }
}
