﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Templates
{
    [Table("EmailType", Schema = "tlgv2")]
    public class EmailType : Entity
    {
        public string Name { get; set; }
        public string Module { get; set; }
        public string Sender { get; set; }
        public string Alias { get; set; }
    }
}
