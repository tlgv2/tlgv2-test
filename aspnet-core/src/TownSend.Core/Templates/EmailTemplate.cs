﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Templates
{
    [Table("EmailTemplates", Schema = "tlgv2")]
    public class EmailTemplate : Entity, ISoftDelete
    {
        public EmailTypeEnum EmailType { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Alias { get; set; }
        public bool IsDeleted { get; set; }

    }
}
