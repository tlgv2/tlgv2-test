﻿using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Invites
{
    [Table("Invite", Schema = "tlgv2")]
    public class Invite : FullAuditedEntity<long>
    {
        public Invite()
        {
            ExpirationDate = this.CreationTime.AddDays(1);
        }

        public long UserId { get; set; }
        public int RoleId { get; set; }
        public int TeamId { get; set; }
        public string InviteToken { get; set; }        
        public DateTime ExpirationDate { get; set; }
        public int StatusId { get; set; }

        public bool IsActive { get; set; }
        public bool? IsProxyInvite { get; set; }
    }
}
