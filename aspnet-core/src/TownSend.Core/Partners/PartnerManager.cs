﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.UI;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TownSend.AuditHistory;
using TownSend.Authorization.Roles;
using TownSend.Authorization.Users;
using TownSend.Configuration;
using TownSend.Invites;

namespace TownSend.Partners
{
    public class PartnerManager : TownSendDomainServiceBase 
    {
        private readonly IRepository<Partner> _partnerRepository;
        private readonly IRepository<Invite, long> _inviteRepository;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IUserEmailer _userEmailer;
        private readonly IPasswordHasher<User> _passwordHasher;

        public PartnerManager(
                                IRepository<Partner> partnerRepository,
                                IRepository<Invite, long> inviteRepository,
                                UserManager userManager,
                                RoleManager roleManager,
                                IUserEmailer userEmailer,
                                IPasswordHasher<User> passwordHasher)
        {
            _partnerRepository = partnerRepository;
            _inviteRepository = inviteRepository;
            _userManager = userManager;
            _roleManager = roleManager;
            _userEmailer = userEmailer;
            _passwordHasher = passwordHasher;
        }
        /// <summary>
        /// This Method Register Partner and Also Create User and Assign Role And Invite them Using Send Email
        /// Affected Entities
        ///     Partners
        ///     User
        ///     UserRole
        ///     Role
        /// </summary>
        /// <param name="tenantId"></param>
        /// <param name="name"></param>
        /// <param name="surname"></param>
        /// <param name="emailAddress"></param>
        /// <param name="plainPassword"></param>
        /// <param name="isEmailConfirmed"></param>
        /// <param name="emailActivationLink"></param>
        /// <param name="roleId"></param>
        /// <param name="partner"></param>
        /// <returns></returns>
        public async Task<Partner> RegisterAsync(int? tenantId, string name, string surname, string emailAddress,string PhoneNmuber , string plainPassword, bool isEmailConfirmed, string emailActivationLink, int? roleId, Partner partner)
        {
          
                var checkEmail = await _userManager.CheckDuplicateUsernameOrEmailAddressAsync(0, userName: emailAddress, emailAddress: emailAddress);
                if(checkEmail.Succeeded)
                {

                }
                var isNewRegisteredUserActiveByDefault = false;
                //Create User
                var user = new User
                {
                    TenantId = tenantId,
                    Name = name,
                    Surname = surname,
                    EmailAddress = emailAddress,
                    PhoneNumber=PhoneNmuber.ToString(),
                    IsActive = isNewRegisteredUserActiveByDefault,
                    UserName = emailAddress,
                    IsEmailConfirmed = isEmailConfirmed,
                    Roles = new List<UserRole>(),
                    //PhoneNumber = PhoneNumber
                    //IsDeleted = isDeleted
                    
                };

                user.SetNormalizedNames();

                user.Password = _passwordHasher.HashPassword(user, plainPassword);

                //assign role
                if (roleId != null && roleId > 0)
                {
                    user.Roles.Add(new UserRole(tenantId, user.Id, roleId ?? 0));
                }
                CheckErrors(await _userManager.CreateAsync(user));
                await CurrentUnitOfWork.SaveChangesAsync();

                var partnerForAudit = new Partner();
                if (user != null && user.Id > 0)
                {
                    //create partner
                    partner.UserId = user.Id;//set user id to partner table
                    partner.PartnerStatus = PartnerStatus.PartnerInvited;
                    var partnerId=await _partnerRepository.InsertAndGetIdAsync(partner);
                    partnerForAudit =await _partnerRepository.GetAsync(partnerId);
                }
                


                //sent email
                if (!user.IsEmailConfirmed)
                {
                    //insert data into invite table
                    var invite = new Invite
                    {
                        UserId = user.Id,
                        RoleId = roleId ?? 0,
                        InviteToken = Guid.NewGuid().ToString(),
                        StatusId = 1, // PartnerInvited
                        IsActive = true,
                    };

                    long inviteId = await _inviteRepository.InsertAndGetIdAsync(invite);

                    await _userEmailer.SendEmailActivationLinkAsync(user, invite, emailActivationLink, plainPassword);
                }

                return partnerForAudit;
            }
        /// <summary>
        /// This Method Is Used In If Partner Want To Change EmailAddress
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newEmail"></param>
        /// <param name="changeEmailLink"></param>
        /// <returns></returns>
        public async Task ChangeEmailAsync(long userId,string newEmail,string changeEmailLink)
        {
            var user = await _userManager.GetUserByIdAsync(userId);           
            var checkEmail = await _userManager.FindByEmailAsync(newEmail);
            if (checkEmail==null)
            {
                user.IsEmailConfirmed = false;
                user.IsActive = false;
                await _userManager.SetEmailAsync(user,newEmail);
                var invite = new Invite
                {
                    UserId = user.Id,
                    InviteToken = Guid.NewGuid().ToString(),
                    StatusId = 1, // PartnerInvited
                    IsActive = true,
                };

                long inviteId = await _inviteRepository.InsertAndGetIdAsync(invite);
                await _userEmailer.ChangeEmailLinkAsync(user, invite, changeEmailLink);
            }
            
        }



        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

    }
}
