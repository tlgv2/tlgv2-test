﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TownSend.Authorization.Users;

namespace TownSend.Partners
{
    [Table("Partners", Schema = "tlgv2")]
    public class Partner : FullAuditedEntity, ISoftDelete
    {
        
        public string FirstName { get; set; }
        
        public string MiddleName { get; set; }
        
        public string LastName { get; set; }
        
        public string Email { get; set; }
        
        public long PhoneNumber { get; set; }
        
        public string AddressLine1 { get; set; }
        
        public string AddressLine2 { get; set; }
        
        public string City { get; set; }
        
        public string State { get; set; }
        
        public int ZipCode { get; set; }
       
        public string AboutMe { get; set; }
       
        public string FullName { 
            get 
            {
                return FirstName + " " + LastName;
            } 
        }
        public long UserId { get; set; }
        public string Country { get; set; }
        public PartnerStatus PartnerStatus { get; set; }
        public string ProfilePath { get; set; }
        public User User { get; set; }
    }
}
