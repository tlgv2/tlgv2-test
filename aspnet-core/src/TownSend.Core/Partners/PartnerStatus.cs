﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Partners
{
    public enum PartnerStatus
    {
        PartnerInvited=1,
        PartnerRegistered=2,
    }
}
