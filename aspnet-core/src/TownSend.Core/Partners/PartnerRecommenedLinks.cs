﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Partners
{
    [Table("PartnerRecommenedLinks", Schema = "tlgv2")]
    public class PartnerRecommenedLinks : FullAuditedEntity
    {
        public int PartnerId { get; set;}
        public string LinkType { get; set; }
        public string Description { get; set; }       
        public string LinkUrl { get; set; }

        public string LinkName { get; set; }

    }
}
