﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TownSend.Partners;

namespace TownSend.Teams
{
    [Table("Teams", Schema = "tlgv2")]
    public class Team : FullAuditedEntity , ISoftDelete
    {
        public string TeamName { get; set; }
        public int StateId { get; set; }
        public string Description { get; set; }
        public decimal TeamPrice { get; set; }
        public decimal Deposit { get; set; }
        public bool IsPaymentRecieved { get; set; }
        public DateTime StartDate { get; set; }                
        public DateTime EndDate { get; set; }
    }
}

