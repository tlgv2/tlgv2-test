﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.Teams
{
    [Table("TeamPartnerMapping", Schema = "tlgv2")]
    public class TeamPartnerMapping : FullAuditedEntity, ISoftDelete
    {
        public int TeamId { get; set; }
        public int PartnerId { get; set; }

        public decimal? PartnerPercentage { get; set; }
    }
}
