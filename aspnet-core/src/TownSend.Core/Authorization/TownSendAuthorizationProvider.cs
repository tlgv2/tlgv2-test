﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace TownSend.Authorization
{
    public class TownSendAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            context.CreatePermission(PermissionNames.Pages_Dashboard, L("Dashboard"));

            context.CreatePermission(PermissionNames.Pages_Members, L("Members"));

            var partner= context.CreatePermission(PermissionNames.Pages_Partners, L("Partners"));
            partner.CreateChildPermission(PermissionNames.Pages_Partners_Create, L("CreatePartners"));
            partner.CreateChildPermission(PermissionNames.Pages_Partners_Edit, L("EditPartners"));
            partner.CreateChildPermission(PermissionNames.Pages_Partners_Delete, L("DeletePartners"));
            partner.CreateChildPermission(PermissionNames.Pages_Partners_List, L("ListPartners"));

            var team = context.CreatePermission(PermissionNames.Pages_Teams, L("Teams"));
            team.CreateChildPermission(PermissionNames.Pages_Teams_Create, L("CreateTeams"));
            team.CreateChildPermission(PermissionNames.Pages_Teams_Edit, L("EditTeams"));
            team.CreateChildPermission(PermissionNames.Pages_Teams_Delete, L("DeleteTeams"));
            team.CreateChildPermission(PermissionNames.Pages_Teams_List, L("ListTeams"));

            context.CreatePermission(PermissionNames.Pages_Meetings, L("Meetings"));
            context.CreatePermission(PermissionNames.Pages_Reports, L("Reports"));
            context.CreatePermission(PermissionNames.Pages_Referrals, L("Referrals"));

            var scholarship = context.CreatePermission(PermissionNames.Pages_Scholarships, L("Scholarships"));
            scholarship.CreateChildPermission(PermissionNames.Pages_Scholarships_List, L("ScholarshipsList"));

            context.CreatePermission(PermissionNames.Pages_FileManager, L("FileManager"));
            context.CreatePermission(PermissionNames.Pages_ExpenseManager, L("ExpenseManager"));
            context.CreatePermission(PermissionNames.Pages_AdminUses, L("AdminUses"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, TownSendConsts.LocalizationSourceName);
        }
    }
}
