﻿using Abp.Authorization;
using TownSend.Authorization.Roles;
using TownSend.Authorization.Users;

namespace TownSend.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
