﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Organizations;
using Abp.Runtime.Caching;
using TownSend.Authorization.Roles;
using System.Threading.Tasks;
using TownSend.Invites;

namespace TownSend.Authorization.Users
{
    public class UserManager : AbpUserManager<Role, User>
    {
        private readonly IRepository<Invite, long> _inviteRepository;
        private readonly IUserEmailer _userEmailer;

        public UserManager(
            RoleManager roleManager,
            UserStore store, 
            IOptions<IdentityOptions> optionsAccessor, 
            IPasswordHasher<User> passwordHasher, 
            IEnumerable<IUserValidator<User>> userValidators, 
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber errors, 
            IServiceProvider services, 
            ILogger<UserManager<User>> logger, 
            IPermissionManager permissionManager, 
            IUnitOfWorkManager unitOfWorkManager, 
            ICacheManager cacheManager, 
            IRepository<OrganizationUnit, long> organizationUnitRepository, 
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository, 
            IOrganizationUnitSettings organizationUnitSettings, 
            ISettingManager settingManager,
            IUserEmailer userEmailer,
            IRepository<Invite,long> inviteRepository)
            : base(
                roleManager, 
                store, 
                optionsAccessor, 
                passwordHasher, 
                userValidators, 
                passwordValidators, 
                keyNormalizer, 
                errors, 
                services, 
                logger, 
                permissionManager, 
                unitOfWorkManager, 
                cacheManager,
                organizationUnitRepository, 
                userOrganizationUnitRepository, 
                organizationUnitSettings, 
                settingManager)
        {
            _inviteRepository = inviteRepository;
            _userEmailer = userEmailer;
        }


        public async Task ForgotPasswordLinkAsync(User user, string resetPasswordLink)
        {
            Invite invite = new Invite()
            {
                UserId = user.Id,
                InviteToken = Guid.NewGuid().ToString(),
               IsActive = true
            };
            var inviteId = await _inviteRepository.InsertAndGetIdAsync(invite);
            await _userEmailer.SendPasswordResetLinkAsync(user,resetPasswordLink,invite);
        }
    }
}
