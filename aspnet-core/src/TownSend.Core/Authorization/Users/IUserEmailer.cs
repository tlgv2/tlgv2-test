﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Invites;
using TownSend.Members;
using TownSend.Scholarships;

namespace TownSend.Authorization.Users
{
    public interface IUserEmailer
    {
        /// <summary>
        /// Send email activation link to user's email address.
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="link">Email activation link</param>
        /// <param name="plainPassword">
        /// Can be set to user's plain password to include it in the email.
        /// </param>
        Task SendEmailActivationLinkAsync(User user, Invite invite, string link, string plainPassword = null);

        /// <summary>
        /// Sends a password reset link to user's email.
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="link">Password reset link (optional)</param>
        Task SendPasswordResetLinkAsync(User user, string link,Invite invite);
        //Task SendPasswordResetLinkAsync(User user, string link = null);

        /// <summary>
        /// Sends a scholarship invitation link to member's email.
        /// </summary>
        /// <param name="member"></param>
        /// <param name="scholarshipId"></param>
        /// <param name="tenantId"></param>
        /// <param name="link"></param>
        /// <returns></returns>
        Task SendEmailScholarshipInviteAsync(Member member, int scholarshipId, int? tenantId, string link);

        /// <summary>
        /// Senr email to Admin for Scholarship Approval
        /// </summary>
        /// <param name="member"></param>
        /// <param name="scholarshipId"></param>
        /// <param name="tenantId"></param>
        /// <param name="link"></param>
        /// <returns></returns>
        Task SendEmailScholarshipApprovalAsync(Scholarship scholarship, int? tenantId, string link);

        Task SendEmailScholarshipStatusUpdateAsync(int scholarshipId, ScholarshipStatusEnum status, int? tenantId, bool toPartner);

        Task SendEmailMemberInviteAsync(User user, int memberId,Member member,  int? tenantId, string link);
        Task ChangeEmailLinkAsync(User user, Invite invite, string link);
        ///// <summary>
        ///// Sends an email for unread chat message to user's email.
        ///// </summary>
        ///// <param name="user"></param>
        ///// <param name="senderUsername"></param>
        ///// <param name="senderTenancyName"></param>
        ///// <param name="chatMessage"></param>
        //void TryToSendChatMessageMail(User user, string senderUsername, string senderTenancyName, ChatMessage chatMessage);


        Task SendPassword(User user, string password);


        //Task SendForgotPasswordLinkAsync(User us)
    }
}
