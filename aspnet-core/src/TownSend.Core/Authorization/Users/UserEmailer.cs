﻿using Abp;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Localization;
using Abp.Net.Mail;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TownSend.Configuration;
using TownSend.Editions;
using TownSend.Emailing;
using TownSend.Invites;
using TownSend.Localization;
using TownSend.Members;
using TownSend.MultiTenancy;
using TownSend.Partners;
using TownSend.Scholarships;
using TownSend.Teams;
using TownSend.Templates;

namespace TownSend.Authorization.Users
{
    /// <summary>
    /// Used to send email to users.
    /// </summary>
    public class UserEmailer : AbpServiceBase, IUserEmailer, ITransientDependency//TownSendAppServiceBase,
    {
        private readonly IEmailTemplateProvider _emailTemplateProvider;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ICurrentUnitOfWorkProvider _unitOfWorkProvider;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<User, long> _userRepository;
        private readonly ISettingManager _settingManager;
        private readonly EditionManager _editionManager;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        private readonly IRepository<EmailType> _emailTypeRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<Member> _memberRepository;
        private readonly IRepository<Scholarship> _scholarshipRepository;
        private readonly IRepository<TeamPartnerMapping> _teamPartnerMappingRepository;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly IConfiguration _config;

        public UserEmailer(
            IEmailTemplateProvider emailTemplateProvider,
            IEmailSender emailSender,
            IRepository<Tenant> tenantRepository,
            ICurrentUnitOfWorkProvider unitOfWorkProvider,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<User, long> userRepository,
            ISettingManager settingManager,
            EditionManager editionManager,
            IRepository<EmailTemplate> emailTemplateRepository,
            IRepository<EmailType> emailTypeRepository,
            IRepository<Team> teamRepository,
            IRepository<Member> memberRepository,
            IRepository<Scholarship> scholarshipRepository,
            IRepository<TeamPartnerMapping> teamPartnerMappingRepository,
            IRepository<Partner> partnerRepository,
            IConfiguration config)
        {
            _emailTemplateProvider = emailTemplateProvider;
            _emailSender = emailSender;
            _tenantRepository = tenantRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
            _unitOfWorkManager = unitOfWorkManager;
            _userRepository = userRepository;
            _settingManager = settingManager;
            _editionManager = editionManager;
            _emailTemplateRepository = emailTemplateRepository;
            _emailTypeRepository = emailTypeRepository;
            _teamRepository = teamRepository;
            _memberRepository = memberRepository;
            _scholarshipRepository = scholarshipRepository;
            _teamPartnerMappingRepository = teamPartnerMappingRepository;
            _partnerRepository = partnerRepository;
            _config = config;
        }

        #region Methods
        /// <summary>
        /// Send email activation link to user's email address.
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="link">Email activation link</param>
        /// <param name="plainPassword">
        /// Can be set to user's plain password to include it in the email.
        /// </param>
        [UnitOfWork]
        public virtual async Task SendEmailActivationLinkAsync(User user, Invite invite, string link, string plainPassword = null)
        {
            if (invite.InviteToken.IsNullOrEmpty())
            {
                throw new Exception("EmailConfirmationCode should be set in order to send email activation link.");
            }

            link = link.Replace("{userId}", user.Id.ToString());
            link = link.Replace("{inviteId}", invite.Id.ToString());
            link = link.Replace("{confirmationCode}", Uri.EscapeDataString(invite.InviteToken));

            var tenancyName = GetTenancyNameOrNull(user.TenantId);
            var emailTemplate = GetTitleAndSubTitle(user.TenantId, "Welcome to TownSend Leadership.", "");
            if (emailTemplate != null)
            {
                emailTemplate.Replace("{FIRST_NAME}", user.Name);
                emailTemplate.Replace("{LINK_TEXT}", "Join as Partner");
                emailTemplate.Replace("{LINK_URL}", link);
            }
            var mailMessage = new StringBuilder();

            string fromEmail = TownSendConsts.EmailSettings.DefaultFromEmailAddress;
            string alias = TownSendConsts.EmailSettings.DefaultFromDisplayName;
            string subject = TownSendConsts.EmailSettings.AddPartnerEmailSubject;
            await ReplaceBodyAndSend(user.EmailAddress, fromEmail, alias, subject, emailTemplate, mailMessage);
        }

        [UnitOfWork]
        public virtual async Task SendEmailScholarshipInviteAsync(Member member, int scholarshipId, int? tenantId, string link)
        {
            //update link with dynamic data
            link = link.Replace("{scholarshipId}", Convert.ToString(scholarshipId));

            //get email tempalte 
            var tenancyName = GetTenancyNameOrNull(tenantId);
            var emailTemplateDetails = await (from template in _emailTemplateRepository.GetAll()
                                              join type in _emailTypeRepository.GetAll() on (int)template.EmailType equals type.Id
                                              where template.Alias == TownSendConsts.EmailTemplateAliasConsts.ScholarshipQuestionnaireToMember
                                              select new
                                              {
                                                  Body = template.Body,
                                                  Subject = template.Subject,
                                                  FromEmail = type.Sender,
                                                  Alias = type.Alias,
                                              }
                                            ).FirstOrDefaultAsync();

            if (emailTemplateDetails == null)
                throw new Exception("Email template not found.");

            //replace dynamic data from email
            var emailTemplate = new StringBuilder(emailTemplateDetails.Body);
            if (emailTemplate != null)
            {
                emailTemplate.Replace("[MemberName]", string.Format("{0} {1}", member.FirstName, member.LastName));
                emailTemplate.Replace("[TeamName]", _teamRepository.Single(x => x.Id == member.TeamId)?.TeamName);
                emailTemplate.Replace("[ScholarshipQuestionnaireLink]", link);
            }

            await ReplaceBodyAndSend(member.Email, emailTemplateDetails.FromEmail, emailTemplateDetails.Alias, emailTemplateDetails.Subject, emailTemplate, new StringBuilder());
        }


        [UnitOfWork]
        public virtual async Task SendEmailScholarshipApprovalAsync(Scholarship scholarship, int? tenantId, string link)
        {
            string toEmail = _config.GetSection("ScholarshipApproverEmail:Email").Value;
            //update link with dynamic data
            link = link.Replace("{scholarshipId}", Convert.ToString(scholarship.Id));

            //get email tempalte 
            var tenancyName = GetTenancyNameOrNull(tenantId);
            var emailTemplateDetails = await (from template in _emailTemplateRepository.GetAll()
                                              join type in _emailTypeRepository.GetAll() on (int)template.EmailType equals type.Id
                                              where template.Alias == TownSendConsts.EmailTemplateAliasConsts.ScholarshipCreatedToAdmin
                                              select new
                                              {
                                                  Body = template.Body,
                                                  Subject = template.Subject,
                                                  FromEmail = type.Sender,
                                                  Alias = type.Alias,
                                              }
                                            ).FirstOrDefaultAsync();

            if (emailTemplateDetails == null)
                throw new Exception("Email template not found.");


            var dynamicData = await (from a in _scholarshipRepository.GetAll()
                                     join b in _teamRepository.GetAll() on a.TeamId equals b.Id
                                     join c in _memberRepository.GetAll() on a.MemberId equals c.Id
                                     select new
                                     {
                                         MemberName = string.Format("{0} {1}", c.FirstName, c.LastName),
                                         TeamName = b.TeamName,
                                     }
                                        ).FirstOrDefaultAsync();

            var dynamicPartner = await (from a in _partnerRepository.GetAll()
                                        join b in _teamPartnerMappingRepository.GetAll() on a.Id equals b.PartnerId
                                        where b.TeamId == scholarship.TeamId
                                        select new
                                        {
                                            PartnerNames = string.Join(", ", string.Format("{0} {1}", a.FirstName, a.LastName))
                                        }
                                        ).FirstOrDefaultAsync();

            //replace dynamic data from email
            var emailTemplate = new StringBuilder(emailTemplateDetails.Body);
            if (emailTemplate != null)
            {
                emailTemplate.Replace("[ApproverName]", TownSendConsts.EmailSettings.ScholarshipApproverFullName);
                emailTemplate.Replace("[MemberName]", dynamicData.MemberName);
                //TODO: in current system they are using NextSessionDateTime & NextSessionName for below two property, as of now taken empty
                emailTemplate.Replace("[SessionDate]", string.Empty);
                emailTemplate.Replace("[SessionName]", string.Empty);
                emailTemplate.Replace("[TeamName]", dynamicData.TeamName);
                emailTemplate.Replace("[PartnerName]", dynamicPartner.PartnerNames);
                emailTemplate.Replace("[ApprovalLink]", link);
                emailTemplate.Replace("[TLGTeam]", "TLGTeam");
            }

            await ReplaceBodyAndSend(toEmail, emailTemplateDetails.FromEmail, emailTemplateDetails.Alias, emailTemplateDetails.Subject, emailTemplate, new StringBuilder());
        }

        [UnitOfWork]
        public virtual async Task SendEmailScholarshipStatusUpdateAsync(int scholarshipId, ScholarshipStatusEnum status, int? tenantId, bool toPartner)
        {
            var tenancyName = GetTenancyNameOrNull(tenantId);
            //string toEmail = string.Empty;
            EmailTemplateDetails emailTemplateDetails = new EmailTemplateDetails();
            var emailTemplate = new StringBuilder();
            var scholarshipStatusEmail = new List<ScholarshipStatusEmailToUser>();
            string supportLink = string.Format("<a href='{0}'>{0}</a>", TownSendConsts.EmailSettings.SupportFromEmailAddress);
            string adminName = TownSendConsts.EmailSettings.ScholarshipApproverFullName;
            if (toPartner)
            {
                if (status == ScholarshipStatusEnum.ApprovedSendInvite)
                {
                    //get email tempalte             
                    emailTemplateDetails = await (from template in _emailTemplateRepository.GetAll()
                                                  join type in _emailTypeRepository.GetAll() on (int)template.EmailType equals type.Id
                                                  where template.Alias == TownSendConsts.EmailTemplateAliasConsts.ScholarshipApprovedToPartner
                                                  select new EmailTemplateDetails
                                                  {
                                                      Body = template.Body,
                                                      Subject = template.Subject,
                                                      FromEmail = type.Sender,
                                                      Alias = type.Alias,
                                                  }
                                                ).FirstOrDefaultAsync();

                    if (emailTemplateDetails == null)
                        throw new Exception("Email template not found.");

                    var dynamicData = await (from a in _scholarshipRepository.GetAll()
                                             join b in _teamRepository.GetAll() on a.TeamId equals b.Id
                                             join c in _memberRepository.GetAll() on a.MemberId equals c.Id
                                             where a.Id == scholarshipId
                                             select new
                                             {
                                                 MemberName = string.Format("{0} {1}", c.FirstName, c.LastName),
                                                 TeamName = b.TeamName,
                                                 TeamId = a.TeamId
                                             }
                                        ).FirstOrDefaultAsync();

                    var dynamicPartner = await (from a in _partnerRepository.GetAll()
                                                join b in _teamPartnerMappingRepository.GetAll() on a.Id equals b.PartnerId
                                                where b.TeamId == dynamicData.TeamId
                                                select a
                                        ).ToListAsync();

                    //replace dynamic data from email

                    foreach(var partner in dynamicPartner)
                    {
                        
                        emailTemplate = new StringBuilder(emailTemplateDetails.Body);
                        if (emailTemplate != null)
                        {
                            emailTemplate.Replace("[PartnerName]", string.Format("{0} {1}", partner.FirstName, partner.LastName));
                            emailTemplate.Replace("[MemberName]", dynamicData.MemberName);
                            emailTemplate.Replace("[GroupName]", dynamicData.TeamName);
                            emailTemplate.Replace("[SupportLink]", supportLink);
                            emailTemplate.Replace("[AdminName]", adminName);
                            scholarshipStatusEmail.Add(new ScholarshipStatusEmailToUser { ToEmail=partner.Email,EmailTemplate=emailTemplate });
                        }
                    }
                   
                }
                else if (status == ScholarshipStatusEnum.DeniedSendInvite)
                {
                    //get email tempalte             
                    emailTemplateDetails = await (from template in _emailTemplateRepository.GetAll()
                                                  join type in _emailTypeRepository.GetAll() on (int)template.EmailType equals type.Id
                                                  where template.Alias == TownSendConsts.EmailTemplateAliasConsts.ScholarshipDeniedToPartner
                                                  select new EmailTemplateDetails
                                                  {
                                                      Body = template.Body,
                                                      Subject = template.Subject,
                                                      FromEmail = type.Sender,
                                                      Alias = type.Alias,
                                                  }
                                                ).FirstOrDefaultAsync();

                    if (emailTemplateDetails == null)
                        throw new Exception("Email template not found.");

                    var dynamicData = await (from a in _scholarshipRepository.GetAll()
                                             join b in _teamRepository.GetAll() on a.TeamId equals b.Id
                                             join c in _memberRepository.GetAll() on a.MemberId equals c.Id
                                             where a.Id == scholarshipId
                                             select new
                                             {
                                                 MemberName = string.Format("{0} {1}", c.FirstName, c.LastName),
                                                 TeamName = b.TeamName,
                                                 TeamId = a.TeamId
                                             }
                                       ).FirstOrDefaultAsync();

                    var dynamicPartner = await (from a in _partnerRepository.GetAll()
                                                join b in _teamPartnerMappingRepository.GetAll() on a.Id equals b.PartnerId
                                                where b.TeamId == dynamicData.TeamId
                                                select a
                                        ).ToListAsync();

                    //replace dynamic data from email                    
                    foreach (var partner in dynamicPartner)
                    {                        
                        emailTemplate = new StringBuilder(emailTemplateDetails.Body);
                        if (emailTemplate != null)
                        {
                            emailTemplate.Replace("[PartnerName]", string.Format("{0} {1}", partner.FirstName, partner.LastName));
                            emailTemplate.Replace("[MemberName]", dynamicData.MemberName);
                            emailTemplate.Replace("[GroupName]", dynamicData.TeamName);
                            emailTemplate.Replace("[SupportLink]", supportLink);
                            emailTemplate.Replace("[AdminName]", adminName);
                            scholarshipStatusEmail.Add(new ScholarshipStatusEmailToUser { ToEmail = partner.Email, EmailTemplate = emailTemplate });
                        }
                    }
                }
            }
            else
            {
                if (status == ScholarshipStatusEnum.ApprovedSendInvite)
                {
                    //get email tempalte             
                    emailTemplateDetails = await (from template in _emailTemplateRepository.GetAll()
                                                  join type in _emailTypeRepository.GetAll() on (int)template.EmailType equals type.Id
                                                  where template.Alias == TownSendConsts.EmailTemplateAliasConsts.ScholarshipApprovedToMember
                                                  select new EmailTemplateDetails
                                                  {
                                                      Body = template.Body,
                                                      Subject = template.Subject,
                                                      FromEmail = type.Sender,
                                                      Alias = type.Alias,
                                                  }
                                                ).FirstOrDefaultAsync();

                    if (emailTemplateDetails == null)
                        throw new Exception("Email template not found.");

                    var dynamicData = await (from a in _scholarshipRepository.GetAll()
                                             join b in _teamRepository.GetAll() on a.TeamId equals b.Id
                                             join c in _memberRepository.GetAll() on a.MemberId equals c.Id
                                             where a.Id == scholarshipId
                                             select new
                                             {
                                                 MemberName = string.Format("{0} {1}", c.FirstName, c.LastName),
                                                 MemberEmail = c.Email,
                                                 TeamName = b.TeamName,
                                                 TeamId = a.TeamId,
                                                 ScholarshipAmount = a.ScholarshipAmount
                                             }
                                       ).FirstOrDefaultAsync();

                    //replace dynamic data from email                                        
                    emailTemplate = new StringBuilder(emailTemplateDetails.Body);
                    if (emailTemplate != null)
                    {
                        emailTemplate.Replace("[MemberName]", dynamicData.MemberName);
                        emailTemplate.Replace("[ScholarshipAmount]", dynamicData.ScholarshipAmount.ToString());
                        emailTemplate.Replace("[SupportLink]", supportLink);
                        emailTemplate.Replace("[AdminName]", adminName);
                        scholarshipStatusEmail.Add(new ScholarshipStatusEmailToUser { ToEmail = dynamicData.MemberEmail, EmailTemplate = emailTemplate });
                    }
                }
                else if (status == ScholarshipStatusEnum.DeniedSendInvite)
                {
                    //get email tempalte             
                    emailTemplateDetails = await (from template in _emailTemplateRepository.GetAll()
                                                  join type in _emailTypeRepository.GetAll() on (int)template.EmailType equals type.Id
                                                  where template.Alias == TownSendConsts.EmailTemplateAliasConsts.ScholarshipDeniedToMember
                                                  select new EmailTemplateDetails
                                                  {
                                                      Body = template.Body,
                                                      Subject = template.Subject,
                                                      FromEmail = type.Sender,
                                                      Alias = type.Alias,
                                                  }
                                                ).FirstOrDefaultAsync();

                    if (emailTemplateDetails == null)
                        throw new Exception("Email template not found.");

                    var dynamicData = await (from a in _scholarshipRepository.GetAll()
                                             join b in _teamRepository.GetAll() on a.TeamId equals b.Id
                                             join c in _memberRepository.GetAll() on a.MemberId equals c.Id
                                             where a.Id == scholarshipId
                                             select new
                                             {
                                                 MemberName = string.Format("{0} {1}", c.FirstName, c.LastName),
                                                 MemberEmail = c.Email,
                                                 TeamName = b.TeamName,
                                                 TeamId = a.TeamId,
                                                 ScholarshipAmount = a.ScholarshipAmount
                                             }
                                       ).FirstOrDefaultAsync();

                    //replace dynamic data from email                    
                    emailTemplate = new StringBuilder(emailTemplateDetails.Body);
                    if (emailTemplate != null)
                    {
                        emailTemplate.Replace("[MemberName]", dynamicData.MemberName);
                        emailTemplate.Replace("[ScholarshipAmount]", dynamicData.ScholarshipAmount.ToString());
                        emailTemplate.Replace("[SupportLink]", supportLink);
                        emailTemplate.Replace("[AdminName]", adminName);

                        scholarshipStatusEmail.Add(new ScholarshipStatusEmailToUser { ToEmail = dynamicData.MemberEmail, EmailTemplate = emailTemplate });
                    }
                }
            }
            foreach(var scholarship in scholarshipStatusEmail)
            {
                await ReplaceBodyAndSend(scholarship.ToEmail, emailTemplateDetails.FromEmail, emailTemplateDetails.Alias, emailTemplateDetails.Subject, scholarship
                    .EmailTemplate, new StringBuilder());
            }

            
        }

        [UnitOfWork]
        public virtual async Task SendEmailMemberInviteAsync(User user, int memberId, Member member, int? tenantId, string link)
        {
            //update link with dynamic data
            link = link.Replace("{memberId}", Convert.ToString(memberId));

            //get email tempalte 
            var tenancyName = GetTenancyNameOrNull(tenantId);
           
            //replace dynamic data from email
            var emailTemplate = GetMemberInvite(user.TenantId);
            if (emailTemplate != null)
            {
                emailTemplate.Replace("{MemberName}", string.Format("{0} {1}", member.FirstName, member.LastName));
                emailTemplate.Replace("{TeamName}", _teamRepository.Single(x => x.Id == member.TeamId)?.TeamName);
                emailTemplate.Replace("{LINK_URL}", link);
                //emailTemplate.Replace("{BasePrice}", member.BasePrice.ToString());
                //emailTemplate.Replace("{Discount}", member.Discount.ToString());
                //emailTemplate.Replace("{Membership_Amount}", member.Membership_Amount.ToString());
                emailTemplate.Replace("{LINK_TEXT}", "Register Now");
                
                //emailTemplate.Replace("[Membership_Amount]", member..ToString());
            }
            string fromEmail = TownSendConsts.EmailSettings.DefaultFromEmailAddress;
            string alias = TownSendConsts.EmailSettings.DefaultFromDisplayName;
            string subject = TownSendConsts.EmailSettings.AddMemberEmailSubject;

            var dynamicPartner = await (from a in _partnerRepository.GetAll()
                                        join b in _teamPartnerMappingRepository.GetAll() on a.Id equals b.PartnerId
                                        where b.TeamId == member.TeamId
                                        select a
                                        ).ToListAsync();
            foreach (var partner in dynamicPartner)
            {
                if (emailTemplate != null)
                {
                    emailTemplate.Replace("{PartnerName}", string.Format("{0} {1}", partner.FirstName, partner.LastName));
                    emailTemplate.Replace("{PartnerEmail}",  partner.Email);
                }

                await ReplaceBodyAndSend(member.Email, fromEmail, alias, subject, emailTemplate, new StringBuilder());
            }

                
        }

        public async Task ChangeEmailLinkAsync(User user, Invite invite, string link)
        {
            if (invite.InviteToken.IsNullOrEmpty())
            {
                throw new Exception("EmailConfirmationCode should be set in order to send email activation link.");
            }

            link = link.Replace("{userId}", user.Id.ToString());
            link = link.Replace("{inviteId}", invite.Id.ToString());
            link = link.Replace("{confirmationCode}", Uri.EscapeDataString(invite.InviteToken));
            var tenancyName = GetTenancyNameOrNull(user.TenantId);
            var emailTemplate = GetTitleAndSubTitle(user.TenantId, "TownSend", "Email Change Verification Code");
            if (emailTemplate != null)
            {
                emailTemplate.Replace("{FIRST_NAME}", user.Name);
                emailTemplate.Replace("{LINK_TEXT}", "Email Change");
                emailTemplate.Replace("{LINK_URL}", link);
            }
            var mailMessage = new StringBuilder();
            await ReplaceBodyAndSend(user.EmailAddress, TownSendConsts.EmailSettings.DefaultFromEmailAddress, TownSendConsts.EmailSettings.DefaultFromDisplayName, "Email Change ", emailTemplate, mailMessage);



        }

        /// <summary>
        /// Sends a password reset link to user's email.
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="link">Reset link</param>
        public virtual async Task SendPasswordResetLinkAsync(User user, string link, Invite invite)
        {
            if (invite.InviteToken.IsNullOrEmpty())
            {
                throw new Exception("PasswordResetCode should be set in order to send password reset link.");
            }
            link = link.Replace("{userId}", user.Id.ToString());
            link = link.Replace("{inviteId}", invite.Id.ToString());
            link = link.Replace("{resetCode}", Uri.EscapeDataString(invite.InviteToken));

            var tenancyName = GetTenancyNameOrNull(user.TenantId);
           
            var mailMessage = new StringBuilder();

            mailMessage.AppendLine("<b>" + "Name" + "</b>: " + user.Name + " " + user.Surname + "<br />");


            if (!link.IsNullOrEmpty())
            {

                mailMessage.AppendLine("<br />");
                mailMessage.AppendLine(TownSendConsts.EmailSettings.ResetPasswordEmailBody);
                mailMessage.Replace("[link]",link);
                

            }


            string fromEmail = TownSendConsts.EmailSettings.SupportFromEmailAddress;
            string alias = TownSendConsts.EmailSettings.ChangePasswordEmailSuject;
            string subject = TownSendConsts.EmailSettings.ChangePasswordTitleSubject;
            await ReplaceBodyAndSend(user.EmailAddress, fromEmail, alias, subject, mailMessage, mailMessage);
        }

        public async Task SendPassword(User user, string password)
        {
            var mailMessage = new StringBuilder();
            mailMessage.Append("Hello, " + user.Name + " " + user.Surname + ",<br>");
            mailMessage.Append("Your Password is Changed by TLG Admin <br>");
            mailMessage.Append("Your New Password is : " + password);

            string fromEmail = TownSendConsts.EmailSettings.DefaultFromEmailAddress;
            string alias = TownSendConsts.EmailSettings.ChangePasswordByAdminEmailSubject;
            string subject = TownSendConsts.EmailSettings.ChangePasswordByAdminTitleSubject;
            await ReplaceBodyAndSend(user.EmailAddress, fromEmail, alias, subject, mailMessage, mailMessage);
        }
        #endregion





        #region Utility
        private string GetTenancyNameOrNull(int? tenantId)
        {
            if (tenantId == null)
            {
                return null;
            }

            using (_unitOfWorkProvider.Current.SetTenantId(null))
            {
                return _tenantRepository.Get(tenantId.Value).TenancyName;
            }
        }

        private StringBuilder GetTitleAndSubTitle(int? tenantId, string title, string subTitle)
        {
            var emailTemplate = new StringBuilder(_emailTemplateProvider.GetDefaultTemplate(tenantId));
            emailTemplate.Replace("{EMAIL_TITLE}", title);
            emailTemplate.Replace("{EMAIL_SUB_TITLE}", subTitle);

            return emailTemplate;
        }

        private StringBuilder GetMemberInvite(int? tenantId)
        {
            var emailTemplate = new StringBuilder(_emailTemplateProvider.GetMemberInviteTemplate(tenantId));
            

            return emailTemplate;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <created by>Arjun</created>
        /// <created on>14 Feb 2020</created>
        /// <description>Send Email via SendGrid</description>
        /// <param name="emailAddress"></param>
        /// <param name="subject"></param>
        /// <param name="emailTemplate"></param>
        /// <param name="mailMessage"></param>
        /// <returns></returns>
        //private async Task ReplaceBodyAndSend(string emailAddress, string subject, StringBuilder emailTemplate, StringBuilder mailMessage)
        private async Task ReplaceBodyAndSend(string toEmail, string fromEmail, string alias, string subject, StringBuilder emailTemplate, StringBuilder mailMessage)
        {
            // string destinationEmail
            //, string sourceEmail
            //, string sourceAlias
            //, string subject
            //, string content
            //await ProcessEmail(emailAddress, "noreply@hubonesystems.com", "HubOne Support", subject, emailTemplate.ToString());
            await ProcessEmail(toEmail, fromEmail, alias, subject, emailTemplate.ToString());
            //emailTemplate.Replace("{EMAIL_BODY}", mailMessage.ToString());
            //await _emailSender.SendAsync(new MailMessage
            //{
            //    To = { emailAddress },
            //    Subject = subject,
            //    Body = emailTemplate.ToString(),
            //    IsBodyHtml = true
            //});
        }

        //dev1
        public static string HubOneCoreSendGridKey()
        {
            return "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";
        }

        public async Task ProcessEmail
        (
            string destinationEmail
            , string sourceEmail
            , string sourceAlias
            , string subject
            , string content

        )
        {
            SendGridClient Client = new SendGridClient(HubOneCoreSendGridKey());

            if (string.IsNullOrWhiteSpace(sourceEmail) || string.IsNullOrWhiteSpace(sourceAlias))
                throw new ArgumentException("SendGridModule.AssignEmailSource() -> parameters empty.");

            EmailAddress EmailFrom = new EmailAddress(sourceEmail, sourceAlias);

            if (string.IsNullOrWhiteSpace(destinationEmail))
                throw new ArgumentException("SendGridModule.AssignEmailDestination -> parameter empty.");


            EmailAddress EmailTo = new EmailAddress(destinationEmail);

            if (content.Length == 0 || string.IsNullOrWhiteSpace(subject))
                throw new ArgumentException("SendGridModule.CreateEmail() -> No content or Subject");


            var emailContent = content;
            SendGridMessage CreatedEmail = MailHelper.CreateSingleEmail(EmailFrom, EmailTo, subject, emailContent, emailContent);
            Task<Response> ResponseFromProvider = Client.SendEmailAsync(CreatedEmail);
            ResponseFromProvider.Result.ToString();
        }

        

        #endregion

        
    }
}
