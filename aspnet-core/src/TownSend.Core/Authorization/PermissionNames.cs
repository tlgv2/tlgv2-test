﻿namespace TownSend.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        //dev1 changes
        public const string Pages_Dashboard = "Pages.Dashboard";

        public const string Pages_Members = "Pages.Members";

        public const string Pages_Partners = "Pages.Partners";
        public const string Pages_Partners_Create = "Pages.Partners.Create";
        public const string Pages_Partners_Edit = "Pages.Partners.Edit";
        public const string Pages_Partners_Delete = "Pages.Partners.Delete";
        public const string Pages_Partners_List = "Pages.Partners.List";

        public const string Pages_Teams = "Pages.Teams";
        public const string Pages_Teams_Create = "Pages.Teams.Create";
        public const string Pages_Teams_Edit = "Pages.Teams.Edit";
        public const string Pages_Teams_Delete = "Pages.Teams.Delete";
        public const string Pages_Teams_List = "Pages.Teams.List";

        public const string Pages_Meetings = "Pages.Meetings";

        public const string Pages_Reports = "Pages.Reports";                            

        public const string Pages_Referrals = "Pages.Referrals";

        public const string Pages_Scholarships = "Pages.Scholarships";
        public const string Pages_Scholarships_List = "Pages.Scholarships.List";
        public const string Pages_Administrator_Scholarships_List = "Pages.Administrator.Scholarships.List";
        public const string Pages_FileManager = "Pages.FileManager";
        public const string Pages_ExpenseManager = "Pages.ExpenseManager";
        public const string Pages_AdminUses = "Pages.AdminUses";

    }
}
