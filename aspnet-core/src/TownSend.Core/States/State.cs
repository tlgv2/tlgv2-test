﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.States
{
    [Table("States", Schema = "tlgv2")]
    public class State : Entity<int>
    {
        public int CountryID { get; set; }
        public string SubCountryCode { get; set; }
        public string StateName { get; set; }
        public int LabelUID { get; set; }
        public int TimeZoneID { get; set; }
        public bool H1Active { get; set; }
    }
}
