﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.ProfileNotes
{
    [Table("NoteTeamMappings", Schema = "tlgv2")]
    public class ProfileNoteTeamMapping : Entity
    {
        public int NoteId { get; set; }
        public int TeamId { get; set; }
    }
}
