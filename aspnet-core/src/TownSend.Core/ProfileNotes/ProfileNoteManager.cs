﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TownSend.Authorization.Users;

namespace TownSend.ProfileNotes
{
    public class ProfileNoteManager : TownSendDomainServiceBase
    {
        private IRepository<ProfileNote> _profileNoteRepository;
        private IRepository<ProfileNoteTeamMapping> _profileNoteTeamRepository;
        private IRepository<User, long> _userRepository;
        private IRepository<ProfileNotePartnerMapping> _profileNotePartnerRepository;
        private IRepository<ProfileNoteMemberMapping> _profileNoteMemberRepository;

        public ProfileNoteManager(IRepository<ProfileNote> profileNoteRepository ,
                                   IRepository<ProfileNotePartnerMapping> profileNotePartnerRepository,
                                   IRepository<ProfileNoteTeamMapping> profileNoteTeamRepository,
                                   IRepository<ProfileNoteMemberMapping> profileNoteMemberRepository,
                                    IRepository<User, long> userRepository)
        {
            _profileNoteRepository = profileNoteRepository;
            _profileNoteTeamRepository = profileNoteTeamRepository;
            _userRepository = userRepository;
            _profileNotePartnerRepository = profileNotePartnerRepository;
            _profileNoteMemberRepository = profileNoteMemberRepository;
        }

        public async Task<List<ProfileNote>> GetNotesForPartnerAsync(int mapId)
        {
            var query = await (from note in _profileNoteRepository.GetAll()
                               join partnerMap in _profileNotePartnerRepository.GetAll() on note.Id equals partnerMap.NoteId
                               join user in _userRepository.GetAll() on note.CreatorUserId equals user.Id
                               where partnerMap.PartnerId == mapId
                               select new ProfileNote
                               {
                                   AddedBy = user.FullName,
                                   Id = note.Id,
                                   Description = note.Description

                               }).ToListAsync();
            return query;
        }
        public async Task<List<ProfileNote>> GetNotesForTeamAsync(int mapId)
        {
            var query = await (from note in _profileNoteRepository.GetAll()
                               join teamMap in _profileNoteTeamRepository.GetAll() on note.Id equals teamMap.NoteId
                               join user in _userRepository.GetAll() on note.CreatorUserId equals user.Id
                               where teamMap.TeamId == mapId
                               select new ProfileNote
                               {
                                   AddedBy = user.FullName,
                                   Id = note.Id,
                                   Description = note.Description

                               }).ToListAsync();
            return query;
        }
        public async Task<List<ProfileNote>> GetNotesForMemberAsync(int mapId)
        {
            var query = await (from note in _profileNoteRepository.GetAll()
                               join memberMap in _profileNoteMemberRepository.GetAll() on note.Id equals memberMap.NoteId
                               join user in _userRepository.GetAll() on note.CreatorUserId equals user.Id
                               where memberMap.MemberId == mapId
                               select new ProfileNote
                               {
                                   AddedBy = user.FullName,
                                   Id = note.Id,
                                   Description = note.Description

                               }).ToListAsync();
            return query;
        }
    }
}
