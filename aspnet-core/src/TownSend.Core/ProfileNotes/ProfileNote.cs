﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.ProfileNotes
{
    [Table("ProfileNote", Schema = "tlgv2")]
    public class ProfileNote : FullAuditedEntity
    {
        public string Description { get; set; }        

        public string AddedBy { get; set; }
    }
}
