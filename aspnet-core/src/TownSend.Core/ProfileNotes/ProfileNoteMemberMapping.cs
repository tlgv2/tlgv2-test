﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.ProfileNotes
{
    [Table("NoteMemberMappings", Schema = "tlgv2")]
    public class ProfileNoteMemberMapping : Entity
    {
        public int NoteId { get; set; }
        public int MemberId { get; set; }
    }
}
