﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TownSend.ProfileNotes
{
    [Table("NotePartnerMappings", Schema = "tlgv2")] 
    
    public class ProfileNotePartnerMapping : Entity
    {
        public int NoteId { get; set; }
        public int PartnerId { get; set; }

    }
}
