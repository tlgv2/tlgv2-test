﻿using Microsoft.Extensions.Configuration;

namespace TownSend.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
