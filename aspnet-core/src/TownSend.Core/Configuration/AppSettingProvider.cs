﻿using System.Collections.Generic;
using Abp.Configuration;
using static Abp.Zero.Configuration.AbpZeroSettingNames;

namespace TownSend.Configuration
{
    public class AppSettingProvider : SettingProvider
    {
        public override IEnumerable<SettingDefinition> GetSettingDefinitions(SettingDefinitionProviderContext context)
        {
            return new[]
            {
                new SettingDefinition(AppSettingNames.UiTheme, "red", scopes: SettingScopes.Application | SettingScopes.Tenant | SettingScopes.User, isVisibleToClients: true),
                new SettingDefinition(
                           UserManagement.PasswordComplexity.RequiredLength,
                           "8",
                           //new FixedLocalizableString("Required length."),
                           scopes: SettingScopes.Application | SettingScopes.Tenant,
                           clientVisibilityProvider: new VisibleSettingClientVisibilityProvider()
                           ),
                new SettingDefinition(
                           UserManagement.PasswordComplexity.RequireUppercase,
                           "true",
                           //new FixedLocalizableString("Required length."),
                           scopes: SettingScopes.Application | SettingScopes.Tenant,
                           clientVisibilityProvider: new VisibleSettingClientVisibilityProvider()
                           ),
                new SettingDefinition(
                           UserManagement.PasswordComplexity.RequireNonAlphanumeric,
                           "false",
                           //new FixedLocalizableString("Require non alphanumeric."),
                           scopes: SettingScopes.Application | SettingScopes.Tenant,
                           clientVisibilityProvider: new VisibleSettingClientVisibilityProvider()
                           ),
                new SettingDefinition(
                           UserManagement.PasswordComplexity.RequireDigit,
                           "true",
                           //new FixedLocalizableString("Require digit."),
                           scopes: SettingScopes.Application | SettingScopes.Tenant,
                           clientVisibilityProvider: new VisibleSettingClientVisibilityProvider()
                           ),

                       new SettingDefinition(
                           UserManagement.PasswordComplexity.RequireLowercase,
                           "true",
                           //new FixedLocalizableString("Require lowercase."),
                           scopes: SettingScopes.Application | SettingScopes.Tenant,
                           clientVisibilityProvider: new VisibleSettingClientVisibilityProvider()
                           ),

            };
        }
    }
}
