﻿namespace TownSend.Configuration
{
    public static class AppSettingNames
    {
        public const string UiTheme = "App.UiTheme";
        public const string ScholarshipApproverEmail = "Mail.Scholarship.ApproverEmail";
        public const string ScholarshipApproverFullName = "Mail.Scholarship.ApproverFullName";
        public const string DefaultFromEmailAddress = "Mail.DefaultFromEmailAddress";
        public const string DefaultFromDisplayName = "Mail.DefaultFromDisplayName";
        public const string AddPartnerEmailSubject = "Mail.AddPartnerEmailSubject";
        public const string SupportFromEmailAddress = "Mail.SupportFromEmailAddress";
    }
}
