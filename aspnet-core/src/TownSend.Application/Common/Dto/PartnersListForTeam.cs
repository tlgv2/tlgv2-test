﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TownSend.Partners;

namespace TownSend.Common.Dto
{
    [AutoMapFrom(typeof(Partner))]
   public class PartnersListForTeam : EntityDto<int>
    {    
       
        public int PartnerId { get; set; }
        public string FullName { get; set; }
        public long UserId { get; set; }
       
   }
}
