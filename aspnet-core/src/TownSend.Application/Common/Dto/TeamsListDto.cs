﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Teams;

namespace TownSend.Common.Dto
{
    [AutoMapFrom(typeof(Team))]
    public class TeamsListDto : Entity
    {        
        public string TeamName { get; set; }
    }
}
