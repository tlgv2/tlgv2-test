﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TownSend.States;

namespace TownSend.Common.Dto
{
    [AutoMapFrom(typeof(State))]
    public class StateListDto : EntityDto<int>
    {
        [Required]
        public string StateName { get; set; }

    }
}
