﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Common.Dto
{
    public class EntityTypeMapDto
    {
        public int MapId { get; set; }
        public EntityType Type { get; set; }
    }
}
