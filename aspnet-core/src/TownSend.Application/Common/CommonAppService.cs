﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TownSend.Common.Dto;
using TownSend.Partners;
using TownSend.States;
using TownSend.Teams;

namespace TownSend.Common
{
    public class CommonAppService : TownSendAppServiceBase, ICommonAppService
    {
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly IRepository<Team> _teamRepository;

        public CommonAppService(IRepository<State> stateRepository,
                                IRepository<Partner> partnerRepository,
                                IRepository<Team> teamRepository)
        {
            _stateRepository = stateRepository;
            _partnerRepository = partnerRepository;
            _teamRepository = teamRepository;
        }

        public async Task<ListResultDto<StateListDto>> GetAll()
        {
            var stateResult = await _stateRepository.GetAll().OrderBy(x=>x.StateName).ToListAsync();
            return new ListResultDto<StateListDto>(ObjectMapper.Map<List<StateListDto>>(stateResult));
        }
        public async Task<List<PartnersListForTeam>> GetAllPartner()
        {
            var partnerResult = await _partnerRepository.GetAll().ToListAsync();
            return new List<PartnersListForTeam>(ObjectMapper.Map<List<PartnersListForTeam>>(partnerResult));
        }

        public async Task<ListResultDto<TeamsListDto>> GetAllTeams()
        {
            var teamList = await _teamRepository.GetAll().ToListAsync();
            return new ListResultDto<TeamsListDto>(ObjectMapper.Map<List<TeamsListDto>>(teamList));
        }
    }
}
