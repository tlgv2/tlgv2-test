﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Common.Dto;

namespace TownSend.Common
{
    public interface ICommonAppService : IApplicationService
    {
        Task<ListResultDto<StateListDto>> GetAll();
        Task<List<PartnersListForTeam>> GetAllPartner();
        Task<ListResultDto<TeamsListDto>> GetAllTeams();
    }
}
