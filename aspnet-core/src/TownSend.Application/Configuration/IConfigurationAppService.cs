﻿using System.Threading.Tasks;
using TownSend.Configuration.Dto;

namespace TownSend.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
