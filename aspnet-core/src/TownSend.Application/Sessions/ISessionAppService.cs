﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TownSend.Sessions.Dto;

namespace TownSend.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
