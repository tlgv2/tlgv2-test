﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TownSend.MultiTenancy.Dto;

namespace TownSend.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

