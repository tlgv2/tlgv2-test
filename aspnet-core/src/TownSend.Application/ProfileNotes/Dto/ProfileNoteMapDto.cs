﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.ProfileNotes.Dto
{
    public class ProfileNoteMapDto
    {
        public int NoteId { get; set; }
        public int MapId { get; set; }
    }
}
