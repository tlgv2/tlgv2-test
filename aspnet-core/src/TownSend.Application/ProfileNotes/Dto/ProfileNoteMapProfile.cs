﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.ProfileNotes.Dto
{
    public class ProfileNoteMapProfile : Profile
    {
        public ProfileNoteMapProfile()
        {
            CreateMap<CreateProfileNoteDto, ProfileNote>();
            CreateMap<ProfileNote, CreateProfileNoteDto>();
            CreateMap<ProfileNoteListDto, ProfileNote>();
            CreateMap<ProfileNote, ProfileNoteListDto>();
            CreateMap<ProfileNoteMapDto, ProfileNotePartnerMapping>().ForMember(dest=>dest.PartnerId,map=>map.MapFrom(src=>src.MapId));
            CreateMap<ProfileNoteMapDto, ProfileNoteTeamMapping>().ForMember(dest => dest.TeamId, map => map.MapFrom(src => src.MapId));
            CreateMap<ProfileNoteMapDto, ProfileNoteMemberMapping>().ForMember(dest => dest.MemberId, map => map.MapFrom(src => src.MapId));
        }
    }
}
