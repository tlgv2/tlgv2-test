﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.ProfileNotes.Dto
{
    public class ProfileNoteListDto
    {
        public string Description { get; set; }
        public string AddedBy { get; set; }
        [DataType(DataType.Date)]
        public DateTime? CreationTime { get; set; }

    }
}
