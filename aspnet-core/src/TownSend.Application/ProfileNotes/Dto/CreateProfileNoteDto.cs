﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.ProfileNotes.Dto
{
    public class CreateProfileNoteDto
    {
        public int MapId { get; set; }
        public string Description { get; set; }
        public EntityType Type { get; set; }
    }
}
