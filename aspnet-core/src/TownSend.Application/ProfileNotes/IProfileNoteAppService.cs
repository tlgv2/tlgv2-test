﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using TownSend.ProfileNotes.Dto;

namespace TownSend.ProfileNotes
{
    public interface IProfileNoteAppService : IApplicationService
    {
        Task CreateProfileNote(CreateProfileNoteDto input);

        Task<ListResultDto<ProfileNoteListDto>> GetNotes(EntityTypeMapDto input);
    }
}
