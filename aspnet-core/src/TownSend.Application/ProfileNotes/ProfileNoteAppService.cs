﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using System.Linq;
using TownSend.ProfileNotes.Dto;

namespace TownSend.ProfileNotes
{
    public class ProfileNoteAppService : TownSendAppServiceBase, IProfileNoteAppService
    {
        private IRepository<ProfileNote> _profileNoteRepository;
        private IRepository<ProfileNoteTeamMapping> _profileNoteTeamRepository;
        private IRepository<ProfileNotePartnerMapping> _profileNotePartnerRepository;
        private ProfileNoteManager _profileNoteManager;
        private IRepository<ProfileNoteMemberMapping> _profileNoteMemberRepository;

        public ProfileNoteAppService(IRepository<ProfileNote> profileNoteRepository,
            IRepository<ProfileNoteTeamMapping> profileNoteTeamRepository,
            IRepository<ProfileNotePartnerMapping> profileNotePartnerRepository,
            IRepository<ProfileNoteMemberMapping> profileNoteMemberRepository,
            ProfileNoteManager profileNoteManager)
        {
            _profileNoteRepository = profileNoteRepository;
            _profileNoteTeamRepository = profileNoteTeamRepository;
            _profileNotePartnerRepository = profileNotePartnerRepository;
            _profileNoteManager = profileNoteManager;
            _profileNoteMemberRepository = profileNoteMemberRepository;
        }
        public async Task CreateProfileNote(CreateProfileNoteDto input)
        {
            var resultNote = ObjectMapper.Map<ProfileNote>(input);
            var noteId=await _profileNoteRepository.InsertAndGetIdAsync(resultNote);
            var noteMapping = new ProfileNoteMapDto() {NoteId=noteId,MapId=input.MapId };
            if (input.Type==EntityType.Partners)
            {
                var resultMap = ObjectMapper.Map<ProfileNotePartnerMapping>(noteMapping);
                await _profileNotePartnerRepository.InsertAsync(resultMap);
            }
            else if(input.Type==EntityType.Teams)
            {
                var resultMap = ObjectMapper.Map<ProfileNoteTeamMapping>(noteMapping);
                await _profileNoteTeamRepository.InsertAsync(resultMap);
            }    
            else if(input.Type==EntityType.Members)
            {
                var resultMap = ObjectMapper.Map<ProfileNoteMemberMapping>(noteMapping);
                await _profileNoteMemberRepository.InsertAsync(resultMap);
            }
        }

        public async Task<ListResultDto<ProfileNoteListDto>> GetNotes(EntityTypeMapDto input)
        {
            var notes = new List<ProfileNote>();
            if(input.Type==EntityType.Partners)
            {
                notes=await _profileNoteManager.GetNotesForPartnerAsync(input.MapId);
            }
            else if(input.Type==EntityType.Teams)
            {
                notes = await _profileNoteManager.GetNotesForTeamAsync(input.MapId);
            }  
            else if(input.Type==EntityType.Members)
            {
                notes = await _profileNoteManager.GetNotesForMemberAsync(input.MapId);
            }
          
            return new ListResultDto<ProfileNoteListDto>(ObjectMapper.Map<List<ProfileNoteListDto>>(notes));
        }
    }
}
