﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Members.Dto;

namespace TownSend.Members
{
    public interface IMemberAppService : IApplicationService
    {

        Task<ListResultDto<MemberListDto>> GetAll();

        Task<MemberCreateResult> CreateAsync(CreateMemberDto input);
        Task<MemberCreateResult> UpdateAsync(MemberEditDto input);

        Task<MemberDto> GetAsync(int id);
        Task<MemberEditDto> GetMemberForEdit(EntityDto input);

        Task<MemberDataTableListDto> GetPaginated(string filter, int initalPage, int pageSize);
        Task<MemberCreateResult> MemberRegisterAsync(MemberRegisterDto input);

        Task<MemberRegisterDto> GetMemberForRegister(EntityDto input);

        Task<Member> GetMemberByEmailAndTeamId(int TeamId, string email);


        Task<List<TeamMembershipDto>> TeamMembershipsAsync(int memberId);
    }
}
