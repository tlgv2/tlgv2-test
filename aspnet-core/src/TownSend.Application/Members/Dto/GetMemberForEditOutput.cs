﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members.Dto
{
    public class GetMemberForEditOutput
    {
        public MemberEditDto Member { get; set; }
    }
}
