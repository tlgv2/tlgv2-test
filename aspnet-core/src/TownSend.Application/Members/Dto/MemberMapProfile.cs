﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members.Dto
{
    public class MemberMapProfile : Profile
    {
        public MemberMapProfile()
        {

            CreateMap<CreateMemberDto, Member>();

            CreateMap<MemberDto, Member>();
            CreateMap<Member, MemberDto>();
            CreateMap<Member, MemberListDto>();
            CreateMap<Member, MemberEditDto>();
            CreateMap<MemberEditDto, Member>();
            CreateMap<Member, MemberRegisterDto>();
            CreateMap<MemberRegisterDto, Member>();
            CreateMap<Member, MemberRegisterDto>();
            CreateMap<MemberAgreementDto, Member>();
        }
    }
}
