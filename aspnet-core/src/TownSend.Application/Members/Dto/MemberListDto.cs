﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members.Dto
{
    [AutoMapFrom(typeof(Member))]
    public class MemberListDto : EntityDto
    {
        public string TeamName { get; set; }
        public string  FullName { get; set; }
        public string Email { get; set; }       
        public string State { get; set; }

        public long PhoneNumber { get; set; }
        



    }
}
