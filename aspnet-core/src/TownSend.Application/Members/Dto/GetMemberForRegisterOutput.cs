﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members.Dto
{
    public class GetMemberForRegisterOutput
    {
        public MemberRegisterDto Member { get; set; }
    }
}
