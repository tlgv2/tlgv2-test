﻿using Abp.Authorization.Users;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Members.Dto
{
    
    public class CreateMemberDto
    {
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        [UniqueEmailAddress]
 
        public string FirstName { get; set; }
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string PhoneNumber { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid Price")]
        public int BasePrice { get; set; }
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid Discount")]
        public int Discount { get; set; }
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid ScholarshipAmount")]
        public int ScholarshipAmount { get; set; }
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid MembershipAmount")]
        public decimal MembershipAmount { get; set; }
        public int TeamId { get; set; }
        public long UserId { get; set; }

        public decimal Deposit { get; set; }

        public DateTime payinfulldate { get; set; }

        public decimal payinfullamount { get; set; }

        public int PayInFullDiscount { get; set; }

        public DateTime[] deposit12monthdate { get; set; }

        public decimal[] deposit12monthamount { get; set; }

        public DateTime[] custompaydate { get; set; }

        public decimal[] custompayamount { get; set; }

        public Boolean Concordia_Discount { get; set; }
        public Boolean Ministry_Discount { get; set; }
        public Boolean Pay_in_Full { get; set; }
        public Boolean Deposit_12_Equal_Payments { get; set; }
        public DateTime CustomStartDate { get; set; }
        public Boolean Custom_Plan { get; set; }
        public Boolean CustomPay_Deposit { get; set; }

        public int Number_of_Installment { get; set; }


    }
}
