﻿using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Members.Dto
{
    public class MemberRegisterDto : EntityDto
    {
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string FirstName { get; set; }
        
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string Email { get; set; }
        public long PhoneNumber { get; set; }
        [Required]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string ZipCode { get; set; }
        [RegularExpression(@"^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,50}$",ErrorMessage = "Your password must be have at least 8 characters long, 1 uppercase, 1 lowercase character, 1 number and 1 special character")]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "Password and Confirm Password must match.")] 
        public string ConfirmPassword { get; set; }
    }
}
