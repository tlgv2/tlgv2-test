﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members.Dto
{
    public class MemberDto : EntityDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int State { get; set; }
        public int ZipCode { get; set; }
        public long PhoneNumber { get; set; }
        public int BasePrice { get; set; }
        public int Discount { get; set; }
        public int Scholarship_Amount { get; set; }
        public int Membership_Amount { get; set; }
        public int TeamId { get; set; }

        public string AboutMe { get; set; }

        public string FullName { get; set; }

        public string Password { get; set; }
    }
}
