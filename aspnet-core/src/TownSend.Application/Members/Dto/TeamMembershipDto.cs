﻿using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Partners;
using TownSend.Teams.Dto;

namespace TownSend.Members.Dto
{
    public class TeamMembershipDto
    {
        public TeamListDto Teams { get; set; }
        public decimal ScholarshipAmount { get; set; }

        public decimal Discount { get; set; }

        public decimal Net { get; set; }
        public decimal Balance { get; set; }
    
        public DateTime DateJoined { get; set; }
    }
}
