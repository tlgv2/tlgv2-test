﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members.Dto
{
    public class MemberDataTableListDto
    {
        public long totalRecord { get; set; }

        public List<MemberListDto> members { get; set; }
    }
}
