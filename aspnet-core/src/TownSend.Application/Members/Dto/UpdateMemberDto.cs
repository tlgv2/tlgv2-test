﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members.Dto
{
        [AutoMapTo(typeof(Member))]
        public class UpdateMemberDto : EntityDto
        {
            public string Team { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public long PhoneNumber { get; set; }
        }
    
}
