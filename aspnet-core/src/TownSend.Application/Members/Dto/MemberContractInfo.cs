﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Members.Dto
{
    public class MemberContractInfo
    {
        public int ContractInfoID { get; set; }
        public int CustomerID { get; set; }
        public int InviteID { get; set; }
        public int? ReferralID { get; set; }
        public int ProductID { get; set; }
        public int? ScholarshipID { get; set; }
        public int ProductDuration { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal InviteDiscount { get; set; }
        public decimal LumpSumDiscount { get; set; }
        public int Quantity { get; set; }
        public decimal GenericDiscount { get; set; }
        public decimal ScolarshipDiscount { get; set; }
        public decimal SubTotal { get; set; }
        public int PaymentTermID { get; set; }
        public int PaymentMethodID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ConfirmationDate { get; set; }
    }
}
