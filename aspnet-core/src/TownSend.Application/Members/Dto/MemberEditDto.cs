﻿using Abp.Authorization.Users;
using Abp.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Members.Dto
{
    public class MemberEditDto : Entity<int>
    {
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string FirstName { get; set; }
        
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [BindProperty]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]        
        public string PhoneNumber { get; set; }
        [Required]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Use letters only please")]
        public string City { get; set; }        
        public string Country { get; set; }
        [Required]
        public int State { get; set; }
        [Required]       
        [RegularExpression("[0-9]{5}",ErrorMessage ="Zip code must be 5 digit")]
        public int ZipCode { get; set; }

        public string FullName { get; set; }
        public DateTime CreationTime { get; set; }
        public string MiddleName { get; set; }
        //[Required]
        public int TeamId { get; set; }
        public long UserId { get; set; }

        //public string AboutMe { get; set; }

    }
}
