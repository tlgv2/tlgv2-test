using Abp.Authorization.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Members.Dto
{
    public class InvitationPaymentDto
    {
        [RegularExpression(@"5[1-5][0-9]{14}|2(?:2(?:2[1-9]|[3-9][0-9])|[3-6][0-9][0-9]|7(?:[01][0-9]|20))[0-9]{12}|3[47][0-9]{13}|4[0-9]{12}(?:[0-9]{3})?|65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})|(?:2131|1800|35[0-9]{3})[0-9]{11}|(3(?:088|096|112|158|337|5(?:2[89]|[3-8][0-9]))\d{12})|
3(?:0[0 - 5] |[68][0 - 9])[0 - 9]{11}|(5018|5081|5044|5020|5038|603845|6304|6759|676[1-3]|6799|6220|504834|504817|504645)[0-9]{8,15}", ErrorMessage = "Invalid {0}")]
        [CreditCard]
        [Required(ErrorMessage = "The field is required")]
        [StringLength(AbpUserBase.MaxNameLength, ErrorMessage = "Max Length is 16")]
        [Display(Name = "Number on card")]
        public int CardNumber { get; set; } 

        //[ConditionalRequired("{0} === '{1}'", nameof(PaymentMethodType), HubOnePaymentMethodType.COACH_CreditCard)]
        public string NameOnCard { get; set; }

        //[ConditionalRequired("{0} === '{1}'", nameof(PaymentMethodType), HubOnePaymentMethodType.COACH_CreditCard, ErrorMessage = "{0} Required")]
        [RegularExpression(@"^(0[1-9]|1[0-2])\/?(([0-9]{2})$)", ErrorMessage = "Invalid {0} format MM/YY")]
        public string Expiration { get; set; }

        //[CRequired("{0} === '{1}'", nameof(PaymentMethodType), HubOnePaymentMethodType.COACH_CreditCard, ErrorMessage = "{0} Required")]
        [Required]
        [Range(100, 999)]
        [StringLength(AbpUserBase.MaxNameLength, ErrorMessage = "Max Length is 3")]
        [RegularExpression(@"^[0-9]{3,4}$", ErrorMessage = "Invalid {0}")]
        public int CVC { get; set; }
        public string Nameoncard  { get; set; }
    }
}
