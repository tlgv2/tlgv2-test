﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Timing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Authorization;
using TownSend.Authorization.Roles;
using TownSend.Authorization.Users;
using TownSend.Invites;
using TownSend.Members.Dto;
using TownSend.Scholarships;
using TownSend.Teams;
using TownSend.Url;
using TownSend.Users;

namespace TownSend.Members
{
    [AbpAuthorize(PermissionNames.Pages_Members)]
    public class MemberAppService : TownSendAppServiceBase, IMemberAppService
    {
        public IAppUrlService AppUrlService { get; set; }

        private readonly IRepository<Member> _memberRepository;
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly UserAppService _userAppService;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<Invite, long> _inviteRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly IRepository<MemberInviteMapping> _memberInviteMappingRepository;
        private readonly IRepository<MemberTeamMapping> _memberTeamMappingRepository;

        private readonly IRepository<MemberPrice> _memberPriceRepository;
        private readonly IRepository<MemberPaymentModeMapping> _memberPaymentModeMappingRepository;
        private readonly IRepository<MemberPaymentModeDetail> _memberPaymentModeDetailRepository;
        private readonly ITeamAppService _teamAppService;
        private readonly IRepository<Scholarship> _scholarshipRepository;
        private readonly IRepository<MemberPaymentModeMapping> _memberPaymentRepository;

        public MemberAppService(IRepository<Member> memberRepository,
                                 RoleManager roleManager,
                                    UserManager userManager,
                                    IRepository<Invite, long> inviteRepository,
                                    UserAppService userAppService,
                                    IRepository<MemberInviteMapping> memberInviteMappingRepository,
                                    IRepository<MemberTeamMapping> memberTeamMappingRepository,
                                    IUserEmailer userEmailer,
                                    IRepository<Team> teamRepository,
                                    IRepository<MemberPrice> memberPriceRepository,
                                    IRepository<MemberPaymentModeMapping> memberPaymentModeMappingRepository,
                                    IRepository<MemberPaymentModeDetail> memberPaymentModeDetailRepository,
                                    ITeamAppService teamAppService,
                                    IRepository<Scholarship> scholarshipRepository)
        {
            _memberRepository = memberRepository;
            _roleManager = roleManager;
            _userManager = userManager;
            _userAppService = userAppService;
            _teamRepository = teamRepository;
            _userEmailer = userEmailer;
            _inviteRepository = inviteRepository;
            _memberInviteMappingRepository = memberInviteMappingRepository;
            _memberTeamMappingRepository = memberTeamMappingRepository;
            _memberPriceRepository = memberPriceRepository;
            _memberPaymentModeMappingRepository = memberPaymentModeMappingRepository;
            _memberPaymentModeDetailRepository = memberPaymentModeDetailRepository;
            _teamAppService = teamAppService;
            _scholarshipRepository = scholarshipRepository;            
        }

        /// <summary>
        /// This Method Generate Member And Also Map To Team
        /// Affected Entities
        ///     Scholarship
        ///     User
        ///     UserRoles
        ///     Role
        ///     Invite
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<MemberCreateResult> CreateAsync(CreateMemberDto input)
        {
            try
            {
                var memberList = await (
                                from m in _memberRepository.GetAll() 
                                where m.Email == input.Email && m.TeamId == input.TeamId
                                select m
                               ).ToListAsync();

                if (memberList == null || (memberList != null && memberList.Any()))
                {
                    //already scholarship
                    var errorMessages = new List<string>();
                    errorMessages.Add($"This person already requested for a member.");
                    return new MemberCreateResult
                    {
                        ErrorMessage = errorMessages,
                        IsSuccess = false
                    };
                }
                var role = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Member);
                // Create partner user for the tenant
                var user = User.CreatePartnerUser(AbpSession.TenantId, input.Email, input.FirstName, input.LastName , input.PhoneNumber.ToString());
                //await _userManager.InitializeOptionsAsync(tenant.Id);
                CheckErrors(await _userManager.CreateAsync(user, "Admin@123"));
                await CurrentUnitOfWork.SaveChangesAsync(); // To get admin user's id

                // Assign partner user to role!
                CheckErrors(await _userManager.AddToRoleAsync(user, role.Name));
                await CurrentUnitOfWork.SaveChangesAsync();

                int? tenantId = AbpSession.TenantId;
                int? roleId = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Member)?.Id;
                //create user

                if (user != null && user.Id > 0)
                {
                    input.UserId = user.Id; //set user id to partner table
                    var data = ObjectMapper.Map<Member>(input);
                    data.MemberStatusId = MemberStatusEnum.InviteSentNeverOpened;
                    data.NotificationDate = DateTime.UtcNow;
                    int memberId = await _memberRepository.InsertAndGetIdAsync(data);

                    if (memberId > 0)
                    {
                        if (!user.IsEmailConfirmed)
                        {
                            //insert data into invite table
                            var invite = new Invite
                            {
                                UserId = user.Id,
                                RoleId = roleId ?? 0,
                                InviteToken = Guid.NewGuid().ToString(),
                                //StatusId = 1, // PartnerInvited
                                TeamId = input.TeamId,
                                IsActive = true,
                                ExpirationDate = Clock.Now.AddYears(1)
                            };

                            var member = new Member
                            {
                                UserId = user.Id,
                                FirstName = input.FirstName,
                                LastName = input.LastName,
                                //Scholarship_Amount = input.ScholarshipAmount, //TODO: need to replace 
                                TeamId = input.TeamId,
                                Email = input.Email,
                                //BasePrice = input.BasePrice,
                                //Discount = input.Discount,
                                //Membership_Amount = input.MembershipAmount
                                
                            };


                            long inviteId = await _inviteRepository.InsertAndGetIdAsync(invite);

                            //send email temp commented  TODO:dev1
                            string emailActivationLink = AppUrlService.CreateMemberInviteEmailUrlFormat(tenantId);
                            await _userEmailer.SendEmailMemberInviteAsync(user,memberId, member, user.TenantId, emailActivationLink);

                            //create mapping
                            if (memberId > 0 && inviteId > 0)
                            {
                                MemberInviteMapping mapping = new MemberInviteMapping
                                {
                                    MemberId = memberId,
                                    InviteId = inviteId
                                };

                                await _memberInviteMappingRepository.InsertAsync(mapping);
                            }

                            if (memberId > 0 && member.TeamId > 0)
                            {
                                MemberTeamMapping mapping = new MemberTeamMapping
                                {
                                    MemberId = memberId,
                                    TeamId = member.TeamId
                                };

                                await _memberTeamMappingRepository.InsertAsync(mapping);
                            }

                            if (memberId > 0 )
                            {
                                MemberPrice memberPrice = new MemberPrice
                                {
                                    MemberId = memberId,
                                    BasePrice = input.BasePrice,
                                    Discount = input.Discount,
                                    Scholarship_Amount = input.ScholarshipAmount,
                                    Membership_Amount = input.MembershipAmount,
                                    Deposit = input.Deposit,
                                    Concordia_Discount = input.Concordia_Discount,
                                    Ministry_Discount = input.Ministry_Discount,
                                    CustomStartDate = input.CustomStartDate,
                                    PayInFullDiscount = input.PayInFullDiscount
                                };

                                await _memberPriceRepository.InsertAsync(memberPrice);

                                if (input.Pay_in_Full)
                                {
                                    MemberPaymentModeMapping mapping = new MemberPaymentModeMapping
                                    {
                                        MemberId = memberId,
                                        PaymentMode = MemberPaymentModeEnum.PayInFull
                                    };
                                    long memberpaymentmodeId = await _memberPaymentModeMappingRepository.InsertAndGetIdAsync(mapping);

                                    MemberPaymentModeDetail memberPaymentModeDetail = new MemberPaymentModeDetail
                                    {
                                        MemberId = memberId,
                                        MemberPaymentModeMappingId = memberpaymentmodeId,
                                        PayAmount = input.payinfullamount,
                                        PayDate = input.payinfulldate
                                    };

                                    await _memberPaymentModeDetailRepository.InsertAsync(memberPaymentModeDetail);
                                }

                                if (input.Deposit_12_Equal_Payments)
                                {
                                    MemberPaymentModeMapping mapping = new MemberPaymentModeMapping
                                    {
                                        MemberId = memberId,
                                        PaymentMode = MemberPaymentModeEnum.Deposit12EqualPayments
                                    };
                                    long memberpaymentmodeId = await _memberPaymentModeMappingRepository.InsertAndGetIdAsync(mapping);
                                    for (int i = 0; i < input.deposit12monthdate.Length; i++)
                                    {
                                        MemberPaymentModeDetail memberPaymentModeDetail = new MemberPaymentModeDetail
                                        {
                                            MemberId = memberId,
                                            MemberPaymentModeMappingId = memberpaymentmodeId,
                                            PayAmount = input.deposit12monthamount[i],
                                            PayDate = input.deposit12monthdate[i]
                                        };

                                        await _memberPaymentModeDetailRepository.InsertAsync(memberPaymentModeDetail);
                                    }
                                }

                                if (input.Custom_Plan)
                                {
                                    MemberPaymentModeMapping mapping = new MemberPaymentModeMapping
                                    {
                                        MemberId = memberId,
                                        PaymentMode = input.CustomPay_Deposit ? MemberPaymentModeEnum.CustomPlanWithDeposit : MemberPaymentModeEnum.CustomPlanWithOutDeposit
                                    };
                                    long memberpaymentmodeId = await _memberPaymentModeMappingRepository.InsertAndGetIdAsync(mapping);
                                    for (int i = 0; i < input.custompaydate.Length; i++)
                                    {
                                        MemberPaymentModeDetail memberPaymentModeDetail = new MemberPaymentModeDetail
                                        {
                                            MemberId = memberId,
                                            MemberPaymentModeMappingId = memberpaymentmodeId,
                                            PayAmount = input.custompayamount[i],
                                            PayDate = input.custompaydate[i]
                                        };

                                        await _memberPaymentModeDetailRepository.InsertAsync(memberPaymentModeDetail);
                                    }
                                }

                            }


                        }
                    }
                }
                return new MemberCreateResult
                {
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<MemberCreateResult> UpdateAsync(MemberEditDto input)
        {
            try
            {
                var data = await _memberRepository.GetAsync(input.Id);
                ObjectMapper.Map(input, data);
                await _memberRepository.UpdateAsync(data);
                return new MemberCreateResult
                {
                    IsSuccess = true
                };
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }

        public async Task<ListResultDto<MemberListDto>> GetAll()
        {
            var memberResult = await _memberRepository.GetAll().ToListAsync();
            return new ListResultDto<MemberListDto>(ObjectMapper.Map<List<MemberListDto>>(memberResult));
        }

        public async Task<MemberDto> GetAsync(int id)
        {
            var data = await _memberRepository.GetAsync(id);
            var result = ObjectMapper.Map<MemberDto>(data);
            return result;
        }

        public async Task<MemberEditDto> GetMemberForEdit(EntityDto input)
        {
            var data = await _memberRepository.GetAsync(input.Id);
            var dataEditDto = ObjectMapper.Map<MemberEditDto>(data);

            return dataEditDto;
        }

        public async Task<MemberRegisterDto> GetMemberForRegister(EntityDto input)
        {
            var data = await _memberRepository.GetAsync(input.Id);
            var dataEditDto = ObjectMapper.Map<MemberRegisterDto>(data);

            return dataEditDto;
        }

        public async Task<MemberCreateResult> MemberRegisterAsync(MemberRegisterDto input)
        {
            try
            {
                var data = await _memberRepository.GetAsync(input.Id);
                ObjectMapper.Map(input, data);
                data.MemberStatusId = MemberStatusEnum.InviteOpenedPendingContractSignature;
                data.NotificationDate = DateTime.UtcNow;
                await _memberRepository.UpdateAsync(data);
                return new MemberCreateResult
                {
                    IsSuccess = true
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
           
            //return input;
        }

        public async Task MemberAgreementAsync(MemberAgreementDto input)
        {
            var data = await _memberRepository.GetAsync(input.Id);
            ObjectMapper.Map(input, data);
            data.MemberStatusId = MemberStatusEnum.InviteOpenedPendingPaymentSelection;
            data.NotificationDate = DateTime.UtcNow;
            await _memberRepository.UpdateAsync(data);
            //return input;
        }

        public async Task<MemberDataTableListDto> GetPaginated(string filter, int initalPage, int pageSize)
        {
            try
            {

                var teamQuery = (from team in _teamRepository.GetAll()
                                 join member in _memberRepository.GetAll() on team.Id equals member.TeamId
                                 select new
                                 {
                                     team.TeamName,
                                     member.FullName,
                                     member.PhoneNumber,
                                     member.Email,
                                     member.Id
                                 }).ToList();

                long totalRecords = teamQuery.Count();
                var memberLists = new List<MemberListDto>();
                foreach (var q1 in teamQuery)
                {
                    var re = new MemberListDto
                    {
                        Id = q1.Id,
                        TeamName = q1.TeamName,
                        FullName = q1.FullName,
                        PhoneNumber = q1.PhoneNumber,
                        Email = q1.Email,

                    };

                    memberLists.Add(re);
                }
                List<MemberListDto> result = new List<MemberListDto>();

                List<MemberListDto> filterResult = new List<MemberListDto>();
                if (IsGranted("Pages.Partners.Create"))
                {
                    result = memberLists.ToList();
                }
                else
                {
                    result = (from list in memberLists
                              join member in _memberRepository.GetAll() on list.Id equals member.Id
                              where member.UserId == AbpSession.UserId
                              select list).ToList();
                    //result = result.Where(x=> result


                    //result = teamLists.Where(x => x.Partners.Select(y => y.UserId).FirstOrDefault() == AbpSession.UserId).ToList();
                }

                if (!string.IsNullOrEmpty(filter))
                {
                    totalRecords = result.Where(x => x.TeamName.ToUpper().Contains(filter.ToUpper())
                                                || x.FullName.ToUpper().Contains(filter.ToUpper())

                                            ).Count();
                    filterResult = result.Where(x => x.TeamName.ToUpper().Contains(filter.ToUpper())
                                                || x.FullName.ToUpper().Contains(filter.ToUpper())

                                            )
                                            .Skip((initalPage * pageSize)).Take(pageSize).ToList();

                }
                else
                {
                    totalRecords = result.Count();
                    filterResult = result.Skip((initalPage * pageSize)).Take(pageSize).ToList();
                }






                //recordsFiltered = result.Count();


                List<MemberListDto> resultPartner = ObjectMapper.Map<List<MemberListDto>>(filterResult);

                MemberDataTableListDto memberDataTableListDto = new MemberDataTableListDto();
                memberDataTableListDto.totalRecord = totalRecords;
                memberDataTableListDto.members = resultPartner;

                return ObjectMapper.Map<MemberDataTableListDto>(memberDataTableListDto);

                //var query = await _partnerRepository.GetAll().ToListAsync();
                //var result = new List<PartnerListDto>();
                //foreach(var partner in query)
                //{
                //    var resultPartner = ObjectMapper.Map<PartnerListDto>(partner);
                //    result.Add(resultPartner);
                //}
                //return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Member> GetMemberByEmailAndTeamId(int TeamId, string email)
        {
            
            var memberResult = await (from member in _memberRepository.GetAll()
                                           join memberteam in _memberTeamMappingRepository.GetAll() on member.Id equals memberteam.MemberId
                                           where member.Email == email && memberteam.TeamId == TeamId
                                           select member).FirstOrDefaultAsync();
            //var result = ObjectMapper.Map<ScholarshipOutputDto>(scholarshipResult);
            //return result;

            return memberResult;
        }

        public async Task<List<TeamMembershipDto>> TeamMembershipsAsync(int memberId)
        {
            //int[] partnerId = null;
            var teams = await _teamAppService.GetAll();

            var query = (from team in teams.Items
                        join memberMap in _memberTeamMappingRepository.GetAll() on team.Id equals memberMap.TeamId
                        join member in _memberRepository.GetAll() on memberMap.MemberId equals member.Id
                        join memberprice in _memberPriceRepository.GetAll() on member.Id equals memberprice.MemberId
                         //where _memberPaymentRepository.GetAll() 
                         //from scholarship in _scholarshipRepository.GetAll()
                         //where member.Id == scholarship.MemberId
                         where member.Id == memberId
                         select new TeamMembershipDto
                         {
                             Teams = team,
                             DateJoined = member.CreationTime,
                             ScholarshipAmount = memberprice.Scholarship_Amount,
                             Discount=memberprice.Discount,
                             Balance=memberprice.Membership_Amount

                         }).ToList();

            return query;
        }
    }
}

