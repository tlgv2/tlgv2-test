﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using TownSend.Authorization;

namespace TownSend
{
    [DependsOn(
        typeof(TownSendCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class TownSendApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<TownSendAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(TownSendApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
