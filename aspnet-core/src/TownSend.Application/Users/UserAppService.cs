﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.UI;
using TownSend.Authorization;
using TownSend.Authorization.Accounts;
using TownSend.Authorization.Roles;
using TownSend.Authorization.Users;
using TownSend.Roles.Dto;
using TownSend.Users.Dto;
using TownSend.Partners;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using Abp.Domain.Uow;
using Abp.Authorization.Users;
using TownSend.Members;

namespace TownSend.Users
{
    [AbpAuthorize(PermissionNames.Pages_Users)]
    public class UserAppService : AsyncCrudAppService<User, UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>, IUserAppService
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IAbpSession _abpSession;
        private readonly LogInManager _logInManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly PartnerManager _partnerManager;
        private readonly IRepository<Member> _ṃemberRepository;
        private readonly IRepository<UserRole,long> _userRoleRepository;
       // private readonly IRepository<UserStatusListDto> _userStatusRepository;

        public UserAppService(
            IRepository<User, long> repository,
            UserManager userManager,
            RoleManager roleManager,
            IRepository<Role> roleRepository,
            IRepository<Partner> partnerRepository,
            IPasswordHasher<User> passwordHasher,
            IAbpSession abpSession,
            LogInManager logInManager,
            IUnitOfWorkManager unitOfWorkManager,
            PartnerManager partnerManager,
            IRepository<Member> ṃemberRepository,
            IRepository<UserRole, long> userRoleRepository)
         //   IRepository<UserStatusListDto> userStatusRepository)
            : base(repository)
        {
            _userRepository = repository;
            _userManager = userManager;
            _roleManager = roleManager;
            _roleRepository = roleRepository;
            _partnerRepository = partnerRepository;
            _passwordHasher = passwordHasher;
            _abpSession = abpSession;
            _logInManager = logInManager;
            _unitOfWorkManager = unitOfWorkManager;
            _partnerManager = partnerManager;
            _ṃemberRepository = ṃemberRepository;
            _userRoleRepository = userRoleRepository;
           // _userStatusRepository = userStatusRepository;
        }

        public override async Task<UserDto> CreateAsync(CreateUserDto input)
        {
            CheckCreatePermission();

            var user = ObjectMapper.Map<User>(input);

            user.TenantId = AbpSession.TenantId;
            user.IsEmailConfirmed = true;

            await _userManager.InitializeOptionsAsync(AbpSession.TenantId);

            CheckErrors(await _userManager.CreateAsync(user, input.Password));

            if (input.RoleNames != null)
            {
                CheckErrors(await _userManager.SetRolesAsync(user, input.RoleNames));
            }

            CurrentUnitOfWork.SaveChanges();

            return MapToEntityDto(user);
        }

        public override async Task<UserDto> UpdateAsync(UserDto input)
        {
            try
            {
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
                {
                    CheckUpdatePermission();

                    var user = await _userManager.GetUserByIdAsync(input.Id);
                    

                    user.SetNormalizedNames();
                     MapToEntity(input, user);
                    //var result = ObjectMapper.Map(input,user);
                    CheckErrors(await _userManager.UpdateAsync(user));

                    //var partner = _userManager.GetUserById(result.Id);
                    var partner = await (from ptr in _partnerRepository.GetAll()
                                         join usr in _userRepository.GetAll() on ptr.UserId equals usr.Id
                                         where ptr.UserId == user.Id
                                         select ptr).FirstOrDefaultAsync();
                    if (partner != null)
                    {
                        partner.FirstName = user.Name;
                        partner.LastName = user.Surname;
                        //partner.PhoneNumber = user.PhoneNumber;
                        //var partners = ObjectMapper.Map(result, partner);
                        //await _partnerRepository.UpdateAsync(partners);
                    }

                    if (input.RoleNames != null)
                    {
                        CheckErrors(await _userManager.SetRolesAsync(user, input.RoleNames));
                    }
                    //var partner = await _partnerManager.GetUserById();
                    //_partnerRepository.
                    return await GetAsync(input);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public async Task<GetUserForEditOutput> GetUserForEdit(EntityDto input)
        //{
        //    var data = await _userRepository.GetAsync(input.Id);
        //    var dataEditDto = ObjectMapper.Map<GetUserForEditOutput>(data);

        //    return new GetUserForEditOutput
        //    {
        //        User = dataEditDto
        //    };
        //}
        public override async Task DeleteAsync(EntityDto<long> input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var user = await _userManager.GetUserByIdAsync(input.Id);
                if (user.IsDeleted == false)
                {
                    user.IsDeleted = true;
                    var partner = await (from ptr in _partnerRepository.GetAll()
                                         join usr in _userRepository.GetAll() on ptr.UserId equals usr.Id
                                         where ptr.UserId == user.Id
                                         select ptr).FirstOrDefaultAsync();
                    var member = await (from mbr in _ṃemberRepository.GetAll()
                                        join usr in _userRepository.GetAll() on mbr.UserId equals usr.Id
                                        where mbr.UserId == user.Id
                                        select mbr).FirstOrDefaultAsync();
                    if (partner != null && partner.IsDeleted == false)
                    {
                        partner.IsDeleted = true;
                        await _partnerRepository.UpdateAsync(partner);
                    }
                    else if (member != null && member.IsDeleted == false)
                    {
                        member.IsDeleted = true;
                        await _ṃemberRepository.UpdateAsync(member);
                    }
                }
                await _userManager.DeleteAsync(user);
            }
        }
        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var roles = await _roleRepository.GetAllListAsync();
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(roles));
        }

        public async Task ChangeLanguage(ChangeUserLanguageDto input)
        {
            await SettingManager.ChangeSettingForUserAsync(
                AbpSession.ToUserIdentifier(),
                LocalizationSettingNames.DefaultLanguage,
                input.LanguageName
            );
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            user.SetNormalizedNames();
            return user;
        }

        protected override void MapToEntity(UserDto input, User user)
        {
            ObjectMapper.Map(input, user);
            user.SetNormalizedNames();
        }

        //protected override UserDto MapToEntityDto(User user)
        //{
        //    var roleIds = user.Roles.Select(x => x.RoleId).ToArray();

        //    var roles = _roleManager.Roles.Where(r => roleIds.Contains(r.Id)).Select(r => r.NormalizedName);

        //    var userDto = base.MapToEntityDto(user);
        //    userDto.RoleNames = roles.ToArray();

        //    return userDto;
        //}

        protected override IQueryable<User> CreateFilteredQuery(PagedUserResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles)
                .WhereIf(!input.Keyword.IsNullOrWhiteSpace(), x => x.UserName.Contains(input.Keyword) || x.Name.Contains(input.Keyword) || x.EmailAddress.Contains(input.Keyword))
                .WhereIf(input.IsActive.HasValue, x => x.IsActive == input.IsActive);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            var user = await Repository.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);

            if (user == null)
            {
                throw new EntityNotFoundException(typeof(User), id);
            }

            return user;
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedUserResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<bool> ChangePassword(ChangePasswordDto input)
        {
            if (_abpSession.UserId == null)
            {
                throw new UserFriendlyException("Please log in before attemping to change password.");
            }
            long userId = _abpSession.UserId.Value;
            var user = await _userManager.GetUserByIdAsync(userId);
            var loginAsync = await _logInManager.LoginAsync(user.UserName, input.CurrentPassword, shouldLockout: false);
            if (loginAsync.Result != AbpLoginResultType.Success)
            {
                throw new UserFriendlyException("Your 'Existing Password' did not match the one on record.  Please try again or contact an administrator for assistance in resetting your password.");
            }
            if (!new Regex(AccountAppService.PasswordRegex).IsMatch(input.NewPassword))
            {
                throw new UserFriendlyException("Passwords must be at least 8 characters, contain a lowercase, uppercase, and number.");
            }
            user.Password = _passwordHasher.HashPassword(user, input.NewPassword);
            CurrentUnitOfWork.SaveChanges();
            return true;
        }

        public async Task<bool> ResetPassword(ResetPasswordDto input)
        {
            if (_abpSession.UserId == null)
            {
                throw new UserFriendlyException("Please log in before attemping to reset password.");
            }
            long currentUserId = _abpSession.UserId.Value;
            var currentUser = await _userManager.GetUserByIdAsync(currentUserId);
            var loginAsync = await _logInManager.LoginAsync(currentUser.UserName, input.AdminPassword, shouldLockout: false);
            if (loginAsync.Result != AbpLoginResultType.Success)
            {
                throw new UserFriendlyException("Your 'Admin Password' did not match the one on record.  Please try again.");
            }
            if (currentUser.IsDeleted || !currentUser.IsActive)
            {
                return false;
            }
            var roles = await _userManager.GetRolesAsync(currentUser);
            if (!roles.Contains(StaticRoleNames.Tenants.Admin))
            {
                throw new UserFriendlyException("Only administrators may reset passwords.");
            }

            var user = await _userManager.GetUserByIdAsync(input.UserId);
            if (user != null)
            {
                user.Password = _passwordHasher.HashPassword(user, input.NewPassword);
                CurrentUnitOfWork.SaveChanges();
            }

            return true;
        }
        public async Task<ListResultDto<UserDto>> GetAllDeletedData(int[] UserType,UserStatusListDto[] Status)
        {
            PagedResultDto<UserDto> result = new PagedResultDto<UserDto>();
            var listuser = new List<User>();
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var query = (from user in _userRepository.GetAllList()
                             join map in _userRoleRepository.GetAllList() on user.Id equals map.UserId
                             join role in _roleRepository.GetAllList() on  map.RoleId equals role.Id
                             select new
                             {
                                 user,
                                 map,
                                 role
                             }).ToList();
                var conditionquery = (from user in _userRepository.GetAll()
                                      join map in _userRoleRepository.GetAll() on user.Id equals map.UserId
                                      join role in _roleRepository.GetAll() on map.RoleId equals role.Id
                                      select new
                                      {
                                          user,
                                          map,
                                          role
                                      }).ToList();

                var deleteUser = _userRepository.GetAll().Where(x => x.IsDeleted == true).ToList();

                var q = conditionquery.Select(x => x.user).Distinct().ToList();
                q.AddRange(deleteUser);
                var userList1 = new List<UserDto>();
                foreach (var q1 in q)
                {
                    var re = new UserDto
                    {
                        Id = q1.Id,
                        Name = q1.Name,
                        Surname = q1.Surname,
                        Rolename = q1.Name,
                        EmailAddress = q1.EmailAddress,
                        IsActive = q1.IsActive,
                        IsDeleted = q1.IsDeleted

                    };
                    var rl = new List<Role>();
                    foreach (var q2 in conditionquery)
                    {
                        if (q1.Id == q2.map.UserId)
                        {
                            rl.Add(q2.role);
                        }
                    }
                    re.Roles = rl;
                    userList1.Add(re);
                }
                var input = new PagedUserResultRequestDto();
                
                if (UserType.Count() > 0)
                {

                    //userList1 = userList1.Where(x =>UserType.Contains(x.Roles.Select(y=>y.Id).ToList())).ToList();
                    conditionquery = conditionquery.Where(x => UserType.Contains(x.role.Id)).ToList();
                    
                }
                if (Status.Count() > 0)
                {

                    foreach (var active in Status)
                    {
                        if (UserStatusListDto.Active==active)
                        {
                            userList1 = userList1.Where(x => x.IsActive == true).ToList();
                        }
                        if (UserStatusListDto.NotActive==active)
                        {
                            userList1 = userList1.Where(x => x.IsActive == false).ToList();
                        }
                        if(UserStatusListDto.Deleted==active)
                        {
                            userList1 = userList1.Where(x => x.IsDeleted == true).ToList();
                        }
                    }
                 
                }
                if (UserType.Count() > 0)
                {
                    var test = (from user in userList1
                                join map in _userRoleRepository.GetAll() on user.Id equals map.UserId
                                join role in _roleRepository.GetAll() on map.RoleId equals role.Id
                                select new
                                {
                                    user,
                                    role
                                }).Where(x => UserType.Contains(x.role.Id)).ToList();
                    userList1 = test.Select(x => x.user).ToList();
                }
                //listuser = await _userRepository.GetAllListAsync();
                
                //List<UserDto> dto = ObjectMapper.Map<List<UserDto>>(userList1);
                //return dto;
               //return new ListResultDto<UserDto>(userList1);
                return new ListResultDto<UserDto>(ObjectMapper.Map<List<UserDto>>(userList1));
            }
            //return new ListResultDto<UserDto>(ObjectMapper.Map<List<UserDto>>(result.Items));
           
        }
        public async Task UnDelete(EntityDto<long> input)
        {
            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var user = await _userManager.GetUserByIdAsync(input.Id);                
                if (user.IsDeleted == true)
                {
                    user.IsDeleted = false;
                    var partner = await (from ptr in _partnerRepository.GetAll()
                                         join usr in _userRepository.GetAll() on ptr.UserId equals usr.Id
                                         where ptr.UserId==user.Id
                                         select ptr).FirstOrDefaultAsync();
                    var member = await (from mbr in _ṃemberRepository.GetAll()
                                        join usr in _userRepository.GetAll() on mbr.UserId equals usr.Id
                                        where mbr.UserId == user.Id
                                        select mbr).FirstOrDefaultAsync();
                    if (partner!=null && partner.IsDeleted==true)
                    {
                        partner.IsDeleted=false;
                        await _partnerRepository.UpdateAsync(partner);
                        await _userManager.AddToRoleAsync(user,StaticRoleNames.Tenants.Partner);
                    }        
                    else if(member!=null && member.IsDeleted==true)
                    {
                        member.IsDeleted = false;
                        await _ṃemberRepository.UpdateAsync(member);
                        await _userManager.AddToRoleAsync(user, StaticRoleNames.Tenants.Member);
                    }
                    
                }

            }
        }
        //public async Task<ListResultDto<UserDto>> GetUserstatusForDD()
        //{
        //    List<UserStatusListDto> dto = new List<UserStatusListDto>();
        //    var data = await _scholarshipStatusRepository.GetAllListAsync();
        //    return new ListResultDto<UserStatusListDto>(ObjectMapper.Map(data, dto));
        //}
    }
}

