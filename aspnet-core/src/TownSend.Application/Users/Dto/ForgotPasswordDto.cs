﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Users.Dto
{
    public class ForgotPasswordDto
    {
        public long UserId { get; set; }
        public long InviteId { get; set; }
        public string ResetCode { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Password and ConfirmPassword does not match.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
