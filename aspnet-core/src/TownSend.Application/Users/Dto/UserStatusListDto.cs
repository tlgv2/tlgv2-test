﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Users.Dto
{
    public enum UserStatusListDto
    {
        Active = 1,
        NotActive,
        Deleted
    }
}
