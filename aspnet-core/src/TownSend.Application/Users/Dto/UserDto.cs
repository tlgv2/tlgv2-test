using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using TownSend.Authorization.Roles;
using TownSend.Authorization.Users;

namespace TownSend.Users.Dto
{
    //[AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        //[Required]
      //  [StringLength(AbpUserBase.MaxUserNameLength)]
        //public string UserName { get; set; }

       // [Required]
       // [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

       // [Required]
      //  [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

     //   [Required]
    ///    [EmailAddress]
      //  [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public string FullName { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public DateTime CreationTime { get; set; }

        public string[] RoleNames { get; set; }
        public int AccessFailedCount { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
        public virtual bool IsLockoutEnabled { get; set; }
        public virtual bool IsEmailConfirmed { get; set; }
        public List<Role> Roles { get; set; }
        public string Rolename { get; set; }

    }
}
