﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Authorization.Users;

namespace TownSend.Users.Dto
{
    //[AutoMapFrom(typeof(User))]
    public class RoleListForUser
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public long UserId { get; set; }
    }
}
