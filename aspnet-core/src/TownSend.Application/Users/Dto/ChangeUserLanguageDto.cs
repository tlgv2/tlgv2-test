using System.ComponentModel.DataAnnotations;

namespace TownSend.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}