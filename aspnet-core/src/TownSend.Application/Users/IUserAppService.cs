using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TownSend.Roles.Dto;
using TownSend.Users.Dto;

namespace TownSend.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);
        Task<ListResultDto<UserDto>> GetAllDeletedData(int[] userType,UserStatusListDto[] status);
        //Task UnDelete(UserDto input);
        Task UnDelete(EntityDto<long> input);
        //Task<UserDto> GetAsync(int id);
    }
}
