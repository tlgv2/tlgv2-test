﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.ExportToFile
{
    public interface IAppFolders
    {
        string TempFileDownloadFolder { get; }

        string SampleProfileImagesFolder { get; }

        string WebLogsFolder { get; set; }
    }
}
