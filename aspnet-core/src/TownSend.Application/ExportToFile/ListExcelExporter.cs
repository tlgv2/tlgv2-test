﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Common;
using TownSend.Common.Dto;
using TownSend.ExportToFile.Dto;
using TownSend.Teams.Dto;

namespace TownSend.ExportToFile
{
    public class ListExcelExporter : EpPlusExcelExporterBase, IListExcelExporter
    {
        public async Task<FileDto> TeamListExportToFile(IReadOnlyList<TeamListDto> teamList,string fileName)
        {
            
            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add("Teams");
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        "Team Name",
                        "Partners",
                        "Team Price",
                        "Deposit",
                        "Start Date",
                        "End Date",
                        "Creation Time"
                        );

                    AddObjects(
                        sheet, 2, teamList,
                        _ => _.TeamName,
                        _=> string.Join(", ",_.Partners),
                         _ => string.Format("$ {0}",_.TeamPrice),
                          _ => string.Format("$ {0}", _.Deposit),
                        _ => _.StartDate.ToString("MM-dd-yyyy"),
                        _ => _.EndDate.ToString("MM-dd-yyyy"),
                         _ => _.CreationTime.ToString()


                        );

                    var lastLoginTimeColumn = sheet.Column(8);
                    lastLoginTimeColumn.Style.Numberformat.Format = "mm-dd-yyyy";

                    var creationTimeColumn = sheet.Column(10);
                    creationTimeColumn.Style.Numberformat.Format = "mm-dd-yyyy";

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }

                });
        }
    }
}
