﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using TownSend.ExportToFile.Dto;
using TownSend.Teams.Dto;

namespace TownSend.ExportToFile
{
    public interface IListExcelExporter
    {
        Task<FileDto> TeamListExportToFile(IReadOnlyList<TeamListDto> teamList,string fileName);
    }
}
