﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOneBankAccount : IPaymentType
    {

        public int PaymentMethodID { get; set; }

        public string HubOneCustomerID { get; set; }

        public string CustAPIIdentifer { get; set; }

        public string AccountApiIdentifier { get; set; }

        public string AccountHolderName { get; set; } //need

        public string AccountNumber { get; set; } //needed

        public string AccountHolderPhone { get; set; } //needed

        public HubOneBankAccountType BankAccountType { get; set; } //need

        public string RoutingNumber { get; set; }//needed

        public string AccountToken { get; set; }

        public HubOneBankAccount()
        {
        }

        public string PrintBankAccountInformation()
        {
            return string.Concat(
                  "Account Holder Name: ", AccountHolderName, Environment.NewLine
                , "Account Number: ", AccountNumber, Environment.NewLine
                , "Routing Number: ", RoutingNumber, Environment.NewLine
                , "Account Type: ", BankAccountType, Environment.NewLine
                , "Holder Phone: ", AccountHolderPhone, Environment.NewLine

                );

        }



    }
}
