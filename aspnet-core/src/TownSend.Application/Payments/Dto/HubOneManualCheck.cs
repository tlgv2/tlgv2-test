﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOneManualCheck : IPaymentType
    {
        public string HubOneCustomerID { get; set; }

        public string SignatorName { get; set; } //need

        public HubOneManualCheck()
        {
        }

        public string PrintManualCheckInformation()
        {
            return string.Concat(
                  "Signator Name: ", SignatorName, Environment.NewLine
                );
        }
    }
}
