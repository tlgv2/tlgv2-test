﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOneCapture
    {

        public decimal CaptureAmount { get; set; }

        public string Created { get; set; }

        public string Description { get; set; }

        public HubOnePaymentContext PaymentContext { get; set; }



    }
}
