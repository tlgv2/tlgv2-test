﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class PricingResult
    {
        public PricingResult(PaymentTerm paymentTerm)
        {
            Discounts = new DiscountCollection();
            PaymentTerm = paymentTerm;
        }

        public PaymentTerm PaymentTerm { get; set; }

        public decimal LumpSumPercentageOff { get; set; }
        public decimal LumpSumRebate { get; set; }
        public DiscountCollection Discounts { get; set; }
        public decimal PayNow { get; set; }
        public decimal Price { get; set; }
        public decimal SubTotal { get; set; }
        public decimal PriceAfterDeposit { get; set; }
        public decimal InstallmentAmount { get; set; }
        public int InstallmentCount { get; set; }
        public DateTime TeamStartDate { get; set; }
    }

}
