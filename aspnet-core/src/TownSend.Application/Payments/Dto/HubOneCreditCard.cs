﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOneCreditCard : IPaymentType //Payment.PaymentMethod in DB
    {
        /// <summary>
        /// Credit Card Owner API Id
        /// </summary>
        public string CustAPIIdentifer { get; set; }

        public int PaymentMethodID { get; set; }

        public string HubOneCustomerID { get; set; }

        public bool H1Active { get; set; }

        public string CardID { get; set; }

        public string CardToken { get; set; }

        public string CardNumber { get; set; }

        public string ExpirationMonth { get; set; }

        public string ExpirationYear { get; set; }

        public string Cvc { get; set; }

        public string Updated { get; set; }

        public string Created { get; set; }

        public string CvcVerification { get; set; }

        public CardType CreditCardType { get; set; }

        public string NameOnCard { get; set; }

        public bool DefaultCard { get; set; }

        public string CommrcialCardCode { get; set; }

        private HubOneAddress Address { set; get; }

        public bool IsBusiness { get; set; }

        public bool IsOwner { get; set; }

        public HubOneCreditCard()
        {
        }

        public void SetCreditCardAddress(HubOneAddress creditCardAddress)
        {
            Address = creditCardAddress;

        }

        public HubOneAddress CreateCreditCardAddress()
        {
            return new HubOneAddress();
        }

        public HubOneAddress GetCreitCardAddressInfo()
        {
            return Address;
        }

    }
}
