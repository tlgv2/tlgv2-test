﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class PaymentMethodInsert
    {
        public int PaymentMethodID { get; set; }
        public string PaymentMethodTypeID { get; set; }
        public string CustomerID { get; set; }
        public string OwnerName { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public string AccountRoutingNumber { get; set; }
        public string Token { get; set; }
        public string ExpirationMonth { get; set; }
        public string ExpirationYear { get; set; }
        public string IsOwner { get; set; }
        public string CallInternalCode { get; set; }
        public string CallRequest { get; set; }
        public string CallResponse { get; set; }
        public string CallException { get; set; }
        public string CallWarning { get; set; }
        public string ExternalCode { get; set; }
        public string CreatedOn { get; set; }

    }
}
