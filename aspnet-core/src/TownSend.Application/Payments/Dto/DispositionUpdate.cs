﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class DispositionUpdate
    {
        public int PaymentID { get; set; }
        public string DispositionName { get; set; }

    }
}
