﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOneECheck : IPaymentAction
    {
        public int HubOnePaymentID { get; set; }

        public int PaymentID { get; set; }

        /// <summary>
        /// In the DB ExternalCode0 in the PaymentDetails Table
        /// This is the ID for the Payment which QB provides us.
        /// </summary>
        public string APIPaymentID { get; set; }

        public int PaymentDetailID { get; set; }

        public string RefundID { get; set; }

        public string PaymentMode { get; set; } = "WEB"; //NEED

        public decimal Amount { get; set; } //NEED

        public decimal RefundAmount { get; set; }

        public string ECheckStatus { get; set; }

        public string CreatedOn { get; set; }

        public string AuthorizationCode { get; set; }

        public string Description { get; set; } //Group info NEEDED TLG

        public string CheckNumber { get; set; }

        public string BankAccount { get; set; }

        public string CheckToken { get; set; }

        public string BankAccountOnFile { get; set; }// bankAccountOnFile 

        public bool IsVoidCheck { get; set; }

        public HubOneECheck()
        {
        }

        public void SetAmountTestStatus(TESTING_AMOUNT testingAmount)
        {
            switch (testingAmount)
            {
                case TESTING_AMOUNT.PENDING:
                    Amount = 1.11M;
                    break;
                case TESTING_AMOUNT.DECLINED:
                    Amount = 3.33M;
                    break;
                case TESTING_AMOUNT.SUCCEEDED:
                    Amount = 5.55M;
                    break;
                default:
                    Amount = 0;
                    break;
            }

        }

        public bool IsValidRefundRequest()
        {
            if (RefundAmount > Amount)
                return false;

            if (RefundAmount == 0.00m)
                return false;

            if (Amount == 0.00m)
                return false;

            return true;

        }




    }
}
