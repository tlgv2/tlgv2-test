﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOneCharge : IPaymentAction
    {
        /// <summary>
        /// ExternalCode in the PaymentDetails table
        /// </summary>
        public string APIPaymentID { get; set; }

        /// <summary>
        /// Value returned by the API for Refunds
        /// </summary>
        public string RefundID { get; set; }

        public int PaymentID { get; set; }

        public int PaymentDetailID { get; set; }

        public string InternalCallCode { get; set; }

        public string Currency { get; set; }

        public decimal Amount { get; set; }

        public HubOnePaymentContext PaymentContext { get; set; }

        public string Created { get; set; }

        public string AuthorizationCode { get; set; }

        public HubOneCapture CaptureDetail { get; set; }

        public string Description { get; set; }

        public string Token { get; set; }

        public decimal RefundAmount { get; set; }

        public HubOneCharge()
        {
        }

    }
}
