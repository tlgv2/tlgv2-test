﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class MemberPayment
    {
        public int PaymentID { get; set; }
        public int InvoiceID { get; set; }
        public int ContractInfoID { get; set; }
        public int CustomerID { get; set; }
        public decimal PaymentTotal { get; set; }
        public string CardExternalID { get; set; }
        public string Currency { get; set; }
        public string TeamName { get; set; }
    }
}
