﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOneAddress
    {
        public string City { get; set; }

        public string StreetAddress { get; set; }

        public string Country { get; set; }

        public string PostalCode { get; set; }

        public string Region { get; set; }

        public HubOneAddress()
        {
        }

    }
}
