﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class PaymentUpdate
    {
        public int PaymentID { get; set; }
        public int CustomerID { get; set; }
        public int ProductID { get; set; }
        public int InvoiceID { get; set; }
        public int CreditID { get; set; }
        public int PaymentMethodID { get; set; }
        public string PaymentDate { get; set; }
        public decimal PaymentTotal { get; set; }
        public decimal ReferralTotal { get; set; }
        public decimal CompensationTotal { get; set; }
        public string CompletionDate { get; set; }
        public bool FlagReady { get; set; }
        public bool? FlagSuccess { get; set; }
        public bool FlagCompleted { get; set; }
        public bool? FlagValid { get; set; }
        public int ActionLevel { get; set; }
        public string CreationDate { get; set; }
        public int CreationUserID { get; set; }
        public string ExternalCode { get; set; }
        public int PaymentDetailID { get; set; }
        public int RecordID { get; set; }
        public bool H1Active { get; set; }
        public bool? LegacyFieldFromHere { get; set; }
        public bool ReadyProcess { get; set; }
        public bool? CompletionSuccess { get; set; }

    }
}
