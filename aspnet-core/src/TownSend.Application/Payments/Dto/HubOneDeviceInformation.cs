﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOneDeviceInformation
    {

        public string DeviceID { get; set; }

        public string MacAddress { get; set; }

        public bool Encrypted { get; set; }

        public string IPAddress { get; set; }

        public string Longitude { get; set; }

        public string PhoneNumber { get; set; }

        public string Latitude { get; set; }

        public string TypeOfDevice { get; set; }


    }
}
