﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class HubOnePaymentContext
    {
        public bool Mobile { get; set; }

        public bool IsEcomm { get; set; }

        public HubOneDeviceInformation DeviceInfo { get; set; }

        public decimal Tax { get; set; }

        public bool Recurring { get; set; }

    }
}
