﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class MemberInvite
    {
        public int InviteID { get; set; }
        public int SupplierID { get; set; }
        public int ProductID { get; set; }
        //public UserType UserType { get; set; }
        public int ContactID { get; set; }
        public string InviteToken { get; set; }
        public int CreationUserID { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int RecordID { get; set; }
        public string AgreementConfirmCode { get; set; }
        public decimal? InviteDiscount { get; set; }
        public int? ScholarshipID { get; set; }
    }
}
