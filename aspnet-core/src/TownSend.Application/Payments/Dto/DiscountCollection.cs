﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class DiscountCollection
    {
        private Dictionary<DiscountType, decimal> discountDictionary = new Dictionary<DiscountType, decimal>();

        public IEnumerator<KeyValuePair<DiscountType, decimal>> GetEnumerator()
        {
            return discountDictionary.GetEnumerator();
        }

        public void AddDiscount(DiscountType type, decimal amount)
        {
            if (amount != 0.0m)
                discountDictionary.Add(type, amount);
        }

        public bool Any
        {
            get
            {
                return discountDictionary.Any();
            }
        }

        public decimal Get(DiscountType type)
        {
            if (discountDictionary.ContainsKey(type))
            {
                return discountDictionary[type];
            }
            else
            {
                return 0.0m;
            }

        }

        public bool Contains(DiscountType type)
        {
            return discountDictionary.ContainsKey(type);
        }

        public void RemoveDiscount(DiscountType type)
        {
            discountDictionary.Remove(type);
        }

        public decimal Total
        {
            get
            {
                return discountDictionary.Sum(x => x.Value);
            }
        }
    }
}
