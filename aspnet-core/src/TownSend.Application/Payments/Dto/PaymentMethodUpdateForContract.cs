﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Payments.Dto
{
    public class PaymentMethodUpdateForContract
    {
        public int PaymentMethodID { get; set; }
        public int CurrentPaymentID { get; set; }
        public int ContractInfoID { get; set; }
    }
}
