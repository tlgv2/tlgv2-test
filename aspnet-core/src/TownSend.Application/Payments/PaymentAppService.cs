﻿using Intuit.Ipp.OAuth2PlatformClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TownSend.Members;
using TownSend.Payments.Dto;
using TownSend.Payments.Quickbooks;

namespace TownSend.Payments
{
    public class PaymentAppService: TownSendAppServiceBase, IPaymentAppService
    {
        #region Fields
        private string _clientId = TownSendConsts.PaymentConsts.QuickBooksDetails.ClientId;
        private string _clientSecret = TownSendConsts.PaymentConsts.QuickBooksDetails.ClientSecret;
        private string _realmId = TownSendConsts.PaymentConsts.QuickBooksDetails.CompanyId;
        private string _paymentBaseURL = TownSendConsts.PaymentConsts.QuickBooksURLs.PaymentBaseSandboxURL;
        private string _accountingBaseURL = TownSendConsts.PaymentConsts.QuickBooksURLs.AccountingBaseSandboxURL;

        

        #endregion

        #region Ctor
        public PaymentAppService()
        {
            
        }

        #endregion

        #region Methods || QuickBooks

        public string GetAccessToken()
        {
            return GetAccessTokenFromAPI();            
        }

        #region Customers
        public Result CreateCustomerInQuickbooks()
        {
            var result = new Result();
            var customer = GenerateCustomerFromContact();
            var response = TryCreateCustomerOrRelinkCustomerToQuickBooksID(customer);
            result.IsSuccess = response.IsSuccessful;

            if (!response.IsSuccessful)
            {
                var errorId = Guid.NewGuid();
                var messagePrefix = $"Could not create a billing account. Error details: {errorId}";
                var responseStackTrace = response.ToString(true, messagePrefix);

                //Logger.LogError(new Exception($"An unexpector error occured while creating a customer in QuickBooks for CustomerID [{memberID}]."), string.Concat(messagePrefix, " - ", responseStackTrace));

                result.Message = messagePrefix;
            }

            return result;
        }

        public HubOnePaymentModuleResponse CheckCustomerIsUnique(HubOneCustomer hubeOneCustomer)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (hubeOneCustomer == null)
                throw new ArgumentNullException("QB22 : CheckCustomerIsUnique -> customer null");

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_accountingBaseURL}/v3/company/{_realmId}/query?query=SELECT PrimaryEmailAddr FROM Customer WHERE PrimaryEmailAddr = '{hubeOneCustomer.PrimaryEmailAddress}'";

            HttpResponseMessage response = BuildAndCallOnGet(callInternalCode, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawRequest", endpointURI);
            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {

                var fieldValues = BindGetCustomerQueryResponseValue(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Customer Uniqueness Information",
                    IsSuccessful = true,
                    ResponseValueType = "Customer Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();

            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }

        public HubOnePaymentModuleResponse CreateCustomer(HubOneCustomer hubeOneCustomer)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (hubeOneCustomer == null)
                throw new ArgumentNullException("QB23 : Create Customer -> customer is null");

            var customerInfo = BindCreateCustomerCall(hubeOneCustomer);

            apiData.Add("RawRequest", customerInfo.ToString());

            var serializedObj = ConvertObjectTostringContent(customerInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_accountingBaseURL}/v3/company/{_realmId}/customer";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {

                var fieldValues = BindCreateCustomerResponseValues(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Customer Created",
                    IsSuccessful = true,
                    ResponseValueType = "Customer Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();

            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }

        public HubOnePaymentModuleResponse BuildResponse(Dictionary<string, string> responseValue, string message, bool isSuccessful)
        {

            var errorList = BuildFrontFacingErrorList(responseValue);

            return new HubOnePaymentModuleResponse()
            {

                ResponseValues = responseValue,
                ResponseMessage = message,
                IsSuccessful = isSuccessful,
                ErrorList = errorList

            };

        }

        public HubOnePaymentModuleResponse GetCustomerPaymentMethods(HubOneCustomer customer, string paymentMethodType, string cardNumber)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/customers/{customer.APIReferenceID}/cards";
            // /quickbooks/v4/customers/<id>/bank-accounts


            if (!string.IsNullOrWhiteSpace(customer.APIReferenceID))
                apiData.Add("RawRequest", endpointURI);

            HttpResponseMessage response = BuildAndCallOnGet(callInternalCode, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseArrayObject = new JArray();
            var responseObject = new JObject();

            if (!string.IsNullOrWhiteSpace(responseBody.Replace("[]", "")))
                responseArrayObject = JArray.Parse(responseBody);

            foreach (JObject card in responseArrayObject)
            {
                if (card["number"].ToString().Substring(card["number"].ToString().Length - 4) == cardNumber.Substring(cardNumber.Length - 4))
                {
                    responseObject = card;
                }

            }

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                //Dictionary<string, string> fieldValues = null; //BindRefundResponse(responseObject);
                var fieldValues = BindGetCreditCardResponseValues(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"",
                    IsSuccessful = true,
                    ResponseValueType = "",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError

            };
        }


        #endregion

        #region Credit Cards
        public HubOnePaymentModuleResponse AddCreditCard(HubOneCreditCard creditCard)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (creditCard == null)
                throw new ArgumentNullException("QB01 : AddCreditCard -> credit card null");

            var cardInfo = BindAddCreditCardCall(creditCard);

            apiData.Add("RawRequest", LastFourCC(cardInfo.ToString()));

            var serializedObj = ConvertObjectTostringContent(cardInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/customers/{creditCard.CustAPIIdentifer}/cards";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {

                var fieldValues = BindAddCreditCardResponseValues(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Credit Card Created",
                    IsSuccessful = true,
                    ResponseValueType = "Credit Card Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();

            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };

        }

        public HubOnePaymentModuleResponse CreateCreditCardToken(HubOneCreditCard creditCard)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (creditCard == null)
                throw new ArgumentNullException("QB02 : CreateCreditCardtoken-> credit card null");

            var cardInfo = BindCreateCreditCardTokenCall(creditCard);

            apiData.Add("RawRequest", cardInfo.ToString());

            var serializedObj = ConvertObjectTostringContent(cardInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/tokens";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {

                var fieldValues = BindTokenValue(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Credit Card Token Created",
                    IsSuccessful = true,
                    ResponseValueType = "Credit Card Token Value",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();

            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }

        public HubOnePaymentModuleResponse DeleteCreditCard(HubOneCreditCard creditCard)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (creditCard == null)
                throw new ArgumentNullException("QB03 : DeleteCreditCard -> credit card null");

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/customers/{creditCard.CustAPIIdentifer}/cards/{creditCard.CardID}";

            apiData.Add("RawRequest", endpointURI);

            HttpResponseMessage response = BuildAndCallDelete(callInternalCode, accessTokenFromAPI, endpointURI);

            apiData.Add("RawResponse", $"HTTP Code : {((int)response.StatusCode).ToString()}");

            if (response.IsSuccessStatusCode)
            {

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Card Deleted",
                    IsSuccessful = true,
                    ResponseValueType = "HTTP Code Result",
                    ResponseValues = new Dictionary<string, string>()
                    {
                        { "response",$"Request was Successful with code: {(int)response.StatusCode}" }
                    },
                    APIData = apiData

                };
            }

            //The response body and response object are after the successful call.
            //It means that if the call is unsuccessful the API is likely returning an error response.
            //The reason this is unlike the other responses, is that a successful request return no content.
            // so it end up breaking the JObject.Parse.
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var responseObject = JObject.Parse(responseBody);

            response.Dispose();

            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData


            };

        }

        public HubOnePaymentModuleResponse AddCreditCardUsingToken(HubOneCreditCard creditCard)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (creditCard == null)
                throw new ArgumentNullException("QB04 : AddCreditCardUsingToken -> credit card null");

            var cardToken = BindCreateCreditCardToken(creditCard);

            apiData.Add("RawRequest", cardToken.ToString());

            var serializedObj = ConvertObjectTostringContent(cardToken);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/customers/{creditCard.CustAPIIdentifer}/cards/createFromToken";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindAddCreditCardResponseValues(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Credit Card Created",
                    IsSuccessful = true,
                    ResponseValueType = "Credit Card Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();

            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }

        #endregion

        #region Bank Account
        public HubOnePaymentModuleResponse AddBankAccount(HubOneBankAccount bankAccount)
        {

            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (bankAccount == null)
                throw new ArgumentNullException("QB05 : AddBankAccount-> bankAccount null");

            var bankAccountInfo = BindBankAccountCall(bankAccount);

            apiData.Add("RawRequest", BankAccountNumberCensor(bankAccountInfo.ToString()));

            var serializedObj = ConvertObjectTostringContent(bankAccountInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/customers/{bankAccount.CustAPIIdentifer}/bank-accounts";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {

                var fieldValues = BindAddBankAccountResponseValues(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Bank Account Created",
                    IsSuccessful = true,
                    ResponseValueType = "Bank Account Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData
            };
        }

        public HubOnePaymentModuleResponse CreateBankAccountToken(HubOneBankAccount bankAccount)
        {
            var apiData = new Dictionary<string, string>();
            var callInternalCode = Guid.NewGuid().ToString();

            if (bankAccount == null)
                throw new ArgumentNullException("QB06 : CreateBankAccountToken-> bankAccount null");

            var bankAccountInfo = BindCreateBankAccountTokenCall(bankAccount);

            apiData.Add("RawRequest", bankAccountInfo.ToString());

            var serializedObj = ConvertObjectTostringContent(bankAccountInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/tokens";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindTokenValue(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Bank Account Token Created",
                    IsSuccessful = true,
                    ResponseValueType = "Bank Account Token",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }

        public HubOnePaymentModuleResponse AddBankAccountUsingToken(HubOneBankAccount bankAccount)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);


            if (bankAccount == null)
                throw new ArgumentNullException("QB07 : AddBankAccountUsingToken -> credit card null");

            var cardToken = BindHubTokenToQBBankAccountToken(bankAccount);

            apiData.Add("RawRequest", cardToken.ToString());

            var serializedObj = ConvertObjectTostringContent(cardToken);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/customers/{bankAccount.CustAPIIdentifer}/bank-accounts/createFromToken";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {

                var fieldValues = BindAddBankAccountResponseValues(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Bank Account Created",
                    IsSuccessful = true,
                    ResponseValueType = "Bank Account Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };

        }

        public HubOnePaymentModuleResponse DeleteBankAccount(HubOneBankAccount bankAccount)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (bankAccount == null)
                throw new ArgumentNullException("QB08 : DeleteBankAccount-> bankAccount null");

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/customers/{bankAccount.CustAPIIdentifer}/bank-accounts/{bankAccount.AccountApiIdentifier}";

            apiData.Add("RawRequest", endpointURI);

            HttpResponseMessage response = BuildAndCallDelete(callInternalCode, accessTokenFromAPI, endpointURI);

            apiData.Add("RawResponse", $"HTTP Code : {((int)response.StatusCode).ToString()}");

            if (response.IsSuccessStatusCode)
            {

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Bank Account Deleted",
                    IsSuccessful = true,
                    ResponseValueType = "HTTP Code Result",
                    ResponseValues = new Dictionary<string, string>()
                    {
                        { "response",$"Request was Successful with code: {(int)response.StatusCode}" }
                    },
                    APIData = apiData

                };
            }

            //The response body and response object are after the successful call.
            //It means that if the call is unsuccessful the API is likely returning an error response.
            //The reason this is unlike the other responses, is that a successful request return no content.
            // so it end up breaking the JObject.Parse.
            string responseBody = response.Content.ReadAsStringAsync().Result;
            var responseObject = JObject.Parse(responseBody);

            response.Dispose();

            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };


        }
        #endregion

        #region Charges related methods
        public HubOnePaymentModuleResponse ChargeCreditCard(HubOneCreditCard creditCard)//, HubOneCharge charge)
        {
            var  charge = new HubOneCharge();
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);


            if (charge == null)
                throw new ArgumentNullException("QB09 : ChargeCreditCard -> charge is null");

            if (creditCard == null)
                throw new ArgumentNullException("QB10 : ChargeCreditCard -> credit card null");

            //Two Bindings here because we need the CC and the charge itself
            var chargeInfo = BindChargeCall(creditCard, charge);

            if (!string.IsNullOrWhiteSpace(chargeInfo.ToString()))
                apiData.Add("RawRequest", chargeInfo.ToString());

            var serializedObj = ConvertObjectTostringContent(chargeInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/charges";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result; ;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindChargeResponse(responseObject, responseBody); //Worky worky

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Charge Successful",
                    IsSuccessful = true,
                    ResponseValueType = "Charge Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };

        }

        public HubOnePaymentModuleResponse RefundACharge(HubOneCharge hubOneCharge)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (hubOneCharge == null)
                throw new ArgumentNullException("QB11 : RefundACharge -> charge is null");

            //Charge only
            var chargeInfo = BindRefundCall(hubOneCharge);

            if (!string.IsNullOrWhiteSpace(chargeInfo.ToString()))
                apiData.Add("RawRequest", chargeInfo.ToString());

            var serializedObj = ConvertObjectTostringContent(chargeInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/charges/{hubOneCharge.APIPaymentID}/refunds";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindRefundResponse(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Refund Successful",
                    IsSuccessful = true,
                    ResponseValueType = "Refund Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError

            };
        }

        public HubOnePaymentModuleResponse GetCharge(HubOneCharge hubOneCharge)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (string.IsNullOrWhiteSpace(hubOneCharge.APIPaymentID.ToString()))
                throw new ArgumentNullException("QB12 : GetCharge -> Charge ID Missing");


            if (!string.IsNullOrWhiteSpace(hubOneCharge.APIPaymentID.ToString()))
                apiData.Add("RawRequest", ContentBody.NO_REQUEST_BODY.ToString());


            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/charges/{hubOneCharge.APIPaymentID}";

            HttpResponseMessage response = BuildAndCallOnGet(callInternalCode, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindChargeInformation(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Get Charge Successful",
                    IsSuccessful = true,
                    ResponseValueType = "Charge Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError

            };
        }


        public HubOnePaymentModuleResponse GetRefund(HubOneCharge hubOneCharge)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (string.IsNullOrWhiteSpace(hubOneCharge.APIPaymentID.ToString()) && string.IsNullOrWhiteSpace(hubOneCharge.RefundID))
                throw new ArgumentNullException("QB13 : GetRefund -> Charge ID or Refund ID Missing");


            if (!string.IsNullOrWhiteSpace(hubOneCharge.APIPaymentID.ToString()))
                apiData.Add("RawRequest", ContentBody.NO_REQUEST_BODY.ToString());


            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/charges/{hubOneCharge.APIPaymentID}/refunds/{hubOneCharge.RefundID}";

            HttpResponseMessage response = BuildAndCallOnGet(callInternalCode, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindRefundResponse(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Get Refund Successful",
                    IsSuccessful = true,
                    ResponseValueType = "Refund Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError

            };
        }

        public HubOnePaymentModuleResponse CaptureCharge(HubOneCharge hubOneCharge)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (hubOneCharge == null)
                throw new ArgumentNullException("QB14 : CaptureCharge -> charge is null");

            var chargeInfo = BindCaputreCall(hubOneCharge);

            if (!string.IsNullOrWhiteSpace(chargeInfo.ToString()))
                apiData.Add("RawRequest", chargeInfo.ToString());

            var serializedObj = ConvertObjectTostringContent(chargeInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/charges/{hubOneCharge.APIPaymentID}/capture";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindChargeInformation(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Capture Successful",
                    IsSuccessful = true,
                    ResponseValueType = "Capture Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError

            };
        }

        public HubOnePaymentModuleResponse VoidCharge(HubOneCharge hubOneCharge)
        {
            return RefundACharge(hubOneCharge);
        }
        #endregion

        #region ECheck related Methods

        public HubOnePaymentModuleResponse CreateDebit(HubOneBankAccount bankAccount)//, HubOneECheck echeck)
        {
            HubOneECheck echeck = new HubOneECheck();
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);


            if (echeck == null)
                throw new ArgumentNullException("QB15 : CreateDebit -> echeck is null");

            if (bankAccount == null)
                throw new ArgumentNullException("QB16 : CreateDebit -> bank account is null");


            var debitInfo = BindECheckCall(bankAccount, echeck);

            if (!string.IsNullOrWhiteSpace(debitInfo.ToString()))
                apiData.Add("RawRequest", debitInfo.ToString());

            var serializedObj = ConvertObjectTostringContent(debitInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/echecks";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindECheckDebitResponse(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Debit Successful",
                    IsSuccessful = true,
                    ResponseValueType = "Debit Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }

        public HubOnePaymentModuleResponse GetECheckRefund(HubOneECheck echeck)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);


            if (echeck == null)
                throw new ArgumentNullException("QB17 : echeck -> echeck is null");

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/echecks/{echeck.APIPaymentID}refunds/{echeck.RefundID}";

            apiData.Add("RawRequest", endpointURI);

            HttpResponseMessage response = BuildAndCallOnGet(callInternalCode, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindECheckDebitResponse(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Get ECheck Successful",
                    IsSuccessful = true,
                    ResponseValueType = "ECheck Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }

        public HubOnePaymentModuleResponse GetECheck(HubOneECheck echeck)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);


            if (echeck == null)
                throw new ArgumentNullException("QB17 : echeck -> echeck is null");

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/echecks/{echeck.APIPaymentID}";

            apiData.Add("RawRequest", endpointURI);

            HttpResponseMessage response = BuildAndCallOnGet(callInternalCode, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindECheckDebitResponse(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Get Check Successful",
                    IsSuccessful = true,
                    ResponseValueType = "Get Check Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }

        public HubOnePaymentModuleResponse VoidECheck(HubOneECheck echeck)
        {
            //Quickbook bundles the void and refund. They use the same information and endpoint
            return RefundECheck(echeck);
        }


        /// <summary>
        /// Full or partial refund an existing ECheck transaction. 
        /// Refund requests made on the same day as the associated debit request may result in the transaction being voided.
        /// Refunds cannot be issued unless the original debit has succeeded (this process typically takes approximately three business days).
        /// </summary>
        /// <param name="echeck"></param>
        /// <returns></returns>
        public HubOnePaymentModuleResponse RefundECheck(HubOneECheck echeck)
        {
            var apiData = new Dictionary<string, string>();

            var callInternalCode = Guid.NewGuid().ToString();
            apiData.Add("InternalCode", callInternalCode);

            if (echeck == null)
                throw new ArgumentNullException("QB21 : RefundECheck -> echeck is null");

            var debitInfo = BindECheckVoidOrRefund(echeck);

            if (!string.IsNullOrWhiteSpace(debitInfo.ToString()))
                apiData.Add("RawRequest", debitInfo.ToString());

            var serializedObj = ConvertObjectTostringContent(debitInfo);

            var accessTokenFromAPI = GetAccessTokenFromAPI();

            string endpointURI = $"{_paymentBaseURL}/quickbooks/v4/payments/echecks/{echeck.APIPaymentID}/refunds";

            HttpResponseMessage response = BuildAndCallOnPost(callInternalCode, serializedObj, accessTokenFromAPI, endpointURI);

            string responseBody = response.Content.ReadAsStringAsync().Result;

            var responseObject = JObject.Parse(responseBody);

            apiData.Add("RawResponse", responseObject.ToString());

            if (response.IsSuccessStatusCode)
            {
                var fieldValues = BindRefundResponse(responseObject, responseBody);

                return new HubOnePaymentModuleResponse()
                {

                    CallInternalCode = callInternalCode,
                    ResponseMessage = $"Refund Successful",
                    IsSuccessful = true,
                    ResponseValueType = "Refund Information",
                    ResponseValues = fieldValues,
                    APIData = apiData

                };
            }

            response.Dispose();


            QuickBooksErrorRoot qbError = JsonConvert.DeserializeObject<QuickBooksErrorRoot>(responseBody);

            var apiResponseError = ErrorBuilder(qbError);

            apiData.Add("ErrorMessage", "API HTTP Status Code Unsuccessful");

            //Return unsuccessful response
            return new HubOnePaymentModuleResponse()
            {

                CallInternalCode = callInternalCode,
                ResponseMessage = "API HTTP Status Code Unsuccessful",
                IsSuccessful = false,
                ResponseValueType = "Error List",
                ResponseValues = apiResponseError,
                APIData = apiData

            };
        }
        #endregion
        #endregion

        #region Utilities

        /// <summary>
        /// Get Access token for API calls
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private string GetAccessTokenFromAPI()
        {
            try
            {
                // Prep for the call
                var oauth2Client = new OAuth2Client(
                          _clientId
                        , _clientSecret
                        , TownSendConsts.PaymentConsts.QuickBooksURLs.RedirectURL//"https://developer.intuit.com/v2/OAuth2Playground/RedirectUrl",
                        , TownSendConsts.PaymentConsts.QuickBooksDetails.Environment//"sandbox"// environment is “sandbox” or “production”
                        );

                string newRefreshToken;
                string newAccessToken = "";

                string refreshToken = null;

                try
                {
                    refreshToken = TownSendConsts.PaymentConsts.QbiBooksTokens.RefreshToken;//GetRefreshTokenFromDB();
                                                                                        //LogInformation("[QuickBooks] - Successfully got tokens from DB");
                                                                                        //LogInformation(string.Concat("[QuickBooks] - Bind Refresh Value:", refreshToken));
                }
                catch (Exception e)
                {

                    //LogInformation(string.Concat("[QuickBooks] - Unable to Get tokens from DB - Exception: ", e.Message));

                }

                //// retry logic
                //LogInformation(string.Concat("[QuickBooks] - Entering Retry Logic"));
                int count = 1;
                bool success = false;
                string tokenValidationResponse = "";

                while (!success & count < 4)
                {
                    //// Refresh the token(s)
                    //LogInformation(string.Concat("[QuickBooks] - Calling RefreshTokenAsync with refresh token: ", refreshToken));
                    var tokenResp = oauth2Client.RefreshTokenAsync(refreshToken);

                    tokenResp.Wait();

                    // The data should contain out new token(s)
                    var data = tokenResp.Result;
                    //LogInformation(string.Concat("[QuickBooks] - Data from RefreshTokenAsync - Refresh Token: ", data.RefreshToken));
                    //LogInformation(string.Concat("[QuickBooks] - Data from RefreshTokenAsync - Access Token: ", data.AccessToken));
                    //LogInformation(string.Concat("[QuickBooks] - Data from RefreshTokenAsync - Error message(if any): ", data.Error));

                    //Refresh Token
                    //LogInformation(string.Concat("[QuickBooks] - Update Refresh Token to:", data.RefreshToken));
                    //RenewRefreshToken(data.RefreshToken);

                    //LogInformation(string.Concat("[QuickBooks] - Starting Attempt: ", count));
                    //LogInformation(string.Concat("[QuickBooks] - Calling TokenResponseVerification"));

                    //tokenValidationResponse = TokenResponseVerification(data.Error, data.RefreshToken, data.AccessToken);

                    if (tokenValidationResponse.Equals("invalid_token"))
                    {
                        //LogInformation(string.Concat("[QuickBooks] - Token Invalid."));

                        count++;
                        continue;
                    }

                    //LogInformation(string.Concat("[QuickBooks] - Token Successfully Acquired: ", "Count: ", count, "- Success: ", success));
                    newAccessToken = data.AccessToken;
                    newRefreshToken = data.RefreshToken;
                    success = true;

                }

                //If the three tries has failed, return invalid token, no need to go forward.
                if (!success & count == 4)
                {
                    //LogInformation(string.Concat("[QuickBooks] - END GetAccessTokenFromAPI()"));
                    //LogTokenResult(false);
                    return "invalid_token";

                }




                //LogInformation(string.Concat("[QuickBooks] - Process complete. Returning Access Token: ", newAccessToken));
                //LogInformation(string.Concat("[QuickBooks] - END GetAccessTokenFromAPI()"));

                if (!tokenValidationResponse.Equals("invalid_token"))
                {
                    //LogTokenResult(true);
                    return newAccessToken;
                }



                //LogTokenResult(false);
                return "invalid_token";
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
            
        }

        private HubOneCustomer GenerateCustomerFromContact()
        {
            var customer = new HubOneCustomer();
            //customer.APIReferenceID
            customer.AppartmentOrSuite = "nehru road";
            customer.BillingAddress = "1A, nehru road, city a, ALB, USA, 8754221";
            customer.City = "dlh";
            customer.CivicNumber = "1A";
            customer.CompanyName = "My company";
            customer.Country = "USA";
            customer.CountrySubDivisionCode = "ALB";
            customer.DisplayName = "dev 1, rody | 1";
            customer.FamilyName = "rody";
            customer.FullyQualifiedName = "dev 1 rody";
            customer.GivenName = "dev d";
            customer.HubOneCustomerID = "1";
            customer.MiddleName = "logic";
            customer.Notes = "hey ! this is dev 1";
            customer.PostalCode = "784512";
            customer.PrimaryEmailAddress = "rovas55564@upcmaill.com";// "fadayi3090@upcmaill.com";
            customer.PrimaryPhone = "986532124578";
            customer.StreetName = "1A";
            customer.Suffix = "arj";

            return customer;
        }

        public HubOnePaymentModuleResponse TryCreateCustomerOrRelinkCustomerToQuickBooksID(HubOneCustomer hubOneCustomer)
        {
            //var paymentModule = GetPaymentModule();

            //validate business rules
            var tlgValErrors = TLGInfoValidation(hubOneCustomer);

            if (tlgValErrors.Count == 0)
            {
                //var result = paymentModule.CheckCustomerIsUnique(hubOneCustomer);
                var result = CheckCustomerIsUnique(hubOneCustomer);

                if (result.ResponseValues.Count == 0)
                {
                    Dictionary<string, string> errDict = new Dictionary<string, string>();
                    errDict.Add("", "Error connecting to Quickbooks API. Please try again later.");
                    return BuildResponse(errDict, "Error connecting to Quickbooks API.Please try again later.", false);
                }

                //does customer exists?
                if (!string.IsNullOrEmpty(result.ResponseValues["QueryResponse"].Replace("{}", "")))
                {
                    var response = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result.ResponseValues["QueryResponse"]);
                    if (response.Customer.HasValues)
                    {
                        var qbCustID = response.Customer.First.Id;

                        //create param to re-bind.
                        var transObj = new Dictionary<string, string>();
                        transObj.Add("HubOneCustomerID", hubOneCustomer.HubOneCustomerID);
                        transObj.Add("QBCustID", qbCustID.ToString());

                        ////add missing bind the HubOneCustomerID to the QuickBooks CustomerId
                        //BindQuickbooksIDToHubOneCustomerID(transObj, Data.HubOneAPIName.QuickbooksAccounting);

                        //Bind API and Value data together into a single list
                        var resultInfo = result.GetAPIDataAndResponses();

                        //QBCustID
                        resultInfo.Add("HubOneCustomerID", hubOneCustomer.HubOneCustomerID);

                        resultInfo.Add("APIName", "Quickbooks");
                        resultInfo.Add("APICallDesc", "TryCreateCustomerOrRelinkCustomerToQuickBooksID");
                        resultInfo.Add("APIEndpoint", "test");

                        ////Get id for status change
                        //LogAPI(resultInfo);

                        //return a response of success
                        return BuildResponse(resultInfo, result.ResponseMessage, true);

                    }
                }
                else //doesn't exists.
                {
                    return CreateCustomerInQuickbooks_Another(hubOneCustomer);
                }
            }

            return BuildResponse(tlgValErrors, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);
        }

        
        private HttpResponseMessage BuildAndCallOnPost(string callInternalCode, StringContent serializedObject, string accessTokenFromAPI, string uri)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessTokenFromAPI}");
            client.DefaultRequestHeaders.Add("Request-Id", callInternalCode);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(uri);
            HttpResponseMessage response = client.PostAsync(uri, serializedObject).Result;
            client.Dispose();
            return response;
        }

        private HttpResponseMessage BuildAndCallOnGet(string callInternalCode, string accessTokenFromAPI, string uri)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessTokenFromAPI}");
            client.DefaultRequestHeaders.Add("Request-Id", callInternalCode);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.BaseAddress = new Uri(uri);
            HttpResponseMessage response = client.GetAsync(uri).Result;
            client.Dispose();
            return response;
        }

        

        /// <summary>
        /// This information MUST be present for TLG
        /// <para>- Address</para>
        /// <para>- Phone </para>
        /// <para>- First Name, LastName</para>
        /// <para>- Email</para>
        /// </summary>
        /// <param name="huboneCustomer"></param>
        /// <returns></returns>
        private Dictionary<string, string> TLGInfoValidation(HubOneCustomer huboneCustomer)
        {
            //** Townsend Specific **//
            var failDic = new Dictionary<string, string>();


            //Location info
            if (string.IsNullOrEmpty(huboneCustomer.City))
                failDic.Add(nameof(huboneCustomer.City), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.CivicNumber))
                failDic.Add(nameof(huboneCustomer.CivicNumber), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.StreetName))
                failDic.Add(nameof(huboneCustomer.StreetName), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.Country))
                failDic.Add(nameof(huboneCustomer.Country), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.PostalCode))
                failDic.Add(nameof(huboneCustomer.PostalCode), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.CountrySubDivisionCode))
                failDic.Add(nameof(huboneCustomer.CountrySubDivisionCode), ValidatorValues.IsEmpty.ToString());

            //Communication info
            if (string.IsNullOrEmpty(huboneCustomer.PrimaryEmailAddress))
                failDic.Add(nameof(huboneCustomer.PrimaryEmailAddress), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.PrimaryPhone))
                failDic.Add(nameof(huboneCustomer.PrimaryPhone), ValidatorValues.IsEmpty.ToString());

            //Personal Info
            if (string.IsNullOrEmpty(huboneCustomer.FamilyName))
                failDic.Add(nameof(huboneCustomer.FamilyName), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.GivenName))
                failDic.Add(nameof(huboneCustomer.GivenName), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.HubOneCustomerID))
                failDic.Add(nameof(huboneCustomer.HubOneCustomerID), ValidatorValues.IsEmpty.ToString());


            return failDic;


        }

        private Dictionary<string, string> BindGetCustomerQueryResponseValue(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("QueryResponse", responseObject["QueryResponse"].ToString());
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private Dictionary<string, string> ErrorBuilder(QuickBooksErrorRoot errorsFromApi)
        {
            //API Error Codes
            //https://developer.intuit.com/app/developer/qbpayments/docs/develop/explore-the-quickbooks-payments-api/error-codes

            //Error needs a revisit

            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            if (errorsFromApi.errors != null)
            {
                foreach (var error in errorsFromApi.errors)
                {

                    responseValues.Add($"ErrorDetail", $"{error.code}:{error.type} - {error.message} - {error.moreInfo}");

                }
            }



            return responseValues;

        }

        
        /// <summary>
        /// This is not a generic process.
        /// The purpose of this method is to create a customer in the 
        /// QB API and apply business and API logic to the HubOneCustomer
        /// onject to make sure it's "clean" for the push to the API.
        /// There are several disqualifying factors for the customer
        /// in terms of info, they're outlined in the validating summaries
        /// in this method.
        /// </summary>
        /// <returns></returns>
        public HubOnePaymentModuleResponse CreateCustomerInQuickbooks_Another(HubOneCustomer hubOneCustomer)
        {
            //validate business rules
            var tlgValErrors = TLGInfoValidation(hubOneCustomer);

            //check if the customer exists
            var notADupe = CheckIfCustomerEmailAlreadyInSystem(hubOneCustomer);

            //bring both dictionaries together
            var allErrors = notADupe.ResponseValues.Union(tlgValErrors).ToDictionary(d => d.Key, d => d.Value);

            //if the customer doesn't exist and the rules are followed
            if (allErrors.Count == 0)
            {

                //then create the customer
                var result = CallAPIToCreateCustomer(hubOneCustomer);

                //Bind API and Value data together into a single list
                var resultInfo = result.GetAPIDataAndResponses();

                //QBCustID
                resultInfo.Add("HubOneCustomerID", hubOneCustomer.HubOneCustomerID);

                resultInfo.Add("APIName", "Quickbooks");
                resultInfo.Add("APICallDesc", "CreateCustomer");
                resultInfo.Add("APIEndpoint", "test");

                ////Get id for status change
                //LogAPI(resultInfo);

                ////when creation is complete, bind the HubOneCustomerID to the QuickBooks CustomerId
                //BindQuickbooksIDToHubOneCustomerID(resultInfo, Data.HubOneAPIName.QuickbooksAccounting);

                //return a response of success
                return BuildResponse(resultInfo, result.ResponseMessage, true);

            }
            else
            {
                return BuildResponse(allErrors, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);//or resturn a fail response
            }

        }

        /// <summary>
        /// This isn't just an api call.
        /// This method also contains sub-logic verification to confirm the results
        /// </summary>
        /// <param name="hubOneCustomer"></param>
        /// <returns></returns>
        public HubOnePaymentModuleResponse CheckIfCustomerEmailAlreadyInSystem(HubOneCustomer hubOneCustomer)
        {
            //var paymentModule = GetPaymentModule();

            ////returns result after call of api
            //var result = paymentModule.CheckCustomerIsUnique(hubOneCustomer);
            var result = CheckCustomerIsUnique(hubOneCustomer);
            //sublogic for dupe check
            if (string.IsNullOrEmpty(result.ResponseValues["QueryResponse"].Replace("{}", "")))
                return new HubOnePaymentModuleResponse()
                {
                    IsSuccessful = true,
                    ResponseMessage = "Your Customer is not a duplicate.",
                    ResponseValues = new Dictionary<string, string>()
                };

            return result;
        }

        private HubOnePaymentModuleResponse CallAPIToCreateCustomer(HubOneCustomer hubOneCustomer)
        {
            //var paymentModule = GetPaymentModule();

            //var response = paymentModule.CreateCustomer(hubOneCustomer);
            var response = CreateCustomer(hubOneCustomer);
            return response;

        }

        private JObject BindCreateCustomerCall(HubOneCustomer huboneCustomer)
        {

            var customerInformation = new JObject()
            {

                {"FullyQualifiedName", huboneCustomer.FullyQualifiedName },
                {"PrimaryEmailAddr",new JObject()
                    {
                        {"Address", huboneCustomer.PrimaryEmailAddress }
                    }
                },
                {"DisplayName", huboneCustomer.DisplayName },
                {"Suffix", huboneCustomer.Suffix??""},
                {"Title", huboneCustomer.Title??""},
                {"MiddleName", huboneCustomer.MiddleName??""},
                {"Notes", string.Concat("HubOneCustomerID - ",huboneCustomer.HubOneCustomerID)},
                {"FamilyName", huboneCustomer.FamilyName},
                {"PrimaryPhone", new JObject()
                    {
                        { "FreeFormNumber", huboneCustomer.PrimaryPhone}
                    }

                },
                { "CompanyName", huboneCustomer.CompanyName??"" },
                { "BillAddr", new JObject()
                    {
                        {"CountrySubDivisionCode", huboneCustomer.CountrySubDivisionCode },
                        {"City", huboneCustomer.City },
                        {"PostalCode", huboneCustomer.PostalCode },
                        {"Line1", string.Concat( huboneCustomer.CivicNumber," ",huboneCustomer.StreetName,",",huboneCustomer.AppartmentOrSuite) },
                        {"Country", huboneCustomer.Country }

                    }
                },
                {  "GivenName", huboneCustomer.GivenName }

            };

            return customerInformation;

        }

        private StringContent ConvertObjectTostringContent<T>(T entity)
        {
            string json = JsonConvert.SerializeObject(entity, Formatting.None);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            return content;
        }

        private Dictionary<string, string> BindCreateCustomerResponseValues(JObject responseObject, string responseBody)
        {

            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                //Appi in use
                responseValues.Add("Domain", responseObject["Customer"]["domain"].ToString());

                /*Customer Information*/
                responseValues.Add("QBCustID", responseObject["Customer"]["Id"].ToString());


                //rVal.Add("FirstName", rObj["Customer"]["GivenName"].ToString());
                //rVal.Add("LastName", rObj["Customer"]["FamilyName"].ToString());
                //rVal.Add("DisplayName", rObj["Customer"]["DisplayName"].ToString());
                //rVal.Add("FullName", rObj["Customer"]["FullyQualifiedName"].ToString());

                //rVal.Add("NameOnCheck", rObj["Customer"]["PrintOnCheckName"].ToString());


                //rVal.Add("EmailAddress", rObj["Customer"]["PrimaryEmailAddr"]["Address"].ToString());
                //rVal.Add("PhoneNumber", rObj["Customer"]["PrimaryPhone"]["FreeFormNumber"].ToString());
                //rVal.Add("DeliveryMethod", rObj["Customer"]["PreferredDeliveryMethod"].ToString());//Preferred delivery method. Values are Print, Email, or None.   

                //rVal.Add("City", rObj["Customer"]["BillAddr"]["City"].ToString());
                //rVal.Add("Country", rObj["Customer"]["BillAddr"]["Country"].ToString());
                //rVal.Add("StreetAddress", rObj["Customer"]["BillAddr"]["Line1"].ToString());
                //rVal.Add("PostalCode", rObj["Customer"]["BillAddr"]["PostalCode"].ToString());
                //rVal.Add("SubDivision", rObj["Customer"]["BillAddr"]["CountrySubDivisionCode"].ToString());


                //rVal.Add("Notes", rObj["Customer"]["Notes"].ToString());
                //rVal.Add("IsActive", rObj["Customer"]["Active"].ToString());


                //rVal.Add("CompanyName", rObj["Customer"]["CompanyName"].ToString());
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        

        // This method is used to build a human-readable list of errors. 
        // The idea is in the front end, we only want to return a select number of errors, we'll catch them here from the API response and make them pretty
        private List<string> BuildFrontFacingErrorList(Dictionary<string, string> responseValue)
        {
            List<string> errorList = new List<string>();

            // Here we can build some rules to add to the error list based on what we find in the response value dictionary
            if (responseValue.ContainsKey("ErrorDetail"))
            {
                if (responseValue["ErrorDetail"].Contains("Card already exists"))
                {
                    errorList.Add("Card already exists.");
                }
                else if (responseValue["ErrorDetail"].Contains("Bank account already exists"))
                {
                    errorList.Add("Bank account already exists.");
                }
                else if (responseValue["ErrorDetail"].Contains("American Express cards require 4 digits cvc"))
                {
                    errorList.Add("American Express cards require 4 digits cvc");
                }
                else if (responseValue["ErrorDetail"].Contains("Bank phone is required"))
                {
                    errorList.Add("Phone number is required for bank account.");
                }
                else
                {
                    errorList.Add(responseValue["ErrorDetail"]);
                }
            }

            return errorList;
        }

        private Dictionary<string, string> BindGetCreditCardResponseValues(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("cardId", responseObject["id"].ToString());
                responseValues.Add("cardNumber", responseObject["number"].ToString());
                responseValues.Add("cardholderName", responseObject["name"].ToString());
                responseValues.Add("cardType", responseObject["cardType"].ToString());
                responseValues.Add("expireMonth", responseObject["expMonth"].ToString());
                responseValues.Add("expireYear", responseObject["expYear"].ToString());
            }
            catch (Exception ex)
            {
                //TODO:MANAGELOGGER
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private JObject BindAddCreditCardCall(HubOneCreditCard hubOneCreditCard)
        {
            var addressInformation = hubOneCreditCard.GetCreitCardAddressInfo();

            var ccInfo = new JObject()
            {
                 { "expMonth"               , hubOneCreditCard.ExpirationMonth }
               , { "number"                 , hubOneCreditCard.CardNumber }
               , { "name"                   , hubOneCreditCard.NameOnCard }
               , { "expYear"                , hubOneCreditCard.ExpirationYear }
               , { "cvc"                    , hubOneCreditCard.Cvc }

            };

            return ccInfo;
        }

        private string LastFourCC(string CCraw)
        {
            //Get the index of where the account number is.
            int indexOfString = CCraw.IndexOf("number");

            //get a substring of "number" and the length of a CC number
            var ccsubstr = CCraw.Substring(indexOfString - 1, 35);

            //turn the string into a char array
            var substrArr = ccsubstr.ToCharArray();

            //get a list of the values
            var cenList = new List<char>();

            //prep string builder for censoring
            var sbuilder = new StringBuilder();

            // loop through the substring and pull out just the card number
            for (int i = 0; i < substrArr.Length; i++)
            {

                int parsable = 0;

                bool isNumber = int.TryParse(substrArr[i].ToString(), out parsable);

                //if the char parsable to a int, then append it to the sbuilder for just the CC number
                if (isNumber)
                {
                    sbuilder.Append(substrArr[i].ToString());
                }

            }

            //get indexes in the sbuilder to pull out the last 4 values
            int lengthMinusFour = sbuilder.Length - 4;
            int lengthOfValue = 4;

            //extract the last four values
            var lastFour = sbuilder.ToString().Substring(lengthMinusFour, lengthOfValue);

            //Create the replacesment value
            var relplacement = string.Concat("XXXX-XXXX-XXXX-", lastFour);

            var censoredRaw = CCraw.Replace(sbuilder.ToString(), relplacement);

            CCraw = null;

            return censoredRaw;

        }

        private Dictionary<string, string> BindAddCreditCardResponseValues(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("cardId", responseObject["id"].ToString());
                responseValues.Add("cardNumber", responseObject["number"].ToString());
                responseValues.Add("cardholderName", responseObject["name"].ToString());
                responseValues.Add("cardType", responseObject["cardType"].ToString());
                responseValues.Add("expireMonth", responseObject["expMonth"].ToString());
                responseValues.Add("expireYear", responseObject["expYear"].ToString());
            }
            catch (Exception ex)
            {
                //TODO:LOGGER
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private JObject BindCreateCreditCardTokenCall(HubOneCreditCard hubOneCreditCard)
        {
            var addressInformation = hubOneCreditCard.GetCreitCardAddressInfo();

            var ccTokenInfo = new JObject()
            {
                { "card", new JObject() {
                        { "name"                , hubOneCreditCard.NameOnCard },
                        { "number"              , hubOneCreditCard.CardNumber },
                        { "expMonth"            , hubOneCreditCard.ExpirationMonth },
                        { "address"             , new JObject()
                          {
                            { "postalCode"      , addressInformation.PostalCode },
                            { "country"         , addressInformation.Country },
                            { "region"          , addressInformation.Region },
                            { "streetAddress"   , addressInformation.StreetAddress },
                            { "city"            , addressInformation.City }
                          }
                        },
                        { "expYear"             , hubOneCreditCard.ExpirationYear },
                        { "cvc"                 , hubOneCreditCard.Cvc }
                    }
                }
            };

            return ccTokenInfo;

        }

        private Dictionary<string, string> BindTokenValue(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("token", responseObject["value"].ToString());
            }
            catch (Exception ex)
            {
                //TODO:LOGGER
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private HttpResponseMessage BuildAndCallDelete(string callInternalCode, string accessTokenFromAPI, string uri)
        {

            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {accessTokenFromAPI}");
            client.DefaultRequestHeaders.Add("Request-Id", callInternalCode);
            client.BaseAddress = new Uri(uri);
            HttpResponseMessage response = client.DeleteAsync(uri).Result;

            client.Dispose();

            return response;
        }

        private JObject BindCreateCreditCardToken(HubOneCreditCard hubOneCreditCard)
        {

            var cctoken = new JObject()
            {
                 { "value" , hubOneCreditCard.CardToken }
            };

            return cctoken;

        }
        private JObject BindBankAccountCall(HubOneBankAccount bankAccount)
        {

            var bankAccountInfo = new JObject()
            {
                   { "phone"            , bankAccount.AccountHolderPhone}
                 , { "routingNumber"    , bankAccount.RoutingNumber}
                 , { "name"             , bankAccount.AccountHolderName}
                 , { "accountType"      , bankAccount.BankAccountType.ToString()}
                 , { "accountNumber"    , bankAccount.AccountNumber}

            };

            return bankAccountInfo;

        }

        private string BankAccountNumberCensor(string CCraw)
        {
            //Get the index of where the account number is.
            int indexOfRoutingNumber = CCraw.IndexOf("routingNumber");
            int indexOfAccountNumber = CCraw.IndexOf("accountNumber");

            //get a substring of "number" and the length of a CC number
            var rtSub = CCraw.Substring(indexOfRoutingNumber - 1, 28);
            var actSub = CCraw.Substring(indexOfAccountNumber - 1, 30);

            //turn the string into a char array
            var routeSubStrr = rtSub.ToCharArray();
            var acctSubStr = actSub.ToCharArray();

            //prep string builder for censoring
            var rtBldStr = new StringBuilder();
            var acBldStr = new StringBuilder();

            // loop through the substring and pull out just the card number
            for (int i = 0; i < routeSubStrr.Length; i++)
            {
                int parsable = 0;

                bool isNumber = int.TryParse(routeSubStrr[i].ToString(), out parsable);

                //if the char parsable to a int, then append it to the sbuilder for just the CC number
                if (isNumber)
                {
                    rtBldStr.Append(routeSubStrr[i].ToString());
                }

            }

            for (int i = 0; i < acctSubStr.Length; i++)
            {
                int parsable = 0;

                bool isNumber = int.TryParse(acctSubStr[i].ToString(), out parsable);

                //if the char parsable to a int, then append it to the sbuilder for just the CC number
                if (isNumber)
                {
                    acBldStr.Append(acctSubStr[i].ToString());
                }

            }


            //get indexes in the sbuilder to pull out the last 4 values
            int lengthMinusFour = acBldStr.Length - 4;
            int lengthOfValue = 4;

            int lengthMinusThree = rtBldStr.Length - 3;
            int lengthOfVal = 3;

            //extract the last four values
            var lastFour = acBldStr.ToString().Substring(lengthMinusFour, lengthOfValue);
            var lastthree = rtBldStr.ToString().Substring(lengthMinusThree, lengthOfVal);


            //Create the replacesment value
            var replacementForAccount = string.Concat("XXXXXXXXXXXXXXXX", lastFour);
            var replacementForRoute = string.Concat("XXXXXXXXX", lastthree);

            var censoredRaw = CCraw.Replace(acBldStr.ToString(), replacementForAccount).Replace(rtBldStr.ToString(), replacementForRoute);

            CCraw = null;

            return censoredRaw;

        }

        private Dictionary<string, string> BindAddBankAccountResponseValues(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("bankAccountId", responseObject["id"].ToString());
                responseValues.Add("bankAccountNumber", responseObject["accountNumber"].ToString());
                responseValues.Add("routingNumber", responseObject["routingNumber"].ToString());
                responseValues.Add("cardholderName", responseObject["name"].ToString());
                responseValues.Add("accountType", responseObject["accountType"].ToString());
            }
            catch (Exception ex)
            {
                //TODO:DBCALL
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private JObject BindCreateBankAccountTokenCall(HubOneBankAccount bankAccount)
        {

            var bankAccountInfo = new JObject()
            {
                {"bankAccount", new JObject()
                    {
                       { "phone"            , bankAccount.AccountHolderPhone}
                     , { "routingNumber"    , bankAccount.RoutingNumber}
                     , { "name"             , bankAccount.AccountHolderName}
                     , { "accountType"      , bankAccount.BankAccountType.ToString()}
                     , { "accountNumber"    , bankAccount.AccountNumber}

                    }
                }
            };

            return bankAccountInfo;

        }

        private JObject BindHubTokenToQBBankAccountToken(HubOneBankAccount bankAccount)
        {

            var cctoken = new JObject()
            {
                 { "value" , bankAccount.AccountToken }
            };

            return cctoken;

        }

        private JObject BindChargeCall(HubOneCreditCard hubOneCreditCard, HubOneCharge hubOneCharge)
        {

            var chargeObject = new JObject()
            {
                { "currency"                , hubOneCharge.Currency},
                { "amount"                  , hubOneCharge.Amount.ToString() },
                { "description"                  , hubOneCharge.Description },
                { "cardOnFile"                   , hubOneCreditCard.CardID },
                     { "context"             , new JObject() {
                        { "mobile"          , hubOneCharge.PaymentContext.Mobile.ToString() },
                        { "isEcommerce"     , hubOneCharge.PaymentContext.IsEcomm.ToString() }
                    }
                }
            };

            return chargeObject;

        }

        private Dictionary<string, string> BindChargeResponse(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("created", responseObject["created"].ToString());
                responseValues.Add("status", responseObject["status"].ToString());
                responseValues.Add("amount", responseObject["amount"].ToString());
                responseValues.Add("cc_number", responseObject["card"]["number"].ToString());
                //responseValues.Add("cc_name"    , responseObject["card"]["name"].ToString());
                responseValues.Add("cc_cardtype", responseObject["card"]["cardType"].ToString());
                responseValues.Add("cc_expMonth", responseObject["card"]["expMonth"].ToString());
                responseValues.Add("cc_expYear", responseObject["card"]["expYear"].ToString());
                responseValues.Add("id", responseObject["id"].ToString());
                responseValues.Add("authCode", responseObject["authCode"].ToString());
            }
            catch (Exception ex)
            {
                //TODO:LOGGER
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private JObject BindRefundCall(HubOneCharge hubOneCharge)
        {

            var chargeObject = new JObject()
            {
                { "amount"      , hubOneCharge.RefundAmount},
                { "description" , hubOneCharge.Description },
                { "id"          , hubOneCharge.APIPaymentID },
                { "created"     , DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")  }
            };

            return chargeObject;

        }

        private Dictionary<string, string> BindRefundResponse(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("id", responseObject["id"].ToString());
                responseValues.Add("amount", responseObject["amount"].ToString());
                responseValues.Add("created", responseObject["created"].ToString());
                responseValues.Add("type", responseObject["type"].ToString());
                responseValues.Add("status", responseObject["status"].ToString());
            }
            catch (Exception ex)
            {
                //TODO:LOGGER
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private Dictionary<string, string> BindChargeInformation(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("created", responseObject["created"].ToString());
                responseValues.Add("status", responseObject["status"].ToString());
                responseValues.Add("amount", responseObject["amount"].ToString());
                responseValues.Add("cc_number", responseObject["card"]["number"].ToString());
                responseValues.Add("cc_name", responseObject["card"]["name"].ToString());
                responseValues.Add("cc_cardtype", responseObject["card"]["cardType"].ToString());
                responseValues.Add("cc_expMonth", responseObject["card"]["expMonth"].ToString());
                responseValues.Add("cc_expYear", responseObject["card"]["expYear"].ToString());
                responseValues.Add("id", responseObject["id"].ToString());
                responseValues.Add("authCode", responseObject["authCode"].ToString());
                responseValues.Add("capture", responseObject["capture"].ToString());
            }
            catch (Exception ex)
            {
                //TODO:LOGGER
                //_logger.LogError(ex, $"Un/*e*/xpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private JObject BindCaputreCall(HubOneCharge hubOneCharge)
        {

            var chargeObject = new JObject()
            {
                { "amount"      , hubOneCharge.Amount},

            };

            return chargeObject;

        }

        #endregion

        private JObject BindECheckCall(HubOneBankAccount bankAccount, HubOneECheck eCheck)
        {

            var chargeObject = new JObject()
            {
                { "bankAccountOnFile", bankAccount.AccountApiIdentifier},
                { "description", eCheck.Description},
                { "paymentMode", "WEB" },
                { "amount", eCheck.Amount }
            };

            return chargeObject;

        }

        private Dictionary<string, string> BindECheckDebitResponse(JObject responseObject, string responseBody)
        {
            Dictionary<string, string> responseValues = new Dictionary<string, string>();

            try
            {
                responseValues.Add("created", responseObject["created"].ToString());
                responseValues.Add("status", responseObject["status"].ToString());
                responseValues.Add("amount", responseObject["amount"].ToString());
                responseValues.Add("accountNumber", responseObject["bankAccount"]["accountNumber"].ToString());
                responseValues.Add("routingNumber", responseObject["bankAccount"]["routingNumber"].ToString());
                responseValues.Add("accountType", responseObject["bankAccount"]["accountType"].ToString());
                //responseValues.Add("checkNumber", responseObject["checkNumber"].ToString());
                responseValues.Add("id", responseObject["id"].ToString());
                responseValues.Add("authCode", responseObject["authCode"].ToString());
            }
            catch (Exception ex)
            {
                //TODO:DBCALL
                //_logger.LogError(ex, $"Unexpected exception during response binding. The raw response body was [{responseBody}].");
                throw;
            }

            return responseValues;
        }

        private JObject BindECheckVoidOrRefund(HubOneECheck eCheck)
        {
            if (eCheck.IsVoidCheck)
            {
                return new JObject()
                {
                };
            }

            return new JObject()
            {
                { "amount", eCheck.RefundAmount }
            };

        }
    }
}
