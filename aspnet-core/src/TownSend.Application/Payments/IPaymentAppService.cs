﻿using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Payments.Dto;
using TownSend.Payments.Quickbooks;

namespace TownSend.Payments
{
    public interface IPaymentAppService
    {
        string GetAccessToken();

        Result CreateCustomerInQuickbooks();

        /*Customer Methods*/
        HubOnePaymentModuleResponse CheckCustomerIsUnique(HubOneCustomer hubeOneCustomer);
        HubOnePaymentModuleResponse CreateCustomer(HubOneCustomer hubeOneCustomer);
        HubOnePaymentModuleResponse BuildResponse(Dictionary<string, string> responseValue, string message, bool isSuccessful);
        HubOnePaymentModuleResponse GetCustomerPaymentMethods(HubOneCustomer hubeOneCustomer, string paymentType, string cardNumber);

        /*Credit Card Methods*/
        HubOnePaymentModuleResponse AddCreditCard(HubOneCreditCard creditCard);
        HubOnePaymentModuleResponse AddCreditCardUsingToken(HubOneCreditCard creditCard);
        HubOnePaymentModuleResponse CreateCreditCardToken(HubOneCreditCard creditCard);
        HubOnePaymentModuleResponse DeleteCreditCard(HubOneCreditCard creditCard);

        /*Bank Account Methods*/
        HubOnePaymentModuleResponse AddBankAccount(HubOneBankAccount bankAccount);
        HubOnePaymentModuleResponse CreateBankAccountToken(HubOneBankAccount bankAccount);
        HubOnePaymentModuleResponse AddBankAccountUsingToken(HubOneBankAccount bankAccount);
        HubOnePaymentModuleResponse DeleteBankAccount(HubOneBankAccount bankAccount);

        /*Charge Methods*/
        HubOnePaymentModuleResponse ChargeCreditCard(HubOneCreditCard creditCard);//, HubOneCharge hubOneCharge);
        HubOnePaymentModuleResponse GetRefund(HubOneCharge hubOneCharge);
        HubOnePaymentModuleResponse GetCharge(HubOneCharge hubOneCharge);
        HubOnePaymentModuleResponse RefundACharge(HubOneCharge hubOneCharge);
        HubOnePaymentModuleResponse CaptureCharge(HubOneCharge hubOneCharge);
        HubOnePaymentModuleResponse VoidCharge(HubOneCharge hubOneCharge);

        /*Check Methods*/
        //HubOnePaymentModuleResponse CreateDebit(HubOneBankAccount bankAccount, HubOneECheck echeck);
        HubOnePaymentModuleResponse CreateDebit(HubOneBankAccount bankAccount);//, HubOneECheck echeck);
        HubOnePaymentModuleResponse GetECheckRefund(HubOneECheck echeck);
        HubOnePaymentModuleResponse GetECheck(HubOneECheck echeck);
        HubOnePaymentModuleResponse VoidECheck(HubOneECheck echeck);
        HubOnePaymentModuleResponse RefundECheck(HubOneECheck echeck);
    }
}
