﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Scholarships.Dto;

namespace TownSend.Scholarships
{
    public interface IScholarshipAppService : IApplicationService
    {
        Task<ScholarshipRegisterResult> CreateAsync(ScholarshipRegisterDto input);

        Task UpdateAsync(QuestionnaireDto input);

        Task UpdateScholarshipInfo(ScholarshipInfoEditDto input);

        Task UpdateQuestionnaire(QuestionnaireDto input);

        Task<List<ScholarshipListDto>> GetAllScholarships(DateTime? StartDate, DateTime? EndDate, int[] TeamIds, ScholarshipStatusEnum[] StatusIds);
        Task<ScholarshipOutputDto> GetScholarshipAsync(int Id);

        Task UpdateScholarshipStatus(int id, ScholarshipStatusEnum status);

        Task<ListResultDto<ScholarshipStatusListDto>> GetScholarshipstatusForDD();

        Task<bool> GetScholarshipToResend(int scholarshipId);

        Task<ScholarshipOutputDto> GetScholarshipByEmailAndTeamId(int TeamId, string email);
    }
}
