﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Timing;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TownSend.Authorization.Roles;
using TownSend.Authorization.Users;
using TownSend.Invites;
using TownSend.Members;
using TownSend.Members.Dto;
using TownSend.Scholarships.Dto;
using TownSend.Teams;
using TownSend.Url;
using static TownSend.Scholarships.ScholarshipManager;
using TownSend.Partners;

namespace TownSend.Scholarships
{
    [AbpAuthorize("Pages.Scholarships")]
    public class ScholarshipAppService : TownSendAppServiceBase, IScholarshipAppService
    {
        public IAppUrlService AppUrlService { get; set; }

        private readonly ScholarshipManager _scholarshipManager;
        private readonly IRepository<Member> _memberRepository;
        private readonly IRepository<Scholarship> _scholarshipRepository;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IRepository<Invite, long> _inviteRepository;
        private readonly IRepository<ScholarshipInviteMapping> _scholarshipInviteMappingRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<ScholarshipStatus> _scholarshipStatusRepository;
        private readonly IRepository<TeamPartnerMapping> _teamPartnerMappingRepository;
        private readonly IRepository<Partner> _partnerRepository;
        public ScholarshipAppService(ScholarshipManager scholarshipManager,
                IRepository<Member> memberRepository,
                IRepository<Scholarship> scholarshipRepository,
                UserManager userManager,
                IPasswordHasher<User> passwordHasher,
                RoleManager roleManager,
                IRepository<Invite, long> inviteRepository,
                IRepository<ScholarshipInviteMapping> scholarshipInviteMappingRepository,
                IUserEmailer userEmailer,
                IRepository<Team> teamRepository,
                IRepository<User, long> userRepository,
                IRepository<ScholarshipStatus> scholarshipStatusRepository,
                IRepository<TeamPartnerMapping> teamPartnerMappingRepository,
                IRepository<Partner> partnerRepository)
        {
            _scholarshipManager = scholarshipManager;
            _memberRepository = memberRepository;
            _scholarshipRepository = scholarshipRepository;
            _userManager = userManager;
            _passwordHasher = passwordHasher;
            _roleManager = roleManager;
            _inviteRepository = inviteRepository;
            _scholarshipInviteMappingRepository = scholarshipInviteMappingRepository;
            AppUrlService = NullAppUrlService.Instance;
            _userEmailer = userEmailer;
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _scholarshipStatusRepository = scholarshipStatusRepository;
            _teamPartnerMappingRepository = teamPartnerMappingRepository;
            _partnerRepository = partnerRepository;
        }

        /// <summary>
        /// This Method Generate Scholarship And Also Creates New Member And Map To Member
        /// Affected Entities
        ///     Scholarship
        ///     Member
        ///     User
        ///     UserRoles
        ///     Role
        ///     Invite
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ScholarshipRegisterResult> CreateAsync(ScholarshipRegisterDto input)
        {
            //check validation 1: check weather scholarship is duplicate or not (already generated)
            //if duplicate return to view
            var scholarshipList = await (
                                from s in _scholarshipRepository.GetAll()
                                join m in _memberRepository.GetAll() on s.MemberId equals m.Id
                                where m.Email == input.EmailAddress
                                select s
                               ).ToListAsync();

            if (scholarshipList == null || (scholarshipList != null && scholarshipList.Any()))
            {
                //already scholarship
                var errorMessages = new List<string>();
                errorMessages.Add($"This person already requested for a scholarship.");
                return new ScholarshipRegisterResult
                {
                    ErrorMessage = errorMessages,
                    IsSuccess = false
                };
            }

            var existingMember = await _memberRepository.FirstOrDefaultAsync(x => x.Email == input.EmailAddress);
            if (existingMember != null)
            {
                //already a member not a new member so cant continue with scholarship
                var errorMessages = new List<string>();
                errorMessages.Add($"This person can not request for a scholarship as member is already generated.");
                return new ScholarshipRegisterResult
                {
                    ErrorMessage = errorMessages,
                    IsSuccess = false
                };
            }

            int? tenantId = AbpSession.TenantId;
            int? roleId = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Member)?.Id;
            var user = await _userRepository.FirstOrDefaultAsync(x => x.EmailAddress == input.EmailAddress);
            if(user == null)
            {
                //means neither member nor partner
                //create user
                user = new User
                {
                    TenantId = tenantId,
                    Name = input.FirstName,
                    Surname = input.LastName,
                    EmailAddress = input.EmailAddress,
                    IsActive = false,
                    UserName = input.EmailAddress,
                    IsEmailConfirmed = false,
                    Roles = new List<UserRole>()
                };

                user.SetNormalizedNames();

                user.Password = _passwordHasher.HashPassword(user, "Abc@1234");

                //assign role                
                if (roleId != null && roleId > 0)
                {
                    user.Roles.Add(new UserRole(tenantId, user.Id, roleId ?? 0));
                }

                CheckErrors(await _userManager.CreateAsync(user));
                await CurrentUnitOfWork.SaveChangesAsync();
            }
           
            //create member
            if (user != null && user.Id > 0)
            {
                var member = new Member
                {
                    UserId = user.Id,
                    FirstName = input.FirstName,
                    LastName = input.LastName,
                    //Scholarship_Amount = Convert.ToInt32(input.ScholarshipAmount), 
                    TeamId = input.TeamId,
                    Email = input.EmailAddress
                };

                int memberId = await _memberRepository.InsertAndGetIdAsync(member);

                //create scholarship
                if (memberId > 0)
                {
                    input.MemberId = memberId;
                    input.ScholarshipStatus = ScholarshipStatusEnum.QuestionnaireSent;
                    var scholarship = ObjectMapper.Map<Scholarship>(input);
                    int scholarshipId = await _scholarshipRepository.InsertAndGetIdAsync(scholarship);

                    //create invitation
                    if (scholarshipId > 0)
                    {
                        if (!user.IsEmailConfirmed)
                        {
                            //insert data into invite table
                            var invite = new Invite
                            {
                                UserId = user.Id,
                                RoleId = roleId ?? 0,
                                InviteToken = Guid.NewGuid().ToString(),
                                //StatusId = 1, // PartnerInvited
                                TeamId = input.TeamId,
                                IsActive = true,
                                ExpirationDate = Clock.Now.AddYears(1)
                            };                            
                            long inviteId = await _inviteRepository.InsertAndGetIdAsync(invite);

                            //send email temp commented  TODO:dev1
                            string emailActivationLink = AppUrlService.CreateScholarshipInviteEmailUrlFormat(tenantId);
                            await _userEmailer.SendEmailScholarshipInviteAsync(member, scholarshipId, user.TenantId, emailActivationLink);

                            //create mapping
                            if (scholarshipId > 0 && inviteId > 0)
                            {
                                ScholarshipInviteMapping mapping = new ScholarshipInviteMapping
                                {
                                    ScholarshipId = scholarshipId,
                                    InviteId = inviteId
                                };

                                await _scholarshipInviteMappingRepository.InsertAsync(mapping);
                            }
                        }
                    }
                }
            }

            return new ScholarshipRegisterResult
            {
                IsSuccess = true
            };
        }

        /// <summary>
        /// This Method Update Questionnaire Information Which Is Fill By Member At Accept Scholarship Form in Schollarship Table
        /// Affected Entities
        /// Scholarship
        /// Member
        /// Invite
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAllowAnonymous]
        public async Task UpdateAsync(QuestionnaireDto input)
        {
            var data = await _scholarshipRepository.GetAsync(input.ScholarshipId);
            if (data == null)
                return;

            //update scholarship details
            ObjectMapper.Map(input, data);

            //update scholarship status
            data.ScholarshipStatus = ScholarshipStatusEnum.PendingApproval;
            data.CompletionDate = DateTime.UtcNow;
            data.NotificationDate = DateTime.UtcNow; //implemented as per current project logic

            await _scholarshipRepository.UpdateAsync(data);

            //sent email to admin for the pending approval
            string emailActivationLink = AppUrlService.CreateScholarshipApprovalEmailUrlFormat(AbpSession.TenantId);
            await _userEmailer.SendEmailScholarshipApprovalAsync(data, AbpSession.TenantId, emailActivationLink);
        }
        /// <summary>
        /// This Method Display Scholarship info And Update Scholarship And Also Update Member Detaiils
        /// Affected Entities 
        /// Scholarship
        /// Member
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize("Pages.Scholarships.List")]
        public async Task UpdateScholarshipInfo(ScholarshipInfoEditDto input)
        {
            var data = await (from s in _scholarshipRepository.GetAll()
                              join m in _memberRepository.GetAll() on s.MemberId equals m.Id
                              join u in _userRepository.GetAll() on m.UserId equals u.Id
                              where s.Id == input.ScholarshipId
                              select new
                              {
                                  s,
                                  m,
                                  u
                              }).FirstOrDefaultAsync();

            //update user table
            data.u.PhoneNumber = input.PhoneNumber;
            data.u.Name = input.FirstName;
            data.u.Surname = input.LastName;

            //update member table
            data.m.FirstName = input.FirstName;
            data.m.LastName = input.LastName;
            data.m.PhoneNumber = Convert.ToInt64(input.PhoneNumber);
            //data.m.Scholarship_Amount = input.ScholarshipAmount;

            //udpate scholarship table
            data.s.ScholarshipAmount = input.ScholarshipAmount;
            
            await _userRepository.UpdateAsync(data.u);
            await _memberRepository.UpdateAsync(data.m);
            await _scholarshipRepository.UpdateAsync(data.s);
        }
        /// <summary>
        /// This Method Update Questionnaire Of Members Scholarship
        /// Affected Entities
        /// Scholarship
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize("Pages.Scholarships.List")]
        public async Task UpdateQuestionnaire(QuestionnaireDto input)
        {
            var data = await _scholarshipRepository.GetAsync(input.ScholarshipId);
            
            //udpate scholarship table
            data.FirstTimeApply = Convert.ToBoolean(input.FirstTimeApply);
            data.FirstTimeinTLP = Convert.ToBoolean(input.FirstTimeinTLP);
            data.GrowthDetails = input.GrowthDetails;
            data.AchivementsDetails = input.AchivementsDetails;
            data.LifeExperienceDetails = input.LifeExperienceDetails;
            data.FutureGoalDetails = input.FutureGoalDetails;

            await _scholarshipRepository.UpdateAsync(data);
        }
        /// <summary>
        /// This Method Display All Scholarship in ScholarshipList View Page 
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="TeamIds"></param>
        /// <param name="StatusIds"></param>
        /// <returns></returns>
        //[AbpAuthorize("Pages.Scholarships.List")]
        public async Task<List<ScholarshipListDto>> GetAllScholarships(DateTime? StartDate, DateTime? EndDate, int[] TeamIds,ScholarshipStatusEnum[] StatusIds)
        {
            var query = (from scholarship in _scholarshipRepository.GetAllList()
                         join member in _memberRepository.GetAllList() on scholarship.MemberId equals member.Id
                         join team in _teamRepository.GetAllList() on scholarship.TeamId equals team.Id
                         join status in _scholarshipStatusRepository.GetAllList() on (int)scholarship.ScholarshipStatus equals status.Id
                         join mapp in _teamPartnerMappingRepository.GetAll() on team.Id equals mapp.TeamId
                         join partner in _partnerRepository.GetAll() on mapp.PartnerId equals partner.Id
                         select new
                         {
                             team,
                             scholarship,
                             member,
                             mapp,
                             status,
                             partner
                         }).ToList();
            var conditionalquery = (from scholarship in _scholarshipRepository.GetAllList()
                         join member in _memberRepository.GetAllList() on scholarship.MemberId equals member.Id
                         join team in _teamRepository.GetAllList() on scholarship.TeamId equals team.Id
                         join status in _scholarshipStatusRepository.GetAllList() on (int)scholarship.ScholarshipStatus equals status.Id
                         join mapp in _teamPartnerMappingRepository.GetAll() on team.Id equals mapp.TeamId
                         join partner in _partnerRepository.GetAll() on mapp.PartnerId equals partner.Id
                         select new
                         {
                             team,
                             scholarship,
                             member,
                             status,
                             mapp,
                             partner
                         }).ToList();
            if (StartDate != null)
            {
                conditionalquery = conditionalquery.Where(x => x.scholarship.CreationTime >= StartDate).ToList();
            }
            if (EndDate != null)
            {
                conditionalquery = conditionalquery.Where(x => x.scholarship.CreationTime <= EndDate).ToList();
            }
            if (TeamIds.Count() > 0)
            {
                conditionalquery = conditionalquery.Where(x => TeamIds.Contains(x.scholarship.TeamId)).ToList();
            }
            if (StatusIds.Count() > 0)
            {
                conditionalquery = conditionalquery.Where(x => StatusIds.Contains(x.scholarship.ScholarshipStatus)).ToList();
            }
            if(!IsGranted("Pages.Scholarships.List"))
            {
                conditionalquery = conditionalquery.Where(x => x.partner.UserId == AbpSession.UserId).ToList();
            }
            var q = conditionalquery.Select(x => new {  x.scholarship,x.member,x.status,x.team }).Distinct();
            var teamList1 = new List<ScholarshipListDto>();
            foreach (var q1 in q)
            {
                
                var re = new ScholarshipListDto
                {
                    Id = q1.scholarship.Id,
                    MemberName = string.Format("{0} {1}", q1.member?.FirstName, q1.member?.LastName),
                    TeamName = q1.team.TeamName,
                    ScholarshipAmount = q1.scholarship.ScholarshipAmount,
                    CreationTime = q1.scholarship.CreationTime,
                    ScholarshipStatus = q1.status.Status,
                    
                };
                var plname = new List<string>();
                foreach(var q2 in query)
                {
                    if(q1.scholarship.Id==q2.scholarship.Id)
                    {
                        if (q1.team.Id == q2.mapp.TeamId)
                        {
                            plname.Add(q2.partner.FullName);
                        }
                    }
                    
                    //if(q1.)
                }

                re.PartnerName = plname;
                teamList1.Add(re);
            }
            List<ScholarshipListDto> dto = ObjectMapper.Map<List<ScholarshipListDto>>(teamList1);
            return dto;
        }
        /// <summary>
        /// This Method Display Scholarship Details Page With Scholarship Details And Also Member Details
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<ScholarshipOutputDto> GetScholarshipAsync(int Id)
        {
            var temp = _scholarshipRepository.GetAll();
            var scholarshipResult = new ScholarshipOutputDto();
              
             
            if (IsGranted("Pages.Scholarships.List"))  //Permission Granter For Admin Only
            {
                scholarshipResult = await (from scholarship in _scholarshipRepository.GetAll()
                                           join team in _teamRepository.GetAll() on scholarship.TeamId equals team.Id
                                           join member in _memberRepository.GetAll() on scholarship.MemberId equals member.Id
                                           join user in _userRepository.GetAll() on scholarship.CreatorUserId equals user.Id
                                           join status in _scholarshipStatusRepository.GetAll() on (int)scholarship.ScholarshipStatus equals status.Id
                                           where scholarship.Id == Id
                                           select new ScholarshipOutputDto
                                           {
                                               ScholarshipRegister = ObjectMapper.Map<ScholarshipRegisterDto>(scholarship),
                                               Questionnaire = ObjectMapper.Map<QuestionnaireDto>(scholarship),
                                               TeamName = team.TeamName,
                                               ReferrerName = user.FullName,
                                               MemberEmail = member.Email,
                                               MemberFirstName = member.FirstName,
                                               MemberLastName = member.LastName,
                                               MemberPhoneNumber = member.PhoneNumber == 0 ? "" : Convert.ToString(member.PhoneNumber),
                                               ScholarshipStatus = status.Alias
                                           }).FirstOrDefaultAsync();
            }
            else
            {
                scholarshipResult = await (from scholarship in _scholarshipRepository.GetAll()
                                           join team in _teamRepository.GetAll() on scholarship.TeamId equals team.Id
                                           join member in _memberRepository.GetAll() on scholarship.MemberId equals member.Id
                                           join user in _userRepository.GetAll() on scholarship.CreatorUserId equals user.Id
                                           join status in _scholarshipStatusRepository.GetAll() on (int)scholarship.ScholarshipStatus equals status.Id
                                           join teampartnermap in _teamPartnerMappingRepository.GetAll() on team.Id equals teampartnermap.TeamId
                                           join partner in _partnerRepository.GetAll() on teampartnermap.PartnerId equals partner.Id
                                           where partner.UserId == AbpSession.UserId
                                           where scholarship.Id == Id
                                           select new ScholarshipOutputDto
                                           {
                                               ScholarshipRegister = ObjectMapper.Map<ScholarshipRegisterDto>(scholarship),
                                               Questionnaire = ObjectMapper.Map<QuestionnaireDto>(scholarship),
                                               TeamName = team.TeamName,
                                               ReferrerName = user.FullName,
                                               MemberEmail = member.Email,
                                               MemberFirstName = member.FirstName,
                                               MemberLastName = member.LastName,
                                               MemberPhoneNumber = member.PhoneNumber == 0 ? "" : Convert.ToString(member.PhoneNumber),
                                               ScholarshipStatus = status.Alias
                                           }).FirstOrDefaultAsync();
            }
            //var result = ObjectMapper.Map<ScholarshipOutputDto>(scholarshipResult);
            //return result;

            if (scholarshipResult != null && scholarshipResult.ScholarshipRegister != null)
            {
                scholarshipResult.ScholarshipRegister.StrApprovedOn = scholarshipResult.ScholarshipRegister.ApprovedOn.HasValue ? scholarshipResult.ScholarshipRegister.ApprovedOn.Value.ToString("MM-dd-yyyy") : string.Empty; 
                scholarshipResult.ScholarshipRegister.StrCompletionDate = scholarshipResult.ScholarshipRegister.CompletionDate.HasValue ? scholarshipResult.ScholarshipRegister.CompletionDate.Value.ToString("MM-dd-yyyy") : string.Empty;
                scholarshipResult.ScholarshipRegister.StrCreationTime = scholarshipResult.ScholarshipRegister.CreationTime.ToString("MM-dd-yyyy");
            }

            return scholarshipResult;
        }
        /// <summary>
        /// This Method Update and Manage Members Scholarship Status
        /// Affected Entities
        /// Scholarship
        /// Scholarship Status
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task UpdateScholarshipStatus(int id, ScholarshipStatusEnum status)
        {
            var data = await _scholarshipRepository.GetAsync(id);
            if (data == null)
                return;

            //update scholarship status
            data.ScholarshipStatus = status;
            if (status == ScholarshipStatusEnum.ApprovedSendInvite)
                data.IsApproved = true;
            else if (status == ScholarshipStatusEnum.DeniedSendInvite)
                data.IsApproved = false;

            data.ApprovedOn = Clock.Now;
            data.ApprovedBy = Convert.ToInt64(AbpSession.UserId);

            await _scholarshipRepository.UpdateAsync(data);

            //send email to member and amdin based on the updated status by admin
            if(status == ScholarshipStatusEnum.ApprovedSendInvite || status == ScholarshipStatusEnum.DeniedSendInvite)
            {
                //to partner
                await _userEmailer.SendEmailScholarshipStatusUpdateAsync(id, status, AbpSession.TenantId, toPartner: true);

                //to member
                await _userEmailer.SendEmailScholarshipStatusUpdateAsync(id, status, AbpSession.TenantId, toPartner: false);
            }
        }
        /// <summary>
        /// This Method Is Give Status Of Scholarship
        /// Affected Entities
        /// Scholarship
        /// Scholarship Status
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<ScholarshipStatusListDto>> GetScholarshipstatusForDD()
        {
            List<ScholarshipStatusListDto> dto = new List<ScholarshipStatusListDto>();
            var data = await _scholarshipStatusRepository.GetAllListAsync();            
            return new ListResultDto<ScholarshipStatusListDto>(ObjectMapper.Map(data, dto));
        }
        /// <summary>
        /// This Method Is Used For Resend Invite to Member Using Email
        /// Affected Entities
        ///     Scholarship
        ///     Member
        ///     Invite
        /// </summary>
        /// <param name="scholarshipId"></param>
        /// <returns></returns>
        public async Task<bool> GetScholarshipToResend(int scholarshipId)
        {
            bool result = false;
            int? tenantId = AbpSession.TenantId;
            int? roleId = _roleManager.Roles.Where(r => r.Name == StaticRoleNames.Tenants.Member).Select(x => x.Id).FirstOrDefault();
            
            var data = (from scholarship in _scholarshipRepository.GetAll()
                        join member in _memberRepository.GetAll() on scholarship.MemberId equals member.Id
                        where scholarship.Id == scholarshipId
                        select new
                        {
                            scholarship,
                            member
                        }
                        ).FirstOrDefault();

            if(data != null && data.scholarship != null && data.member != null)
            {
                //insert data into invite table
                var invite = new Invite
                {
                    UserId = data.member.Id,
                    RoleId = roleId ?? 0,
                    InviteToken = Guid.NewGuid().ToString(),
                    TeamId = data.scholarship.TeamId,
                    IsActive = true,
                    ExpirationDate = Clock.Now.AddYears(1)
                };

                long inviteId = await _inviteRepository.InsertAndGetIdAsync(invite);

                //send email
                string emailActivationLink = AppUrlService.CreateScholarshipInviteEmailUrlFormat(tenantId);
                await _userEmailer.SendEmailScholarshipInviteAsync(data.member, scholarshipId, tenantId, emailActivationLink);

                result = true;
            }

            return result;
        }

        /// <summary>
        /// This Method Display Scholarship Details Page With Scholarship Details And Also Member Details
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<ScholarshipOutputDto> GetScholarshipByEmailAndTeamId(int TeamId,string email)
        {
            var temp = _scholarshipRepository.GetAll();
            var scholarshipResult = await (from scholarship in _scholarshipRepository.GetAll()
                                           join team in _teamRepository.GetAll() on scholarship.TeamId equals team.Id
                                           join member in _memberRepository.GetAll() on scholarship.MemberId equals member.Id
                                           join user in _userRepository.GetAll() on scholarship.CreatorUserId equals user.Id
                                           join status in _scholarshipStatusRepository.GetAll() on (int)scholarship.ScholarshipStatus equals status.Id
                                           where member.Email == email && scholarship.TeamId == TeamId
                                           select new ScholarshipOutputDto
                                           {
                                               ScholarshipRegister = ObjectMapper.Map<ScholarshipRegisterDto>(scholarship),
                                               Questionnaire = ObjectMapper.Map<QuestionnaireDto>(scholarship),
                                               TeamName = team.TeamName,
                                               ReferrerName = user.FullName,
                                               MemberEmail = member.Email,
                                               MemberFirstName = member.FirstName,
                                               MemberLastName = member.LastName,
                                               MemberPhoneNumber = member.PhoneNumber == 0 ? "" : Convert.ToString(member.PhoneNumber),
                                               ScholarshipStatus = status.Alias
                                           }).FirstOrDefaultAsync();
            //var result = ObjectMapper.Map<ScholarshipOutputDto>(scholarshipResult);
            //return result;

            if (scholarshipResult != null && scholarshipResult.ScholarshipRegister != null)
            {
                scholarshipResult.ScholarshipRegister.StrApprovedOn = scholarshipResult.ScholarshipRegister.ApprovedOn.HasValue ? scholarshipResult.ScholarshipRegister.ApprovedOn.Value.ToString("MM-dd-yyyy") : string.Empty;
                scholarshipResult.ScholarshipRegister.StrCompletionDate = scholarshipResult.ScholarshipRegister.CompletionDate.HasValue ? scholarshipResult.ScholarshipRegister.CompletionDate.Value.ToString("MM-dd-yyyy") : string.Empty;
                scholarshipResult.ScholarshipRegister.StrCreationTime = scholarshipResult.ScholarshipRegister.CreationTime.ToString("MM-dd-yyyy");
            }

            return scholarshipResult;
        }

    }
}
