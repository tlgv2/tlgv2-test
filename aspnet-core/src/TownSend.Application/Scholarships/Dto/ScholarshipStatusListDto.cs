﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Scholarships.Dto
{
    public class ScholarshipStatusListDto : EntityDto
    {
        public string Status { get; set; }

        public string Alias { get; set; }
    }
}
