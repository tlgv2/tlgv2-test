﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TownSend.Members.Dto;

namespace TownSend.Scholarships.Dto
{
    public class ScholarshipOutputDto
    {
        public ScholarshipOutputDto()
        {
            ScholarshipRegister = new ScholarshipRegisterDto();
            Questionnaire = new QuestionnaireDto();
        }

        public ScholarshipRegisterDto ScholarshipRegister { get; set; }
        public QuestionnaireDto Questionnaire { get; set; }

        public string TeamName { get; set; }
        public string ReferrerName { get; set; }

        //member info
        [Required]
        [Display(Name = "First Name")]
        public string MemberFirstName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string MemberLastName { get; set; }

        [RegularExpression(@"^[+]?[01]?[- .]?\(?[1-9]\d{2}\)?[- .]?\d{3}[- .]?\d{4}\s*((?:[ ]+|[xX]|[eE][xX][tT])\.?\s*(\d+))*$", ErrorMessage = "Invalid {0}")]
        [Display(Name = "Phone number")]
        public string MemberPhoneNumber { get; set; }
        public string MemberEmail { get; set; }

        public string ScholarshipStatus { get; set; }

    }
}
