﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Scholarships.Dto
{
    public class ScholarshipMapProfile : Profile
    {
        public ScholarshipMapProfile()
        {
            CreateMap<Scholarship, ScholarshipDto>();
            CreateMap<ScholarshipDto, Scholarship>();

            CreateMap<Scholarship, ScholarshipRegisterDto>();
            CreateMap<ScholarshipRegisterDto, Scholarship>();

            CreateMap<Scholarship, QuestionnaireDto>().ForMember(x => x.ScholarshipId, y => y.MapFrom(z => z.Id));
            CreateMap<QuestionnaireDto, Scholarship>();

            CreateMap<Scholarship, ScholarshipListDto>();
            CreateMap<ScholarshipListDto, Scholarship>();

            CreateMap<ScholarshipStatus, ScholarshipStatusListDto>();
            CreateMap<ScholarshipStatusListDto, ScholarshipStatus>();
        }
    }
}
