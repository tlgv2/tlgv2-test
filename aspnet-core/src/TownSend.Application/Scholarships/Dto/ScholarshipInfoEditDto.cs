﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Scholarships.Dto
{
    public class ScholarshipInfoEditDto
    {
        [Required]
        public int ScholarshipId { get; set; }
        [Required]
        [Display(Name = "Member First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Member Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Amount of scholarship request (TLG allows up to $2,000.00)")]
        [Range(1.0, 2000.0)]
        public decimal ScholarshipAmount { get; set; }

        public string PhoneNumber { get; set; }
    }
}
