﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Scholarships.Dto
{
    public class ScholarshipDto : EntityDto
    {
        public int MemberId { get; set; }
        public int TeamId { get; set; }
        public decimal ScholarshipAmount { get; set; }
        public bool? IsApproved { get; set; }
        public long ApprovedBy { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public bool IsDeleted { get; set; }
        public bool FirstTimeApply { get; set; }
        public bool FirtTimeinTLP { get; set; }
        public string GrowthDetails { get; set; }
        public string AchivementsDetails { get; set; }
        public string LifeExperienceDetails { get; set; }
        public string FutureGoalDetails { get; set; }
        public DateTime CompletionDate { get; set; }
        public DateTime NotificationDate { get; set; }
        public ScholarshipStatusEnum ScholarshipStatus { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
