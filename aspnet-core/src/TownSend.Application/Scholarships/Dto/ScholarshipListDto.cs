﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Scholarships.Dto
{
    public class ScholarshipListDto : EntityDto
    {
        public int MemberId { get; set; }
        public int TeamId { get; set; }

        public string MemberName { get; set; }
        public string TeamName { get; set; }
        public decimal ScholarshipAmount { get; set; }
        public DateTime CreationTime { get; set; }
        public string ScholarshipStatus { get; set; }
        public List<string> PartnerName { get; set; }
    }
}
