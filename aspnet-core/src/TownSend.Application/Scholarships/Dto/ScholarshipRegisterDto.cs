﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Scholarships.Dto
{
    public class ScholarshipRegisterDto : ICreationAudited
    {
        [Required]
        [Display(Name = "Member Email Address")]
        public string EmailAddress { get; set; }
        public int MemberId { get; set; }

        [Required]
        [Display(Name = "TLP group are you applying for")]
        [Range(1, int.MaxValue, ErrorMessage = "The TLP group field is required.")]
        public int TeamId { get; set; }
        [Required]
        [Display(Name = "Member First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Member Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Amount of scholarship request (TLG allows up to $2,000.00)")]
        [Range(1.0, 2000.0)]
        public decimal ScholarshipAmount { get; set; }
        public bool? IsApproved { get; set; }
        public long ApprovedBy { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public string StrApprovedOn { get; set; }
        public bool IsDeleted { get; set; }
        public ScholarshipStatusEnum ScholarshipStatus { get; set; }
        public string PhoneNumber { get; set; }
        public long? CreatorUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public string StrCreationTime { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string StrCompletionDate { get; set; }
    }
}
