﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Scholarships.Dto
{
    public class QuestionnaireDto 
    {
        [Required(ErrorMessage = "This field is required.")]
        public int ScholarshipId { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Is this the first time you apply for a scholarship?")]
        [Range(0, 1, ErrorMessage = "This field is required.")]
        public int? FirstTimeApply { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Is this your first time in TLP?")]
        [Range(0, 1, ErrorMessage = "This field is required.")]
        public int? FirstTimeinTLP { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Please state your understanding of the impact thats TLP will have on your personal and professional growth.")]
        //[RegularExpression(@"^[A-Za-z0-9](?!.*?\s$)[a-zA-Z0-9\s-_/.#$%^*()}{:;+=|!,?~@&amp<>'""]{0,10000}$", ErrorMessage = "White Space is not allow")]
        [StringLength(TownSendConsts.QuestionnaireTextAreaStringServerLimit, ErrorMessage = "This field exceed the maximum character limit.")]
        public string GrowthDetails { get; set; } //Quest 1

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "What are three things you hope to achieve this year?")]
        //[RegularExpression(@"^[A-Za-z0-9](?!.*?\s$)[a-zA-Z0-9\s-_/.#$%^*()}{:;+=|!,?~@&amp<>'""]{0,10000}$", ErrorMessage = "White Space is not allow")]
        [StringLength(TownSendConsts.QuestionnaireTextAreaStringServerLimit, ErrorMessage = "This field exceed the maximum character limit.")]
        public string AchivementsDetails { get; set; } //Quest 2

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "The Townsend growth model addresses developing both comptetency and character to accelerate growth. Can you briefly describe your own history of both exceptional and difficult life experiences of growth?")]
        //[RegularExpression(@"^[A-Za-z0-9](?!.*?\s$)[a-zA-Z0-9\s-_/.#$%^*()}{:;+=|!,?~@&amp<>'""]{0,10000}$", ErrorMessage = "White Space is not allow")]
        [StringLength(TownSendConsts.QuestionnaireTextAreaStringServerLimit, ErrorMessage = "This field exceed the maximum character limit.")]
        public string LifeExperienceDetails { get; set; } //Quest 3

        [Required(ErrorMessage = "This field is required.")]
        [Display(Name = "Where do you see yourself in 5 years?")]
        //[RegularExpression(@"^[A-Za-z0-9](?!.*?\s$)[a-zA-Z0-9\s-_/.#$%^*()}{:;+=|!,?~@&amp<>'""]{0,10000}$", ErrorMessage = "White Space is not allow")]
        [StringLength(TownSendConsts.QuestionnaireTextAreaStringServerLimit, ErrorMessage = "This field exceed the maximum character limit.")]
        public string FutureGoalDetails { get; set; } //Quest 4
    }
}

