﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Url
{
    public interface IAppUrlService
    {
        string CreateEmailActivationUrlFormat(int? tenantId);
        string CreatePasswordResetUrlFormat(int? tenantId);

        string ResetPasswordUrlFormat(int? tenantId);

        string CreateEmailActivationUrlFormat(string tenancyName);
        string CreatePasswordResetUrlFormat(string tenancyName);
        string CreateScholarshipInviteEmailUrlFormat(int? tenantId);
        string CreateScholarshipInviteEmailUrlFormat(string tenancyName);
        string ChangeEmailUrlFormat(int? tenantId);
        string CreateScholarshipApprovalEmailUrlFormat(int? tenantId);
        string CreateScholarshipApprovalEmailUrlFormat(string tenancyName);

        string CreateMemberInviteEmailUrlFormat(int? tenantId);
        string CreateMemberInviteEmailUrlFormat(string tenancyName);

        string ChangeEmailUrlFormat(string tenancyName);
        string EmailCallBackURL(string returnUrl);

        string ResetPasswordUrlFormat(string tenancyName);
    }
}
