﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Url
{
    public class NullAppUrlService : IAppUrlService
    {
        public static IAppUrlService Instance { get; } = new NullAppUrlService();

        private NullAppUrlService()
        {

        }

        public string CreateEmailActivationUrlFormat(int? tenantId)
        {
            throw new NotImplementedException();
        }

        public string CreatePasswordResetUrlFormat(int? tenantId)
        {
            throw new NotImplementedException();
        }

        public string CreateScholarshipInviteEmailUrlFormat(int? tenantId)
        {
            throw new NotImplementedException();
        }

        public string CreateScholarshipApprovalEmailUrlFormat(int? tenantId)
        {
            throw new NotImplementedException();
        }

        public string CreateMemberInviteEmailUrlFormat(int? tenantId)
        {
            throw new NotImplementedException();
        }

        public string CreateEmailActivationUrlFormat(string tenancyName)
        {
            throw new NotImplementedException();
        }

        public string CreatePasswordResetUrlFormat(string tenancyName)
        {
            throw new NotImplementedException();
        }

        public string CreateScholarshipInviteEmailUrlFormat(string tenancyName)
        {
            throw new NotImplementedException();
        }

        public string CreateScholarshipApprovalEmailUrlFormat(string tenancyName)
        {
            throw new NotImplementedException();
        }

        public string CreateMemberInviteEmailUrlFormat(string tenancyName)
        {
            throw new NotImplementedException();
        }

        public string ChangeEmailUrlFormat(int? tenantId)
        {
            throw new NotImplementedException();
        }

        public string ChangeEmailUrlFormat(string tenancyName)
        {
            throw new NotImplementedException();
        }

        public string EmailCallBackURL(string returnUrl)
        {
            throw new NotImplementedException();
        }

        public string ResetPasswordUrlFormat(int? tenantId)
        {
            throw new NotImplementedException();
        }

        public string ResetPasswordUrlFormat(string tenancyName)
        {
            throw new NotImplementedException();
        }
    }
}
