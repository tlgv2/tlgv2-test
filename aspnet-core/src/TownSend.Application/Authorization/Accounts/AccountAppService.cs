using System.Linq;
using System.Threading.Tasks;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Timing;
using Abp.UI;
using Abp.Zero.Configuration;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TownSend.Authorization.Accounts.Dto;
using TownSend.Authorization.Users;
using TownSend.Invites;
using TownSend.Partners;
using TownSend.Url;

namespace TownSend.Authorization.Accounts
{
    public class AccountAppService : TownSendAppServiceBase, IAccountAppService
    {
        public IAppUrlService AppUrlService { get; set; }
        private readonly IUserEmailer _userEmailer;
        private readonly UserManager _userManager;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly IRepository<Invite, long> _inviteRepository;
        private readonly IPasswordHasher<User> _passwordHasher;

        // from: http://regexlib.com/REDetails.aspx?regexp_id=1923
        public const string PasswordRegex = "(?=^.{8,}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s)[0-9a-zA-Z!@#$%^&*()]*$";

        private readonly UserRegistrationManager _userRegistrationManager;

        public AccountAppService(
            IRepository<Partner> partnerRepository,
            UserRegistrationManager userRegistrationManager,
            IUserEmailer userEmailer,
            IRepository<Invite, long> inviteRepository,
            IPasswordHasher<User> passwordHasher,
            UserManager userManager)
        {
            _userRegistrationManager = userRegistrationManager;
            _userEmailer = userEmailer;
            _inviteRepository = inviteRepository;
            _passwordHasher = passwordHasher;
            _userManager = userManager;
            _partnerRepository = partnerRepository;
        }

        public async Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input)
        {
            var tenant = await TenantManager.FindByTenancyNameAsync(input.TenancyName);
            if (tenant == null)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.NotFound);
            }

            if (!tenant.IsActive)
            {
                return new IsTenantAvailableOutput(TenantAvailabilityState.InActive);
            }

            return new IsTenantAvailableOutput(TenantAvailabilityState.Available, tenant.Id);
        }

        public async Task<RegisterOutput> Register(RegisterInput input)
        {
            var user = await _userRegistrationManager.RegisterAsync(
                input.Name,
                input.Surname,
                input.EmailAddress,
                input.UserName,
                input.Password,
                true // Assumed email address is always confirmed. Change this if you want to implement email confirmation.
            );

            var isEmailConfirmationRequiredForLogin = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.IsEmailConfirmationRequiredForLogin);

            return new RegisterOutput
            {
                CanLogin = user.IsActive && (user.IsEmailConfirmed || !isEmailConfirmationRequiredForLogin)
            };
        }
        public async Task SendEmailActivationLink(SendEmailActivationLinkInput input)
        {
            var user = await GetUserByChecking(input.EmailAddress);
            //user.SetNewEmailConfirmationCode();
            await _userEmailer.SendEmailActivationLinkAsync(
                user,
                new Invite(),
                AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId)
            );
        }

        public async Task<bool> ActivateEmail(ActivateEmailInput input, bool isUpdateRequire)
        {
            try
            {
                var invite = await _inviteRepository.GetAsync(input.InviteId);
                if (
                    //user == null                    || 
                    invite == null
                    || (invite != null
                            && (!invite.IsActive
                                    || invite.StatusId == 2 //partner already registerd
                                    || invite.ExpirationDate <= Clock.Now
                                    || invite.InviteToken != input.ConfirmationCode)))
                {
                    return false;
                }

                if (isUpdateRequire)
                {
                    var partner=await _partnerRepository.GetAll().Where(x => x.UserId == invite.UserId).FirstOrDefaultAsync();
                    partner.PartnerStatus = PartnerStatus.PartnerRegistered;

                    //update invite table
                    invite.IsActive = false;
                    invite.StatusId = 2; // partner registerd
                    
                    await _inviteRepository.UpdateAsync(invite);
                    await _partnerRepository.UpdateAsync(partner);
                    return true;
                }

                return true;
            }
            catch (System.Exception ex)
            {

                return false;
            }
            
        }

        private async Task<User> GetUserByChecking(string inputEmailAddress)
        {
            var user = await UserManager.FindByEmailAsync(inputEmailAddress);
            if (user == null)
            {
                throw new UserFriendlyException(L("InvalidEmailAddress"));
            }

            return user;
        }
    }
}
