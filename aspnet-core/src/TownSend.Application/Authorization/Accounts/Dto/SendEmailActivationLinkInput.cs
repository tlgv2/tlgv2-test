﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        [EmailAddress(ErrorMessage = "Please enter valid email address")]
        public string EmailAddress { get; set; }
    }
}
