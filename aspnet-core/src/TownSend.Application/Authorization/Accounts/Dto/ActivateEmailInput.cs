﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TownSend.Users.Dto;

namespace TownSend.Authorization.Accounts.Dto
{
    public class ActivateEmailInput
    {
        public long UserId { get; set; }

        public long InviteId { get; set; }
        public string ConfirmationCode { get; set; }
      
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

    }
    public class ActivateEmailInput1
    {
        public long UserId { get; set; }

        public long InviteId { get; set; }
        public string ConfirmationCode { get; set; }

        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Email { get; set; }

    }
}
