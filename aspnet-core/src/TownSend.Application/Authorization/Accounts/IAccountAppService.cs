﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TownSend.Authorization.Accounts.Dto;

namespace TownSend.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
        Task SendEmailActivationLink(SendEmailActivationLinkInput input);

        Task<bool> ActivateEmail(ActivateEmailInput input, bool isUpdateRequire);
    }
}
