﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using TownSend.Common;
using TownSend.Partners;
using TownSend.Teams.Dto;
using TownSend.Common.Dto;
using Abp.Authorization;
using TownSend.Authorization;
using Abp.Linq.Extensions;
using TownSend.Documents.Dto;
using Abp.Domain.Uow;
using TownSend.AuditHistory;
using TownSend.Partners.Dto;

namespace TownSend.Teams
{
    [AbpAuthorize(PermissionNames.Pages_Teams)]
    public class TeamAppService : TownSendAppServiceBase, ITeamAppService
    {
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<TeamPartnerMapping> _teamPartnerMappingRepository;
        private readonly IRepository<TeamPartnerAuditHistoryMapping> _teamPartnerAuditHistoryMappingRepository;
        private readonly ICommonAppService _commonAppService;
        private readonly IRepository<Partner> _partnerRepository;

        public TeamAppService(
                              IRepository<Team> teamRepository,
                              ICommonAppService commonAppService,
                              IRepository<Partner> partnerRepository,
                              IRepository<TeamPartnerMapping> teamPartnerMappingRepository,
                              IRepository<TeamPartnerAuditHistoryMapping> teamPartnerAuditHistoryMappingRepository)
        {
            _teamRepository = teamRepository;
            _commonAppService = commonAppService;
            _partnerRepository = partnerRepository;
            _teamPartnerMappingRepository = teamPartnerMappingRepository;
            _teamPartnerAuditHistoryMappingRepository = teamPartnerAuditHistoryMappingRepository;
        }

        /// <summary>
        /// This Method Creates Teams And Also Add Partners and map to partner
        /// Affected Entities
        ///     Teams
        ///     TeamPartnerMap
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(PermissionNames.Pages_Teams_Create)]
        public async Task CreateAsync(CreateTeamDto input)
        {
            try
            {
                var data = ObjectMapper.Map<Team>(input);
                input.Id = await _teamRepository.InsertAndGetIdAsync(data);
                TeamPartnerMapDto mapDto = new TeamPartnerMapDto()
                {
                    PartnerIds=input.PartnerIds,
                    TeamId=input.Id
                };
                await CreateTeamPartnerMappingAsync(mapDto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// This Method Map Team And Multiple Partner And Insert Into Mapping Table
        /// Affected Entities
        /// TeamPartnerMapping
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(PermissionNames.Pages_Teams_Edit)]
        public async Task CreateTeamPartnerMappingAsync(TeamPartnerMapDto input)
        {
            try
            {
                //insert data into mapping table
                foreach (var partner in input.PartnerIds)
                {
                    TeamPartnerMapping mappings = new TeamPartnerMapping();
                    mappings.PartnerId = partner;
                    mappings.TeamId = input.TeamId;
                    mappings.PartnerPercentage = 0.0M; //partner percentage will be 0 when added first time
                    var teamParnterMapId = await _teamPartnerMappingRepository.InsertAndGetIdAsync(mappings);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This Method Remove Mapping Team And Partner And Also Remove From Mapping Table
        /// Affected Entities
        /// TeamPartnerMapping
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        [AbpAuthorize(PermissionNames.Pages_Teams_Edit)]
        public async Task RemoveTeamPartnerMappingAsync(int teamId, int partnerId)
        {
            try
            {
                var teamPartnerMapping = await (from tpm in _teamPartnerMappingRepository.GetAll()
                                                where tpm.TeamId == teamId && tpm.PartnerId == partnerId && tpm.IsDeleted == false
                                                select tpm).FirstOrDefaultAsync();

                if(teamPartnerMapping != null)
                {
                    teamPartnerMapping.IsDeleted = true;
                    await _teamPartnerMappingRepository.UpdateAsync(teamPartnerMapping);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// This Method Display Teams Of Partner
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<TeamListDto>> GetAll()
        {
            var query = (from team in _teamRepository.GetAll()
                         join map in _teamPartnerMappingRepository.GetAll() on team.Id equals map.TeamId
                         join partner in _partnerRepository.GetAll() on map.PartnerId equals partner.Id
                         select new
                         {
                             team,
                             map,
                             partner
                         }).ToList();

            var q = query.Select(x => x.team).Distinct();
            var teamList1 = new List<TeamListDto>();
            foreach (var q1 in q)
            {
                var re = ObjectMapper.Map<TeamListDto>(q1);
                var pl = new List<string>();
                foreach (var q2 in query)
                {
                    if (q1.Id == q2.map.TeamId)
                    {
                        pl.Add(q2.partner.FullName);
                    }

                }
                re.Partners = pl;
                teamList1.Add(re);
            }
            List<Team> result = new List<Team>();
            List<TeamListDto> teamList = new List<TeamListDto>();

            if (IsGranted("Pages.Teams.List"))
            {
                //Get TeamList
                result = await _teamRepository.GetAll().ToListAsync();
            }
            else
            {
                result = await (from team in _teamRepository.GetAll()
                                join map in _teamPartnerMappingRepository.GetAll() on team.Id equals map.TeamId
                                join part in _partnerRepository.GetAll() on map.PartnerId equals part.Id
                                where part.UserId == AbpSession.UserId
                                select team).ToListAsync();
            }

            //Get PartnerList
            var resultPartner = await _partnerRepository.GetAll().ToListAsync();
            //Linq Query for Assigning Partner Name Based on Partner Id
            foreach (var team in result)
            {
                var partnerName = (from partner in resultPartner
                                   select partner.FullName).FirstOrDefault();

                var list = ObjectMapper.Map<TeamListDto>(team);
                teamList.Add(list);
            }

            return new ListResultDto<TeamListDto>(teamList1);

        }

        /// <summary>
        /// This Method Is Display Teams Based On Filter
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="PartnerIds"></param>
        /// <returns></returns>
        public async Task<ListResultDto<TeamListDto>> GetAllTeamList(DateTime? StartDate, DateTime? EndDate, int[] PartnerIds)
        {
            var query = (from team in _teamRepository.GetAll()
                         join map in _teamPartnerMappingRepository.GetAll() on team.Id equals map.TeamId
                         join partner in _partnerRepository.GetAll() on map.PartnerId equals partner.Id
                         select new
                         {
                             team,
                             map,
                             partner
                         }).ToList();

            var conditionquery = (from team in _teamRepository.GetAll()
                                  join map in _teamPartnerMappingRepository.GetAll() on team.Id equals map.TeamId
                                  join partner in _partnerRepository.GetAll() on map.PartnerId equals partner.Id
                                  select new
                                  {
                                      team,
                                      map,
                                      partner
                                  }).ToList();
            if (StartDate != null)
            {
                conditionquery = conditionquery.Where(x => x.team.StartDate >= StartDate).ToList();
            }
            if (EndDate != null)
            {
                conditionquery = conditionquery.Where(x => x.team.StartDate <= EndDate).ToList();
            }
            if (PartnerIds.Count() > 0)
            {
                conditionquery = conditionquery.Where(x => PartnerIds.Contains(x.map.PartnerId)).ToList();
            }
            if (IsGranted("Pages.Partners.Create"))
            {

            }
            else
            {
                conditionquery = conditionquery.Where(x => x.partner.UserId == AbpSession.UserId).ToList();
            }
            var q = conditionquery.Select(x => x.team).Distinct();
            var teamList1 = new List<TeamListDto>();
            foreach (var q1 in q)
            {
                var re = ObjectMapper.Map<TeamListDto>(q1);
                var pl = new List<string>();
                foreach (var q2 in query)
                {
                    if (q1.Id == q2.map.TeamId)
                    {
                        pl.Add(q2.partner.FullName);
                    }

                }
                re.Partners = pl;
                teamList1.Add(re);
            }


            return new ListResultDto<TeamListDto>(teamList1);

        }

        /// <summary>
        /// This Method Update Team On Received Input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(PermissionNames.Pages_Teams_Edit)]
        public async Task<TeamDto> UpdateAsync(TeamDto input)
        {
            var data = await _teamRepository.GetAsync(input.Id);
            ObjectMapper.Map(input, data);
            await _teamRepository.UpdateAsync(data);
            return input;
        }

        /// <summary>
        /// This Method Is Call When Click On EditTeam And Display EditTeam Page
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<GetTeamForEditOutput> GetTeamForEdit(EntityDto input)
        {
            var data = new Team();
            if(IsGranted("Pages.Teams.List"))
            {
                data = await _teamRepository.GetAsync(input.Id);
            }
            else
            {
                data = await (from teams in _teamRepository.GetAll()
                              join teamMap in _teamPartnerMappingRepository.GetAll() on teams.Id equals teamMap.TeamId
                              join partner in _partnerRepository.GetAll() on teamMap.PartnerId equals partner.Id
                              where teams.Id == input.Id
                              where partner.UserId == AbpSession.UserId                              
                              select teams).FirstOrDefaultAsync();
            }
            var dataPartners = await (from map in _teamPartnerMappingRepository.GetAll()
                                      join partner in _partnerRepository.GetAll() on map.PartnerId equals partner.Id
                                      where map.TeamId == input.Id
                                      select new TeamPartnerEditDto
                                      {
                                          PartnerId = partner.Id,
                                          TeamId = map.TeamId,
                                          PartnerName = string.Format("{0} {1}", partner.FirstName, partner.LastName),
                                          PartnerPercentage = map.PartnerPercentage
                                      }).ToListAsync();


            var teamEditDto = ObjectMapper.Map<TeamEditDto>(data);

            return new GetTeamForEditOutput
            {
                Team = teamEditDto,
                PartnerList = dataPartners,
            };
        }

        /// <summary>
        /// This Method Is Called When Partner Need To Change Add_Partner And Also Change Percentage in EditTeam Page  
        /// Affected Entities
        /// TeamPartnerMapping
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<int> UpdatePartnerForTeamAsync(List<TeamPartnerEditDto> input)
        {
            int teamId = 0;
            try
            {
                if (input == null || !input.Any())
                {
                    return teamId;
                }

                teamId = input.Select(x => x.TeamId).FirstOrDefault();

                var deleteIds = _teamPartnerMappingRepository.GetAll().Where(x => x.TeamId == teamId);
                foreach (var id in deleteIds)
                {
                    await _teamPartnerMappingRepository.DeleteAsync(id);
                }

                //insert data into mapping table
                foreach (var mappingdata in input)
                {
                    TeamPartnerMapping mappings = new TeamPartnerMapping();
                    mappings.PartnerId = mappingdata.PartnerId;
                    mappings.TeamId = mappingdata.TeamId;
                    mappings.PartnerPercentage = mappingdata.PartnerPercentage ?? 0.0M;

                    var result = await _teamPartnerMappingRepository.InsertAndGetIdAsync(mappings);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return teamId;
        }

        /// <summary>
        /// This Method Is For get Team by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Team> GetTeamById(int id)
        {
            var data = await _teamRepository.GetAsync(id);
            return data;
        }
    }
}
