﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using TownSend.Documents.Dto;
using TownSend.Teams.Dto;

namespace TownSend.Teams
{
    public interface ITeamAppService : IApplicationService
    {
        Task CreateAsync(CreateTeamDto input);
        Task CreateTeamPartnerMappingAsync(TeamPartnerMapDto input);

        Task RemoveTeamPartnerMappingAsync(int teamId, int partnerId);
        Task<ListResultDto<TeamListDto>> GetAll();
        Task<ListResultDto<TeamListDto>> GetAllTeamList(DateTime? StartDate, DateTime? EndDate, int[] PartnerIds);
        
        Task<TeamDto> UpdateAsync(TeamDto input);
        Task<GetTeamForEditOutput> GetTeamForEdit(EntityDto input);
        Task<int> UpdatePartnerForTeamAsync(List<TeamPartnerEditDto> input);
        Task<Team> GetTeamById(int id);

        

    }
}
