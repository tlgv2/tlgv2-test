﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;

namespace TownSend.Teams.Dto
{
    public class CreateTeamDto : EntityDto
    {    
        public CreateTeamDto()
        {
            PartnerIds = new List<int>();            
        }
        [Required(ErrorMessage = "The field is required")]
        public string TeamName { get; set; }

        public string Description { get; set; }
        
        [Required(ErrorMessage ="The field is required")]
        public List<int> PartnerIds { get; set; }
        
        [Required(ErrorMessage = "The field is required")]
        //[GreaterThan]        
        [Range(1.0, double.MaxValue, ErrorMessage = "Team Price must be greater than 0")]
        
        public decimal TeamPrice { get; set; }
        
        [Required(ErrorMessage = "The field is required")]
        [Range(1.0, double.MaxValue, ErrorMessage = "Deposit must be greater than 0")]
        public decimal Deposit { get; set; }
        //   public bool IsPaymentRecieved { get; set; }
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "The field is required")]
        public string StringStartDate { get; set; }
        [Required(ErrorMessage = "The field is required")]
        public int StateId { get; set; }
        [Required(ErrorMessage = "The field is required")]
        public int DefaultPartnerId { get; set; }
        //public string StartDate { get; set; }

    }
    

}
