﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Teams.Dto
{
    public class TeamPartnerEditDto
    {
        public int PartnerId { get; set; }
        public string PartnerName { get; set; }
        public decimal? PartnerPercentage { get; set; }
        public int TeamId { get; set; }
    }
}
