﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Teams.Dto
{
    public class TeamPartnerMapDto 
    {
        public List<int> PartnerIds { get; set; }
        public int TeamId { get; set; }
    }
}
