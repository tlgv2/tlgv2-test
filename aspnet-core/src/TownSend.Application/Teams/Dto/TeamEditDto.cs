﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Teams.Dto
{
    public class TeamEditDto : EntityDto<int>
    {
        [Required(ErrorMessage = "The field is required")]
        public string TeamName { get; set; }
        public string Description { get; set; }
        //public int  PartnerId { get; set; }
        //public string PartnerName { get; set; }
        [Required(ErrorMessage = "The field is required")]
        [Range(1.0, double.MaxValue, ErrorMessage = "Team Price Value must be greater than 0")]
        public decimal? TeamPrice { get; set; }
        [Required(ErrorMessage = "The field is required")]
        [Range(1.0, double.MaxValue, ErrorMessage = "Deposit Value must be greater than 0")]
        public decimal? Deposit { get; set; }
        [Required(ErrorMessage = "The field is required")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "The field is required")]
        public int StateId { get; set; }
        public DateTime CreationTime { get; set; }
        public List<string> PartnerIds { get; set; }
    }
}
