﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Teams.Dto
{
    public class TeamMapProfile : Profile
    {
        public TeamMapProfile()
        {
            CreateMap<CreateTeamDto, Team>().ForMember(dest=>dest.EndDate,map=>map.MapFrom(src=>src.StartDate.AddYears(1)));
            CreateMap<TeamDto , Team>();
            CreateMap<Team, TeamDto>();
            CreateMap<Team , TeamListDto>();
            CreateMap<TeamListDto, Team>();
            CreateMap<Team , TeamEditDto>();
            CreateMap<TeamEditDto, TeamDto>();

            CreateMap<Team, TeamPartnerEditDto>();
        }
    }
}
