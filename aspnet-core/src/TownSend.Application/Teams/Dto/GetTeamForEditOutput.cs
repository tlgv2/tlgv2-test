﻿using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Common.Dto;
using TownSend.Partners.Dto;

namespace TownSend.Teams.Dto
{
    public class GetTeamForEditOutput
    {
        public TeamEditDto Team { get; set; }
        public List<int> PartnerIds { get; set; }
        public List<PartnersListForTeam> Partners { get; set; }
        public List<TeamPartnerEditDto> PartnerList { get; set; }
    }
}
