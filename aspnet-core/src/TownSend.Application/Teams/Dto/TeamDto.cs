﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Teams.Dto
{
    public class TeamDto : EntityDto<int>
    {
        public string TeamName { get; set; }
        public int StateId { get; set; }
        public string Description { get; set; }
        public decimal? TeamPrice { get; set; }
        public decimal? Deposit { get; set; }
        public bool IsPaymentRecieved { get; set; }
        public DateTime StartDate { get; set; }
    }
}
