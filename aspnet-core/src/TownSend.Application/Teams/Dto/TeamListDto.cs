﻿    using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Partners;

namespace TownSend.Teams.Dto
{
    public class TeamListDto : EntityDto
    {
        public string TeamName { get; set; }
        public List<string> Partners { get; set; }
        public DateTime StartDate { get; set; }
        public decimal TeamPrice { get; set; }
        public decimal Deposit { get; set; }
        
        public string Description { get; set; }        
        public DateTime EndDate { get; set; }
        public DateTime CreationTime { get; set; }

    }
}
