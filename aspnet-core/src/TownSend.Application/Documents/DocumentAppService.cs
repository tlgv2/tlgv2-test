﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Documents.Dto;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TownSend.Common.Dto;
using TownSend.Partners.Dto;

namespace TownSend.Documents
{
    public class DocumentAppService : TownSendAppServiceBase, IDocumentAppService
    {
        private IRepository<Document, long> _documentRepository;
        private IRepository<DocumentTeamMapping, long> _documentTeamRepository;
        private IRepository<DocumentPartnerMapping, long> _documentPartnerRepository;
        private DocumentManager _documentManager;
        private IRepository<DocumentMemberMapping, long> _documentMemberRepository;

        public DocumentAppService(
                                    IRepository<Document, long> documentRepository,
                                    IRepository<DocumentTeamMapping, long> documentTeamRepository,
                                    IRepository<DocumentPartnerMapping, long> documentPartnerRepository,
                                    DocumentManager documentManager,
                                    IRepository<DocumentMemberMapping, long> documentMemberRepository 
                                    )
        {
            _documentRepository = documentRepository;
            _documentTeamRepository = documentTeamRepository;
            _documentPartnerRepository = documentPartnerRepository;
            _documentManager = documentManager;
            _documentMemberRepository = documentMemberRepository;
        }
        public async Task<ListResultDto<DocumentListDto>> GetAllDocumentsAsync()
        {
            var documents = await _documentRepository.GetAllListAsync();
            return new ListResultDto<DocumentListDto>(ObjectMapper.Map<List<DocumentListDto>>(documents));
        }
        public async Task CreateDocument(CreateDocumentDto input)
        {
            var document = ObjectMapper.Map<Document>(input);
            var id = await _documentRepository.InsertAndGetIdAsync(document);
            var documentMapping = new DocumentMapDto() { DocumentId = id, MapId = input.MapId };
            if (input.Type==EntityType.Partners)
            {
                var resultMap = ObjectMapper.Map<DocumentPartnerMapping>(documentMapping);
                await _documentPartnerRepository.InsertAsync(resultMap);
            }
            else if (input.Type == EntityType.Teams)
            {
                var resultMap = ObjectMapper.Map<DocumentTeamMapping>(documentMapping);
                await _documentTeamRepository.InsertAsync(resultMap);
            }
            else if(input.Type==EntityType.Members)
            {
                var resultMap = ObjectMapper.Map<DocumentMemberMapping>(documentMapping);
                await _documentMemberRepository.InsertAsync(resultMap);
            }
        }

        public async Task<ListResultDto<DocumentListDto>> GetDocuments(EntityTypeMapDto input)
        {
            var documents = new List<Document>();
           if(input.Type==EntityType.Partners)
            {
                documents = await _documentManager.GetDocumentForPartnerAsync(input.MapId);
            }
           else if(input.Type==EntityType.Teams)
            {
                documents = await _documentManager.GetDocumentForTeamAsync(input.MapId);
            }
           else if(input.Type==EntityType.Members)
            {
                documents = await _documentManager.GetDocumentForMemberAsync(input.MapId);
            }



            return new ListResultDto<DocumentListDto>(ObjectMapper.Map<List<DocumentListDto>>(documents));
        }

        public async Task UpdateDocumentAsync(DocumentDto input)
        {
            var data = await _documentRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            ObjectMapper.Map(input, data);
            await _documentRepository.UpdateAsync(data);
        }
        public async Task<DocumentListDto> GetDocumentForEditAsync(int id)
        {
            var document = await _documentRepository.GetAll().Where(x => x.Id == id).FirstOrDefaultAsync();
            var result = ObjectMapper.Map<DocumentListDto>(document);
            return result;
        }

        public async Task DeleteDocumentAsync(long id,EntityType type)
        {
            var doc =await _documentRepository.GetAsync(id);
            doc.IsDeleted = true;
            if(EntityType.Partners==type)
            {
                var docPartner = await _documentPartnerRepository.FirstOrDefaultAsync(x => x.DocumentId == id);
                docPartner.IsDeleted = false;
                await _documentPartnerRepository.UpdateAsync(docPartner);
            }
            else if(EntityType.Members==type)
            {
                var docMember = await _documentMemberRepository.FirstOrDefaultAsync(x=>x.DocumentId==id);
                docMember.IsDeleted = false;
                await _documentMemberRepository.UpdateAsync(docMember);
            }
            else if (EntityType.Teams == type)
            {
                var docTeam = await _documentTeamRepository.FirstOrDefaultAsync(x=>x.DocumentId==id);
                docTeam.IsDeleted = false;
                await _documentTeamRepository.UpdateAsync(docTeam);
            }
            await _documentRepository.UpdateAsync(doc);
        }
    }

    
}
