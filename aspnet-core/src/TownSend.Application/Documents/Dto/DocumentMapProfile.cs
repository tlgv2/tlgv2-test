﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Documents.Dto
{
    public class DocumentMapProfile : Profile
    {
        public DocumentMapProfile()
        {
            CreateMap<CreateDocumentDto, Document>();
            CreateMap<Document,CreateDocumentDto>();
            CreateMap<Document, DocumentListDto>();
            CreateMap<DocumentListDto,Document > ();

            CreateMap<DocumentMapDto, DocumentPartnerMapping>().ForMember(dest=>dest.PartnerId,map=>map.MapFrom(src=>src.MapId));
            CreateMap<DocumentMapDto, DocumentTeamMapping>().ForMember(dest => dest.TeamId, map => map.MapFrom(src => src.MapId));
            CreateMap<DocumentMapDto, DocumentMemberMapping>().ForMember(dest => dest.MemberId, map => map.MapFrom(src => src.MapId));
        }
    }
}
