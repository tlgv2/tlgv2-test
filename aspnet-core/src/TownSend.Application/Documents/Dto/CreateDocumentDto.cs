﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Documents.Dto
{
    public class CreateDocumentDto 
    {
        public long MapId { get; set; }
       public EntityType Type { get; set; }
        public string Name { get; set; }
        public IFormFile DocumentFile { get; set; }
        public bool? Restrict { get; set; }
        public string Description { get; set; }

        public string DocumentPath { get; set; }

        public DateTime? VisibleDateFrom { get; set; }

        public DateTime? VisibleDateTo { get; set; }
        public Decimal Size { get; set; }
    }
}
