﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Documents.Dto
{
    public class DocumentDeleteDto
    {
        public long DocumentId { get; set; }
        //public EntityType EntityType { get; set; }
        public string entityType { get; set; }
    }
}
