﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Documents.Dto
{
    public class DocumentMapDto
    {
        public long DocumentId { get; set; }
        public long MapId { get; set; }
    }
}
