﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TownSend.Documents.Dto
{
    public class DocumentListDto : EntityDto<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public string DocumentPath { get; set; }

        [DataType(DataType.Date)]

        public DateTime? VisibleDateFrom { get; set; }
        [DataType(DataType.Date)]
        public DateTime? VisibleDateTo { get; set; }
        public Decimal Size { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
