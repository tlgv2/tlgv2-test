﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using TownSend.Documents.Dto;
using TownSend.Partners.Dto;

namespace TownSend.Documents
{
    public interface IDocumentAppService : IApplicationService
    {
        Task<ListResultDto<DocumentListDto>> GetAllDocumentsAsync();
        Task CreateDocument(CreateDocumentDto input);
        Task<ListResultDto<DocumentListDto>> GetDocuments(EntityTypeMapDto input);
        Task<DocumentListDto> GetDocumentForEditAsync(int id);

        Task UpdateDocumentAsync(DocumentDto input);
        Task DeleteDocumentAsync(long id,EntityType type);
    }
}
