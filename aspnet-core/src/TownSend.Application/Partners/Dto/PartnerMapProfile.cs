﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Authorization.Users;
using TownSend.Users.Dto;
using TownSend.Documents;
using TownSend.ProfileNotes;
using TownSend.Teams.Dto;

namespace TownSend.Partners.Dto
{
    public class PartnerMapProfile : Profile
    {
        public PartnerMapProfile()
        {
            /* This method is used for mapping Partner entities with notes, document, reference link etc*/
            //partner
            CreateMap<CreatePartnerDto, Partner>();
            CreateMap<PartnerDto, Partner>();
            CreateMap<Partner, PartnerDto>();
            CreateMap<Partner, PartnerListDto>();
            CreateMap<Partner, PartnerEditDto>().ForMember(dest=>dest.ProfilePathGet,map=>map.MapFrom(src=>src.ProfilePath));
            CreateMap<PartnerEditDto, PartnerDto>();
            //referencelink
            CreateMap<CreatePartnerRecommenedLinksDto, PartnerRecommenedLinks>();
            CreateMap<PartnerRecommenedLinks, CreatePartnerRecommenedLinksDto>();
            CreateMap<PartnerRecommenedLinks, RecommendedLinkListDto>();
            CreateMap< RecommendedLinkListDto, PartnerRecommenedLinks>();

            ////documents
            CreateMap<DocumentDto, Document>();
            CreateMap<Document, DocumentDto>();


            CreateMap<PartnerEditDto, Partner>();

            CreateMap<Partner, TeamPartnerEditDto>().ForMember(dest => dest.PartnerId, map => map.MapFrom(src => src.Id));
        }
    }
}
