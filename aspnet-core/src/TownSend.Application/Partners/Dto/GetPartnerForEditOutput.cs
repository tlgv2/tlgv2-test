﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Partners.Dto
{
    public class GetPartnerForEditOutput
    {
        public PartnerEditDto Partner { get; set; }
    }
}
