﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Partners.Dto
{
    public class GetDocumentInput
    {
        public int PartnerId { get; set; }
    }
}
