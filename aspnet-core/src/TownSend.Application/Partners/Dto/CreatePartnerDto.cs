﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using Ext.Net;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.RegularExpressions;
using TownSend.Partners;

namespace TownSend.Partners.Dto
{
    public class CreatePartnerDto  : FullAuditedEntity
    {
        [Required(ErrorMessage ="The field is required")]
        [StringLength(AbpUserBase.MaxNameLength,ErrorMessage ="Max Length is 10")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "The field is required")]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "The field is required")]
        [EmailAddress(ErrorMessage ="Please enter valid email address")]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string Email { get; set; }
        [Required(ErrorMessage = "The field is required")]
        //[ValidPhoneNumber]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "The field is required")]
        public string AddressLine1 { get; set; }        
        public string AddressLine2 { get; set; }
        [Required(ErrorMessage = "The field is required")]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Use letters only please")]
        public string City { get; set; }
        [Required(ErrorMessage = "The field is required")]
        public string State { get; set; }
        [Required(ErrorMessage = "The field is required")]
        //[MaxLength(5, ErrorMessage = "Zip Code must be 5 digits")]
        //[MinLength(5, ErrorMessage = "Zip Code must be 5 digits")]
        public int ZipCode { get; set; }

        public long UserId { get; set; }
    }
    
}
