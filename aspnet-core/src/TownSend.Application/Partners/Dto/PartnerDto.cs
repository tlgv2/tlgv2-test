﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TownSend.Partners;

namespace TownSend.Partners.Dto
{
    public class PartnerDto : EntityDto
    {
        public string FullName { get; set; }
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string FirstName { get; set; }
        
        public string MiddleName { get; set; }
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; } 
        public string City { get; set; }
        public string State { get; set; }
        public int ZipCode { get; set; }
        
        public string AboutMe { get; set; }

        public string ProfilePath { get; set; }
    }
}
