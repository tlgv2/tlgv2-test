﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Partners.Dto
{
    public class GetRecommendedLinksOutput
    {
        public List<RecommendedLinkListDto> RecommendedLinks { get; set; }
    }
}
