﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Partners.Dto
{
    public class GetDocumentOutput
    {
        public List<ProfileNoteDTO> ProfileNotes { get; set; }
    }
}
