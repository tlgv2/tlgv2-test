﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Partners.Dto
{
    public class ProfileNoteDTO : FullAuditedEntityDto
    {
        public string Description { get; set; }
        public int PartnerId { get; set; }
        public string AddedBy { get; set; }
    }
}
