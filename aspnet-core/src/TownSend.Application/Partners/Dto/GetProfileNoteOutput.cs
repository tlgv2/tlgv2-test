﻿using System;
using System.Collections.Generic;
using System.Text;
using TownSend.ProfileNotes.Dto;

namespace TownSend.Partners.Dto
{
    public class GetProfileNoteOutput
    {
        public IReadOnlyList<ProfileNoteListDto> ProfileNotes { get; set; }
    }
}
