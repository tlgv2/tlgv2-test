﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Partners.Dto
{
    public class DocumentDto 
    {
        public long Id { get; set; }
        public int MapId { get; set; }
        public string Name { get; set; }
        public EntityType Type { get; set; }
        public string Description { get; set; }
        public bool? Restrict { get; set; }
        public DateTime? VisibleDateFrom { get; set; }

        public DateTime? VisibleDateTo { get; set; }
        public string StringVisibleDateFrom { get; set; }
        public string StringVisibleDateTo { get; set; }
    }
}
