﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TownSend.Partners;
using TownSend.ProfileNotes;

namespace TownSend.Partners.Dto
{
    public class PartnerEditDto : FullAuditedEntityDto<int>
    {
        [Required(ErrorMessage = "The field is required")]
        [StringLength(AbpUserBase.MaxNameLength, ErrorMessage = "Max Length is 10")]
        public string FirstName { get; set; }
        
        [Required(ErrorMessage = "The field is required")]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string LastName { get; set; }
        
        public string MiddleName { get; set; }
       
        [Required(ErrorMessage = "The field is required")]
        [EmailAddress(ErrorMessage = "Please enter valid email address")]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string Email { get; set; }
        [Required(ErrorMessage = "The field is required")]
        //[MaxLength(10, ErrorMessage = "Please enter valid mobile number ")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "The field is required")]
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "The field is required")]
        [RegularExpression(@"^[a-zA-Z ]+$", ErrorMessage = "Use letters only please")]
        public string City { get; set; }
       
        [Required(ErrorMessage = "The field is required")]
        public string State { get; set; }
        //public string Country { get; set; }
        
        [Required(ErrorMessage = "The field is required")]
        //[MaxLength(5, ErrorMessage = "Zip Code must be 5 digits")]
        //[MinLength(5, ErrorMessage = "Zip Code must be 5 digits")]
        public int ZipCode { get; set; }

        public string AboutMe { get; set; }
        
        public string FullName { get; set; }
        
        public List<ProfileNote> ProfileNotes { get; set; }

        public string ProfilePathGet { get; set; }

        public string ProfilePathSet { get; set; }
    }
}
