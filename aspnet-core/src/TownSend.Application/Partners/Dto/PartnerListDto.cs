﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TownSend.Partners;

namespace TownSend.Partners.Dto
{

    public class PartnerListDto : EntityDto
    {
        public string FullName { get;set; }
        public long PhoneNumber { get; set; }
        public string Email { get; set; }

      
    }
}
