﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Partners.Dto
{
    public class CreatePartnerRecommenedLinksDto
    {
        public int PartnerId { get; set; }
        public string LinkType { get; set; }
        public string Description { get; set; }
        public string LinkUrl { get; set; }

        public string LinkName { get; set; }
    }
}
