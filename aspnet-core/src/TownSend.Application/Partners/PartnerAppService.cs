﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TownSend.Authorization.Roles;
using TownSend.Authorization.Users;
using TownSend.Partners.Dto;
using TownSend.Partners;
using TownSend.Users;
using TownSend.Users.Dto;
using TownSend.Url;
using Abp.Authorization;
using TownSend.Authorization;
using Abp.Domain.Uow;
using TownSend.Documents;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using TownSend.BlobUtilitys;
using TownSend.AuditHistory;
using Abp.Collections.Extensions;
using TownSend.ProfileNotes;

namespace TownSend.Partners
{
    [AbpAuthorize(PermissionNames.Pages_Partners)]
    public class PartnerAppService : TownSendAppServiceBase, IPartnerAppService
    {
        #region Fields
        public IAppUrlService AppUrlService { get; set; }
        private IRepository<PartnerRecommenedLinks> _partnerRecommenendRepository;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly UserAppService _userAppService;
        private readonly PartnerManager _partnerManager;
        private readonly IRepository<Document, long> _documentRepository;
        private readonly IRepository<ProfileNote, int> _profileNoteRepository;
        private readonly IRepository<User, long> _userRepository;
        #endregion

        #region Ctor
        public PartnerAppService(
                                    IRepository<Partner> partnerRepository,
                                    IRepository<Document, long> documentRepository,
                                    RoleManager roleManager,
                                    UserManager userManager,
                                    UserAppService userAppService,
                                    PartnerManager partnerManager,
                                    IRepository<PartnerRecommenedLinks> partnerRecommenendRepository,
                                   IRepository<ProfileNote, int> profileNoteRepository,
                                    IRepository<User, long> userRepository
                                    )
        {
            _partnerRepository = partnerRepository;
            _roleManager = roleManager;
            _userManager = userManager;
            _userAppService = userAppService;
            _partnerManager = partnerManager;
            _documentRepository = documentRepository;
            AppUrlService = NullAppUrlService.Instance;
            _partnerRecommenendRepository = partnerRecommenendRepository;
            _profileNoteRepository = profileNoteRepository;
            _userRepository = userRepository;
        }
        #endregion

        #region Partner

        /// <summary>
        /// This Method Creates Partner and also Creates User and map to partner
        /// Affected Entities
        ///     Partners
        ///     User
        ///     UserRole
        ///     Role
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(PermissionNames.Pages_Partners_Create)]
        public async Task CreateAsync(CreatePartnerDto input)
        {
            var data = ObjectMapper.Map<Partner>(input);

            //register user and partner
            var partner = await _partnerManager.RegisterAsync(
                AbpSession.TenantId,
                input.FirstName,
                input.LastName,
                input.Email,
                input.PhoneNumber,
                "Abc@123",
                false,
                AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Partner)?.Id,
                data
            );
        }
        /// <summary>
        /// This Method Take Partner Details To Update Partner
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(PermissionNames.Pages_Partners_Edit)]
        public async Task<PartnerDto> UpdateAsync(PartnerDto input)
        {
            var data = await _partnerRepository.GetAsync(input.Id);
            if (data.Email != input.Email)
            {
                await _partnerManager.ChangeEmailAsync(data.UserId, input.Email, AppUrlService.ChangeEmailUrlFormat(AbpSession.TenantId));

            }
            ObjectMapper.Map(input, data);
            if (input.ProfilePath == null)
            {
                data.ProfilePath = null;
            }
            await _partnerRepository.UpdateAsync(data);
            return input;
        }
        /// <summary>
        /// This Mathod Gets All Partners Value In Index Page
        /// </summary>
        /// <returns></returns>
        public async Task<ListResultDto<PartnerListDto>> GetAll()
        {
            List<Partner> result = new List<Partner>();
            if (IsGranted("Pages.Partners.List"))
            {
                result = await _partnerRepository.GetAll().ToListAsync();
            }
            else
            {
                result = await (from partner in _partnerRepository.GetAll()
                                join user in _userRepository.GetAll() on partner.UserId equals user.Id
                                where user.Id == AbpSession.UserId
                                select partner).ToListAsync();
            }


            return new ListResultDto<PartnerListDto>(ObjectMapper.Map<List<PartnerListDto>>(result));
        }
        /// <summary>
        /// This Method Get Partner Details By PartnerId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<PartnerDto> GetAsync(int id)
        {
            var data = await _partnerRepository.GetAsync(id);
            var result = ObjectMapper.Map<PartnerDto>(data);
            return result;
        }
        /// <summary>
        /// This Method Remove Partner From PartnerListing And Also Delete User
        /// Affected Entities
        ///     Partners
        ///     User
        ///     UserRole
        ///     Role
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AbpAuthorize(PermissionNames.Pages_Partners_Delete)]
        [UnitOfWork(isTransactional: false)]
        public async Task Delete(EntityDto input)
        {
            try
            {
                var partner = _partnerRepository.Get(input.Id);
                var result = ObjectMapper.Map<Partner>(partner);
                result.IsDeleted = true;

                var data = await _partnerRepository.UpdateAsync(result);
                var userpartner = _userManager.GetUserById(result.UserId);
                userpartner.IsDeleted = true;
                userpartner.IsActive = false;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// This Method Take PartnerId As Input And Display PartnerInfo
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<GetPartnerForEditOutput> GetPartnerForEdit(EntityDto input)
        {
            var data = await _partnerRepository.GetAsync(input.Id);
            var dataEditDto = ObjectMapper.Map<PartnerEditDto>(data);

            return new GetPartnerForEditOutput
            {
                Partner = dataEditDto
            };
        }

        #endregion

        #region Notes
        /// <summary>
        /// This Method Take ProfileNote For Edit In PartnerProfile
        /// Affected Entities
        ///     Partners
        ///     ProfileNote
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task EditProfileNote(ProfileNoteDTO input)
        {
            var result = ObjectMapper.Map<ProfileNote>(input);
            await _profileNoteRepository.UpdateAsync(result);
        }
        #endregion

        #region Documents
        /// <summary>
        /// This Method Take Document For Edit In PartnerDocument
        /// Affected Entities
        ///     Partners
        ///     Documents
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task UpdateDocumentAsync(DocumentDto input)
        {
            var data = await _documentRepository.GetAll().Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            ObjectMapper.Map(input, data);
            await _documentRepository.UpdateAsync(data);
        }
        /// <summary>
        /// This Method Remove Document From PartnersDocument
        /// Affected Entities
        ///     Partners
        ///     Documents
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteDocumentAsync(int id)
        {
            var result = await _documentRepository.GetAsync(id);
            result.IsDeleted = true;
            await _documentRepository.UpdateAsync(result);
        }
        #endregion

        #region Recommended Link
        /// <summary>
        /// This Method Get All RecommendedLinks For Particular Partner and Display It.  
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<GetRecommendedLinksOutput> GetRecommendedLinksByPartnerId(EntityDto input)
        {
            var result = await (from links in _partnerRecommenendRepository.GetAll()
                                join partner in _partnerRepository.GetAll() on links.PartnerId equals partner.Id
                                where links.PartnerId == input.Id
                                select new RecommendedLinkListDto
                                {
                                    Id = links.Id,
                                    Description = links.Description,
                                    LinkName = links.LinkName,
                                    LinkType = links.LinkType,
                                    LinkUrl = links.LinkUrl,
                                    PartnerId = links.PartnerId

                                }).ToListAsync();
            return new GetRecommendedLinksOutput
            {
                RecommendedLinks = result
            };
        }
        /// <summary>
        /// This Method Create RecommendedLinks For Partner
        ///  Affected Entities
        ///    Partners
        ///    PartnerRecommendedLink
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task CreateLinks(CreatePartnerRecommenedLinksDto input)
        {
            var result = ObjectMapper.Map<PartnerRecommenedLinks>(input);
            await _partnerRecommenendRepository.InsertAsync(result);
        }
        /// <summary>
        /// This Method Remove RecommenendLinks From Partner
        ///  Affected Entities
        ///    Partners
        ///    PartnerRecommendedLink
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task DeleteLinks(EntityDto input)
        {
            var link = await _partnerRecommenendRepository.GetAsync(input.Id);
            link.IsDeleted = true;
            await _partnerRecommenendRepository.UpdateAsync(link);
        }
        #endregion

    }
}

