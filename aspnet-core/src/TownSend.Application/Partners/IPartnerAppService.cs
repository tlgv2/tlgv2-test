﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Partners.Dto;
using TownSend.Partners;
using Microsoft.AspNetCore.Http;

namespace TownSend.Partners
{
    public interface IPartnerAppService : IApplicationService
    {
        Task<ListResultDto<PartnerListDto>> GetAll();

        Task CreateAsync(CreatePartnerDto input);
        Task<PartnerDto> UpdateAsync(PartnerDto input);

        Task<PartnerDto> GetAsync(int id);
        Task Delete(EntityDto id);

        Task<GetPartnerForEditOutput> GetPartnerForEdit(EntityDto input);
      
        Task<GetRecommendedLinksOutput> GetRecommendedLinksByPartnerId(EntityDto input);
        Task CreateLinks(CreatePartnerRecommenedLinksDto input);

        Task DeleteLinks(EntityDto input);

              
       
        Task UpdateDocumentAsync(DocumentDto input);
        Task DeleteDocumentAsync(int id);

    }
}
