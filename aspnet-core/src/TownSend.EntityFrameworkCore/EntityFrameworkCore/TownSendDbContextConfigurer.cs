using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace TownSend.EntityFrameworkCore
{
    public static class TownSendDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<TownSendDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<TownSendDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
