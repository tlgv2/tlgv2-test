﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using TownSend.Authorization.Roles;
using TownSend.Authorization.Users;
using TownSend.MultiTenancy;
using TownSend.Partners;
using TownSend.Members;
using TownSend.States;
using TownSend.Invites;
using TownSend.Documents;
using TownSend.AuditHistory;
using TownSend.Teams;
using TownSend.Scholarships;
using TownSend.Templates;
using TownSend.ProfileNotes;

namespace TownSend.EntityFrameworkCore
{
    public class TownSendDbContext : AbpZeroDbContext<Tenant, Role, User, TownSendDbContext>
    {
        /* Define a DbSet for each entity of the application */

        //dev-1 changes
        public DbSet<EmailType> EmailTypes { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<ScholarshipInviteMapping> ScholarshipInviteMapping { get; set; }
        public DbSet<ScholarshipAuditHistory> ScholarshipAuditHistory { get; set; }
        public DbSet<ScholarshipStatus> ScholarshipStatus { get; set; }
        public DbSet<Scholarship> Scholarships { get; set; }
        public DbSet<TeamPartnerMapping> TeamPartnerMappings { get; set; }
       
        public DbSet<ProfileNote> ProfileNotes { get; set; }
        public DbSet<Invite> Invites { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamAuditHistory> TeamAuditHistory { get; set; }

        public DbSet<Document> Documents { get; set; }        
        public DbSet<PartnerRecommenedLinks> PartnerRecommenedLinks { get; set; }
        public DbSet<PartnerAuditHistory> PartnerAuditHistory { get; set; }
        public DbSet<TeamPartnerAuditHistoryMapping> TeamPartnerAuditHistoryMapping { get; set; }
        public DbSet<DocumentTeamMapping> DocumentTeamMappings { get; set; }
        public DbSet<DocumentPartnerMapping> DocumentPartnerMappings { get; set; }

        public DbSet<ProfileNotePartnerMapping> NotePartnerMappings { get; set; }
        public DbSet<ProfileNoteTeamMapping> NoteTeamMappings { get; set; }
        public DbSet<MemberInviteMapping> MemberInviteMappings { get; set; }
        public DbSet<MemberTeamMapping> MemberTeamMappings { get; set; }

        public DbSet<ProfileNoteMemberMapping> ProfileNoteMemberMappings { get; set; }

        public DbSet<PaymentMode> PaymentModes { get; set; }
        public DbSet<MemberPrice> MemberPrices { get; set; }
        public DbSet<MemberPaymentModeMapping> MemberPaymentModeMappings { get; set; }
        public DbSet<MemberPaymentModeDetail> MemberPaymentModeDetails { get; set; }

        public DbSet<MemberStatus> MemberStatus { get; set; }

        public DbSet<DocumentMemberMapping> DocumentMemberMappings { get; set; }
        public TownSendDbContext(DbContextOptions<TownSendDbContext> options)
            : base(options)
        {
        }
    }
}
