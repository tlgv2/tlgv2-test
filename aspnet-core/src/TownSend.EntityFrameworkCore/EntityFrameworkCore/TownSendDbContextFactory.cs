﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using TownSend.Configuration;
using TownSend.Web;

namespace TownSend.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class TownSendDbContextFactory : IDesignTimeDbContextFactory<TownSendDbContext>
    {
        public TownSendDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TownSendDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            TownSendDbContextConfigurer.Configure(builder, configuration.GetConnectionString(TownSendConsts.ConnectionStringName));

            return new TownSendDbContext(builder.Options);
        }
    }
}
