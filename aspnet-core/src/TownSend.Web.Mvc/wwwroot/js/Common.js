﻿
$('#cancel-edit-info').on("click", function () {
    location.reload();
});

$('#cancel-edit-info-partner').on("click", function () {
    location.reload();
});

$('.modal').on('hidden.bs.modal', function () {
    if ($("#CheckEmailValidation")) {
        $("#CheckEmailValidation").html('');
    }
    if ($("#PartnerForm")) {
        $(".field-validation-error").empty();
    }
    if ($("#checkpriceanddeposit")) {
        $("#checkpriceanddeposit").empty();
    }
    if ($("#CreateTeamForm"))
        {
        $(".field-validation-error").empty();
    }
    $(this)
        .find("input[type='text'],input[type=number],input[type=date],input[type=email],input[type=datetime],textarea,select")
        .val('')
        .end()
        ;
    $("label.error").hide();
    $(".error").removeClass("error");


    
    
    
})


function confirmDelete(callback) {
    bootbox.confirm({
        message: "<h3>Are you sure, You want to delete this record...!!</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) callback();
        }
    });
}

function confirmScholarshipApprove(callback) {
    bootbox.confirm({
        title: "Confirm?",
        message: "<h3>Are you sure, You want to approve this scholarship?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) callback();
        }
    });
}

function confirmScholarshipDenied(callback) {
    bootbox.confirm({
        title: "Confirm?",
        message: "<h3>Are you sure, You want to deny this scholarship?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) callback();
        }
    });
}

function confirmScholarshipCancel(callback) {
    bootbox.confirm({
        title: "Confirm?",
        message: "<h3>Are you sure, You want to cancel this scholarship?</h3>",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) callback();
        }
    });
}

$.each($.validator.methods, function (key, value) {
    $.validator.methods[key] = function () {
        if (arguments.length > 0) {
            arguments[0] = $.trim(arguments[0]);
        }

        return value.apply(this, arguments);
    };
});
$('.calldatepickerStart').datepicker({
    dateFormat: 'mm-dd-yy',
    onSelect: function (selected) {
        var dateforVisibleFrom = moment(selected).format('MM-DD-YYYY');
        var dt = new Date(dateforVisibleFrom);
        console.log(dt.getDate());
        dt.setDate(dt.getDate() + 1);
        $('.calldatepickerEnd').datepicker("option", "minDate", dt);
    }
}).attr("readonly", "true").
    keypress(function (event) {
        if (event.keyCode == 8) {
            event.preventDefault();
        }
    });
$('.calldatepickerEnd').datepicker({
    dateFormat: 'mm-dd-yy',
    onSelect: function (selected) {
        var dateforVisibleTo = moment(selected).format('MM-DD-YYYY');
        var dt = new Date(dateforVisibleTo);
        dt.setDate(dt.getDate() - 1);
        $('.calldatepickerStart').datepicker("option", "maxDate", dt);
    }
}).attr("readonly", "true").
    keypress(function (event) {
        if (event.keyCode == 8) {
            event.preventDefault();
        }
    });

$(".calldatePiker").datepicker({
    dateFormat: 'mm-dd-yy',
    minDate: 0,
}).attr("readonly", "true").
    keypress(function (event) {
        if (event.keyCode == 8) {
            event.preventDefault();
        }
    });

//$('input[name="StringVisibleDateTo"]').datepicker({
//    dateFormat: 'mm-dd-yy',
//    minDate: 0,
//    onSelect: function (selected) {
//        var dateforVisibleTo = moment(selected).format('MM-DD-YYYY');
//        var dt = new Date(dateforVisibleTo);
//        dt.setDate(dt.getDate() - 1);
//        //selected.setDate(selected.getDate() - 1);
//        $('input[name="StringVisibleDateFrom"]').datepicker("option", "maxDate", dt);
//    }
//}).attr("readonly", "true").
//    keypress(function (event) {
//        if (event.keyCode == 8) {
//            event.preventDefault();
//        }
//    });


    //$('input[name="StringVisibleDateFrom"]').datepicker({
    //    dateFormat: 'mm-dd-yy',
    //    minDate: 0,
    //    onSelect: function (selected) {
    //        var dateforVisibleFrom = moment(selected).format('MM-DD-YYYY');
    //        var dt = new Date(dateforVisibleFrom);
    //        console.log(dt.getDate());
    //        dt.setDate(dt.getDate() + 1);
    //        //selected.setDate(selected.getDate() + 1);
    //        $('input[name="StringVisibleDateTo"]').datepicker("option", "minDate", dt);
    //    }
    //}).attr("readonly", "true").
    //    keypress(function (event) {
    //        if (event.keyCode == 8) {
    //            event.preventDefault();
    //        }
    //    });

$("#Zipcode").keydown(function (event) {


    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
    //if a decimal has been added, disable the "."-button

});

function formatPhoneNumber(phoneNumberString) {
    var cleaned = ('' + phoneNumberString).replace(/\D/g, '')
    var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/)
    if (match) {
        return '(' + match[1] + ') ' + match[2] + '-' + match[3]
    }
    return null
}