﻿$(document).ready(function () {
    $('#teamInfo').ViewLoader();
    //$('#TitleInfo').ViewLoader();
    //$('#SupplierDetails').ViewLoader();
    //$('#TeamMeetings').ViewLoader();
    //$('#TeamMembers').ViewLoader();
    //$('#TeamDocuments').ViewLoader();
    //$('#TeamTramsactions').ViewLoader();
   // $('#TeamNotes').ViewLoader();
    $('#notes-tab').on('click', function () {
       
        $('#TeamNoteForm').submit();
        //$('#TeamNotes').ViewLoader();
    });

    $('#meeting-tab').on('click', function () {
        $('#TeamMeetings').ViewLoader();
    });

    $('#member-tab').on('click', function () {
        $('#TeamMembers').ViewLoader();
    });

    $('#documents-tab').on('click', function () {
        $('#TeamDocuments').ViewLoader();
    });

    $('#transactions-tab').on('click', function () {
        $('#TeamTramsactions').ViewLoader();
    });

    $('#TeamDocuments').on('click', ".btn-download", function (event) {
        event.preventDefault();

        downloadFile(event);
    });

    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        var myDropzone = new Dropzone("#uploader", {
            paramName: "file",
            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 5,
            maxFiles: 5,
            maxFilesize: 500,
            timeout: 3000000,
            init: function () {

                var myDropzone = this;

                $("#save-documents").click(function (e) {
                    if (myDropzone.files.length > 0) {
                        // allow to upload file
                        $("#overlay").show();
                        myDropzone.processQueue();
                    }
                    else {
                        $("#alert-upload-empty").toggleClass("d-none");
                    }
                });

                this.on("sending", function (file, xhr, formData) {
                    let restrictVisibility = $("#RestrictedVisibility").is(':checked');
                    let visibleDateFrom = $("#VisibleDateFrom").val();
                    let visibleDateTo = $("#VisibleDateTo").val();
                    let contextCoreName = $("#context-core-name").val();
                    let contextLinkId = $("#context-link-id").val();
                    let teamID = $("#team-id").val();

                    formData.append("RestrictedVisibility", restrictVisibility);
                    formData.append("VisibleDateFrom", visibleDateFrom);
                    formData.append("VisibleDateTo", visibleDateTo);
                    formData.append("ContextCoreName", contextCoreName);
                    formData.append("ContextLinkID", contextLinkId);
                    formData.append("TeamID", teamID);
                });

                this.on("success", function (file, data) {
                    $("#overlay").hide();
                   // $("#alert-upload-success").toggleClass("d-none");
                    Site.onModalSuccess(data);
                    $('#FileUploadModal').modal('toggle');
                    $('#TeamDocuments').ViewLoader('refresh');
                });

                this.on("canceled", function (file, data) {
                    $("#overlay").hide();
                    $("#alert-upload-success").toggleClass("d-none");
                    Site.onModalSuccess(data);
                });

                this.on("error", function (file, data) {
                    $("#overlay").hide();
                    $("#alert-upload-error").toggleClass("d-none");
                });

                this.on("queuecomplete", function (file) {
                    this.removeAllFiles();
                    $("#overlay").hide();
                });
            }
        });
    });

    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        $("#ui-datepicker-div").hide();
    });

    $("#SupplierDetails").on("click", "#save-partner-percentage", function () {
        UpdatePartnerPercentages();
    });

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});
function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();


    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    $(button).closest(".content-container").find(".info").prop("disabled", function (i, v) { return !v; });
    $(button).closest(".content-container").find(".input-group").find(".btn").prop("disabled", function (i, v) { return !v; });
    $(".dropdown-toggle").toggleClass("disabled");

}
function downloadFile(event) {
    Site.onDownloadActionLink(event, { withUrlParamOverride: null });
}

function UpdatePartnerPercentages() {
    let parameters = [];

    let supplierIDs = $(".supplierID");

    supplierIDs.each(function () {
        let SupplierID = $(this).val();

        let SupplierPercentage = $(this).parent().find(".supplierPercentage");

        let dict = {
            "SupplierID": SupplierID,
            "SupplierPercentage": SupplierPercentage.val()
        };

        parameters.push(dict);
    });

    let productID = $("#productID").val();

    //$.ajax({
    //    type: "POST",
    //    url: '/TeamProfile/TeamProfile?handler=UpdateSupplierPercentages',
    //    dataType: "json",
    //        data: { parameters: parameters, ProductID: productID},
    //    headers: {
    //        RequestVerificationToken:
    //            $('input:hidden[name="RequestVerificationToken"]').val()
    //    }
    //}).done(function (result) {
    //    Site.onModalSuccess(result);
    //    if (result.isSuccess === true) {
    //        setTimeout(function () { location.reload(); }, 1700);
    //    } else {
    //        $(".supplierPercentage").addClass("red-line");
    //    }

    //}).fail(function (error) {
    //    showErrorMessage('#ajaxError');
    //});
}