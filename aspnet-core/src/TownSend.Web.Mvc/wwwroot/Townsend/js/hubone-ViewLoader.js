﻿;
(function ($) {
    var pluginName = 'ViewLoader';

    function Plugin(element, options) {
        var el = element;
        var $el = $(element);

        var defaultOnSuccess = function (response) {
            $el.html(response);

            $('[data-dynamic-view-loader=true]:not(.view-loaded)', $el).ViewLoader();
        };

        var defaultOnError = function (request, status, error) {
            $el.html('<div class="alert alert-danger">' + options.errorMessage + '<div>');
        };

        options = $.extend({}, $.fn[pluginName].defaults, options);

        function init() {

            if (options.url === null && $el.data().hasOwnProperty('url')) {
                options.url = $el.data('url');
            }

            if (options.data === null && $el.data().hasOwnProperty('provider')) {
                options.data = window[$el.data('provider')](el);
            }

            if ($el.data().hasOwnProperty('form')) {
                options.dataForm = $el.data('form');
            }

            if ($el.data().hasOwnProperty('dataType')) {
                options.dataType = $el.data('dataType');
            }

            if ($el.data().hasOwnProperty('errorMessage')) {
                options.errorMessage = $el.data('errorMessage');
            }

            if ($el.data().hasOwnProperty('isDatatable')) {
                options.isDatatable = $el.data('isDatatable');
            }

            if ($el.data().hasOwnProperty('type')) {
                options.type = $el.data('type');
            }

            if (options.onSuccess === null) {
                options.onSuccess = defaultOnSuccess;
            }

            if (options.onError === null) {
                options.onError = defaultOnError;
            }

            load();

            hook('onInit');
        }

        function load() {
            var $loading;

            //create and append the spinner
            $loading = $('<div id="loading" style="display: flex; align-items: center; justify-content: center; z-index: 1000"></div>');
            var spinner = '<div id="spinner" style="text-align: center;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></div>';
            $loading.append(spinner);

            if ($el.hasClass('view-loaded')) {
                $loading.width($el.width());
                $loading.height($el.height());
                $loading.css('position', 'absolute');
                //$loading.css('left', $el.position().left);
                //$loading.css('top', $el.position().top);
                $loading.css('background-color', 'rgb(128,128,128, 0.3)');
            }
            else {
            }

            $el.prepend($loading);
            $el.addClass('view-loaded');

            if (options.dataForm !== null) {

                var $form = $(options.dataForm);
                if ($form.length === 1) {
                    var validationInfo = $form.data("unobtrusiveValidation");
                    if (!validationInfo || !validationInfo.validate || validationInfo.validate()) {
                        var method = 'post';
                        if (!$form.data().hasOwnProperty('ajaxMethod')) {
                            $form.attr('data-ajax-method', 'post');
                            $form.data('ajaxMethod', 'post)');
                        }
                        else {
                            method = $form.data('ajaxMethod');
                        }

                        if (!$form[0].hasAttribute("method") || $form.attr(method) !== method) {
                            if ($form.attr(method) !== undefined)

                            $form.attr('method', method);
                        }

                        $form.submit();
                    }
                }
                else {
                }
            }
            else {
                //prepare ajax call options
                var ajaxOptions = {
                    url: options.url,
                    type: options.type,
                    dataType: options.dataType,
                    processData: false,
                    contentType: false,
                    headers: {
                        RequestVerificationToken:
                            $('input:hidden[name="RequestVerificationToken"]').val()
                    },
                    success: options.onSuccess,
                    error: options.onError
                };

                //prep and append the data param if any.
                var dataObject = options.data;

                var dataFunctionParams = {};
                if (typeof options.data === 'function') {
                    dataFunctionParams = options.data();
                }

                var dataAttributeParams = {};
                $.each($el.data(), function (key, value) {
                    if (key.startsWith("param")) {
                        dataAttributeParams[key.substring(5, 6).toLowerCase() + key.substring(6, key.length)] = value;
                    }
                });

                if (!$.isEmptyObject(dataFunctionParams) || !$.isEmptyObject(dataAttributeParams)) {
                    if ($.isEmptyObject(dataObject)) {
                        dataObject = {};
                    }

                    if (!$.isEmptyObject(dataFunctionParams)) {
                        $.extend(dataObject, dataFunctionParams);
                    }

                    if (!$.isEmptyObject(dataAttributeParams)) {
                        $.extend(dataObject, dataAttributeParams);
                    }
                }


                if (!$.isEmptyObject(dataObject)) {
                    var formData = new FormData();

                    $.each(dataObject, function (key, value) {
                        formData.append(key, value);
                    });

                    $.extend(ajaxOptions, { data: formData });
                }

                //do ajax call
                $.ajax(ajaxOptions).done(function () {

                    $loading.remove();

                    hook('onLoaded');

                    if (typeof options['initializeComponents'] === 'function') {
                        options.initializeComponents(el);
                    }

                    if ($el.data().hasOwnProperty('loadedCallback')) {
                        window[$el.data('loadedCallback')](el, el);
                    }
                });
            }
        }

        function refreshPublic() {
            load();
        }

        function option(key, val) {
            if (val) {
                options[key] = val;
            } else {
                return options[key];
            }
        }

        function destroy() {
            $el.each(function () {
                var el = this;
                var $el = $(this);

                // Add code to restore the element to its original state...
                $el.removeClass('view-loaded');

                $el.empty();

                hook('onDestroy');
                $el.removeData('plugin_' + pluginName);
            });
        }

        function hook(hookName) {
            if (options[hookName] !== undefined) {
                options[hookName].call(el, el);
            }
        }

        init();

        return {
            option: option,
            destroy: destroy,
            refresh: refreshPublic
        };
    }

    $.fn[pluginName] = function (options) {
        if (typeof arguments[0] === 'string') {
            var methodName = arguments[0];
            var args = Array.prototype.slice.call(arguments, 1);
            var returnVal;
            this.each(function () {
                if ($.data(this, 'plugin_' + pluginName) && typeof $.data(this, 'plugin_' + pluginName)[methodName] === 'function') {
                    returnVal = $.data(this, 'plugin_' + pluginName)[methodName].apply(this, args);
                } else {
                    throw new Error('Method ' + methodName + ' does not exist on jQuery.' + pluginName);
                }
            });
            if (returnVal !== undefined) {
                return returnVal;
            } else {
                return this;
            }
        } else if (typeof options === "object" || !options) {
            return this.each(function () {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
                }
            });
        }
    };

    $.fn[pluginName].defaults = {
        onInit: function () { },
        onLoaded: function () { },
        onDestroy: function () { },
        onSuccess: null,
        onError: null,
        initializeComponents: null,
        data: null,
        dataForm: null,
        dataType: 'html',
        errorMessage: 'An unexpected error has occured, please try reloading the page.',
        isDatatable: null,
        type: 'POST',
        url: null
    };

})(jQuery);