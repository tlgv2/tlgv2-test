﻿/*eslint-disable */
$(document).ready(function () {
    $('#ExpenseList').ViewLoader();
    $('#ExpenseDetail').ViewLoader();
    $('#ExpenseDocuments').ViewLoader();

    $('#ExpenseDocuments').on('click', ".btn-download", function (event) {
        event.preventDefault();

        downloadFile(event);
    });


    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        var myDropzone = new Dropzone("#uploader", {
            paramName: "file",
            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            maxFilesize: 500,
            timeout: 3000000,
            init: function () {

                var myDropzone = this;
                
                myDropzone.on("maxfilesexceeded", function (file) {
                    $("#alert-upload-error").toggleClass("d-none");
                    $("#alert-max-file").toggleClass("d-none");
                  
                    this.removeFile(file);
                    //("#overlay").hide();
                });
                $("#save-documents").click(function (e) {
                   
                   
                    if (myDropzone.files.length > 0) {

                        if ($("#Description").val().length != '0' || $("#Description").val() != '') {

                            // allow to upload file
                            $("#overlay").show();
                            myDropzone.processQueue();
                        }
                        else {
                            $("#alert-desc-empty").toggleClass("d-none");
                            $("#overlay").hide();
                        }
                    }
                    else {
                        $("#alert-upload-empty").toggleClass("d-none");
                    }
                  
                });

                this.on("sending", function (file, xhr, formData) {
                   
                    let contextLinkId = $("#context-link-id").val();
                    let Description = $("#Description").val();

                    formData.append("ContextLinkID", contextLinkId);
                    formData.append("Description", Description);
                });

                this.on("success", function (file, data) {
                    $("#overlay").hide();
                    Site.onModalSuccess(data);
                    $('#FileUploadModal').modal('toggle');
                    $('#ExpenseDocuments').ViewLoader('refresh');
                });

                this.on("error", function (file, data) {
                   
                    console.log(data);
                    $("#overlay").hide();
                    $("#alert-upload-error").toggleClass("d-none");
                });

                this.on("queuecomplete", function (file) {
                    this.removeAllFiles();
                    $("#overlay").hide();
                });
            }
        });
    });
});
function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();


    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
   $(".info").prop("disabled", function (i, v) { return !v; });
    $(".hubone-date-picker").toggleClass("disabled");
}
function downloadFile(event) {
    Site.onDownloadActionLink(event, { withUrlParamOverride: null });
}