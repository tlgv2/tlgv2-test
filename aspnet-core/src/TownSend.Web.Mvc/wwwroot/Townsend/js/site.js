﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.


$(document).ready(function () {
    $('#customer-listing-table tr').click(function () {
        var href = $(this).find("a").attr("href");
        if (href) {
            window.location = href;
        }
    });

    if (jQuery().datetimepicker) {
        jQuery.datetimepicker.setLocale('en');
    }

    // enabling dynamic anchor tags
    dynamicAnchorTags();

    Site.hookActionLinks();
    Site.hookDownloadActionLinks();
    Site.hookModalLinks();
    Site.applyDefaultUnobtrusiveCallbacks();
    Site.initDatePickers();
    Site.initDateTimePickers();
    Site.initSelectPickers();

    //$.prototype.ViewLoader.defaults.initializeComponents = function (element) { initializeComponents(element); };
    //$.fn.dataTable.ext.classes.sFilterInput = 'form-control form-control-sm';
    //$.fn.dataTable.ext.classes.sLengthSelect = 'form-control form-control-sm';

    ////extend the search filter from datatables to include text boxes and selects.
    //$.fn.dataTableExt.ofnSearch['is-input'] = function (value) {
    //    return $(value).val();
    //};

    ///* Create an array with the values of all the checkboxes in a column */
    //$.fn.dataTable.ext.order['dom-checkbox'] = function (settings, col) {
    //    return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
    //        return $('input', td).prop('checked') ? '1' : '0';
    //    });
    //};

    ///* Create an array with the values of all the input boxes in a column */
    //$.fn.dataTable.ext.order['dom-text'] = function (settings, col) {
    //    return this.api().column(col, { order: 'index' }).nodes().map(function (td, i) {
    //        return $('input', td).val();
    //    });
    //};

    $('#btn-customer-filter-to').click(function () {
        $('#customer-filter-to').datepicker('show');
    });

    $('#customer-filter-from').datepicker({
        changeMonth: true,
        changeYear: true
    });

    $('#btn-customer-filter-from').click(function () {
        $('#customer-filter-from').datepicker('show');
    });

    $('#customer-filter-to').datepicker({
        changeMonth: true,
        changeYear: true
    });
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});
//to open the submenu
function openSubMenu(liId) {
    var href = $('a', '#' + liId).attr('href'); //href is the #panel-name ex: #mm-1
    $("#my-menu").data('mmenu').openPanel($(href)[0]);
    $("#my-menu").data('mmenu').open();
}

/**
 * This function works on anchor tags "<a>" that are children of a form.
 * This function will take all data fields and append them as query parameters.
 * The anchor tag needs to have the "dynamic-href" attribute set with the desired endpoint
 * 
 * ****************************************   NOTE   ****************************************
 * Supported tags:
 * -INPUT (without list attribute)
 * -SELECT 
 * If more needed, add a new case in the swich block
 * 
 * *************************************!!! ATENTION !!!*************************************
 * This function assumes that the form elements were created from a C# model object 
 * using Razor HTML helpers. Thus it expects and will pick up only elements 
 * which have their name attribute following this pattern "<object_name>.<property_name>". 
 * The function then appends a query parameter of the following form "&<property_name>=<value>".
 * */
function dynamicAnchorTags() {
    $('a[dynamic-href]').on('click', function (event) {
        // checking if href attribute is present and adding it if not present.
        // function will fail at first click if no href attribute is present
        if ($(this).attr('href') === undefined) {
            $(this).attr('href', '#');
        }
        
        var endpoint = $(this).attr('dynamic-href').trim();

        if (endpoint === undefined || endpoint === null || endpoint === '') {
            console.error('Dynamic anchor tag has no endpoint specified');
            return;
        }
        
        var queryParams = '';

        // get containing form
        var form = $(this).closest('form');

        if (form === undefined || form === null || form.length === 0) {
            console.error('Dynamic anchor is not contained in a form');
            return;
        }

        // loop through all form data fields
        $(form).find('[name*="."]').each(function () {
            var element = 0;
            var propertyName = 1;

            // determine element tag type, form and append param
            switch ($(this).get(element).tagName) {
                case 'INPUT':
                    if ($(this).is('[list]')) {
                        // datalist support not implemented
                    } else {
                        queryParams += '&' + $(this).get(element).name.split('.')[propertyName] + '=' + $(this).val();
                    }
                    break;
                case 'SELECT':
                    var selectId = $(this).get(element).name.split('.')[propertyName];
                    $(this).children('option:checked').each(function () {
                        queryParams += '&' + selectId + '=' + $(this).val();
                    });
                    break;
                default:
                    break;
            }
        });
        $(this).attr('href', endpoint + queryParams);
        console.log('Sent GET request from dynamic anchor tag');
    });
}

//Get Filters Parameters functions
function getParameters(name)
{
    var params = "";

    $(name).each(function (index, value) {
        var name = $(this).attr('id');

        if (name !== undefined) {
            if ($(this).attr('type') === 'checkbox') {
                params = params + "&" + $(this).attr('type') + "|" + name + "=" + $(this).prop('checked');
            }
            else {
                params = params + "&" + $(this).attr('type') + "|" + name + "=" + $(this).val();
            }
        }
    });

    return params;
}

//Progress Bar functions
function showProgressBar(textName, barName, text)
{
    $(textName).html(text);

    $(barName).show();
}

function hideProgressBar(textName, barName)
{
    $(barName).hide();

    $(textName).html('');
}

//Buttons functions
function disableButtons(name)
{
    $(name).attr("disabled", true);
}

function enableButtons(name)
{
    $(name).attr("disabled", false);
}

//Error Message functions
function showErrorMessage(name)
{
    $(name).html('<div class="alert alert-danger" id="alert-danger"><strong>Critical Error! </strong> Failed to call the server.</div>');
}

//Download Excel functions
function downloadFile(pathHandler, response)
{
    var data = JSON.parse(response);

    window.location.assign(pathHandler + '&FileName=' + data.FileName + '&Container=' + data.Container + '&FilePath=' + data.FilePath);
}

//Datatable functions
function emptyDataTable(name)
{
    $(name).empty();

    $(name).append('<table class="table table-condensed display" id="ReportTable"> </table> ');
}

function buildDataTable(response)
{
    var fields = [];
    var d = jQuery.parseJSON(response);

    if (undefined !== d[0] && null !== d[0] && typeof d[0] === 'object') {
        $.each(Object.keys(d[0]), function (index, name) {
            var field = {};

            field['data'] = name;
            field['title'] = name;
            field['searchable'] = true;
            field['visible'] = true;
            field['sort'] = true;

            fields.push(field);
        });

        rTable = $('#ReportTable').dataTable({
            "data": d,
            "order": [],
            "columns": fields,
            "iDisplayLength": 25,
            "scroller": true,
            "oLanguage": {
                "sEmptyTable": "(no match)"
            }
        });
    }
}

var Site = function () {

    var CustomModals = function () {
        function customWaiting(heading) {
            var waitingModal =
                $('<div class="modal fade" data-keyboard="false">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<h5 class="modal-title">' + heading + '</h5>' +
                    '</div>' +

                    '<div class="modal-body">' +
                    '<div class="progress">' +
                    '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="99" aria-valuemin="99" aria-valuemax="100" style="width: 100%"><span class="sr-only">' + heading + '</span></div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');

            waitingModal.on('hidden.bs.modal', function () {
                waitingModal.remove(); //destroy the object otherwise they accumulate on the DOM with each open.
            });

            waitingModal.modal({
                backdrop: 'static',
                keyboard: true
            });

            return waitingModal;
        }

        /*
        usage:
    
        customAlert('Alert Title', 'The Alert', 'confirm button text');
        */
        function customAlert(heading, html, okButtonTxt) {
            var alertModal =
                $('<div class="modal fade">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<h5 class="modal-title">' + heading + '</h5>' +
                    '<button type="button" id="closeButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    html +
                    '</div>' +
                    '<div class="modal-footer">' +
                    '<a href="#" id="okButton" class="btn btn-primary" data-dismiss="modal">' +
                    okButtonTxt +
                    '</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');

            alertModal.on('hidden.bs.modal', function () {
                alertModal.remove(); //destroy the object otherwise they accumulate on the DOM with each open.
            });

            alertModal.modal('show');

            return alertModal;
        }

        /*
        usage:
            var callbackOK = function () {
                //do this when OK is pressed.
            };
    
            var callbackCancel = function () {
                //do this when cancel or close is clicked.
            };
    
            customConfirm('Prompt Title', 'The prompt', 'cancel button text', 'confirm button text', callbackOK, callbackCancel);
            customConfirm('Prompt Title', 'The prompt', 'cancel button text', 'confirm button text', callbackOK);
        */
        function customConfirm(heading, question, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel) {
            var okCalled = false;
            var cancelCalled = false;

            var confirmModal =
                $('<div class="modal fade">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<h5 class="modal-title">' + heading + '</h5>' +
                    '<button type="button" id="closeButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '</div>' +

                    '<div class="modal-body">' +
                    '<p>' + question + '</p>' +
                    '</div>' +

                    '<div class="modal-footer">' +
                    '<a href="#" id="cancelButton" class="btn" data-dismiss="modal">' +
                    cancelButtonTxt +
                    '</a>' +
                    '<a href="#" id="okButton" class="btn btn-primary" data-dismiss="modal">' +
                    okButtonTxt +
                    '</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');

            confirmModal.find('#okButton').click(function (event) {
                okCalled = true;

                if (typeof callbackOk === 'function') {
                    callbackOk();
                }
            });

            confirmModal.find('#cancelButton').click(function (event) {
                cancelCalled = true;

                if (typeof callbackCancel === 'function') {
                    callbackCancel();
                }
            });

            confirmModal.on('hidden.bs.modal', function () {
                //        if (!okCalled) {
                //            if (typeof callbackCancel == 'function') {
                //                callbackCancel();
                //            }
                //        }

                confirmModal.remove(); //destroy the object otherwise they accumulate on the DOM with each open.
            });

            confirmModal.modal('show');

            return confirmModal;
        }

        /*
        usage:
        var callbackOK = function (newValue) {
            //do this when OK is pressed.
            //assign param 'newValue' to whatever
        };
    
        var callbackCancel = function () {
            //do this when cancel or close is clicked.
        };
    
        customPrompt('Prompt Title', 'The prompt', 'default text', 'cancel button text', 'confirm button text', callbackOK, callbackCancel);
        customPrompt('Prompt Title', 'The prompt', 'default text', 'cancel button text', 'confirm button text', callbackOK);
        */
        function customPrompt(heading, question, defaultText, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel) {
            var okCalled = false;

            var promptModal =
                $('<div class="modal fade">' +
                    '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<h5 class="modal-title">' + heading + '</h5>' +
                    '<button type="button" id="closeButton" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '</div>' +

                    '<div class="modal-body">' +
                    '<p>' + question + '</p>' +
                    '<div class="row"><div class="col-12"><input class="form-control" type="text" id="customPromptText" value="' + defaultText + '" /></div></div>' +
                    '</div>' +

                    '<div class="modal-footer">' +
                    '<a href="#" id="cancelButton" class="btn" data-dismiss="modal">' +
                    cancelButtonTxt +
                    '</a>' +
                    '<a href="#" id="okButton" class="btn btn-primary" data-dismiss="modal">' +
                    okButtonTxt +
                    '</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>');

            promptModal.find('#okButton').click(function (event) {
                okCalled = true;

                if (typeof callbackOk === 'function') {
                    callbackOk(promptModal.find('#customPromptText').val());
                }
            });

            promptModal.on('hidden.bs.modal', function () {
                if (!okCalled) {
                    if (typeof callbackCancel === 'function') {
                        callbackCancel();
                    }
                }

                promptModal.remove(); //destroy the object otherwise they accumulate on the DOM with each open.
            });

            promptModal.modal('show');

            return promptModal;
        }

        return {
            customWaiting: function (heading) {
                return customWaiting(heading);
            },
            customAlert: function (heading, html, okButtonTxt) {
                return customAlert(heading, html, okButtonTxt);
            },
            customConfirm: function (heading, question, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel) {
                return customConfirm(heading, question, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel);
            },
            customPrompt: function (heading, question, defaultText, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel) {
                return customPrompt(heading, question, defaultText, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel);
            }
        };
    }(); //end custom modals

    var PartialModals = function () {

        var _common = null;
        var _ajaxHelpers = null;

        function init(common, ajaxHelpers) {
            _common = common;
            _ajaxHelpers = ajaxHelpers;
        }

        //element is the calling button or anchor
        function onSuccessShowModal(element, modalId) {
            // If the modal is null, undefined, empty, use the deafult ID for the modal
            if (modalId === null || modalId === undefined || modalId === "")
                modalId = "#DefaultModal";

            $modal = $(modalId);

            var lastZIndex = parseInt($('div.modal-backdrop:last', 'body').css('z-index'));

            var $lastHubOneModal = $('div.hub-one-modal:last', 'body');
            if ($lastHubOneModal.length === 1) {
                var paddingRight = $('div.hub-one-modal:last', 'body').css('padding-right');
                $(modalId).css('padding-right', paddingRight);
            }

            $modal.addClass('hub-one-modal');

            //take the refresh data attrbutes from the button, and add them to the modal.
            if ($(element).data().hasOwnProperty('viewLoaderRefresh')) {
                $modal.data('viewLoaderRefresh', $(element).data('viewLoaderRefresh'));
            }
            if ($(element).data().hasOwnProperty('viewLoaderRefreshParent')) {
                $modal.data('viewLoaderRefreshParent', $(element).data('viewLoaderRefreshParent'));
            }            

            //passing along the element, as the caller, to the modal function.
            $modal.modal('show', element).on('shown.bs.modal', function (e) {

                //this is hack, need to be able to globally set this from site.js.
                if (typeof window["initializeComponents"] === 'function') {
                    window['initializeComponents']($modal);
                }

                if (e.relatedTarget && $(e.relatedTarget).data().hasOwnProperty('loadedCallback')) {
                    //window[$(e.relatedTarget).data('loadedCallback')]({ target: e.target, relatedTarget: e.relatedTarget });
                    _common.getFunction($(e.relatedTarget).data('loadedCallback'), ["element"]).apply(e.target, arguments);
                }
                else {
                    $(':input:enabled:visible:not([readonly]):not(button):first', e.target).focus();
                }

                $modal.css('z-index', lastZIndex + 30);
                $('div.modal-backdrop:last', 'body').css('z-index', lastZIndex + 20);
            }).on('hide.bs.modal', function (e) {
                if ($('div.modal-backdrop', 'body').length > 1) {
                    e.preventDefault();
                    e.stopPropagation();
                    $modal.animate({ top: '-20px', opacity: 0 }, 150, function () {
                        $modal.remove();
                        $('div.modal-backdrop:last', 'body').remove();
                    });
                }
            });
        }

        function onBeginModal(xhr, element) {
            var targetName = $(element).data('ajaxUpdate');
            var $target = $(targetName);

            if ($target.length === 0) {
                $('<div id="' + targetName.substring(1, targetName.length) + '"></div>').appendTo('body'); //.insertAfter($('#ModalContainer'));
            }
        }

        /*
         * Title: onModalSuccess (Success Function for post Ajax call)
         * Description: Accepts JSON object from [HttpPost] response consisting
         * of all ModelState errors, then displays in default places in modal form.
         */
        function onModalSuccess(data, status, xhr, modalId) {
            console.log('onModalSuccess for modal [' + modalId + ']');
            console.log(data);

            // If the modal is null, undefined, empty, use the deafult ID for the modal
            if (modalId === null || modalId === undefined || modalId === "")
                modalId = "#DefaultModal";

            var $modal = $(modalId);
            var $errorMessage = $('.errorMessage', $modal);

            if ($errorMessage.length === 1) {
                $errorMessage.empty(); //clear any existing messages.
                $errorMessage.removeClass('show');
            }

            // Null check on the data parameter (should never be null)
            if (data !== null) {

                $('button, a', $modal).blur();

                if ($modal.length === 1 && $modal.data().hasOwnProperty('afterSubmitCallback')) {
                    window[$modal.data('afterSubmitCallback')]($modal, data);
                }

                // Check which element has been returned from the controller (errors or Url for success)
                if (data.errors) {
                    console.log(data.errors);

                    if (data.errorsMessage !== null)
                        $.growl.error({ title: '', message: data.errorsMessage });

                    // Clear any errors already on the form
                    $("span[data-valmsg-for]", $modal).text("");

                    // Note: #validation-summary is a custom field added to the form as .NET's won't fire
                    var $valSum = $('#validation-summary', $modal);
                    $('ul', $valSum).empty();
                    $valSum.hide();

                    // Access actual data  in object per controller "data = errors*.Data"
                    var errors = data.errors;

                    // Access all keys in object and determine length
                    var keys = Object.keys(errors);
                    var len = keys.length;
                    // Loop over keys and select the span on the form which 
                    // has the data-valmsg-for attribute set to the key (managed by .NET),
                    // then set the value of the error message to the key's corresponding value.
                    for (var i = 0; i < len; i++) {
                        var $span = $('span[data-valmsg-for="' + keys[i] + '"]', $modal);
                        // Check if the span doesn't exist...
                        if ($span.length === 0 || $span === undefined || $span === null || keys[i] === "") {
                            // ...then the error message is for something other than a field so display custom
                            if (keys[i]) {
                                $('#validation-summary > ul', $modal).append("<li>" + keys[i] + ": " + errors[keys[i]] + "</li>");
                            }
                            else {
                                $('#validation-summary > ul', $modal).append("<li>" + errors[keys[i]] + "</li>");
                            }

                            $valSum.show();
                        }
                        else {
                            // Otherwise add the value (actual error message) to the span
                            $span.text(errors[keys[i]]);
                            $span.show();
                        }
                    }
                }
                else if (data.errorMessage) {
                    if ($errorMessage.length === 0) {
                        $errorMessage = $('<div class="alert alert-danger errorMessage fade show" role="alert"></div>');
                        $('div.modal-body', $modal).prepend($errorMessage);
                        $errorMessage.wrap('<div class="col-12"></div>');
                    }
                    else {
                        $errorMessage.addClass('show');
                    }

                    var errorMessageHtml = '<div class="row" style="margin: 0px"><div class="col-auto align-self-center"><div class="fa fa-times-circle"></div></div><div class="col">' + data.errorMessage + '</div></div>';
                    $($errorMessage).html(errorMessageHtml);
                }

                _common.onGrowl(data);
                _ajaxHelpers.onSuccessActionLink(data, status, xhr, $modal);

                if (!data.keepModalOpen) {
                    $modal.modal('hide');
                }
            }
            else {
                // 'data' should never be null from the Ajax call
                $.growl.error({ title: 'Error', message: 'Data parameter cannot be null.' });
            }
        }

        return {
            init: function (common, ajaxHelpers) {
                return init(common, ajaxHelpers);
            },
            onBeginModal: function (xhr, element) {
                return onBeginModal(xhr, element);
            },
            onModalSuccess: function (data, status, xhr, modalId) {
                return onModalSuccess(data, status, xhr, modalId);
            },
            onSuccessShowModal: function (element, modalId) {
                return onSuccessShowModal(element, modalId);
            }
        };
    }(Common, AjaxHelpers); //end partial modals

    var AjaxHelpers = function () {

        var _common = null;
        var _customModals = null;

        function init(common, customModals) {
            _common = common;
            _customModals = customModals;
        }

        function applyDefaultUnobtrusiveCallbacks() {
            var applyDefaultCallbacksToUnobtrusiveAjax = function ($element, evt) {

                if (!$element.data().hasOwnProperty('ajaxFailure')) {
                    $element.attr('data-ajax-failure', 'onFailure(xhr, status, error)');
                    $element.data('ajaxFailure', 'onFailure(xhr, status, error)');
                }

                if ($element.is('form')) {
                    var method = 'post';
                    if (!$element.data().hasOwnProperty('ajaxMethod')) {
                        $element.attr('data-ajax-method', 'post');
                        $element.data('ajaxMethod', 'post)');
                    }
                    else {
                        method = $element.data('ajaxMethod');
                    }

                    if (!$element[0].hasAttribute("method") || $element.attr(method) !== method) {
                        if ($element.attr(method) !== undefined)
                            console.log('Correcting mismatch between ajax and form methods. Was [' + $element.attr(method) + '], setting it to [' + method + ']');

                        $element.attr('method', method);
                    }
                }

                if ($element.data().hasOwnProperty('ajaxUpdate')) {
                    var updateTarget = $element.data('ajaxUpdate');
                    var $updateTarget = $(updateTarget);
                    if ($updateTarget.length === 1) {
                        if ($updateTarget.data().hasOwnProperty('isDatatable') && !$updateTarget.data().hasOwnProperty('ajaxSuccess')) {
                            $element.attr('data-ajax-success', 'Site.initDataTable(this)');
                            $updateTarget.data('ajaxSuccess', 'Site.initDataTable(this)');
                        }
                    }
                }
            };

            $(document).on("click", "a[data-ajax=true]", function (evt) {
                applyDefaultCallbacksToUnobtrusiveAjax($(this), evt);
            });

            $(document).on("click", "form[data-ajax=true] input[type=image]", function (evt) {
                var target = $(evt.target),
                    form = $(target.parents("form")[0]);

                applyDefaultCallbacksToUnobtrusiveAjax($(form), evt);
            });

            $(document).on("click", "form[data-ajax=true] :submit", function (evt) {
                var target = $(evt.target),
                    form = $(target.parents("form")[0]);

                applyDefaultCallbacksToUnobtrusiveAjax($(form), evt);
            });

            $(document).on("submit", "form[data-ajax=true]", function (evt) {
                applyDefaultCallbacksToUnobtrusiveAjax($(this), evt);
            });
        }

        function hookActionLinks() {
            var data_click = "actionLinkClick";

            $(document).on("click", "a[data-action-link=true], button[data-action-link=true]", function (event) {
                event.preventDefault();
                Site.onActionLink(event);
            });

            $.each($("a[data-action-link=true] > *, button[data-action-link=true] > *"), function (index, item) {
                $(item).css('pointer-events', 'none');
            });

            $(document).on("click", "form[data-action-link=true] input[type=image]", function (evt) {
                var name = evt.target.name,
                    target = $(evt.target),
                    form = $(target.parents("form")[0]),
                    offset = target.offset();

                form.data(data_click, [
                    { name: name + ".x", value: Math.round(evt.pageX - offset.left) },
                    { name: name + ".y", value: Math.round(evt.pageY - offset.top) }
                ]);

                setTimeout(function () {
                    form.removeData(data_click);
                }, 0);
            });
        }

        function hookDownloadActionLinks() {
            var data_click = "downloadActionLinkClick";

            $(document).on("click", "a[data-download-action-link=true], button[data-download-action-link=true]", function (event) {
                event.preventDefault();
                Site.onDownloadActionLink(event);
            });

            $.each($("a[data-download-action-link=true] > *, button[data-download-action-link=true] > *"), function (index, item) {
                $(item).css('pointer-events', 'none');
            });

            $(document).on("click", "form[data-action-link=true] input[type=image]", function (evt) {
                var name = evt.target.name,
                    target = $(evt.target),
                    form = $(target.parents("form")[0]),
                    offset = target.offset();

                form.data(data_click, [
                    { name: name + ".x", value: Math.round(evt.pageX - offset.left) },
                    { name: name + ".y", value: Math.round(evt.pageY - offset.top) }
                ]);

                setTimeout(function () {
                    form.removeData(data_click);
                }, 0);
            });
        }

        function hookModalLinks() {
            var data_click = "modalLinkClick";

            var applyDefaultModalCallbacksToUnobtrusiveAjax = function ($element, evt) {
                if ($element.data().hasOwnProperty('isModal')) {
                    if (!$element.data().hasOwnProperty('ajaxBegin')) {
                        $element.attr('data-ajax-begin', "onBeginModal(xhr, this)");
                        $element.data('ajaxBegin', "onBeginModal(xhr, this)");
                    }

                    if (!$element.data().hasOwnProperty('ajaxSuccess')) {
                        $element.attr('data-ajax-success', "onSuccessShowModal(this)");
                        $element.data('ajaxSuccess', "onSuccessShowModal(this)");
                    }
                }
            };

            $(document).on("click", "a[data-is-modal=true], button[data-is-modal=true]", function (event) {
                applyDefaultModalCallbacksToUnobtrusiveAjax($(this), event);
            });

            $.each($("a[data-is-modal=true] > *, button[data-is-modal=true] > *"), function (index, item) {
                $(item).css('pointer-events', 'none');
            });

            $(document).on("click", "form[data-is-modal=true] input[type=image]", function (evt) {
                var name = evt.target.name,
                    target = $(evt.target),
                    form = $(target.parents("form")[0]),
                    offset = target.offset();

                applyDefaultModalCallbacksToUnobtrusiveAjax($(form), evt);

                form.data(data_click, [
                    { name: name + ".x", value: Math.round(evt.pageX - offset.left) },
                    { name: name + ".y", value: Math.round(evt.pageY - offset.top) }
                ]);

                setTimeout(function () {
                    form.removeData(data_click);
                }, 0);
            });

            //move my events to the top
            var selectors = ['a[data-is-modal=true], button[data-is-modal=true]', 'form[data-is-modal=true] input[type=image]'];
            var eventList = $._data($(document)[0], "events");
            var unobtrusiveEventIndex = 0;
            var modalEventIndex = 0;

            selectors.forEach(function (selector) {
                //console.log('moving up: ' + selector);
                $.each(eventList.click, function (index, item) {
                    if (item.selector === selector) {
                        modalEventIndex = index;
                    }
                    else if (item.selector === 'a[data-ajax=true]') {
                        unobtrusiveEventIndex = index;
                    }
                });

                //console.log('unobtrusiveEventIndex: ' + unobtrusiveEventIndex);
                //console.log('modalEventIndex: ' + modalEventIndex);
                if (unobtrusiveEventIndex < modalEventIndex) {
                    for (var i = modalEventIndex; i > unobtrusiveEventIndex; i--) {
                        [eventList.click[i], eventList.click[i - 1]] = [eventList.click[i - 1], eventList.click[i]];
                    }
                }
            });
        }

        function onSuccessActionLink(data, status, xhr, target) {

            console.log('onSuccessActionLink');
            console.log(data);

            var $target = $(target);

            var updatePage = _common.valueOrDefault($target.data('updatePage'), false);
          
            if (updatePage) {
                var redirectUrl = _common.valueOrDefault(data['redirectUrl'], '');
                var isSuccess = _common.valueOrDefault(data['isSuccess'], false);
                if (isSuccess) {
                    if (redirectUrl !== '') {
                        console.log("page refreshed with redirect");
                        window.location = redirectUrl;
                    }
                    else {
                        console.log("page refreshed");
                        location.reload();
                    }
                }
            }

            var updateTargetParent = _common.valueOrDefault($target.data('viewLoaderRefreshParent'), false);
            if (updateTargetParent) {
                var $viewLoader = $($target.parents('.view-loaded').first());
                if ($viewLoader.length === 1) {
                    $viewLoader.ViewLoader('refresh');
                }
            }

            var updateTargetNames = _common.valueOrDefault($target.data('viewLoaderRefresh'), null);
            console.log(updateTargetNames);
            if (updateTargetNames !== null) {
                console.log('updating Target Names');
                var updateTargetNamesList = updateTargetNames.split(',');

                console.log(updateTargetNamesList);

                var clearState = _common.valueOrDefault($target.data('viewLoaderClearState'), false);

                $.each(updateTargetNamesList, function (i, updateTargetName) {
                    console.log(updateTargetName);
                    var updateTargetList = $(updateTargetName);
                    $.each(updateTargetList, function (j, updateTarget) {
                        var $updateTarget = $(updateTarget);

                        if ($updateTarget.length !== 0 && $updateTarget.hasClass('view-loaded')) {
                            
                            if (clearState) {
                                var $table = $('table.dataTable', $updateTarget);
                                if ($table.length === 1 && $.fn.dataTable.isDataTable($table)) {
                                    $table.DataTable().state.clear();
                                }
                            }

                            //initializeComponents($updateTarget);
                            $updateTarget.ViewLoader('refresh');
                        }
                    });
                });
            }

            var redirectRefresh = _common.valueOrDefault($target.data('redirectRefresh'), null);
            if (redirectRefresh !== null) {
                window.location = redirectRefresh;
            }
        }

        function onActionLink(event) {
            var $target = $(event.target);

            console.log(event);
            console.log($target);

            if ($target.length === 1) {

                var url = $target.data('url');
                var type = _common.valueOrDefault($target.data('type'), "POST");
                var dataType = _common.valueOrDefault($target.data('dataType'), "json");
                var confirmMessage = $target.data('confirm');
                var defaultSpinner = _common.valueOrDefault($target.data('defaultSpinner'), true);

                var callbackOk = function () {

                    if (defaultSpinner) {
                        var $spinner = $('#spinner', $target);
                        if ($spinner.length === 0) {
                            $spinner = $('<span id="spinner" style="display: none; padding-left: 1em;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></span>');
                            $target.append($spinner);
                        }
                        $spinner.show();
                    }

                    var ajaxOptions = {
                        url: url,
                        type: type,
                        processData: false,
                        contentType: false,
                        headers: {
                            RequestVerificationToken:
                                $('input:hidden[name="RequestVerificationToken"]').val()
                        },
                        success: function (data, status, xhr) {
                            console.log(data);
                            onGrowl(data);
                            if (data.isSuccess) {
                                onSuccessActionLink(data, status, xhr, $target);
                                //getFunction($target.data("success"), ["data", "status", "xhr"]).apply(element, arguments);
                            }
                        },
                        error: function (xhr, error) {
                            _common.onFailure(xhr, null, error);
                            if (defaultSpinner) {
                                var $spinner = $('#spinner', $target);
                                if ($spinner.length !== 0) {
                                    $spinner.hide();
                                }
                            }
                        }
                    };

                    var formData = new FormData();
                    $.each($target.data(), function (key, value) {
                        if (key.startsWith("param")) {
                            formData.append(key.substring(5, 6).toLowerCase() + key.substring(6, key.length), value);
                        }
                    });

                    $.extend(ajaxOptions, { data: formData });

                    //if (!$.isEmptyObject(dataObject)) {
                    //    $.extend(ajaxOptions, { data: JSON.stringify(dataObject) });
                    //}

                    //console.log(dataObject);
                    console.log(ajaxOptions);

                    //do ajax call
                    $.ajax(ajaxOptions).done(function () {

                        if (defaultSpinner) {
                            var $spinner = $('#spinner', $target);
                            if ($spinner.length !== 0) {
                                $spinner.hide();
                            }
                        }

                        if ($target.data().hasOwnProperty('loadedCallback')) {
                            window[$target.data('loadedCallback')](event, event.target);
                        }
                    });
                };

                if (_common.valueOrDefault(url, null) !== null) {
                    if (_common.valueOrDefault(confirmMessage, null) !== null) {
                        var confirmTitle = _common.valueOrDefault($target.data('confirmTitle'), "Please Confirm");
                        var confirmOk = _common.valueOrDefault($target.data('confirmOk'), "OK");
                        var confirmCancel = _common.valueOrDefault($target.data('confirmCancel'), "Cancel");

                        _customModals.customConfirm(confirmTitle, confirmMessage, confirmCancel, confirmOk, callbackOk);
                    }
                    else {
                        callbackOk();
                    }
                }
                else {
                    onSuccessActionLink({}, null, null, $target);
                }
            }
        }

        function onDownloadActionLink(event, params) {
            var $target = $(event.target);

            console.log(event);
            console.log($target);

            if ($target.length === 1) {

                var url = $target.attr('href');
                var defaultSpinner = _common.valueOrDefault($target.data('defaultSpinner'), true);
                var defaultSpinnerMessage = _common.valueOrDefault($target.data('defaultSpinnerMessage'), "Generating...");
                var form = _common.valueOrDefault($target.data('form'), null);

                //var hideDefaultSpinner = function () {
                //    if (defaultSpinner) {
                //        var $spinner = $('#spinner', $target);
                //        if ($spinner.length === 1) {
                //            $spinner.hide();
                //        }
                //    }
                //};

                //if (defaultSpinner) {
                //    var $spinner = $('#spinner', $target);
                //    if ($spinner.length === 0) {
                //        $spinner = $('<span id="spinner" style="display: none; padding-left: 1em;"><i class="fa fa-spinner fa-spin" aria-hidden="true"></i></span>');
                //        $target.append($spinner);
                //    }
                //    $spinner.show();
                //}

                var waiting = null;
                if (defaultSpinner) {
                   // waiting = Site.customWaiting("Generating...");
                    waiting = Site.customWaiting(defaultSpinnerMessage);
                }

                var dataParamsFromOverride = null;
                var overrideData = false;
                if (params !== null && params !== undefined) {
                    if (params.hasOwnProperty("withFormDataOverride") && params.withFormDataOverride !== undefined && params.withFormDataOverride !== null) {
                        dataParamsFromOverride = $(params.withFormDataOverride).serialize();
                    }
                    else if (params.hasOwnProperty("withObjectDataOverride") && params.withObjectDataOverride !== undefined && params.withObjectDataOverride !== null) {
                        var formData = new FormData();

                        $.each(params.withObjectDataOverride, function (key, value) {
                            formData.append(key, value);
                        });

                        dataParamsFromOverride = $(formData).serialize();
                    }
                    else if (params.hasOwnProperty("withUrlParamOverride") && params.withUrlParamOverride !== undefined && params.withUrlParamOverride !== null) {
                        dataParamsFromOverride = params.withUrlParamOverride;
                    }
                }

                if (dataParamsFromOverride !== null)
                    overrideData = true;

                console.log('overrideData: ' + overrideData);

                var urlParamsFromParams = null;
                if (!overrideData) {
                    $.each($target.data(), function (key, value) {
                        var formData = new FormData();
                        var hasParams = false;

                        if (key.startsWith("param")) {
                            formData.append(key.substring(5, 6).toLowerCase() + key.substring(6, key.length), value);
                            hasParams = true;
                        }

                        if (hasParams)
                            urlParamsFromParams = $(formData).serialize();
                    });
                }

                var urlParamsFromForm = null;
                if (!overrideData) {
                    if (form !== null) {
                        console.log('download action linked to data form named "' + form + '"');

                        var $form = $(form);
                        if ($form.length === 1) {
                            var validationInfo = $form.data("unobtrusiveValidation");
                            if (!validationInfo || !validationInfo.validate || validationInfo.validate()) {
                                urlParamsFromForm = $form.serialize();
                            }
                        }
                        else {
                            console.log('Cannot have more than one form with the same name in the page. Form name "' + form + '"');
                        }
                    }
                }

                var fullUrl = url;
                if (urlParamsFromParams !== null) {
                    fullUrl += fullUrl.indexOf('?') === -1 ? '?' : '&';
                    fullUrl += urlParamsFromParams;
                }

                if (urlParamsFromForm !== null) {
                    fullUrl += fullUrl.indexOf('?') === -1 ? '?' : '&';
                    fullUrl += urlParamsFromForm;
                }

                if (dataParamsFromOverride !== null) {
                    fullUrl += fullUrl.indexOf('?') === -1 ? '?' : '&';
                    fullUrl += dataParamsFromOverride;
                }

                $.fileDownload(fullUrl, {
                    successCallback: function (url) {
                        if (waiting !== null && defaultSpinner)
                            waiting.modal('hide');

                        $.growl.notice({ title: '', message: 'File Downloaded.' });
                    },
                    failCallback: function (html, url) {
                        if (waiting !== null && defaultSpinner)
                            waiting.modal('hide');

                        $.growl.error({ title: '', message: 'Unable to download file.' });
                    }
                });
            }
        }

        /*
         * Title: onPostFormSuccess (Success Function for post Ajax call)
         * Description: Accepts JSON object from [HttpPost] response consisting
         * of all ModelState errors, then displays in default places in form.
         */
        function onPostFormSuccess(data, status, xhr, formOrId, omitRefresh = false) {
            //console.log('onPostFormSuccess for form [' + formId + ']');
            //console.log(data);
            console.log(omitRefresh);

            var isSuccess = false;
            //var $form = $(formId);
            var $form = $(formOrId);
            var $errorMessage = $('.errorMessage', $form);

            if ($errorMessage.length === 1)
                $errorMessage.empty(); //clear any existing messages.

            // Null check on the data parameter (should never be null)
            if (data !== null) {

                if ($form.length === 1 && $form.data().hasOwnProperty('afterSubmitCallback')) {
                    window[$form.data('afterSubmitCallback')]($form);
                }

                // Check which element has been returned from the controller (errors or Url for success)
                if (data.errors) {
                    console.log(data.errors);

                    if (data.errorsMessage !== null)
                        $.growl.error({ title: '', message: data.errorsMessage });

                    // Clear any errors already on the form
                    $("span[data-valmsg-for]", $form).text("");

                    // Note: #validation-summary is a custom field added to the form as .NET's won't fire
                    var $valSum = $('#validation-summary', $form);
                    $('ul', $valSum).empty();
                    $valSum.hide();

                    // Access actual data  in object per controller "data = errors*.Data"
                    var errors = data.errors;

                    // Access all keys in object and determine length
                    var keys = Object.keys(errors);
                    var len = keys.length;
                    // Loop over keys and select the span on the form which 
                    // has the data-valmsg-for attribute set to the key (managed by .NET),
                    // then set the value of the error message to the key's corresponding value.
                    for (var i = 0; i < len; i++) {
                        var $span = $('span[data-valmsg-for="' + keys[i] + '"]', $form);
                        // Check if the span doesn't exist...
                        if ($span.length === 0 || $span === undefined || $span === null || keys[i] === "") {
                            // ...then the error message is for something other than a field so display custom
                            if (keys[i]) {
                                $('#validation-summary > ul', $form).append("<li>" + keys[i] + ": " + errors[keys[i]] + "</li>");
                            }
                            else {
                                $('#validation-summary > ul', $form).append("<li>" + errors[keys[i]] + "</li>");
                            }

                            $valSum.show();
                        }
                        else {
                            // Otherwise add the value (actual error message) to the span
                            $span.text(errors[keys[i]]);
                            $span.show();
                        }
                    }
                }
                else {
                    isSuccess = true;
                    $("span[data-valmsg-for]", $form).text("");
                    $('#validation-summary > ul', $form).empty();
                }

                _common.onGrowl(data);
                if (!omitRefresh) {
                    onSuccessActionLink(data, status, xhr, $form);
                }
                //_ajaxHelpers.onSuccessActionLink(data, status, xhr, $modal);
            }
            else {
                // 'data' should never be null from the Ajax call
                $.growl.error({ title: 'Error', message: 'Data parameter cannot be null.' });
            }

            return isSuccess;
        }

        function onRowAction(event, url, actionType = '') {
            event.preventDefault();

            var $target = $(event.target);
            var confirmMessage = $target.data('confirm');
            var defaultSpinner = _common.valueOrDefault($target.data('defaultSpinner'), true);

            var callbackOk = function () {
                var $tr = $target.closest('tr');
                var rowData = $('input, select, textarea', $tr).serializeArray();

                //var rowIdentifier = 'rowId-' + rowId;
                if (!$tr.hasClass('initialized')) {
                    //$tr.attr('id', rowIdentifier);
                    $tr.addClass('initialized');

                    $('td:not(.align-top)', $tr).each(function (index, item) {
                        $(item).addClass('align-top');
                    });
                }

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: "json",
                    data: rowData,
                    headers: {
                        RequestVerificationToken:
                            $('input:hidden[name="RequestVerificationToken"]').val()
                    },
                    success: function (data, status, xhr) {
                        //For ease, the 3 handlers should always return the ID
                        if (onPostFormSuccess(data, status, xhr, $tr, true)) {
                            //var $tbody = $tr.parent().siblings('tbody');
                            var $datatable = $tr.parents('table.dataTable').DataTable(); ///.parent().DataTable();

                            if (actionType === 'Create') {
                                //the two following lines work great without sorting and filtering enabled.
                                /*$tr.prev().after(data.newRow);
                                $tr.ViewLoader('refresh');*/
                                $datatable.rows.add($(data.newRow)).draw(false);

                                $tr.ViewLoader('refresh');
                            }
                            else if (actionType === 'Delete') {
                                //delete with strikethrough effect
                                //$(':input', $tr).attr('Disabled', 'Disabled');
                                //$tr.css('opacity', 0.5);

                                ////add a strike through effect
                                //$tr.css('background-image', 'url(\'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAEklEQVQYV2NkwAIYKRf8j24EABKTAQUAwSCiAAAAAElFTkSuQmCC\')');
                                //$tr.css('background-position', '50% 50%');
                                //$tr.css('background-repeat', 'repeat-x');

                                ////show line via transparent inputs
                                //$('input', $tr).css('background', 'transparent');

                                //or add line to background of inputs
                                /*
                                $(':input', $tr).css('background-image', 'url(\'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAEklEQVQYV2NkwAIYKRf8j24EABKTAQUAwSCiAAAAAElFTkSuQmCC\')');
                                $(':input', $tr).css('background-position', '50% 53%');
                                $(':input', $tr).css('background-repeat', 'repeat-x');
                                */

                                //$('td:first', $tr).prepend('<hr style="position: absolute; display: flex; flex: 1; border-top: 1px solid #000000;" />')

                                $datatable.row($tr).remove().draw(false);
                            }
                        }
                    },
                    error: function (xhr, status, error) {
                        _common.onFailure(xhr, status, error);
                    }
                });
            };

            if (_common.valueOrDefault(url, null) !== null) {
                if (_common.valueOrDefault(confirmMessage, null) !== null) {
                    var confirmTitle = _common.valueOrDefault($target.data('confirmTitle'), "Please Confirm");
                    var confirmOk = _common.valueOrDefault($target.data('confirmOk'), "OK");
                    var confirmCancel = _common.valueOrDefault($target.data('confirmCancel'), "Cancel");

                    _customModals.customConfirm(confirmTitle, confirmMessage, confirmCancel, confirmOk, callbackOk);
                }
                else {
                    callbackOk();
                }
            }
        }

        return {
            init: function (common, customModals) {
                return init(common, customModals);
            },
            applyDefaultUnobtrusiveCallbacks: function () {
                return applyDefaultUnobtrusiveCallbacks();
            },
            hookActionLinks: function () {
                return hookActionLinks();
            },
            hookDownloadActionLinks: function () {
                return hookDownloadActionLinks();
            },
            hookModalLinks: function () {
                return hookModalLinks();
            },
            onDownloadActionLink: function (event, params) {
                return onDownloadActionLink(event, params);
            },
            onSuccessActionLink: function (data, status, xhr, target) {
                return onSuccessActionLink(data, status, xhr, target);
            },
            //onPostFormSuccess: function (data, formId) {
            //    return onPostFormSuccess(data, formId);
            //},
            onPostFormSuccess: function (data, status, xhr, formOrId, omitRefresh = false) {
                return onPostFormSuccess(data, status, xhr, formOrId, omitRefresh);
            },
            onActionLink: function (event) {
                return onActionLink(event);
            },
            onRowAction: function (event, url, actionType = '') {
                return onRowAction(event, url, actionType);
            }
        };
    }();

    var Common = function () {

        function autoGrow(target) {
            var $target = $(target);
            if ($target.length !== 0) {
                var element = $(target).get(0);
                if (element.scrollHeight > 50)
                    $target.animate({ 'height': element.scrollHeight }, 'fast');
            }
        }

        function getFunction(code, argNames) {
            var fn = window, parts = (code || "").split(".");
            while (fn && parts.length) {
                fn = fn[parts.shift()];
            }
            if (typeof (fn) === "function") {
                return fn;
            }
            argNames.push(code);
            return Function.constructor.apply(null, argNames);
        }

        function initDataTable(target) {
            var $target = $(target);

            var containerName = $target.data('ajaxUpdate');
            var $container = $(containerName);

            if ($container.length === 1 || $target.is('table')) {
                var $table = $('table', $container);

                if ($table.length !== 1)
                    $table = $target;

                var paging = true;
                if ($table.data.hasOwnProperty('paging'))
                    paging = $table.data('paging') === 'true' ? true : false;

                var emptyMsg = valueOrDefault($table.data('emptyMsg'), 'No data found');

                if ($table.length === 1) {
                    $table.DataTable({
                        "stateSave": false,
                        "paging": paging,
                        "stateDuration": -1,
                        "sort": true,
                        //"lengthMenu": [[3, 15, 25, 50, -1], [3, 15, 25, 50, "All"]],
                        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]],
                        "oLanguage": {
                            "sEmptyTable": emptyMsg
                        },
                        'columnDefs': [
                            {
                                sortable: false,
                                targets: 'dt-col-nosort'
                            },
                            {
                                className: "col-align-right",
                                targets: "dt-col-align-right"
                            },
                            {
                                type: 'is-input',
                                targets: 'is-input'
                            },
                            {
                                orderDataType: 'dom-checkbox',
                                targets: 'is-checkbox'
                            },
                            {
                                orderDataType: 'dom-text',
                                targets: 'is-input'
                            }
                        ]
                    });
                }
            }
        }

        function initDatePickers(target) {

            console.log('initDatePickers: ' + target);

            var $target = $(target);
            if (target === undefined) {
                $target = $('body');
            }
           
            if ($target.length !== 0) {
                $('.hubone-date-picker', $target).each(function (index, item) {
                    initDatePicker(item);
                });
            }
        }

        function initDatePicker(target) {
            console.log('initDatePicker');

            var $target = $(target);
            if ($target.length === 1) {
                var options = {};

                if ($target.data().hasOwnProperty('changeMonth')) {
                    options['changeMonth'] = JSON.parse($target.data('changeMonth').toLowerCase());
                }

                if ($target.data().hasOwnProperty('changeYear')) {
                    options['changeYear'] = JSON.parse($target.data('changeYear').toLowerCase());
                }

                if ($target.data().hasOwnProperty('yearRange')) {
                    options['yearRange'] = $target.data('yearRange');
                }

                if ($target.data().hasOwnProperty('minDate')) {
                    options['minDate'] = new Date(Date.parse($target.data('minDate')));
                }

                if ($target.data().hasOwnProperty('maxDate')) {
                    options['maxDate'] = new Date(Date.parse($target.data('maxDate')));
                }

                if ($target.parent().is('.input-group')) {
                    $target.next('.input-group-append').children('button').on('click', function (event) {
                        $(this).parent().prev('input').focus();
                    });
                }

                console.log(options);

                $target.datepicker(options);
            }
        }

        function initDateTimePickers(target) {

            console.log('initDateTimePickers: ' + target);

            var $target = $(target);
            if (target === undefined) {
                $target = $('body');
            }

            if ($target.length !== 0) {
                $('.hubone-date-time-picker', $target).each(function (index, item) {
                    initDateTimePicker(item);
                });
            }
        }

        function initDateTimePicker(target) {
            console.log('initDateTimePicker');

            var $target = $(target);
            if ($target.length === 1) {
                var options = {};

                if ($target.data().hasOwnProperty('minDate')) {
                    options['minDate'] = new Date(Date.parse($target.data('minDate')));
                }

                if ($target.data().hasOwnProperty('maxDate')) {
                    options['maxDate'] = new Date(Date.parse($target.data('maxDate')));
                }

                if ($target.data().hasOwnProperty('minTime')) {
                    options['minTime'] = $target.data('minTime');
                }

                if ($target.data().hasOwnProperty('maxTime')) {
                    options['maxTime'] = $target.data('maxTime');
                }

                if ($target.data().hasOwnProperty('step')) {
                    options['step'] = $target.data('step');
                }

                options['format'] = 'n/j/Y h:i A';
                options['formatTime'] = 'h:i A';
                options['validateOnBlur'] = false; //must do this or remove the AM/PM. Otherwise, you lose an hour on blur.
                if ($target.hasClass('hubone-time-only')) {
                    options['datepicker'] = false;
                    options['format'] = 'h:i A';
                }

                if ($target.parent().is('.input-group')) {
                    $target.next('.input-group-append').children('button').on('click', function (event) {
                        $(this).parent().prev('input').focus();
                    });
                }

                console.log(options);

                $target.datetimepicker(options);
            }
        }


        function initSelectPickers(target) {

            console.log('initSelectPickers: ' + target);

            var $target = $(target);
            if (target === undefined) {
                $target = $('body');
            }

            $('.selectpicker', $target).each(function (index, select) {
                if ($(select).data().hasOwnProperty('showAll')) {
                    $("option:first", select).prop('selected', true);
                }

                if ($(select).data().hasOwnProperty('selectAll')) {
                    $("option", select).prop('selected', true);
                }
                $(select).trigger('change');
            });

            $('.selectpicker', $target).selectpicker({
                countSelectedText: function (numSelected, numTotal) {
                    if ($(this)[0].hasOwnProperty('countSelectedCustomText') && $(this)[0].hasOwnProperty('countSelectedCustomTextPlural')) {
                        return numSelected === 1 ? $(this)[0].countSelectedCustomText : $(this)[0].countSelectedCustomTextPlural;
                    }
                    else {
                        return numSelected === 1 ? '{0} item selected' : '{0} items selected';
                    }
                }
            }).on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
                if (event.target.hasAttribute('multiple')) {
                    var $target = $(event.target);
                    var selectedItems = $target.val();

                    if (selectedItems.length === 0) {
                        $target.val([0]);
                        $target.selectpicker('refresh');
                    }
                    else if (oldValue !== undefined && oldValue.length > 0 && $.inArray('0', oldValue) !== -1) {
                        var index = selectedItems.indexOf('0');
                        selectedItems.splice(index, 1);
                        $target.val(selectedItems);
                        $target.selectpicker('refresh');
                    }
                    else if (selectedItems.length > 1 && $.inArray('0', selectedItems) !== -1) {
                        $target.val([0]);
                        $target.selectpicker('refresh');
                    }
                }
            });

            $('.bs-actionsbox .btn', $target).addClass('btn-dark');
        }

        function onFailure(xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);

            if (valueOrDefault(xhr.responseText, null) !== null) {
                var html = '<textarea id="errorDetails" style="min-height: 6rem;">' + xhr.responseText + '</textarea>';
                CustomModals.customAlert('Unexpected Error Occured', html, 'Ok')
                    .on('shown.bs.modal', function () {
                        $('.modal-dialog', $(this)).addClass('modal-lg');
                        autoGrow($('#errorDetails', $(this)));
                    });
            }
            else
                CustomModals.customAlert('Unexpected Error Occured', 'Http Status ' + xhr.status + ' ' + xhr.statusText + '.', 'Ok');
        }

        function onGrowl(data) {
            if (data.growlError !== null && data.growlError !== undefined) {
                $.growl.error({ title: "Error", message: data.growlError });
            }
            else if (data.growlWarning !== null && data.growlWarning !== undefined) {
                $.growl.warning({ title: "Warning", message: data.growlWarning });
            }
            else if (data.growlSuccess !== null && data.growlSuccess !== undefined) {
                $.growl.notice({ title: "Success", message: data.growlSuccess });
            }
        }

        function valueOrDefault(value, defaultValue) {
            if (typeof (value) !== "undefined" && value)
                return value;
            else
                return defaultValue;
        }

        return {
            autoGrow: function (target) {
                return autoGrow(target);
            },
            getFunction: function (code, argNames) {
                return getFunction(code, argNames);
            },
            initDataTable: function (target) {
                return initDataTable(target);
            },
            initDatePicker: function (target) {
                return initDatePicker(target);
            },
            initDatePickers: function (target) {
                return initDatePickers(target);
            },
            initDateTimePickers: function (target) {
                return initDateTimePickers(target);
            },
            initSelectPickers: function (target) {
                return initSelectPickers(target);
            },
            onFailure: function (xhr, status, error) {
                return onFailure(xhr, status, error);
            },
            onGrowl: function (data) {
                return onGrowl(data);
            },
            valueOrDefault: function (value, defaultValue) {
                return valueOrDefault(value, defaultValue);
            }
        };
    }();

    AjaxHelpers.init(Common, CustomModals);
    PartialModals.init(Common, AjaxHelpers);

    return {
        applyDefaultUnobtrusiveCallbacks: function () {
            return AjaxHelpers.applyDefaultUnobtrusiveCallbacks();
        },
        autoGrow: function (target) {
            return Common.autoGrow(target);
        },
        getFunction: function (code, argNames) {
            return Common.getFunction(code, argNames);
        },
        customWaiting: function (heading) {
            return CustomModals.customWaiting(heading);
        },
        customAlert: function (heading, html, okButtonTxt) {
            return CustomModals.customAlert(heading, html, okButtonTxt);
        },
        customConfirm: function (heading, question, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel) {
            return CustomModals.customConfirm(heading, question, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel);
        },
        customPrompt: function (heading, question, defaultText, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel) {
            return CustomModals.customPrompt(heading, question, defaultText, cancelButtonTxt, okButtonTxt, callbackOk, callbackCancel);
        },
        hookActionLinks: function () {
            return AjaxHelpers.hookActionLinks();
        },
        hookDownloadActionLinks: function () {
            return AjaxHelpers.hookDownloadActionLinks();
        },
        hookModalLinks: function () {
            return AjaxHelpers.hookModalLinks();
        },
        initDataTable: function (target) {
            return Common.initDataTable(target);
        },
        initDatePicker: function (target) {
            return Common.initDatePicker(target);
        },
        initDatePickers: function (target) {
            return Common.initDatePickers(target);
        },
        initDateTimePickers: function (target) {
            return Common.initDateTimePickers(target);
        },
        initSelectPickers: function (target) {
            return Common.initSelectPickers(target);
        },
        onBeginModal(xhr, element) {
            return PartialModals.onBeginModal(xhr, element);
        },
        onFailure: function (xhr, status, error) {
            return Common.onFailure(xhr, status, error);
        },
        onGrowl: function (data) {
            return Common.onGrowl(data);
        },
        onDownloadActionLink: function(event, params) {
            return AjaxHelpers.onDownloadActionLink(event, params);
        },
        onModalSuccess: function (data, status, xhr, modalId) {
            return PartialModals.onModalSuccess(data, status, xhr, modalId);
        },
        //onPostFormSuccess: function (data, formId) {
        //    return AjaxHelpers.onPostFormSuccess(data, formId);
        //},
        onPostFormSuccess: function (data, status, xhr, formOrId, omitRefresh = false) {
            return AjaxHelpers.onPostFormSuccess(data, status, xhr, formOrId, omitRefresh);
        },
        onActionLink: function (event) {
            AjaxHelpers.onActionLink(event);
        },
        onSuccessActionLink: function (data, status, xhr, target) {
            return AjaxHelpers.onSuccessActionLink(data, status, xhr, target);
        },
        onSuccessShowModal: function (element, modalId) {
            return PartialModals.onSuccessShowModal(element, modalId);
        },
        onRowAction: function (event, url, actionType = '') {
            return AjaxHelpers.onRowAction(event, url, actionType);
        },
        valueOrDefault: function (value, defaultValue) {
            return Common.valueOrDefault(value, defaultValue);
        }
    };
}();

//these functions need to be global because they are called from unobtrusive ajax
function autoGrow(target) {
    return Site.autoGrow(target);
}

function onBeginModal(xhr, element) {
    return Site.onBeginModal(xhr, element);
}

function onFailure(xhr, status, error) {
    return Site.onFailure(xhr, status, error);
}

function onGrowl(data) {
    return Site.onGrowl(data);
}

function onModalSuccess(data, status, xhr, modalId) {
    return Site.onModalSuccess(data, status, xhr, modalId);
}

function onPostFormSuccess(data, status, xhr, formOrId, omitRefresh = false) {
    return Site.onPostFormSuccess(data, status, xhr, formOrId, omitRefresh);
}

function onSuccessActionLink(data, status, xhr, target) {
    return Site.onSuccessActionLink(data, status, xhr, target);
}

function onSuccessShowModal(element, modalId) {
    return Site.onSuccessShowModal(element, modalId);
}

function initializeComponents(element) {
    Site.initDatePickers(element);
    Site.initDateTimePickers(element);
    Site.initSelectPickers(element);
}