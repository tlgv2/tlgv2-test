﻿$(document).ready(function () {
    $('#myAccountForm').ViewLoader();
    $('#ContactInfo').ViewLoader();
    $('#TitleInfo').ViewLoader();
    $('#RecommendedLinks').ViewLoader();
    $('#Documents').ViewLoader();
    $('#notes-tab').on('click', function () {

        $('#SupplierNoteForm').submit();

    });
    $('#Documents').on('click', ".btn-download", function (event) {
        event.preventDefault();

        downloadFile(event);
    });

    $('.customer-body-right').on('click', '#RecommendedLinks', function (e) {
        console.log("hello 0");
        console.log($(this));
    });

    //$('#RecommendedLinks .addLinkBtn').on('click', function (e) {
    $('a.addLinkBtn').on('click', function (e) {
        console.log("hello");
        console.log($(this));

        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        console.log("trig1");

        var myDropzone = new Dropzone("#uploader", {
            paramName: "file",
            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 5,
            maxFiles: 5,
            maxFilesize: 500,
            timeout: 3000000,
            init: function () {

                var myDropzone = this;

                $("#save-documents").click(function (e) {
                    $("#overlay").show();
                    myDropzone.processQueue();

                });

                this.on("sending", function (file, xhr, formData) {
                    let restrictVisibility = $("#RestrictedVisibility").is(':checked');
                    let visibleDateFrom = $("#VisibleDateFrom").val();
                    let visibleDateTo = $("#VisibleDateTo").val();
                    let contextCoreName = $("#context-core-name").val();
                    let contextLinkId = $("#context-link-id").val();
                    let partnerName = $("#partner-name").text();

                    formData.append("RestrictedVisibility", restrictVisibility);
                    formData.append("VisibleDateFrom", visibleDateFrom);
                    formData.append("VisibleDateTo", visibleDateTo);
                    formData.append("ContextCoreName", contextCoreName);
                    formData.append("ContextLinkID", contextLinkId);
                    formData.append("PartnerName", partnerName);
                });

                this.on("success", function (file, data) {
                    $("#overlay").hide();
                   // $("#alert-upload-success").toggleClass("d-none");
                    Site.onModalSuccess(data);
                    $('#FileUploadModal').modal('toggle');
                    $('#Documents').ViewLoader('refresh');
                });

                this.on("error", function (file, data) {
                    console.log(data);
                    $("#overlay").hide();
                    $("#alert-upload-error").toggleClass("d-none");
                });

                this.on("queuecomplete", function (file) {
                    this.removeAllFiles();
                    $("#overlay").hide();
                });
            }
        });
    });

    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        console.log("trig2");

        $("#ui-datepicker-div").hide();
    });
});

function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
     

    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    $(".info").prop("disabled", function (i, v) { return !v; });
    $(".dropdown-toggle").toggleClass("disabled");
}

function downloadFile(event) {
    Site.onDownloadActionLink(event, { withUrlParamOverride: null });
}
