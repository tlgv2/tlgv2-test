﻿$(document).ready(function () {
    $('#UserInfo').ViewLoader();
    $('#Settings').ViewLoader();
    $('#UserLocation').ViewLoader();
});

function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }

    $(".basic-info-field").prop("disabled", function (i, v) {
        if ($(this).is("select")) {
            $(this).next("button").toggleClass("disabled");
        }
        return !v;
    });
}
