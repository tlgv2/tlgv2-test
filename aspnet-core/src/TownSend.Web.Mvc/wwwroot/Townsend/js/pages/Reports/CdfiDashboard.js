﻿$(document).ready(function () {
    var $loanStatus = $('select#DashboardRequest_LoanStatus');
    $("option[value=2]", $loanStatus).prop('selected', true);
    $("option[value=2]", $loanStatus).data('showPipeline', false);
    $(":not(option[value=2])", $loanStatus).data('showPipeline', true);

    $loanStatus.on('change', function (e) {
        var target = e.target;
        var $target = $(target);

        var showPipeline = false;
        $('option:selected', $target).map(function () {
            if ($(this).data('showPipeline')) {
                showPipeline = true;
            }
        });

        if (showPipeline) {
            $('#DashboardRequest_PipelinePercent_Wrapper').show();
            //$('label[for="DashboardRequest_PipelinePercent"]').show();
        }
        else {
            $('#DashboardRequest_PipelinePercent_Wrapper').hide();
            //$('label[for="DashboardRequest_PipelinePercent"]').hide();
        }
    });

    $('select#DashboardRequest_LoanStatus').trigger('change');

    $('#DashboardRequest_DateEnd').datepicker({
        onSelect: function (d, i) {
            if (d !== i.lastVal) {
                $(this).change();
            }
        }
    });

    $('#btnRefresh').on('click', function (e) {
        e.preventDefault();
        e.target.blur();

        //if ($("#dashboardFilterForm").valid()) {
            loadDashboardData();
        //}
    });

    $('#btnExportData').on('click', function (event) {
        event.preventDefault();

        if ($('#btnExport').data('isEnabled') === true) {
            if (JSON.stringify(createDashboardRequest(true)) !== JSON.stringify(createDashboardRequest(false))) {
                console.log('downloadExcelPreviousFilters');
                downloadExcel(event, true);
            }
            else {
                console.log('downloadExcelCurrentFilters');
                downloadExcel(event, false);
            }
        }
        else {
            $.growl.warning({ title: 'Warning', message: 'No Data to Export.' });
        }
    });

    $('#btnExportSheetSummary').on('click', function (event) {
        event.preventDefault();

        if ($('#btnExport').data('isEnabled') === true) {
            if (JSON.stringify(createDashboardRequest(true)) !== JSON.stringify(createDashboardRequest(false))) {
                console.log('downloadPDFPreviousFilters');
                downloadPDF(event, true);
            }
            else {
                console.log('downloadPDFCurrentFilters');
                downloadPDF(event, false);
            }
        }
        else {
            $.growl.warning({ title: 'Warning', message: 'No Data to Export.' });
        }
    });

    loadDashboardData();
    $('.chart-container').show();
    
});

function createDashboardRequest(usePrevious) {
    var dashboardRequest = '';

    if (usePrevious) {
        dashboardRequest = JSON.parse($('#PreviousFilters').val());
    }
    else {
        dashboardRequest = $('#dashboardFilterForm').serialize();
        //console.log('dashboardRequest...');
        //console.log(dashboardRequest);
    }

    return dashboardRequest;
}

function loadDashboardData() {
    var dashboardRequest = createDashboardRequest(false);

    $('#PreviousFilters').val(JSON.stringify(dashboardRequest));

    //var formData = new FormData();

    //$.each(dashboardRequest, function (key, value) {
    //    formData.append(key, value);
    //});

    return $.ajax({
        type: "POST",
        url: "/Reports/CdfiDashboard",
        data: dashboardRequest,
        //processData: false,
        //contentType: false,
        dataType: "json",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            Site.onFailure(XMLHttpRequest, textStatus, errorThrown);
        },
        success: function (data) {
            //var data = JSON.parse(response.d);
            console.log(data);
            if (Site.onPostFormSuccess(data, '#dashboardFilterForm')) {
                generateDashboard(data);
            }
        }
    });
}

function downloadExcel(event, usePrevious) {
    var dashboardRequest = createDashboardRequest(usePrevious);
    Site.onDownloadActionLink(event, { withUrlParamOverride: dashboardRequest });
}

function downloadPDF(event, usePrevious) {
    var dashboardRequest = createDashboardRequest(usePrevious);
    Site.onDownloadActionLink(event, { withUrlParamOverride: dashboardRequest });
}

function generateDashboard(data) {
    //console.log('data.HasResults: ' + data.HasResults);
    $('#btnExport').data('isEnabled', (data.HasResults));

    setSummaryValue(data, 'MissingCdfiTractCount');
    setSummaryValue(data, 'LastDataUpload');

    drawUnitPieChart(data);
    drawLoanTotalPieChart(data);

    drawUnitBarChart(data);
    drawLoanTotalBarChart(data);
}

function setSummaryValue(data, target) {
    $('span#' + target).html(data[target]);
}

function drawUnitPieChart(data) {
    drawPieChart('UnitCountPieCanvas', 'By Unit', data.UnitQualificationSummary, '', false);
}

function drawLoanTotalPieChart(data) {
    console.log(data.LoanTotalQualificationSummary);
    drawPieChart('LoanTotalPieCanvas', 'By Dollars', data.LoanTotalQualificationSummary, '$', true);
}

function drawPieChart(chartCanvas, title, data, unitPrefix, useMetricFormatting) {
    var $canvas = $('canvas#' + chartCanvas);
    var ctx = document.getElementById(chartCanvas).getContext("2d");

    var pieData = data.map(function (summary) { return summary.Data; });
    var pieLabels = data.map(function (summary) { return summary.Label; });
    var pieBackgroundColor = data.map(function (summary) { return summary.BackgroundColor; });
    var pieSubDataSet = data.map(function (summary) { return summary.SubData; });

    var datasets = [{
        data: pieData,
        subdata: pieSubDataSet,
        backgroundColor: pieBackgroundColor,
        label: 'outer'
    }];

    renderPieChart($canvas, ctx, title, datasets, pieLabels, unitPrefix, useMetricFormatting);
}

function drawUnitBarChart(data) {
    var $canvas = $('canvas#UnitCountBarCanvas');
    var datasets = data.UnitMonthSummaryBreakdown;
    var labels = data.UnitMonthSummary;
    var minTicks = 0;
    var maxTicks = 0;
    var ticks = {
        //max: maxTicks,
        //min: minTicks,
        //stepSize: 1,
        callback: function (value, index, values) {
            if (value % 1 === 0) { return numeral(value).format('0.[0]a'); }
        }
    };

    renderBarChart($canvas, datasets, labels, ticks, '');
}

function drawLoanTotalBarChart(data) {
    var $canvas = $('canvas#LoanTotalBarCanvas');
    var datasets = data.LoanTotalMonthSummaryBreakdown;
    var labels = data.LoanTotalMonthSummary;
    var minTicks = 0; //data.LoanTotalMinTicks; <--- Chart draws below the horizontal when zero. Not good.
    var maxTicks = 0; //data.LoanTotalMaxTicks;

    var ticks = {
        //max: maxTicks,
        //min: minTicks,
        beginAtZero: true,
        callback: function (value, index, values) {
            return '$' + numeral(value).format('0.[0]a');
        }
    };

    renderBarChart($canvas, datasets, labels, ticks, '$');
}

function renderBarChart(canvas, datasets, labels, ticks, unitPrefix) {

    var data = {
        labels: labels,
        datasets: datasets
    };

    if (canvas.data('chart') === undefined) {
        var newChart = new Chart(canvas, {
            animation: true,
            type: 'bar',
            options: {
                barValueSpacing: 20,
                custom: {
                    precision: 1
                },
                hover: { mode: null },
                legendCallback: function (chart) {
                    var text = [];
                    text.push('<div class="' + chart.id + '-legend">');
                    text.push('<div class="legend-row">');

                    var data = chart.data;
                    var datasets = data.datasets;

                    if (datasets.length) {
                        for (var i = 0; i < datasets.length; i++) {
                            if (i % 2 === 0) {
                                text.push('<div class="legend-item" data-chart-id="' + chart.canvas.id + '" data-chart-legend-index="' + i + '"><span style="background-color:' + datasets[i].backgroundColor + '"></span>');
                                if (labels[i]) {
                                    text.push('<div>' + datasets[i].label + '</div>');
                                }
                                text.push('</div>');
                            }
                        }
                    }

                    text.push('</div>');
                    text.push('</div>');
                    return text.join('');
                },
                legend: {
                    display: false,
                    labels: {
                        boxWidth: 10,
                        filter: function (legendItem, data) {
                            return legendItem.datasetIndex % 2 === 0; // display legend only for 1st stack
                        }
                    },
                    position: 'top'
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 10
                    }
                },
                //responsive: true,textStrokeColor: '#737373'
                maintainAspectRatio: false,
                responsive: true,
                //aspectRatio: 1,
                plugins: {
                    // Change options for ALL labels of THIS CHART
                    datalabels: {
                        align: 'top',
                        anchor: 'start',
                        //offset: 6,
                        clamp: false,
                        //clip: true,
                        //backgroundColor: 'rgba(0, 0, 0, 0.1)',
                        //borderRadius: 10,
                        textStrokeWidth: 0.7,
                        textStrokeColor: '#737373',
                        color: 'white',
                        font: {
                            size: 12,
                            //lineHeight: 0.9
                        },
                        /*padding: {
                            top: 10,
                            left: 10,
                            right: 10,
                            bottom: 10
                        },*/
                        display: function (context) {
                            if (context.datasetIndex % 2 === 0) {
                                var approved = context.chart.data.datasets[context.datasetIndex].data[context.dataIndex];
                                var notApproved = context.chart.data.datasets[context.datasetIndex + 1].data[context.dataIndex];
                                var total = approved + notApproved;

                                if (total !== 0)
                                    return true;
                            }

                            return false;

                            //return context.datasetIndex % 2 === 0; // display labels only for 1st stack
                            //return /*context.dataIndex === 0 // display labels only for the 1st group
                        },
                        formatter: function (value, context) {
                            var datasetMeta = context.chart.getDatasetMeta(0);
                            var data = datasetMeta.data[context.dataIndex];

                            if (data._datasetIndex % 2 === 0) {
                                var approved = value;
                                var notApproved = context.chart.data.datasets[context.datasetIndex + 1].data[context.dataIndex];
                                var total = approved + notApproved;
                                var percentage = total === 0 ? 0 : approved / total * 100;
                                var percentageValue = total === 0 ? '' : percentage.toFixed(context.chart.config.options.custom.precision) + '%';

                                return percentageValue;
                            }
                            else {
                                return '';
                            }
                            //return context.dataset.label;
                        },
                        rotation: 270,
                        textShadowBlur: 6,
                        textShadowColor: '#000000'
                    }
                },

                tooltips: {
                    enabled: true,
                    footerFontStyle: 'normal',
                    footerFontColor: '#cfcfcf',
                    footerFontSize: 10,

                    //mode: 'x',
                    //axis: 'x',
                    //intersect: true,
                    //titleMarginBottom: 0,
                    callbacks: {
                        title: function (tooltipItem, data, x) {
                            if (tooltipItem[0].datasetIndex % 2 === 0) {
                                return 'Qualified';
                            }
                            else {
                                return 'Not Qualified';
                            }
                            //return data.labels[tooltipItem[0].index]; //basic title, return xAxis text.
                        },
                        //beforeBody: function (tooltipItem, data) {
                        //var line1 = '';
                        //var line2 = 'Inside CM: 2';
                        //var line3 = 'Outide CM: 2';
                        //var line4 = '';

                        //return [line1, line2, line3, line4];
                        //return [line2, line3];
                        //},
                        footer: function (tooltipItem, data) {
                            var datasetIndex = tooltipItem[0].datasetIndex;
                            var index = tooltipItem[0].index;
                            var dataset = data.datasets[datasetIndex];
                            var subDataset = dataset.subdata;
                            var subDataLabels = dataset.subdatalabels;

                            var lines = [];
                            if (subDataset !== undefined && subDataLabels !== undefined) {
                                var subData = subDataset[index];
                                lines = new Array(subDataLabels.length);
                                subDataLabels.forEach(function (item, i) {
                                    lines[i] = '' + item + ': ' + numeral(subData[i]).format(unitPrefix + '0,0');
                                });
                            }

                            return lines;
                        },
                        label: function (tooltipItem, data) {

                            var thisIndex = tooltipItem.datasetIndex;
                            var otherIndex = thisIndex;

                            if (thisIndex % 2 === 0)
                                otherIndex++;
                            else
                                otherIndex--;

                            var thisValue = data.datasets[thisIndex].data[tooltipItem.index];
                            var otherValue = data.datasets[otherIndex].data[tooltipItem.index];
                            var total = thisValue + otherValue;

                            var percentage = total === 0 ? 0 : thisValue / total * 100;
                            var percentageValue = total === 0 ? '' : percentage.toFixed(1) + '%';
                            var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                            return numeral(amount).format(unitPrefix + '0,0') + ' ( ' + percentageValue + ' )';
                            //var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                            //return numeral(amount).format(unitPrefix + '0,0');
                        }
                    }
                }
            }
        });

        canvas.data('chart', newChart);
    }

    //at this point, the canvas always has a 'chart' element stuffed in the $.data()
    var chart = canvas.data('chart');
    chart.data = data;

    chart.config.options.scales = {
        yAxes: [{
            gridLines: {
                display: true
            },
            stacked: true,
            ticks: ticks
        }],
        xAxes: [{
            stacked: true,
            ticks: {
                //fontLineHeight: 0.9, <-- will be available in 2.8
                fontSize: 10,
                //fontColor: "red"
            }
        }]
    };

    chart.update();

    var legendBy = canvas.data('legendBy'); //add attribute data-legend-by="xxxxx"  to your canvas.
    if (legendBy !== undefined) {
        var $legend = $('div#' + legendBy);
        if (!$legend.hasClass('chart-legend')) {
            $legend.addClass('chart-legend');
            //$legend.wrap('<fieldset class="border p-1"></fieldset>');
            //$legend.parent().prepend('<legend class="w-auto">Legend</legend>');
        }

        $legend.html(chart.generateLegend());
    }
}

/*
	 * Evaluates the given `inputs` sequentially and returns the first defined value.
	 * @param {Array[]} inputs - An array of values, falling back to the last value.
	 * @param {Object} [context] - If defined and the current value is a function, the value
	 * is called with `context` as first argument and the result becomes the new input.
	 * @param {Number} [index] - If defined and the current value is an array, the value
	 * at `index` become the new input.
	 * @since 2.7.0
	 */
function resolve(inputs, context, index) {
    var i, ilen, value;

    for (i = 0, ilen = inputs.length; i < ilen; ++i) {
        value = inputs[i];
        if (value === undefined) {
            continue;
        }
        if (context !== undefined && typeof value === 'function') {
            value = value(context);
        }
        if (index !== undefined && isArray(value)) {
            value = value[index];
        }
        if (value !== undefined) {
            return value;
        }
    }
}

/*
 * Returns true if `value` is an array (including typed arrays), else returns false.
 * @param {*} value - The value to test.
 * @returns {Boolean}
 * @function
 */
function isArray(value) {
    if (Array.isArray && Array.isArray(value)) {
        return true;
    }
    var type = Object.prototype.toString.call(value);
    if (type.substr(0, 7) === '[object' && type.substr(-6) === 'Array]') {
        return true;
    }
    return false;
}

function renderPieChart(canvas, ctx, title, datasets, labels, unitPrefix, useMetricFormatting) {

    if (canvas.data('chart') === undefined) {
        var newChart = new Chart(ctx, {
            animation: true,
            type: 'pie',
            // Configuration options go here
            options: {
                animation: {
                    onProgress: function (animation) {
                        drawTotal(animation, false);
                    },
                    onComplete: function (animation) {
                        drawTotal(animation, true);
                    }
                },
                borderColor: ['#000000'],
                custom: {
                    mode: 'percentage', //value
                    precision: 1,
                    unitPrefix: unitPrefix,
                    useMetricFormatting: useMetricFormatting
                },
                //events: ['click'],
                //events: [], <-- prevents tooltips too. this does not. --> //hover: { mode: null },
                hover: { mode: null },
                //responsive: true,
                maintainAspectRatio: false,
                //aspectRatio: 1,

                legendCallback: function (chart) {
                    var text = [];
                    text.push('<div class="' + chart.id + '-legend">');
                    

                    var data = chart.data;
                    var datasets = data.datasets;
                    var labels = data.labels;
                    var unitPrefix = chart.config.options.custom.unitPrefix;
                    var useMetricFormatting = chart.config.options.custom.useMetricFormatting;
                    var formatting = '0,0';
                    if (useMetricFormatting)
                        formatting = '0,0.0a';

                    if (datasets.length) {
                        for (var i = 0; i < datasets[0].data.length; ++i) {
                            if (i % 2 === 0)
                                text.push('<div class="legend-row">');

                            text.push('<div class="legend-item" data-chart-id="' + chart.canvas.id + '" data-chart-legend-index="' + i + '"><span style="background-color:' + datasets[0].backgroundColor[i] + '"></span>');
                            if (labels[i]) {
                                text.push('<div>' + (labels[i].indexOf(',') !== -1 ? labels[i].replace(',', ',<br />') : labels[i]) + ' (' + numeral(datasets[0].data[i]).format(unitPrefix + formatting) + ')</div>');
                            }
                            text.push('</div>');

                            if (i % 2 === 1)
                                text.push('</div>');
                        }
                    }

                    text.push('</div>');
                    return text.join('');
                },

                legend: {
                    display: false,
                    position: 'top',
                    onClick: null,
                    labels: {
                        boxWidth: 10,
                        generateLabels: function (chart) {
                            var data = chart.data;
                            if (data.labels.length && data.datasets.length) {
                                return data.labels.map(function (label, i) {
                                    var meta = chart.getDatasetMeta(0);
                                    var ds = data.datasets[0];
                                    var arc = meta.data[i];
                                    var custom = arc && arc.custom || {};
                                    var arcOpts = chart.options.elements.arc;
                                    var fill = resolve([custom.backgroundColor, ds.backgroundColor, arcOpts.backgroundColor], undefined, i);
                                    var stroke = resolve([custom.borderColor, ds.borderColor, arcOpts.borderColor], undefined, i);
                                    var bw = resolve([custom.borderWidth, ds.borderWidth, arcOpts.borderWidth], undefined, i);
                                    //console.log(ds.subdata[i]);
                                    return {
                                        text: label + ' (' + ds.data[i] + ')',
                                        fillStyle: fill,
                                        strokeStyle: stroke,
                                        lineWidth: bw,
                                        hidden: isNaN(ds.data[i]) || meta.data[i].hidden,

                                        // Extra data used for toggling the correct item
                                        index: i
                                    };
                                });
                            }
                            return [];
                        }
                    }
                },

                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                },

                plugins: {
                    // Change options for ALL labels of THIS CHART
                    datalabels: {
                        textStrokeWidth: 0.7,
                        textStrokeColor: '#737373',
                        color: 'white',
                        font: function (context) {
                            var radius = chart.outerRadius;

                            //var width = context.chart.width;
                            var size = Math.round(radius / 10);
                            return {
                                size: size,
                                weight: 600
                            };
                        },
                        formatter: function (value, context) {
                            var datasetMeta = context.chart.getDatasetMeta(context.datasetIndex);
                            var data = datasetMeta.data[context.dataIndex];

                            var displayValue = '';
                            if (context.chart.config.options.custom.mode === 'percentage') {
                                var percentage = data._model.circumference / context.chart.config.options.circumference * 100;
                                percentage = parseFloat(percentage.toFixed(context.chart.config.options.custom.precision));
                                if (percentage === 0)
                                    displayValue = '';
                                else
                                    displayValue = percentage + '%';
                            }
                            else {
                                displayValue = '$' + parseFloat(value.toFixed(0));
                            }

                            return displayValue;
                        },
                        textShadowBlur: 5,
                        textShadowColor: '#000000'
                    }
                },

                tooltips: {
                    enabled: true,
                    callbacks: {
                        title: function (tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem[0].datasetIndex];
                            var index = tooltipItem[0].index;
                            return data.labels[tooltipItem[0].index];
                        },
                        label: function (tooltipItem, data) {
                            var metaKey = Object.keys(data.datasets[tooltipItem.datasetIndex]._meta)[0];

                            var chart = data.datasets[tooltipItem.datasetIndex]._meta[metaKey].controller.chart;

                            var meta = chart.getDatasetMeta(tooltipItem.datasetIndex);
                            var data2 = meta.data[tooltipItem.index];

                            var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                            var percentage = data2._model.circumference / chart.config.options.circumference * 100;

                            return numeral(amount).format(unitPrefix + '0,0') + ' ( ' + parseFloat(percentage).toFixed(1) + '% )';
                        }
                    }
                }
            }
        });

        canvas.data('chart', newChart);
    }

    var chart = canvas.data('chart');
    var titleFontSize = Math.round(canvas.width() / 12);

    chart.data = {
        labels: labels,
        datasets: datasets
    };

    chart.config.options.title = {
        display: false,
        position: 'left',
        text: title,
        fontSize: titleFontSize,
        padding: 5
    };

    chart.update();

    var legendBy = canvas.data('legendBy'); //add attribute data-legend-by="xxxxx"  to your canvas.
    if (legendBy !== undefined) {
        var $legend = $('div#' + legendBy);
        if (!$legend.hasClass('chart-legend')) {
            $legend.addClass('chart-legend');
            //$legend.wrap('<fieldset class="border p-1"></fieldset>');
            //$legend.parent().prepend('<legend class="w-auto">Legend</legend>');
        }

        $legend.html(chart.generateLegend());
    }
}

function drawTotal(animation, complete) {
    //console.log(animation.chart.data.datasets[0]);
    var chart = animation.chart;
    var datasetMeta = chart.getDatasetMeta(0);
    //var ctx = chart.ctx;
    var datasets = animation.chart.data.datasets;

    var $canvas = $(chart.canvas);
    var totaledBy = $canvas.data('totaledBy'); //add attribute data-totaled-by="xxxxx" to your canvas.
    if (totaledBy === undefined)
        return;

    var $totals = $('div#' + totaledBy);
    if (!$totals.hasClass('chart-totals')) {
        $totals.addClass('chart-totals');

        var text = [];
        for (var i = 0; i < 3; i++) {
            text.push('<div id="totalLine' + i + '" class="total-line">');
            text.push('<span class="title"></span>');
            text.push('<span class="value"></span>');
            text.push('<span class="percent"></span>');
            text.push('</div>');
        }

        $totals.html(text.join(''));
    }

    //console.log($canvas);

    var inInIndex = 0;
    var outInIndex = 1;
    var outIndex = 2;
    var nqIndex = 3;

    //var datasetMeta = chart.getDatasetMeta(0);
    var NqCirc = datasetMeta.data[nqIndex]._model.circumference;
    var QCirc1 = datasetMeta.data[inInIndex]._model.circumference;
    var QCirc2 = datasetMeta.data[outInIndex]._model.circumference;
    var QCirc3 = datasetMeta.data[outIndex]._model.circumference;
    var QCirc = QCirc1 + QCirc2 + QCirc3;

    datasets.forEach(function (dataset, i) {
        var data = datasets[i].data;

        //var datasetMeta = chart.getDatasetMeta(i);
        var totalNonQualified = NqCirc !== 0 ? data[nqIndex] : 0.0;
        var tq1 = QCirc1 !== 0 ? data[inInIndex] : 0.0;
        var tq2 = QCirc2 !== 0 ? data[outInIndex] : 0.0;
        var tq3 = QCirc3 !== 0 ? data[outIndex] : 0.0;
        var totalQualified = tq1 + tq2 + tq3;
        var total = totalNonQualified + totalQualified;

        var circumference = chart.config.options.circumference;
        var percentageNonQualified = NqCirc / circumference * 100;
        var percentageQualified = QCirc / circumference * 100;
        var precision = chart.config.options.custom.precision;
        var unitPrefix = chart.config.options.custom.unitPrefix;
        percentageNonQualified = parseFloat(percentageNonQualified.toFixed(precision));
        percentageQualified = parseFloat(percentageQualified.toFixed(precision));
        var tmpvalue = totalQualified + ' ( ' + percentageQualified.toFixed(precision) + '% )' + '            ' + totalNonQualified + ' ( ' + percentageNonQualified.toFixed(precision) + '% )';

        var fun = 1;
        if (tmpvalue !== $(chart.canvas).data('total')) //the reason we do this is because the animation is triggered on hover. This prevents it from animating.
            fun = animation.animationObject.currentStep / animation.animationObject.numSteps;

        if (isNaN(fun))
            fun = 1;

        //console.log(fun);
        //console.log('total: ' + total + ', totalQualified: ' + totalQualified + ', totalNonQualified: ' + totalNonQualified + ', percentageQualified: ' + percentageQualified + ', percentageNonQualified: ' + percentageNonQualified);

        total = Math.floor(total * fun);
        totalQualified = Math.floor(totalQualified * fun);
        totalNonQualified = Math.floor(totalNonQualified * fun);
        percentageNonQualified = parseFloat((percentageNonQualified * fun).toFixed(precision));
        percentageQualified = parseFloat((percentageQualified * fun).toFixed(precision));

        var value = totalQualified + ' ( ' + percentageQualified.toFixed(precision) + '% )' + '            ' + totalNonQualified + ' ( ' + percentageNonQualified.toFixed(precision) + '% )';

        setTotalsLine($totals, 0, 'Qualified:', totalQualified, percentageQualified, precision, unitPrefix);
        setTotalsLine($totals, 1, 'Not Qualified:', totalNonQualified, percentageNonQualified, precision, unitPrefix);
        setTotalsLine($totals, 2, 'Total:', total, null, precision, unitPrefix);

        if (complete) { //store the total as a string in the data to compare later aganst tmpValue
            $(chart.canvas).data('total', value);
        }
    });
}

function setTotalsLine($totalsElement, lineIndex, label, amount, percentage, precision, unitPrefix) {
    //console.log('lineIndex: ' + lineIndex + ', label: ' + label + ', amount: ' + amount + ', percentage: ' + percentage);
    var $line = $('#totalLine' + lineIndex, $totalsElement);
    $('.title', $line).text(label);
    $('.value', $line).text(numeral(amount).format(unitPrefix + '0,0'));

    if (percentage !== null)
        $('.percent', $line).text('(' + percentage.toFixed(precision) + '%)');
}