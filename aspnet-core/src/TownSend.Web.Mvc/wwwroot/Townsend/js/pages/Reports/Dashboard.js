﻿$(document).ready(function () {
    $("#chart").hide();

    $("#dashboardListing").ViewLoader();

    $('#dashboard-refresh').on('click', function (e) {
        e.preventDefault();
        e.target.blur();

        loadDashboardData();
    });

    loadDashboardData();

    $("#chart").show();
});

function loadDashboardData() {
    var DashboardFilter = $('#DashboardForm').serialize();

    if (!DashboardFilter.includes("DashboardFilter.ProductID")) {
        $.growl.warning({ title: "Warning", message: "Please select a team" });
    }

    else {
        $.ajax({
            url: "/Reports/Dashboard?handler=Dashboard",
            type: "GET",
            data: DashboardFilter,
            dataType: "json",
            success: function (data) {
                if (Site.onPostFormSuccess(data, '#DashboardForm')) {
                    var $canvas = $('canvas#AmountBarCanvas');
                    var datasets = data.AmountMonthSummaryBreakdown;
                    var labels = data.AmountMonthSummary;

                    var ticks = {
                        beginAtZero: true,
                        callback: function (value, index, values) {
                            return '$' + numeral(value).format('0.[0]a');
                        }
                    };

                    renderHeader(data);

                    renderBarChart($canvas, datasets, labels, ticks, '$');
                }
            },
            error: function (ex) {
                $("#alert").html('<div class="seperator"></div><div class="alert alert-danger">There is No Data To Display<div>');
            }
        });
    }
}

function renderHeader(data) {
    $("#header-Team").html(data.Team);
    $("#header-ActiveMemberContract").html(data.ActiveMemberContract);
    $("#header-AvgMemberPerTeam").html(data.AvgMemberPerTeam);
    $("#header-AvgMemberContract").html(data.AvgMemberContract);
    $("#header-AvgTeamContract").html(data.AvgTeamContract);
    $("#header-Tier").html(data.Tier);
    $("#header-TotalRevenue").html(data.TotalRevenue);
}

function renderBarChart(canvas, datasets, labels, ticks, unitPrefix) {

    var data = {
        labels: labels,
        datasets: datasets
    };

    if (canvas.data('chart') === undefined) {
        var newChart = new Chart(canvas, {
            animation: true,
            type: 'bar',
            options: {
                barValueSpacing: 20,
                custom: {
                    precision: 1
                },
                hover: { mode: null },
                legendCallback: function (chart) {
                    var text = [];
                    text.push('<div class="' + chart.id + '-legend">');
                    text.push('<div class="legend-row">');

                    var data = chart.data;
                    var datasets = data.datasets;

                    if (datasets.length) {
                        for (var i = 0; i < datasets.length; i++) {
                            text.push('<div class="legend-item" data-chart-id="' + chart.canvas.id + '" data-chart-legend-index="' + i + '"><span style="background-color:' + datasets[i].backgroundColor + '"></span>');

                            if (labels[i]) {
                                text.push('<div>' + datasets[i].label + '</div>');
                            }

                            text.push('</div>');
                        }
                    }

                    text.push('</div>');
                    text.push('</div>');
                    return text.join('');
                },
                legend: {
                    display: false,
                    labels: {
                        boxWidth: 10,
                        filter: function (legendItem, data) {
                            return legendItem.datasetIndex;
                        }
                    },
                    position: 'top'
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 5,
                        bottom: 10
                    }
                },
                maintainAspectRatio: false,
                responsive: true,
                plugins: {
                    datalabels: {
                        align: 'top',
                        anchor: 'start',
                        clamp: false,
                        textStrokeWidth: 0.7,
                        textStrokeColor: '#737373',
                        color: 'white',
                        font: {
                            size: 12,
                        },
                        display: function (context) {
                            return true;
                        },
                        formatter: function (value, context) {
                            var datasetMeta = context.chart.getDatasetMeta(0);

                            return '';
                        },
                        rotation: 270,
                        textShadowBlur: 6,
                        textShadowColor: '#000000'
                    }
                },

                tooltips: {
                    enabled: true,
                    footerFontStyle: 'normal',
                    footerFontColor: '#cfcfcf',
                    footerFontSize: 10,

                    callbacks: {
                        title: function (tooltipItem, data, x) {
                            return data.datasets[tooltipItem[0].datasetIndex].label;
                        },
                        footer: function (tooltipItem, data) {
                            var datasetIndex = tooltipItem[0].datasetIndex;
                            var index = tooltipItem[0].index;
                            var dataset = data.datasets[datasetIndex];
                            var subDataset = dataset.subdata;
                            var subDataLabels = dataset.subdatalabels;

                            var lines = [];
                            if (subDataset !== undefined && subDataLabels !== undefined) {
                                var subData = subDataset[index];
                                lines = new Array(subDataLabels.length);
                                subDataLabels.forEach(function (item, i) {
                                    lines[i] = '' + item + ': ' + numeral(subData[i]).format(unitPrefix + '0,0');
                                });
                            }

                            return lines;
                        },
                        label: function (tooltipItem, data) {

                            var thisIndex = tooltipItem.datasetIndex;

                            var thisValue = 0;
                            var otherValue = 0;

                            switch (thisIndex) {
                                case 0:
                                    thisValue = data.datasets[thisIndex].data[tooltipItem.index];

                                    otherValue = data.datasets[1].data[tooltipItem.index] + data.datasets[2].data[tooltipItem.index];

                                    break;
                                case 1:
                                    thisValue = data.datasets[thisIndex].data[tooltipItem.index];

                                    otherValue = data.datasets[0].data[tooltipItem.index] + data.datasets[2].data[tooltipItem.index];

                                    break;
                                case 2:
                                    thisValue = data.datasets[thisIndex].data[tooltipItem.index];

                                    otherValue = data.datasets[0].data[tooltipItem.index] + data.datasets[1].data[tooltipItem.index];

                                    break;
                            }

                            var total = thisValue + otherValue;

                            var percentage = total === 0 ? 0 : thisValue / total * 100;
                            var percentageValue = total === 0 ? '' : percentage.toFixed(1) + '%';
                            var amount = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

                            var hoverResult = numeral(amount).format(unitPrefix + '0,0.00') + ' ( ' + percentageValue + ' )';

                            return hoverResult;
                        }
                    }
                },

                onClick: function (e) {
                    var activePoints = newChart.getElementsAtEvent(e);
                    var selectedIndex = activePoints[0]._index;
                    loadListing(this.data.labels[selectedIndex][0] + this.data.labels[selectedIndex][1]);
                }
            }
        });

        canvas.data('chart', newChart);
    }

    var chart = canvas.data('chart');
    chart.data = data;

    chart.config.options.scales = {
        yAxes: [{
            gridLines: {
                display: true
            },
            stacked: true,
            ticks: ticks
        }],
        xAxes: [{
            stacked: true,
            ticks: {
                fontSize: 10,
            }
        }]
    };

    chart.update();

    var legendBy = canvas.data('legendBy');
    if (legendBy !== undefined) {
        var $legend = $('div#' + legendBy);
        if (!$legend.hasClass('chart-legend')) {
            $legend.addClass('chart-legend');
        }

        $legend.html(chart.generateLegend());
    }
}

function loadListing(monthYear) {
    $("#DashboardFilter_monthYear").val(monthYear);

    var DashboardFilter = $('#DashboardForm').serialize();

    $("#DashboardFilter_monthYear").val("0");

    $.ajax({
        url: "/Reports/Dashboard?handler=DashboardListing",
        type: "GET",
        data: DashboardFilter,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            Site.onFailure(XMLHttpRequest, textStatus, errorThrown);
        },
        success: function (data) {
            if (Site.onPostFormSuccess(data, '#DashboardForm')) {
                $("#dashboardListing").html(data);
            }
        }
    });
}