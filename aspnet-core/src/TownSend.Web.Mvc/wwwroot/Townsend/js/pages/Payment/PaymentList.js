﻿$(document).ready(function () {
    updateSummaryList();
    updateMonthlySummaryList();
});

function updateSummaryList() {
    var params = getParameters(".filters");

    $.ajax({
        type: "GET",
        url: '/Payment/PaymentList/?handler=PaymentSummary',
        dataType: "html",
        data: { params: params },
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="RequestVerificationToken"]').val()
        }
    }).done(function (result) {
        $('#member-payment-summary-container').html(result);
    }).fail(function (error) {
        showErrorMessage('#ajaxError');
    });
}

function updateMonthlySummaryList() {
    var params = getParameters(".filters");

    $.ajax({
        type: "GET",
        url: '/Payment/PaymentList/?handler=MonthlyBreakdown',
        dataType: "html",
        data: { params: params },
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="RequestVerificationToken"]').val()
        }
    }).done(function (result) {
        $('#member-monthly-payment-summary-container').html(result);
    }).fail(function (error) {
        showErrorMessage('#ajaxError');
    });
}