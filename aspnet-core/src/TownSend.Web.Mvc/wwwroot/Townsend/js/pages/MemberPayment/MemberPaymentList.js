﻿$(document).ready(function () {
    $('#MemberPaymentList').ViewLoader();

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $('#btn-clear').click(function () {
        $('#MemberPaymentListFilter_DateFrom').val('');
        $('#MemberPaymentListFilter_DateTo').val('');
    });
});
