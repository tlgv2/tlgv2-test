﻿$(document).ready(function () {
    $('#InviteEvolutionStatusesInfo').ViewLoader();
    $('#ContactInfo').ViewLoader();
    $('#TitleInfo').ViewLoader();

    $('#btn-send-invite').click(function () {
        let params = { "InvitationID": $("#InvitationID").val()};
        let url = '/InviteEvolution/InviteEvolution?handler=SendInvite';
        
        doAjaxCall(url, params);
    });

    $('#btn-cancel-invite').click(function () {
        let params = { "InvitationID": $("#InvitationID").val()};
        let url = '/InviteEvolution/InviteEvolution?handler=CancelInvite';
        
        doAjaxCall(url, params);
    });

    $('body').on('hidden.bs.modal', '#DefaultModal', function (e) {
        let productID = $("#ProductID").val();
        let scholarshipLink = $("#scholarship-link");
        let href = scholarshipLink.attr('href');
        href = href + "&ProductID=" + productID;
        scholarshipLink.attr('href', href);
        console.log(href);
    });
});

function doAjaxCall(url, params) {
    console.log(params);
    $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: { parameters: params },
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="RequestVerificationToken"]').val()
        }
    }).done(function (data) {
        Site.onModalSuccess(data);

        $('#InviteEvolutionStatusesInfo, #ContactInfo, #TitleInfo').ViewLoader('refresh');
    }).fail(function (error) {
        showErrorMessage('#ajaxError');
    });
}

function onNewMemberProductChange(element) {
    var $element = $(element);
    if ($element.length === 1) {
        var $form = $element.parents('form');
        if ($form.length === 1) {
            var $priceLoader = $('#ProductPriceLoader', $form);
            var $scholarshipLoader = $('#ScholarshipAmountLoader', $form);

            var productID = $element.val();
            var val1 = productID.split("-");
            var productCategoryID = val1[1];
            if (productCategoryID == 3) {
                $("#DiscountPrice").hide();
                $("#ScholarshipAmountLoader").hide();

            }
            else {
                $("#DiscountPrice").show();
                $("#ScholarshipAmountLoader").show();
            }
            var val = val1[0];
            // var val = $element.val();
            if (val > 0) {
                $priceLoader.data('paramProductID', val);

                $scholarshipLoader.data('paramEmail', $('.member-email', $form).val());
                $scholarshipLoader.data('paramProductID', val);

                $discount = $('#InviteDiscount', $form);
                $scholarship = $('#ScholarshipAmount', $form);

                if ($scholarshipLoader.hasClass('view-loaded')) {
                    $scholarshipLoader.ViewLoader('refresh');
                }
                else {
                    //$scholarshipLoader.ViewLoader({ onLoaded: function (el) { calcMembershipPrice($(el).parents('form')); } });
                    $scholarshipLoader.ViewLoader();
                }

                if ($priceLoader.hasClass('view-loaded')) {
                    $priceLoader.ViewLoader('refresh');
                }
                else {
                    $priceLoader.ViewLoader({ onLoaded: function (el) { calcMembershipPrice($(el).parents('form')); } });

                    $discount.on('input', $.debounce(500, function (event) {
                        var $target = $(event.target);
                        var $form = $target.parents('form');
                        calcMembershipPrice($form);
                    }));
                }
            }
        }
    }
}

function initAddMemberEvents(event) {
    console.log('initAddMemberEvents');
    var $modal = $(event.target);

    console.log($modal);
    console.log($(".member-email", $modal));

    $(".member-email", $modal).on('input', $.debounce(500, function (e) {
        initOpenItems(e);
    }));
}

function initOpenItems(event) {
    console.log('initOpenItems');

    var $target = $(event.target);
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var re = new RegExp(pattern);
    if ($target.val().match(re)) {
        if ($target.length === 1) {
            var $form = $target.parents('form');

            var $scholarshipLoader = $('#ScholarshipAmountLoader', $form);
            $scholarshipLoader.data('paramEmail', $('.member-email', $form).val());
            $scholarshipLoader.ViewLoader('refresh');
        }
    }
}

function calcMembershipPrice($form) {
    console.log('calculating membership price');
    var $priceLoader = $('#MembershipPriceLoader', $form);
    if ($priceLoader.length === 1) {
        var discount = parseFloat($('#InviteDiscount', $form).val());
        var email = $('.member-email', $form).val();

        var productId = $('#ProductIDString', $form).val();

        var val1 = productId.split("-");
        var productId1 = val1[0];
        $priceLoader.data('paramInviteDiscount', discount);
        $priceLoader.data('paramEmail', email);
        $priceLoader.data('paramProductID', productId1);

        if ($priceLoader.hasClass('view-loaded'))
            $priceLoader.ViewLoader('refresh');
        else
            $priceLoader.ViewLoader();

        $('#PricingSection', $form).show();
    }
}