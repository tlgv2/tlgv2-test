﻿$(document).ready(function () {

    $('#btnRefresh').on('click', function (e) {

        //$('#resultsGrid').html('<div class="progress ml-auto mr-auto" style="width: 50%;margin-top:50px;"><div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>');
        $('#resultsGrid').hide();
        $('#resultsGridLoad').show();

        e.preventDefault();
        e.target.blur();

        loadDataManagerRows()

    });

    loadDataManagerRows();

    $('#btnExport').on('click', function (event) {
        event.preventDefault();

        downloadExcel(event, true);
    });

});

function downloadExcel(event, usePrevious) {
    var dataManagerRequest = $('#dataManagerFilterForm').serialize();
    Site.onDownloadActionLink(event, { withUrlParamOverride: dataManagerRequest });
}

function loadDataManagerRows() {

    var dataManagerRequest = $('#dataManagerFilterForm').serialize();

    return $.ajax({
        type: "POST",
        url: "/CDFIDataManager/CDFIDataManager?handler=DataManagerResults",
        data: dataManagerRequest,
        dataType: "html",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            Site.onFailure(XMLHttpRequest, textStatus, errorThrown);
        },
        success: function (response) {
            $('#resultsGridLoad').hide();
            $('#resultsGrid').show();
            $('#resultsGrid').html(response);
        }
    });
}

function toggleHeaderCB() {
    var headerCBState = $('#headerCB').prop('checked');
    var allCBs = $("input[id*='loanCB-']");

    allCBs.not(':disabled').prop('checked', headerCBState);
}

function flagSelected(action) {
    var checkedCBs = $("input[id*='loanCB-']:checkbox:checked");
    var numOfUpdates = checkedCBs.length;

    if (numOfUpdates > 0) {
        var r = confirm("Are you sure you want to flag the selected records?");

        if (r) {
            $('#resultsGrid').hide();
            $('#resultsGridLoad').show();
            //$('#resultsGrid').html('<div class="progress ml-auto mr-auto" style="width: 50%;margin-top:50px;"><div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div></div>');
            $.each(checkedCBs, function (index, cb) {
                var loanID = cb.id.substr(cb.id.indexOf('-') + 1);
                var importStatus = $('#loanImportStatus-' + loanID).text();

                $.ajax({
                    type: "GET",
                    url: '/CDFIDataManager/CDFIDataManager?handler=UpdateRecord&loanID=' + loanID + '&importStatus=' + importStatus + '&action=' + action,
                    dataType: "html",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        Site.onFailure(XMLHttpRequest, textStatus, errorThrown);
                    },
                    success: function (response) {
                        if (index >= numOfUpdates - 1) {
                            setTimeout(function () {
                                loadDataManagerRows();
                            }, 3000);
                        }
                    }
                });
            })
        }
    }
    else {
        alert('Error - Please select some loans to flag.')
    }
}