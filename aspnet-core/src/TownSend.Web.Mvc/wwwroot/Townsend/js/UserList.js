﻿
$(document).ready(function () {
    //$('#btnExport').on('click', function (e) {
    //    event.preventDefault();

    //    downloadExcel(event);
    //});

    $('#UserList').ViewLoader();

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});

function downloadExcel(event) {
    var dataManagerRequest = $('#UserListForm').serialize();
    Site.onDownloadActionLink(event, { withUrlParamOverride: dataManagerRequest });
}
