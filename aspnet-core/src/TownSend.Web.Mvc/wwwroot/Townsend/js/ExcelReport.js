﻿var rTable = null;

$(document).ready(function () {
    $('#SelectedReportID').on('change', function (e) {
        getReportFilters();
    });

    $('#generateReport').on('click', function (e) {
        var excelDownload = false;

        getReport(excelDownload);
    });

    $('#generateExcel').on('click', function (e) {
        var excelDownload = true;

        getReport(excelDownload);
    });
});

function getReportFilters()
{
    disableButtons('.btn-main');

    emptyDataTable('#ReportContent');

    var reportID = $('#SelectedReportID').val();

    function dateChange(e) {
        console.log("date changed", e);
    }

    $.ajax({
        url: '/ExcelReports/ExcelReport?handler=ReportFilters&ReportGeneratorID=' + reportID,
        type: 'GET',
        dataType: "html",
        contentType: 'application/json; charset=utf-8',
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="RequestVerificationToken"]').val()
        },
        success: function (response) {
            $('#FilterContainer').html(response);

            $('.DatePickerbtn').click(function () {
                $('.DatePicker').datepicker('show');
            });

            $('.DatePicker').datepicker({
                changeMonth: true,
                changeYear: true,
                onChange: dateChange
            });

            $('.selectpicker').selectpicker();

            enableButtons('.btn-main');
        },
        error: function (xhr) {
            enableButtons('.btn-main');

            showErrorMessage("#FilterContainer");
        }
    });
}

function getReport(excelDownload)
{
    disableButtons('.btn-main');

    var reportID = $('#SelectedReportID').val();
    var params = getParameters('.filters');

    var progressBarText = '';

    if (!excelDownload) {
        emptyDataTable('#ReportContent');

        progressBarText = 'Generating Report...';
    }
    else
    {
        progressBarText = 'Generating Excel...';
        
    }

    showProgressBar('#progressText', '#progressBar', progressBarText);

    $.ajax({
        url: '/ExcelReports/ExcelReport?handler=Report&ReportGeneratorID=' + reportID + '&ExcelDownload=' + excelDownload + params,
        type: 'GET',
        dataType: "html",
        contentType: 'application/json; charset=utf-8',
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="RequestVerificationToken"]').val()
        },
        success: function (response) {
            hideProgressBar('#progressText', '#progressBar');

            enableButtons('.btn-main');

            if (!excelDownload) {
                buildDataTable(response);
            }
            else
            {
                downloadFile('/ExcelReports/ExcelReport?handler=DownloadExcel', response);
            }
        },
        error: function (xhr) {
            hideProgressBar('#progressText', '#progressBar');

            enableButtons('.btn-main');

            showErrorMessage("#ReportContent");
        }
    });
}