﻿$(document).ready(function () {
    $('#MeetingTeams').ViewLoader();
    $('#MeetingList').ViewLoader();

    $("body").on("change", "#ProductID", function () {
        getProductPartners($(this).val());
    });

    $("body").on("change", "#MeetingListFilter_SupplierID", function () {
        getPartnersProducts($(this).val());
    });

    $("body").on('shown.bs.modal', '#DefaultModal', function (e) {
        getProductPartners($("#ProductID").val());
    });

    $("body").on('hidden.bs.modal', '#DefaultModal', function (e) {
        $('#meeting-list-refresh').trigger('click');
    });

    $('#btn-clear').click(function () {
        $('#MeetingListFilter_DateFrom').val('');
        $('#MeetingListFilter_DateTo').val('');
    });

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});

function getProductPartners(productID) {
    var params = { "ProductID": productID };
    $.ajax({
        type: "GET",
        url: '/Meeting/MeetingList/?handler=LoadPartners',
        dataType: "html",
        data: params,
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="RequestVerificationToken"]').val()
        }
    }).done(function (result) {
        $('#partners-container').html(result);
    }).fail(function (error) {
        showErrorMessage('#ajaxError');
    });
}

function getPartnersProducts(supplierID) {
    var params = { "SupplierID": supplierID };
    console.log(supplierID);
    $('#MeetingTeams').data('paramShowAll', 0);
    $('#MeetingTeams').data('paramSupplierID', supplierID);
    $('#MeetingTeams').ViewLoader('refresh');
    //$.ajax({
    //    type: "POST",
    //    url: '/Meeting/MeetingList/?handler=LoadSupplierTeams',
    //    dataType: "html",
    //    data: params,
    //    headers: {
    //        RequestVerificationToken:
    //            $('input:hidden[name="RequestVerificationToken"]').val()
    //    }
    //}).done(function (result) {
    //    console.log(result);
    //    $("#MeetingTeams").html(result);
    //}).fail(function (error) {
    //    showErrorMessage('#ajaxError');
    //});
}