﻿
$(document).ready(function () {
    $('#MemberInfo').ViewLoader();
    $('#ContactInfo').ViewLoader();
    $('#TitleInfo').ViewLoader();
   // $('#Password').ViewLoader();

    $('#documents-info-tab').on('click', function () {
        $('#Documents').ViewLoader();
    });

    $('#payment-method-tab').on('click', function () {
        $('#Payment-Method').ViewLoader();
    });

    $('#payment-log-tab').on('click', function () {
        $('#Payment-Log').ViewLoader();
    });
    $('#notes-tab').on('click', function () {
        $('#CustomerNoteForm').submit();
    });

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });

    $('#btn-clear').click(function () {
        $('#MemberPaymentListFilter_DateFrom').val('');
        $('#MemberPaymentListFilter_DateTo').val('');
    });
   
    $('#Documents').on('click', ".btn-download", function (event) {
        event.preventDefault();

        downloadFile(event);
         
    });

    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        var myDropzone = new Dropzone("#uploader", {
            paramName: "file",
            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 5,
            maxFiles: 5,
            maxFilesize: 500,
            timeout: 3000000,
            init: function () {

                var myDropzone = this;

                $("#save-documents").click(function (e) {
                    if ( myDropzone.files.length > 0) {
                        // allow to upload file
                        $("#overlay").show();
                        myDropzone.processQueue();
                    }
                    else {
                        $("#alert-upload-empty").toggleClass("d-none");
                    }
                });

                this.on("sending", function (file, xhr, formData) {
                    let restrictVisibility = $("#RestrictedVisibility").is(':checked');
                    let visibleDateFrom = $("#VisibleDateFrom").val();
                    let visibleDateTo = $("#VisibleDateTo").val();
                    let contextCoreName = $("#context-core-name").val();
                    let contextLinkId = $("#context-link-id").val();
                    let customerName = $("#customer-name").text();

                    formData.append("RestrictedVisibility", restrictVisibility);
                    formData.append("VisibleDateFrom", visibleDateFrom);
                    formData.append("VisibleDateTo", visibleDateTo);
                    formData.append("ContextCoreName", contextCoreName);
                    formData.append("ContextLinkID", contextLinkId);
                    formData.append("CustomerName", customerName);
                });

                this.on("success", function (file, data) {
                    $("#overlay").hide();
                    //$("#alert-upload-success").toggleClass("d-none");
                    Site.onModalSuccess(data);
                    $('#FileUploadModal').modal('toggle');
                    $('#Documents').ViewLoader('refresh');
                });

                this.on("error", function (file, data) {
                    console.log(data);
                    $("#overlay").hide();
                    $("#alert-upload-error").toggleClass("d-none");
                });

                this.on("queuecomplete", function (file) {
                    this.removeAllFiles();
                    $("#overlay").hide();
                });
            }
        });
    });

    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        $("#ui-datepicker-div").hide();
    });

});
function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
        //$(".dropdown-toggle").removeClass("disabled");
      
    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    
    $(".info").prop("disabled", function (i, v) { return !v; });
    $(".dropdown-toggle").toggleClass("disabled");
}


function downloadFile(event) {
    Site.onDownloadActionLink(event, { withUrlParamOverride: null });
    
}

function onNewPaymentChange() {

    var select = $('#PaymentMethod option:selected').val();

    if (select === "COACH_BankAccount") {
        $('#bankAccount').show();
        $('#creditCard').hide();
    }
    if (select === "COACH_CreditCard") {
        $('#bankAccount').hide();
        $('#creditCard').show();

   
    }
  
  
}