﻿$(document).ready(function ($) {

    $("#my-menu").mmenu({
        "iconbar": {
            "use": true,
            "top": [
                "<a style='height:70px;'></a>",
                "<a id='open-menu' href='#my-menu'style='height:57px;padding:20px 0;'><i class='fa fa-bars'></i></a>"

            ]
        },
        navbar: {
            "title": "<span style='float:right'>Main Menu <a style='padding:0 20px 0 125px' href='#hubone-page-wrapper'><i class='fa fa-times'></i></a></span>"
        },

        "extensions": [
            "border-full",
            "pagedim-black",
            "position-front"
        ]

    });


});