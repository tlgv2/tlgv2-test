﻿$(document).ready(function () {
    //updateCustomerList();
});

function updateCustomerList() {
    var params = getParameters(".filters");

    $.ajax({
        type: "GET",
        url: '/Customer/CustomerList/?handler=UpdateListing',
        dataType: "html",
        data: { params: params },
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="RequestVerificationToken"]').val()
        }
    }).done(function (result) {
        $('#grid').html(result);
    }).fail(function (error) {
        showErrorMessage('#ajaxError');
    });
}