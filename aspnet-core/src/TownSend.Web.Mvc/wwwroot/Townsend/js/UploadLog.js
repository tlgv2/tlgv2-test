﻿$(document).ready(function () {
    if (getURLParameter('redirectedFrom') == 'CDFIDashboard') {
        var options = $('#UploadTypeID > option');
        options.each(function (index) {
            if ($(this).text() == ' CDFI Data - Excel ')
                $('#UploadTypeID').val($(this).val());
        });
        $('.selectpicker').selectpicker('refresh');
        updateUploadList();
    }
});

function updateUploadList() {
    var params = getParameters(".filters");

    $.ajax({
        type: "GET",
        url: '/Inbound/UploadLog/?handler=UpdateListing',
        dataType: "html",
        data: { params: params },
        headers: {
            RequestVerificationToken:
                $('input:hidden[name="RequestVerificationToken"]').val()
        }
    }).done(function (result) {
        $('#grid').html(result);
    }).fail(function (error) {
        showErrorMessage('#ajaxError');
    });
}

// returns param value by name
// could be added to site.js in the future
function getURLParameter(sParam) {

    var sPageURL = window.location.search.substring(1);

    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}