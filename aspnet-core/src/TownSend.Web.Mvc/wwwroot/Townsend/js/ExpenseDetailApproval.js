﻿$(document).ready(function () {
    $('#ExpenseDetail').ViewLoader();
   
    $('#ExpenseApprovalDocuments').ViewLoader();

    $('#ExpenseApprovalDocuments').on('click', ".btn-download", function (event) {
        event.preventDefault();

        downloadFile(event);
    });

});



function downloadFile(event) {
    Site.onDownloadActionLink(event, { withUrlParamOverride: null });
}