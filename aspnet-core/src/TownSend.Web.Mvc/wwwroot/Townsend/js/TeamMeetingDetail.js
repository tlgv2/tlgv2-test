﻿$(document).ready(function () {
    $('#MeetingDetails').ViewLoader();
    $('#MeetingInfo').ViewLoader();
    $('#TitleInfo').ViewLoader();
    $('#SupplierDetails').ViewLoader();
    $('#MeetingMap').ViewLoader();

    $('#notes-tab').on('click', function () {
        $('#MeetingNoteForm').submit();
    });

    $('#documents-tab').on('click', function () {
        $('#MeetingDocuments').ViewLoader();
    });

    $('#member-tab').on('click', function () {
        $('#MeetingMembers').ViewLoader();
    });

    $('#links-tab').on('click', function () {
        $('#MeetingLinks').ViewLoader();
    });
    
    $('#MeetingDocuments').on('click', ".btn-download", function (event) {
        event.preventDefault();

        downloadFile(event);
    });

    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        var myDropzone = new Dropzone("#uploader", {
            paramName: "file",
            autoProcessQueue: false,
            uploadMultiple: false,
            parallelUploads: 5,
            maxFiles: 5,
            maxFilesize: 500,
            timeout: 3000000,
            init: function () {

                var myDropzone = this;

                $("#save-documents").click(function (e) {
                    if (myDropzone.files.length > 0) {
                        // allow to upload file
                        $("#overlay").show();
                        myDropzone.processQueue();
                    }
                    else {
                        $("#alert-upload-empty").toggleClass("d-none");
                    }
                });

                this.on("sending", function (file, xhr, formData) {
                    let restrictVisibility = $("#RestrictedVisibility").is(':checked');
                    let visibleDateFrom = $("#VisibleDateFrom").val();
                    let visibleDateTo = $("#VisibleDateTo").val();
                    let contextCoreName = $("#context-core-name").val();
                    let contextLinkId = $("#context-link-id").val();
                    let sessionID = $("#session-id").val();

                    formData.append("RestrictedVisibility", restrictVisibility);
                    formData.append("VisibleDateFrom", visibleDateFrom);
                    formData.append("VisibleDateTo", visibleDateTo);
                    formData.append("ContextCoreName", contextCoreName);
                    formData.append("ContextLinkID", contextLinkId);
                    formData.append("SessionID", sessionID);
                });

                this.on("success", function (file, data) {
                    $("#overlay").hide();
                   // $("#alert-upload-success").toggleClass("d-none");
                    Site.onModalSuccess(data);
                    $('#FileUploadModal').modal('toggle');
                    $('#MeetingDocuments').ViewLoader('refresh');
                });

                this.on("error", function (file, data) {
                    console.log(data);
                    $("#overlay").hide();
                    $("#alert-upload-error").toggleClass("d-none");
                });

                this.on("queuecomplete", function (file) {
                    this.removeAllFiles();
                    $("#overlay").hide();
                });
            }
        });
    });

    $("body").on('shown.bs.modal', '#FileUploadModal', function (e) {
        $("#ui-datepicker-div").hide();
    });

    $('body').tooltip({
        selector: '[data-toggle="tooltip"]'
    });
});
function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();


    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    $(".info").prop("disabled", function (i, v) { return !v; });
    $(button).closest(".content-container").find(".input-group").find(".btn").prop("disabled", function (i, v) { return !v; });
    $(".dropdown-toggle").toggleClass("disabled");

}
function downloadFile(event) {
    Site.onDownloadActionLink(event, { withUrlParamOverride: null });
}