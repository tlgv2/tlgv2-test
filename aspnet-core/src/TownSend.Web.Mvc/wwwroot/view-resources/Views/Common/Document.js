﻿
var _docService = abp.services.app.document;
var docForm = $('#PartnerDocumentForm');
console.log(_docService);
docForm.validate();

//$("#visiblefrom").datepicker({
//    numberOfMonths: 2,
//    onSelect: function (selected) {
//        var dt = new Date(selected);
//        dt.setDate(dt.getDate() + 1);
//        $("#visibleto").datepicker("option", "minDate", dt);
//    }
//});
//$("#visibleto").datepicker({
//    numberOfMonths: 2,
//    onSelect: function (selected) {
//        var dt = new Date(selected);
//        dt.setDate(dt.getDate() - 1);
//        $("#visiblefrom").datepicker("option", "maxDate", dt);
//    }
//});
var editdocForm = $('#EditDocumentFormForDoc');
editdocForm.validate();
//docForm.find('button[type="submit"]').click(function () {
    

//    if (!docForm.valid()) {
//        return;
//    }
//    console.log(_docService);
//    var document = serialize(docForm);
//    debugger;
//    console.log(document);
//    _docService.createDocument(document).done(function () {
//        setTimeout(function () {
//            //window.location = abp.appPath + 'Partners';
//            location.reload(true); //reload page to see edited role!
//        }, 5000);
//    });
//});
$('#DocumentCreateModal').on('hidden.bs.modal', function () {
    console.log(this);
    dZUpload[0].dropzone.removeAllFiles();
    //$(this)
    //    .find("input,textarea,select")
    //    .val('')
    //    .end()
    //    .find("input[type=checkbox], input[type=radio]")
    //    .prop("checked", "")
    //    .end();
    $(".col.visibility-date").css("display", "none");
    $(".col.visibility-date").css("display", "none");
    $('input[name="VisibleDateFrom"]').datepicker('destroy');
    $('input[name="VisibleDateTo"]').datepicker('destroy'); 
    $('input[name="VisibleDateFrom"]').prop('required', false);
    $('input[name="VisibleDateTo"]').prop('required', false);
    $("label.error").hide();
    $(".error").removeClass("error");
})

$('#EditDocumentModal').on('hidden.bs.modal', function () {
    console.log(this);
    dZUpload[0].dropzone.removeAllFiles();   
    $(".col.visibility-date").css("display", "none");
    $(".col.visibility-date").css("display", "none");
    $('input[name="VisibleDateFrom"]').prop('required', false);
    $('input[name="VisibleDateTo"]').prop('required', false);
    $("label.error").hide();
    $(".error").removeClass("error");
})
$("#CreateDocModal").on('click', function () {
    $('input[name="VisibleDateFrom"]').datepicker({
        dateFormat: 'mm-dd-yy',
        minDate: 0,
        onSelect: function (selected) {
            var dateforVisibleFrom1 = moment(selected).format('MM-DD-YYYY');
            var dt = new Date(dateforVisibleFrom1);
            dt.setDate(dt.getDate() + 1);
            $('input[name="VisibleDateTo"]').datepicker("option", "minDate", dt);
        }
    }).attr("readonly", "true").
        keypress(function (event) {
            if (event.keyCode == 8) {
                event.preventDefault();
            }
        });


    $('input[name="VisibleDateTo"]').datepicker({
        dateFormat: 'mm-dd-yy',
        minDate: 0,
        onSelect: function (selected) {
            var dateforVisibleTo1 = moment(selected).format('MM-DD-YYYY');
            var dt = new Date(dateforVisibleTo1);
            dt.setDate(dt.getDate() - 1);
            $('input[name="VisibleDateFrom"]').datepicker("option", "maxDate", dt);
        }
    }).attr("readonly", "true").
        keypress(function (event) {
            if (event.keyCode == 8) {
                event.preventDefault();
            }
        });
});


function editDoc(Id) {
    
    
    _docService.getDocumentForEdit(Id).done(function (data) {
        console.log(data);
    
        $(".modal-body #Id").val(data.id);
        //$(".modal-body #PId").val(data.result.partnerId);
        $(".modal-body #Name").val(data.name);
        $(".modal-body #Desc").val(data.description);
        console.log(data.visibleDateFrom);
        if (data.visibleDateFrom != null) {
            $("#RestrictEdit").prop("checked", true);
            $(".col.visibility-date").css("display", "block");
            $(".col.visibility-date").css("display", "block");
            var dateforVisibleFrom = moment(data.visibleDateFrom).format('MM-DD-YYYY');
            var dateforVisibleTo = moment(data.visibleDateTo).format('MM-DD-YYYY');
            //var test = data.visibleDateFrom.
            //var myDate = new Date(dateforVisibleFrom);

            //$('.modal-body #visiblefromedit').datepicker();
            //$('.modal-body #visiblefromedit').datepicker('setDate', myDate, { dateFormat: 'mm-dd-yy' });



            //$(".modal-body #visiblefromedit").datepicker().datepicker('setDate', dateforVisibleFrom);     
            //$(".modal-body #visibletoedit").datepicker().datepicker('setDate', dateforVisibleTo);     

            //console.log(dateforVisibleFrom);
            $(".modal-body #visiblefromedit").val(dateforVisibleFrom);
            $(".modal-body #visibletoedit").val(dateforVisibleTo);
            $('input[name="StringVisibleDateFrom"]').datepicker({
                dateFormat: 'mm-dd-yy',
                minDate: 0,
                onSelect: function (selected) {
                    var dateforVisibleFrom = moment(selected).format('MM-DD-YYYY');
                    var dt = new Date(dateforVisibleFrom);
                    console.log(dt.getDate());
                    dt.setDate(dt.getDate() + 1);
                    //selected.setDate(selected.getDate() + 1);
                    $('input[name="StringVisibleDateTo"]').datepicker("option", "minDate", dt);
                }
            }).attr("readonly", "true").
                keypress(function (event) {
                    if (event.keyCode == 8) {
                        event.preventDefault();
                    }
                });

            $('input[name="StringVisibleDateTo"]').datepicker({
                dateFormat: 'mm-dd-yy',
                minDate: 0,
                onSelect: function (selected) {
                    var dateforVisibleTo = moment(selected).format('MM-DD-YYYY');
                    var dt = new Date(dateforVisibleTo);
                    dt.setDate(dt.getDate() - 1);
                    //selected.setDate(selected.getDate() - 1);
                    $('input[name="StringVisibleDateFrom"]').datepicker("option", "maxDate", dt);
                }
            }).attr("readonly", "true").
                keypress(function (event) {
                    if (event.keyCode == 8) {
                        event.preventDefault();
                    }
                });
        }
        else {
            $('input[name="StringVisibleDateFrom"]').datepicker({
                dateFormat: 'mm-dd-yy',
                minDate: 0,
                onSelect: function (selected) {
                    var dateforVisibleFrom = moment(selected).format('MM-DD-YYYY');
                    var dt = new Date(dateforVisibleFrom);
                    console.log(dt.getDate());
                    dt.setDate(dt.getDate() + 1);
                    //selected.setDate(selected.getDate() + 1);
                    $('input[name="StringVisibleDateTo"]').datepicker("option", "minDate", dt);
                }
            }).attr("readonly", "true").
                keypress(function (event) {
                    if (event.keyCode == 8) {
                        event.preventDefault();
                    }
                });

            $('input[name="StringVisibleDateTo"]').datepicker({
                dateFormat: 'mm-dd-yy',
                minDate: 0,
                onSelect: function (selected) {
                    var dateforVisibleTo = moment(selected).format('MM-DD-YYYY');
                    var dt = new Date(dateforVisibleTo);
                    dt.setDate(dt.getDate() - 1);
                    //selected.setDate(selected.getDate() - 1);
                    $('input[name="StringVisibleDateFrom"]').datepicker("option", "maxDate", dt);
                }
            }).attr("readonly", "true").
                keypress(function (event) {
                    if (event.keyCode == 8) {
                        event.preventDefault();
                    }
                });

        }
        

        //$(".modal-body #visibletoedit").datepicker('setDate', dateforVisibleFrom);
        
        //$(".modal-body #visiblefromedit").val(data.visibleDateFrom);
        //$(".modal-body #visibletoedit").val(data.visibleDateTo);

    });
}
function getDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = dd + '/' + mm + '/' + yyyy;
    return today;
}
//$(".col.visibility-date").css("display", "none");
//$(".col.visibility-date").css("display", "none");



$("#RestrictCreate").click(function () {
    if ($(this).is(":checked")) {

        $(".col.visibility-date").css("display", "block");
        $(".col.visibility-date").css("display", "block");
        $('input[name="VisibleDateFrom"]').prop('required', true)
        $('input[name="VisibleDateTo"]').prop('required', true);        
    }
    else {
        $(".col.visibility-date").css("display", "none");
        $(".col.visibility-date").css("display", "none");
        $('input[name="VisibleDateFrom"]').prop('required', false);
        $('input[name="VisibleDateTo"]').prop('required', false);
    }
});

$("#RestrictEdit").click(function () {
    if ($(this).is(":checked")) {

        $(".col.visibility-date").css("display", "block");
        $(".col.visibility-date").css("display", "block");
        $('input[name="StringVisibleDateFrom"]').prop('required', true);
        $('input[name="StringVisibleDateTo"]').prop('required', true);
        //$('input[name="VisibleDateFrom"]').val(getDate());
        //$('input[name="VisibleDateTo"]').val(getDate());
    }
    else {
        $(".col.visibility-date").css("display", "none");
        $(".col.visibility-date").css("display", "none");
        $('input[name="StringVisibleDateFrom"]').prop('required', false);
        $('input[name="StringVisibleDateTo"]').prop('required', false);
        //$('input[name="VisibleDateFrom"]').val('');
    }
});
(function () {
    $(function () {

        $('.delete-document').click(function () {
            var docId = $(this).attr("data-document-id");
            var etype = $(this).attr("data-document-type");
            //var mapId = $(this).attr("data-document-mapid");
            //var roleName = $(this).attr('data-role-name');

            deletePartner(docId, etype);
        });
        function deletePartner(docId, etype) {
            var formData = new FormData();
            formData.append("docId", docId);
            formData.append("etype", etype);
            confirmDelete(function () {
                var newPerson = {
                    DocumentId: docId,
                    entityType: etype
                };
                abp.ajax({
                    url: '/Common/DeleteDocument',
                    data: JSON.stringify(newPerson)
                }).done(function (data) {
                    if (data == true) {
                        //$.growl.notice({ title: 'Success', message: "Delete Document Successfully", ttl: 1 });
                        location.reload(true);

                    }
                });

            });
        }
    });

})();