﻿(function ($) {
    
    $('.calldatepicker').datepicker({
        dateFormat: 'mm-dd-yy',
        minDate: 0
    })
    var _teamService = abp.services.app.team;
    var _$form = $('form[name=TeamEditForm]');
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $('#edit-partner-info').click(function (e) {
        //validate percentage
        var totalPercent = 0;
        $(".partner-percentage").each(function () {
            var value = $(this).val();
            value = value || 0; //if null or undefined then convert to 0
            totalPercent += parseFloat(value)
        });

        ////totalPercent = totalPercent || 0; //if null or undefined then convert to 0
        if (totalPercent != 100) {
            $.growl.error({ title: 'Error', message: percentageValidation, ttl: 3000 });
            $(".partner-percentage").first().focus();
            return false;
        }   

        FillHiddenFields(objPartners);
    });


    $('#addpartner').on('click', function () {
        var partnerId = $("#PartnerIds option:selected").val();
        if (!partnerId) {
            $('#partner-error').show();
            return false;
        }
    });



    $("#AddPartnerModal").on('hide.bs.modal', function () {
        $('#partner-error').hide();
        $('#partner-error1').hide();
        $("#PartnerIds option:selected").prop("selected", false);
        $("#PartnerIds").change();
    });

    $("#PartnerIds").on('change', function () {
        $('#partner-error').hide();
        $('#partner-error1').hide();
    });

    $('.show-modal').on('click', function () {
        $('#partner-error').hide();
        $("#AddPartnerModal").modal("show");
    });

    function FillHiddenFields(listdata) {
        for (var key in listdata) {
            if (key) {
                $('<input>', { type: 'hidden', id: 'hdnPartnerId' + key, name: 'PartnerList[' + key + '].PartnerId', value: objPartners[key].partnerId }).appendTo('#append-hdn-value');
                $('<input>', { type: 'hidden', id: 'hdnPartnerName' + key, name: 'PartnerList[' + key + '].PartnerName', value: objPartners[key].partnerName }).appendTo('#append-hdn-value');
                $('<input>', { type: 'hidden', id: 'hdnPercentage' + key, name: 'PartnerList[' + key + '].PartnerPercentage', value: objPartners[key].partnerPercentage }).appendTo('#append-hdn-value');
                $('<input>', { type: 'hidden', id: 'hdnTeamId' + key, name: 'PartnerList[' + key + '].TeamId', value: objPartners[key].teamId }).appendTo('#append-hdn-value');
            }
        }
    }

    $('.partner-percentage').on("change keyup paste", function (event) {
        var id = $(this).data('id');
        var val = $(this).val();
        objPartners.forEach((element, index) => {
            if (element.partnerId === id) {
                objPartners[index].partnerPercentage = val;
            }
        });
    });

    $('#cancel-edit-info-partner').on("click", function (event) {
        location.reload();
    });
    

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    $(".partner-percentage, #editpercentage").keydown(function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) ||
            (event.keyCode >= 96 && event.keyCode <= 105) ||
            event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
            event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
            event.preventDefault();
    });


})(jQuery);

function toggleConfirm(button, ismainform) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
        if (ismainform) {
            $(".info").find('input[type=text]:first').focus();
        }
        else {
            $(".info1").find('input[type=text]:first').focus();
            $(".percentage-note").show();
        }
    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    if (ismainform) {
        $(".info").prop("disabled", function (i, v) { return !v; });
        $(".dropdown-toggle").toggleClass("disabled");
    }
    else {
        $(".info1").prop("disabled", function (i, v) { if (v == false) $(".percentage-note").hide(); return !v; });
    }
}

$("#Deposit").on('blur', function () {
    var tp = $("#TeamPrice").val();
    var d = $("#Deposit").val();

    if (d != "" && tp != "")
    {
        if (parseInt(tp) >= parseInt(d)) {
            $("#EditBasicInfo").attr('disabled', false);
            $("#checkpriceanddeposit").empty();
        }
        else {
            $("#EditBasicInfo").attr('disabled',true);
            $("#checkpriceanddeposit").empty();
            $("#checkpriceanddeposit").append("Deposit must be less than the Team Price");
            //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
            return false;
        }
       
    }
    else {
        $("#EditBasicInfo").attr('disabled', false);
        $("#checkpriceanddeposit").empty();

    }
    //$("#checkpriceanddeposit").empty();
});
$("#TeamPrice").on('blur', function () {
    $("#checkpriceanddeposit").empty();
    var tp = $("#TeamPrice").val();
    var d = $("#Deposit").val();
    if (tp != "" && d != "") {
        $("#EditBasicInfo").attr('disabled', false);
        if (parseInt(tp) >= parseInt(d)) {
            $("#checkpriceanddeposit").empty();
            $("#EditBasicInfo").attr('disabled', false);
        }
        else {
            $("#checkpriceanddeposit").append("Deposit must be less than the Team Price");
            //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
            $("#EditBasicInfo").attr('disabled', true);
            return false;
        }
    }
    else {
        $("#EditBasicInfo").attr('disabled', false);
    }

});
$("#EditBasicInfo").on('click', function () {
    $("#checkpriceanddeposit").empty();
    var tp = $("#TeamPrice").val();
    var d = $("#Deposit").val();
    if (tp != "" && d != "") {
        //$("#EditBasicInfo").attr('disabled', false);
        if (parseInt(tp) >= parseInt(d)) {
            $("#checkpriceanddeposit").empty();
            //$("#EditBasicInfo").attr('disabled', false);
        }
        else {
            $("#checkpriceanddeposit").append("Deposit must be less than the Team Price");
            //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
            //$("#EditBasicInfo").attr('disabled', true);
            return false;
        }
    }
    else {
        $("#EditBasicInfo").attr('disabled', false);
    }
});
$("#deposit").keydown(function () {
    $("#checkpriceanddeposit").empty();
});
$("#deposit").keydown(function (event) {


    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();

});
$("#teamprice").keydown(function (event) {


    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();

});


//temporary added

var serialize = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    var obj = {};
    data.map(function (x) { obj[x.name] = x.value; });

    return obj;
};


var serializeForm = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    var obj = {};
    obj.PartnerIds = [];
    data.map(function (x) {
        if (x.name == "PartnerIds") {
            obj.PartnerIds.push(x.value);
        }
        else {
            obj[x.name] = x.value;
        }
    });

    return obj;
};

$("#TeamPrice").keydown(function (event) {


    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
    //if a decimal has been added, disable the "."-button

});
$("#Deposit").keydown(function (event) {


    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();
    //if a decimal has been added, disable the "."-button

});



function deleteMapping(teamId, partnerId) {
    if (!ValidateDeletePartnerPercent(partnerId)) {
        $('.info1').removeAttr('style');
        $("#percentage_" + partnerId).focus().css('border-color', 'red');
        $(".percentage-note").css('color', 'red')
        return false;
    }

    confirmDelete(function () {
        var data = {
            'TeamId': teamId,
            'PartnerId': partnerId
        };
        abp.ajax({
            url: '/Teams/RemoveTeamPartnerMapping',
            data: JSON.stringify(data)
        }).done(function (data) {
            if (data == true) {
                location.reload(true);
            }
        });
    });
}

function ValidateDeletePartnerPercent(partnerId) {
    var flag = true;
    $('.info1, .percentage-note').removeAttr('style');
    verifyPartner.forEach((element, index) => {
        if (element.partnerId === partnerId) {
            if (verifyPartner[index].partnerPercentage != null && parseInt(verifyPartner[index].partnerPercentage) != 0) {
                flag = false;
                return;
            }
        }
    });

    return flag;
}