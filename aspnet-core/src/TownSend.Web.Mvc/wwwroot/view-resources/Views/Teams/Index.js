﻿(function () {
    $(function () {

        var _teamService = abp.services.app.team;
        var _$modal = $('#TeamCreateModal');
        var _$form = _$modal.find('form');

        //_$form.validate();

        $('#RefreshButton').click(function () {
            refreshTeamList();
        });

        $('.edit-team').click(function (e) {
            var teamId = $(this).attr("data-team-id");

            e.preventDefault();
            abp.ajax({
                url: abp.appPath + 'Teams/EditTeamModal?teamId=' + teamId,
                type: 'POST',
                dataType: 'html',
                success: function (content) {
                    $('#TeamEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });
        function refreshTeamList() {
            location.reload(true); //reload page to see new tenant!
        }
    });
})();
    //$("#deposit").keypress(function () {
    //    $("#checkpriceanddeposit").empty();
    //    var tp = $("#teamprice").val();
    //    var d = $("#deposit").val();
    //    if (parseInt(tp) >= parseInt(d)) {
    //        $("#checkpriceanddeposit").empty();
    //    }
    //    else {
    //        $("#checkpriceanddeposit").append("Deposit must be less than the Team Price");
    //        $.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
    //        return false;

    //    }
    //});
$('#FullName').on('change', function (e) {
    if (this.value) {
        $("#FullName-error").html('');
        console.log(this.value,
            this.options[this.selectedIndex].value,
            $(this).find("option:selected").val());
    } else {

    }
    
});
$("#teamprice").on('blur', function () {
    $("#checkpriceanddeposit").empty();
    var tp = $("#teamprice").val();
    var d = $("#deposit").val();
    
    if (tp != "" && d != "") {
        $("#add-team-modal").attr('disabled', false);
        if (parseInt(tp) >= parseInt(d)) {
            $("#checkpriceanddeposit").empty();
            //$("#add-team-modal").attr('disabled', false);
        }
        else {
            $("#checkpriceanddeposit").append("Deposit must be less than the Team Price");
            //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
            //$("#add-team-modal").attr('disabled', true);
            //return false;
        }
    }
    else {
        $("#add-team-modal").attr('disabled', false);
    }
   
});

$("#add-team-modal").on('click', function () {

    if (tp != "" && d != "") {
        //$("#add-team-modal").attr('disabled', false);
        if (parseInt(tp) >= parseInt(d)) {
            $("#checkpriceanddeposit").empty();
            //$("#add-team-modal").attr('disabled', false);
        }
        else {
            $("#checkpriceanddeposit").append("Deposit must be less than the Team Price");
            //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
            //$("#add-team-modal").attr('disabled', true);
            //return false;
        }
    }
    else {
        //$("#add-team-modal").attr('disabled', false);
    }
});
$("#deposit").on('blur', function () {
    $("#checkpriceanddeposit").empty();
    var tp = $("#teamprice").val();
    var d = $("#deposit").val();

    if (tp != "" && d != "") {
        $("#add-team-modal").attr('disabled', false);
        if (parseInt(tp) >= parseInt(d)) {
            $("#checkpriceanddeposit").empty();
            $("#add-team-modal").attr('disabled', false);
        }
        else {
            $("#checkpriceanddeposit").append("Deposit must be less than the Team Price");
            //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
            $("#add-team-modal").attr('disabled', true);
            return false;
        }
    }
    else {
        $("#add-team-modal").attr('disabled', false);
        $("#checkpriceanddeposit").empty();

    }
    
});
$(document).ready(function () {
    $("#add-team-modal").attr('disabled', false);
});
$("#datepicker").on('blur', function () {
    var datepicker = $("#datepicker").val();
    if (datepicker != null) {
        $("#datepicker-error").empty();
    }
});
$(".filter-option").on('blur', function () {
    var pt = $("#FullName").val();
    if (pt != null) {
        $("#FullName-error").empty();
    }
});
$("#deposit").keydown(function (event) {


    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();

});
$("#teamprice").keydown(function (event) {


    if (event.shiftKey == true) {
        event.preventDefault();
    }

    if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 ||
        event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
        event.preventDefault();

});

$("#deposit").keydown(function () {
    $("#checkpriceanddeposit").empty(); 
});
var serialize = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    var obj = {};
    obj.PartnerIds = [];
    data.map(function (x) {
        if (x.name == "PartnerIds") {
            obj.PartnerIds.push(x.value);
        }
        else {
            obj[x.name] = x.value;
        }
    });

    return obj;
};


$("#TeamForm").validate({
    rules: {
        TeamPrice: {
            required: true,
            maxlength: 10,
            minlength: 0
        },
        InitialDeposit: {
            required: true,
            maxlength: 10,
            minlength: 0
        }
    },
    messages: {

        TeamPrice: {
            minlength: "Team Price is Not Empty",
            maxlength: "Team Price Must be Valid"
        },
        InitialDeposit: {
            minlength: "Deposit is Not Empty",
            maxlength: "PhoneNumber Must be 10 Digits",
        },

    }

});






function clearFilter() {
    $(".FilterPartnerIds").selectpicker('deselectAll');
    document.getElementById('StartDate').value = '';
    document.getElementById('EndDate').value = '';
    document.getElementById('teamList').submit();
}
$('#TeamCreateModal').on('hidden.bs.modal', function () {
    console.log($("#FullName"));
    $('.filter-option-inner-inner').html('Nothing selected');
    $("#FullName").selectpicker('deselectAll');
})


$('#edit-role-info').click(function (e) {
    grantedPermissions = [];
    var _$permissionCheckboxes = $("input[name='permission']:checked:visible");
    if (_$permissionCheckboxes) {
        for (var permissionIndex = 0; permissionIndex < _$permissionCheckboxes.length; permissionIndex++) {
            var _$permissionCheckbox = $(_$permissionCheckboxes[permissionIndex]);
            grantedPermissions.push(_$permissionCheckbox.val());
        }
    }

    FillHiddenFields(objPartners);
    return false;
});

function FillHiddenFields(listdata) {
    for (var key in listdata) {
        if (key) {
            $('<input>', { type: 'hidden', id: 'hdnPermissionId' + key, name: 'GrantedPermissionNames[' + key + ']', value: objPartners[key].partnerId }).appendTo('#append-hdn-value');
            //$('<input>', { type: 'hidden', id: 'hdnPartnerName' + key, name: 'PartnerList[' + key + '].PartnerName', value: objPartners[key].partnerName }).appendTo('#append-hdn-value');
            //$('<input>', { type: 'hidden', id: 'hdnPercentage' + key, name: 'PartnerList[' + key + '].PartnerPercentage', value: objPartners[key].partnerPercentage }).appendTo('#append-hdn-value');
            //$('<input>', { type: 'hidden', id: 'hdnTeamId' + key, name: 'PartnerList[' + key + '].TeamId', value: objPartners[key].teamId }).appendTo('#append-hdn-value');
        }
    }
}