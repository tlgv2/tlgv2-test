﻿(function ($) {
    var _userService = abp.services.app.user;
    
    //var _$modal = $('#PartnerEditModal');
    var _$form = $('form[name=UserEditForm]');
    _$form.validate({
        rules: {
            //TeamPrice: {
            //    required: true,
            //    //digits: true,
            //    maxlength: 10,
            //    minlength: 0
            //},
            //InitialDeposit: {
            //    required: true,
            //    //digits: true,
            //    maxlength: 10,
            //    minlength: 0
            //}
        },
        messages: {

            //TeamPrice: {
            //    //required: "Zip Code is Required",
            //    minlength: "Team Price is Not Empty",
            //    maxlength: "Team Price Must be Valid"
            //},
            //InitialDeposit: {
            //    minlength: "Deposit is Not Empty",
            //    maxlength: "PhoneNumber Must be 10 Digits",
            //    //required: "Phone Number is Required"
            //},
        }
    });
    function save() {

        if (!_$form.valid()) {
            return;
        }

        var team = serialize(_$form);//_$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        //role.grantedPermissions = [];
        //var _$permissionCheckboxes = $("input[name='permission']:checked:visible");
        //if (_$permissionCheckboxes) {
        //    for (var permissionIndex = 0; permissionIndex < _$permissionCheckboxes.length; permissionIndex++) {
        //        var _$permissionCheckbox = $(_$permissionCheckboxes[permissionIndex]);
        //        role.grantedPermissions.push(_$permissionCheckbox.val());
        //    }
        //}

        //abp.ui.setBusy(_$form);
        //_teamService.update(team).done(function () {
        //    //_$modal.modal('hide');
        //    //window.location = abp.appPath + 'Teams'; //comment by dev1
        //    //location.reload(true); //reload page to see edited role!
        //    //$.growl.notice({ title: "Success", message: 'Data updated' });
        //    //setTimeout(function () {
        //    //    //window.location = abp.appPath + 'Partners';
        //    //    location.reload(true); //reload page to see edited role!
        //    //}, 5000);

        //}).always(function () {
        //    //abp.ui.clearBusy(_$modal);
        //});
        //Task-1 Changes
        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();

            //$("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
        });
        //Task-1 Changes
    }

    //Handle save button click
    //_$form.find('#EditBasicInfo').click(function (e) {
    //    e.preventDefault();
    //    save();
    //});

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    //$.AdminBSB.input.activate(_$form);

    //_$modal.on('shown.bs.modal', function () {
    //    _$form.find('input[type=text]:first').focus();
    //});

    //update partner form    
    //var _$formPassword = $('form[name=ChangePaaswordForm]');
    //_$formPassword.validate({
    //    rules: {
    //        PartnerIds: {
    //            required: true,
    //        },
    //    },
    //    messages: {
    //        PartnerIds: {
    //            required: "Please select atleast one partner",
    //        }
    //    }
    //});

    //_$formPassword.find('#UpdatePartner').click(function (e) {
    //    e.preventDefault();
    //    if (!_$formPartner.valid()) {
    //        return;
    //    }

    //    abp.ajax({
    //        contentType: 'application/x-www-form-urlencoded',
    //        url: _$formPassword.attr('action'),
    //        data: serializeForm(_$formPassword),// _$formPartner.serialize(),
    //        success: function (content) {
    //            location.reload(true);
    //        },
    //        //error: function (e) {
    //        //    alert('error');
    //        //}
    //    })

        //abp.ajax({
        //    url: abp.appPath + 'Teams/EditPartnerForTeam',
        //    type: 'POST',
        //    dataType: 'json',
        //    data : partners,
        //    success: function (content) {
        //        //$('#PartnerEditModal div.modal-content').html(content);
        //        alert('success');
        //    },
        //    error: function (e) {
        //        alert('error');}
        //});
        ////abp.ui.setBusy(_$form);
        //_teamService.update(team).done(function () {
        //    //_$modal.modal('hide');
        //    //window.location = abp.appPath + 'Teams'; //comment by dev1
        //    //location.reload(true); //reload page to see edited role!
        //    $.growl.notice({ title: "Success", message: 'Data updated' });
        //    setTimeout(function () {
        //        //window.location = abp.appPath + 'Partners';
        //        location.reload(true); //reload page to see edited role!
        //    }, 5000);

        //}).always(function () {
        //    //abp.ui.clearBusy(_$modal);
        //});
        ////Task-1 Changes
        //_$modal.on('shown.bs.modal', function () {
        //    _$modal.find('input:not([type=hidden]):first').focus();

        //    //$("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
        //});
    //});


})(jQuery);


function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
        $(".info").find('input[type=text]:first').focus();
    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    $(".info").prop("disabled", function (i, v) { return !v; });

    $(".dropdown-toggle").toggleClass("disabled");
}


//temporary added

var serialize = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    var obj = {};
    data.map(function (x) { obj[x.name] = x.value; });

    return obj;
};


var serializeForm = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    //var obj = {};
    //obj.PartnerIds = [];
    //data.map(function (x) {
    //    if (x.name == "PartnerIds") {
    //        obj.PartnerIds.push(x.value);
    //    }
    //    else {
    //        obj[x.name] = x.value;
    //    }
    //});

    //return obj;
};

//$(document).ready(function () {
//    $("#PhoneNumber").inputmask("9999999999");
//});