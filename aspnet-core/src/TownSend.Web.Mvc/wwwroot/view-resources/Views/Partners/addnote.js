(function ($) {
    var _partnerService = abp.services.app.partner;
    var _$form = $('form[name=PartnerEditForm]');
    var _$documentModal = $('#PartnerDocumentCreateModal');
    //_$form.validate({
    //    rules: {
    //        ZipCode: {
    //            required: true,
    //            digits: true,
    //            maxlength: 6,
    //            minlength: 6
    //        },
        
    //});
    function save() {

        if (!_$form.valid()) {
            return;
        }

        var partner = serialize(_$form);
        _partnerService.update(partner).done(function () {

        }).always(function () {            
        });
        
    }



    //Handle save button click
    _$form.find('#EditBasicInfo').click(function (e) {
        e.preventDefault();
        save();
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

})(jQuery);

function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
        $(".info").find('input[type=text]:first').focus();
    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    $(".info").prop("disabled", function (i, v) { return !v; });

    $(".dropdown-toggle").toggleClass("disabled");
}

