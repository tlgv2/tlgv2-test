﻿$(document).ready(function () {
    $("#PhoneNumber").inputmask("9999999999");
    
});

(function ($) {
    var _partnerService = abp.services.app.partner;
    var _$form = $('form[name=PartnerEditForm]');
    var _$documentModal = $('#PartnerDocumentCreateModal');
    //_$form.validate({
    //    rules: {
    //        ZipCode: {
    //            required: true,
    //            digits: true,
    //            maxlength: 5,
    //            minlength: 5
    //        }
    //    },
    //    messages: {

    //        ZipCode: {
    //            minlength: "Zip Code Must be 5 Digits",
    //            maxlength: "Zip Code Must be 5 Digits"
    //        }
    //    }
    //});
    function save() {

        if (!_$form.valid()) {
            return;
        }

        var partner = serialize(_$form);//_$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
    }
    $("#Email").on('change', function () {
        $("#CheckEmailValidation").html('');
    });
    $(".EditBtn").on('click', function () {
        $("#ValidationPhoneNumber").empty();
        var ph = $('#PrimaryPhone').inputmask('remove');
        console.log(ph[0].value.length);
        if (ph[0].value.length != 10 && ph[0].value.length >= 8) {
            //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
            $("#ValidationPhoneNumber").append("The field Phone Number is invalid.");
            //$("#CreateBtn").attr("disabled", true);
            //$('#PhoneNumber').inputmask("(999) 999-9999");
            return false;
        }
        else {
            $("#ValidationPhoneNumber").empty();
            $(".EditBtn").attr("disabled", false);
            $('#PrimaryPhone').inputmask('remove');
        }


    });
    //$("#PrimaryPhone").on('change', function () {
    //    $("#ValidationPhoneNumber").html('');
    //});
    $("#PrimaryPhone").on('blur', function () {
        $("#ValidationPhoneNumber").empty();
        var ph = $('#PrimaryPhone').inputmask('remove');
        console.log(ph[0].value.length);
        $("#CreateBtn").attr("disabled", false);
        if (ph[0].value.length != 10 && ph[0].value.length != 0) {
            //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
            $("#ValidationPhoneNumber").append("The field Phone Number is invalid.");
            $(".EditBtn").attr("disabled", true);
            $('#PrimaryPhone').inputmask("(999) 999-9999");
            return false;
        }
        else {
            $("#ValidationPhoneNumber").empty();
            $(".EditBtn").attr("disabled", false);
        }
    });
    $("#Email").on("blur", function () {
        $(".CheckEmailValidation").html('');
        
        var email = $("#Email").val();
        if (oldemail = !email) {

        
            if (email != null) {
                abp.ajax({
                    url: '/Partners/CheckEmail',
                    data: JSON.stringify(email)
                }).done(function (data) {
                    console.log(data);
                    $(".CheckEmailValidation").html('');
                    $(".EditBtn").attr("disabled", false);
                    if (data == true) {
                        $(".EditBtn").attr("disabled", false);
                        $(".CheckEmailValidation").html("");
                    
                    }
                    else if (data == false) {

                        $(".CheckEmailValidation").html("This email already exists");
                        $(".EditBtn").attr("disabled", true);
                    }
                });
             }
        }
    });
})(jQuery);

function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
        $(".info").find('input[type=text]:first').focus();
    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    $(".info").prop("disabled", function (i, v) { return !v; });

    $(".dropdown-toggle").toggleClass("disabled");
}

//$('#EditBasicInfo').on('click', function () { 
//    $("#ValidationPhoneNumber").empty();
//    var ph = $('#PrimaryPhone').inputmask('remove');
//    console.log(ph[0].value.length);
//    if (ph[0].value.length != 10) {
//        $.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
//        $("#ValidationPhoneNumber").append("The field Phone Number is invalid.");
//        $('#PrimaryPhone').inputmask("(999) 999-9999");
//        return false;
//    }
//    else {
//        $("#ValidationPhoneNumber").empty();
//    }
//    });

//temporary added

var serialize = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    var obj = {};
    data.map(function (x) { obj[x.name] = x.value; });

    return obj;
};

