﻿(function () {
    $(function () {
        
        var _partnerService = abp.services.app.partner;
        var _$modal = $('#PartnerCreateModal');
        var _$form = _$modal.find('form');

        //_$form.validate();

        $('#RefreshButton').click(function () {
            refreshPartnerList();
        });
        $('.edit-partner').click(function (e) {
            var partnerId = $(this).attr("data-partner-id");

            e.preventDefault();
            abp.ajax({
                url: abp.appPath + 'Partners/EditPartnerModal?partnerId=' + partnerId,
                type: 'POST',
                dataType: 'html',
                success: function (content) {
                    $('#PartnerEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });
        function deletePartner(partnerId)
        {
            deletePartner(partnerId);
        }

        function refreshPartnerList() {
            location.reload(true); //reload page to see new tenant!
        }
    });

   
})();

        var serialize = function (_$form) {
        //serialize to array
        var data = $(_$form).serializeArray();

        //add also disabled items
        $(':disabled[name]', this).each(function () {
            data.push({ name: this.name, value: $(this).val() });
        });

        //map to object
        var obj = {};
        data.map(function (x) { obj[x.name] = x.value; });

        return obj;
};

//$("#PartnerForm").validate({
//    rules: {
//        ZipCode: {
//            required: true,
//            digits: true,
//            maxlength: 5,
//            minlength: 5,
//        }
      
//    },
//    messages: {      
//        ZipCode: {
//            minlength: "Zip Code Must be 5 Digits",
//            maxlength: "Zip Code Must be 5 Digits"
//        }          
        
//    }
   
//});


   
$('#PhoneNumber').inputmask("(999) 999-9999");
$("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
$("#Email").on("blur", function () {
    $("#CheckEmailValidation").html('')
    var email = $("#Email").val();
    if (email != null) {
        abp.ajax({
            url: '/Partners/CheckEmail',
            data: JSON.stringify(email)
        }).done(function (data) {
            $("#CheckEmailValidation").html('')
            if (data == true) {
                $("#CreateBtn").attr("disabled", false);                
                $("#CheckEmailValidation").html("");
            }
            else if (data == false) {

                $("#CheckEmailValidation").html("This email already exists");
                $("#CreateBtn").attr("disabled", true);
            }
        });
    }
});

$("#PhoneNumber").on('blur', function () {
    $("#ValidationPhoneNumber").empty();
    var ph = $('#PhoneNumber').inputmask('remove');
    console.log(ph[0].value.length);
    $("#CreateBtn").attr("disabled", false);
    if (ph[0].value.length != 10 && ph[0].value.length!=0) {
        //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
        $("#ValidationPhoneNumber").append("The field Phone Number is invalid.");
        $("#CreateBtn").attr("disabled", true);
        $('#PhoneNumber').inputmask("(999) 999-9999");
        return false;
    }
    else {
        $("#ValidationPhoneNumber").empty();
        $("#CreateBtn").attr("disabled", false);
    }
});

$("#CreateBtn").on('click', function () {
    $("#ValidationPhoneNumber").empty();
    var ph = $('#PhoneNumber').inputmask('remove');
    console.log(ph[0].value.length);
    if (ph[0].value.length != 10 && ph[0].value.length >=8 ) {
        //$.growl.error({ title: 'Error', message: "Please See validation errors", ttl: 1 });
        $("#ValidationPhoneNumber").append("The field Phone Number is invalid.");
        //$("#CreateBtn").attr("disabled", true);
        //$('#PhoneNumber').inputmask("(999) 999-9999");
        return false;
    }
    else {
        $("#ValidationPhoneNumber").empty();
        $("#CreateBtn").attr("disabled", false);
        $('#PhoneNumber').inputmask('remove');
    }


});

function deletePartner(partnerId) {

    confirmDelete(function () {

        abp.ajax({
            url: '/Partners/DeletePartner',
            data: JSON.stringify(partnerId)
        }).done(function (data) {
            if (data == true) {
                location.reload(true);

            }
        });

    });
}
