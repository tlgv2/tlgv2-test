﻿(function ($) {
    var _memberService = abp.services.app.member;
    var _$form = $('form[name=MemberEditForm]');
    
    function save() {
        $("#PhoneNumber").inputmask("remove");
        if (!_$form.valid()) {
            return;
        }

        var member = serialize(_$form);//_$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        //role.grantedPermissions = [];
        //var _$permissionCheckboxes = $("input[name='permission']:checked:visible");
        //if (_$permissionCheckboxes) {
        //    for (var permissionIndex = 0; permissionIndex < _$permissionCheckboxes.length; permissionIndex++) {
        //        var _$permissionCheckbox = $(_$permissionCheckboxes[permissionIndex]);
        //        role.grantedPermissions.push(_$permissionCheckbox.val());
        //    }
        //}
        
        //abp.ui.setBusy(_$form);
        //_memberService.update(member).done(function () {
           
        //    //_$modal.modal('hide');
        //    window.location = abp.appPath + 'Members';
        //    //location.reload(true); //reload page to see edited role!
        //}).always(function () {
        //    //abp.ui.clearBusy(_$modal);
        //});

        //Task-1 Changes
        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
            
            //$("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
        });
        //Task-1 Changes
    }

    //Handle save button click
    //_$form.find('#EditBasicInfo').click(function (e) {
    //    e.preventDefault();
    //    save();
    //});

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            //save();
        }
    });

    

    //$.AdminBSB.input.activate(_$form);

    //_$modal.on('shown.bs.modal', function () {
    //    _$form.find('input[type=text]:first').focus();
    //});

    

})(jQuery);

function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
        $(".info").find('input[type=text]:first').focus();
    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    $(".info").prop("disabled", function (i, v) { return !v; });
    $(".dropdown-toggle").toggleClass("disabled");
}


//temporary added

var serialize = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    var obj = {};
    data.map(function (x) { obj[x.name] = x.value; });

    return obj;
};


$(document).ready(function () {
    if ($("#PhoneNumber").val() == "0")
        $("#PhoneNumber").val("");
    $("#PhoneNumber").inputmask("(999) 999-9999");
    $("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
});
