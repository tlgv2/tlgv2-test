﻿(function () {
    $(function () {
        
        calculateMemberPrice();
        var _memberService = abp.services.app.member;
        var _$modal = $('#MemberCreateModal');
        var _$form = _$modal.find('form');

       // _$form.validate();

        $('#RefreshButton').click(function () {
            refreshMemberList();
        });

        //$('.delete-partner').click(function () {
        //    var partnerId = $(this).attr("data-tenant-id");


        //    deleteTenant(tenantId, tenancyName);
        //});

        $('.edit-member').click(function (e) {
            var memberId = $(this).attr("data-member-id");

            e.preventDefault();
            abp.ajax({
                url: abp.appPath + 'Members/EditMemberModal?memberId=' + memberId,
                type: 'POST',
                dataType: 'html',
                success: function (content) {
                    $('#MemberEditModal div.modal-content').html(content);
                },
                error: function (e) { }
            });
        });
        _$form.find('button[type="submit"]').click(function (e) {
            if ($("#memberCreateViewModel_CreateMember_Email").val() != "") {
                checkEmailExist();
                console.log("IsemailExist", IsemailExist);

                if (IsemailExist) {
                    $("#memberCreateViewModel_CreateMember_Email").focus();
                    return false;
                }

            }
            var teamId = $('#memberCreateViewModel_CreateMember_TeamId').val();
            if (teamId != 0) {
                var msg = validatePayment();
                if (msg) {
                    $("#dvpaymenterrorMessage").css('display', '');
                    $("#dvpaymenterrorMessage").html(msg);
                    $("#dvpaymenterrorMessage").focus();

                    return false;
                } else {
                    $("#dvpaymenterrorMessage").css('display', 'none');
                    $("#dvpaymenterrorMessage").html('');
                    //return true;
                }
            }
        })
        //_$form.find('button[type="submit"]').click(function (e) {
        //    $("#PhoneNumber").inputmask("remove");
        //    e.preventDefault();

        //    if (!_$form.valid()) {
        //        return;
        //    }

        //    var member = serialize(_$form);// serializeFormToObject(); //serializeFormToObject is defined in main.js            
        //    //abp.ui.setBusy(_$modal);
        //    _memberService.create(member).done(function () {
        //        _$modal.modal('hide');
        //        $("#MemberCreateModal .close").click();
        //        setTimeout(function () {
        //            $.growl.notice({ title: 'Success', message: 'Add Member Successfully', ttl: 3000 });
        //        }, 100);
        //        currentPageNumber = 0;
        //        callTableFilter();
        //        //location.reload(true); //reload page to see new tenant!
        //    }).always(function () {
        //        //abp.ui.clearBusy(_$modal);
        //    });
        //});
        //dev1changes
        _$modal.on('shown.bs.modal', function () {
            var date = new Date();
            var formattedDate = ('0' + date.getDate()).slice(-2);
            var formattedMonth = ('0' + (date.getMonth() + 1)).slice(-2);
            var formattedYear = date.getFullYear();
            var dateString = formattedMonth + '-' + formattedDate + '-' + formattedYear;
            $('input[name="memberCreateViewModel.CreateMember.deposit12monthamount"]').val("0");
            $('input[name="memberCreateViewModel.CreateMember.deposit12monthdate"]').val(dateString);
            _$modal.find('input:not([type=hidden]):first').focus();
            $("#PhoneNumber").inputmask("(999) 999-9999");
            $("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
            //$("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
        });

        //Task-1 Changes
        _$modal.on('shown.bs.modal', function () {
            _$modal.find('input:not([type=hidden]):first').focus();
            $("#PhoneNumber").inputmask("(999) 999-9999");
            $("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
            //$("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
        });
        //Task-1 Changes

        _$modal.on('shown.bs.modal', function () {
            $('.paymentType').click(function (e) {
                if (!$("#Pay_in_Full").prop("checked") && !$("#Deposit_12_Equal_Payments").prop("checked") && !$("#Custom_Plan").prop("checked")) {
                    //this.attr("checked", true);
                    return false;
                }
                console.log('this', this.value);
                if (this.value == 'Pay_in_Full') {
                    $("#divPay_in_Full").css("display", '');
                    $("#divDeposit_12_Equal_Payments").css("display", 'none');
                    $("#divCustom_Plan").css("display", 'none');
                    $(".callpayinfulldatepicker").datepicker();
                }
                if (this.value == 'Deposit_12_Equal_Payments') {
                    $("#divPay_in_Full").css("display", 'none');
                    $("#divDeposit_12_Equal_Payments").css("display", '');
                    $("#divCustom_Plan").css("display", 'none');
                }
                if (this.value == 'Custom_Plan') {
                    $("#divPay_in_Full").css("display", 'none');
                    $("#divDeposit_12_Equal_Payments").css("display", 'none');
                    $("#divCustom_Plan").css("display", '');
                }
                console.log('e', e);
                var date = new Date();
                var formattedMonth = ('0' + (date.getMonth() + 1)).slice(-2);
                var formattedDate = ('0' + date.getDate()).slice(-2);

                var formattedYear = date.getFullYear();

                var displayyear = formattedYear;
                var dateString = formattedMonth + '-' + formattedDate + '-' + displayyear;
                $("#memberCreateViewModel_CreateMember_CustomStartDate").val(dateString);
                manageDisplayPaymentTab();
                managePaymentmode();
                
                
            });
            manageDisplayPaymentTab();
            onvaluechange();
            _$modal.find('input:not([type=hidden]):first').focus();
        });

        _$modal.on('hidden.bs.modal', function () {
            $(this).find("input[name='memberCreateViewModel.CreateMember.Concordia_Discount'], input[name='memberCreateViewModel.CreateMember.Ministry_Discount'],input[name = 'memberCreateViewModel.CreateMember.Deposit_12_Equal_Payments'],input[name = 'memberCreateViewModel.CreateMember.Custom_Plan']")
                .prop("checked", "")
                .end()
        });

        Pay_in_Full

        function refreshMemberList() {
            location.reload(true); //reload page to see new tenant!
        }

        
        //function deleteTenant(tenantId, tenancyName) {
        //    abp.message.confirm(
        //        abp.utils.formatString(abp.localization.localize('AreYouSureWantToDelete', 'TownSend'), tenancyName),
        //        function (isConfirmed) {
        //            if (isConfirmed) {
        //                _tenantService.delete({
        //                    id: tenantId
        //                }).done(function () {
        //                    refreshTenantList();
        //                });
        //            }
        //        }
        //    );
        //}
    });
})();
var IsemailExist = false;
var serialize = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    var obj = {};
    data.map(function (x) { obj[x.name] = x.value; });

    return obj;
};

function onvaluechange() {
    var _teamService = abp.services.app.team;
    var teamId = $('#memberCreateViewModel_CreateMember_TeamId').val();
    if (teamId != 0) {
        $('#PricingSection').css('display', '');
        _teamService.getTeamById(teamId).done(function (data) {
            console.log('data', data);
            if (data && data.teamPrice) {
                $('#memberCreateViewModel_CreateMember_BasePrice').val(data.teamPrice);
            } else {
                $('#memberCreateViewModel_CreateMember_BasePrice').val(0);
            }
            if (data && data.deposit) {
                $('#memberCreateViewModel_CreateMember_Deposit').val(data.deposit);
            } else {
                $('#memberCreateViewModel_CreateMember_Deposit').val(0);
            }
            calculateMemberPrice();
            //location.reload(true); //reload page to see new tenant!
        }).always(function () {
            //abp.ui.clearBusy(_$modal);
        });

    }
    else {
        $('#PricingSection').css('display', 'none');
        $('#memberCreateViewModel_CreateMember_BasePrice').val(0);
        $('#memberCreateViewModel_CreateMember_Discount').val(0);
        $('#memberCreateViewModel_CreateMember_ScholarshipAmount').val(0);
        $('#memberCreateViewModel_CreateMember_MembershipAmount').val(0);
        $('#memberCreateViewModel_CreateMember_Deposit').val(0);
        
    }
    onEmailChange();
    
}

function onEmailChange() {
    var _scholarshipervice = abp.services.app.scholarship;
    var _memberservice = abp.services.app.member;
    var teamid = $('#memberCreateViewModel_CreateMember_TeamId').val();
    var email = $('#memberCreateViewModel_CreateMember_Email').val();
    if (email && email != '') {
        _scholarshipervice.getScholarshipByEmailAndTeamId(teamid, email).done(function (data) {
            console.log('_scholarshipervice data', data);
            if (data && data.scholarshipRegister) {
                var scholarshipAmount = data.scholarshipRegister.scholarshipAmount;
                $('#memberCreateViewModel_CreateMember_ScholarshipAmount').val(scholarshipAmount);
                calculateMemberPrice();
            }

            //location.reload(true); //reload page to see new tenant!
        }).always(function () {
            //abp.ui.clearBusy(_$modal);
        });
        checkEmailExist();
    }
    else {
        $('#memberCreateViewModel_CreateMember_ScholarshipAmount').val(0);
        calculateMemberPrice();
    }
    
    
    
}

function checkEmailExist() {
    var returndata;
    var _memberservice = abp.services.app.member;
    var teamid = $('#memberCreateViewModel_CreateMember_TeamId').val();
    var email = $('#memberCreateViewModel_CreateMember_Email').val();
    if (email && email != '') {
        _memberservice.getMemberByEmailAndTeamId(teamid, email).done(function (data) {
            console.log('_memberservice data', data);
            if (data && data.email) {
                $("#CheckEmailValidation").html("This email already exists");
                IsemailExist = true;
            } else {
                $("#CheckEmailValidation").html("");
                IsemailExist = false;
            }

            //location.reload(true); //reload page to see new tenant!
        }).always(function () {
            //abp.ui.clearBusy(_$modal);
        });
    }
    else {
       
    }
    return returndata;



}



function calculateMemberPrice() {
    
    var basePrice = $('#memberCreateViewModel_CreateMember_BasePrice').val();
    var discount = $('#memberCreateViewModel_CreateMember_Discount').val();
    var scholarshipAmount = $('#memberCreateViewModel_CreateMember_ScholarshipAmount').val();
    var membershipAmount = $('#memberCreateViewModel_CreateMember_MembershipAmount').val();
    var concordiaDiscount = 0;
    var ministryDiscount = 0;
    if (!$.isNumeric(discount)) {
        $('#memberCreateViewModel_CreateMember_Discount').val(0);
        discount = 0;
    }
    
    basePrice = basePrice ? basePrice : 0;

    if ($("#Concordia_Discount").prop("checked")) {
        concordiaDiscount = basePrice * 0.1;
    }
    if ($("#Ministry_Discount").prop("checked")) {
        ministryDiscount = 5000;
    }
    discount = discount ? discount : 0;
    scholarshipAmount = scholarshipAmount ? scholarshipAmount : 0;
    membershipAmount = basePrice - discount - scholarshipAmount - concordiaDiscount - ministryDiscount;

    $('#memberCreateViewModel_CreateMember_MembershipAmount').val(membershipAmount);
    managePaymentmode();
}

function fngobuttonclick() {
    var txtNumber = document.getElementById("memberCreateViewModel_CreateMember_Number_of_Installment");
    var tblCustom = document.getElementById("tblCustom");

    var customtbody = document.getElementById("customtbody");
    customtbody.innerHTML = '';
    if (parseInt(txtNumber.value) > 0) {
        tblCustom.style.display = '';
    }
    for (var i = 0; i < parseInt(txtNumber.value); i++) {
        var tr = customtbody.insertRow(-1);
        var td0 = tr.insertCell(0);
        var td1 = tr.insertCell(1);
        td0.innerHTML = "<input type='text' name='memberCreateViewModel.CreateMember.custompaydate' class='form-control text-box single-line customcalldatepicker'>";
        td1.innerHTML = '<div class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><input type="text" name="memberCreateViewModel.CreateMember.custompayamount" class="form-control text-box single-line"></div>';
        
    }
    $(".customcalldatepicker").datepicker({
        dateFormat: 'mm-dd-yy',
    });
    managePaymentmode();
}

function managePaymentmode() {
    var date = new Date();
    var formattedDate = ('0' + date.getDate()).slice(-2);

    var formattedYear = date.getFullYear();
    var deposit = $('#memberCreateViewModel_CreateMember_Deposit').val();
    var membershipAmount = $('#memberCreateViewModel_CreateMember_MembershipAmount').val();
    if ($("#Pay_in_Full").prop("checked")) {
        var formattedMonth = ('0' + (date.getMonth() + 1)).slice(-2);
        var dateString = formattedMonth + '-' + formattedDate + '-' + formattedYear;
        $("#memberCreateViewModel_CreateMember_payinfulldate").val(dateString);
        $("#memberCreateViewModel_CreateMember_payinfullamount").val(membershipAmount);
    }
    if ($("#Deposit_12_Equal_Payments").prop("checked")) {
        

        // Combine and format date string
        
        var tbodyDeposit_12_Equal_Payments = document.getElementById("tbodyDeposit_12_Equal_Payments");
        var amount = (membershipAmount - deposit) / tbodyDeposit_12_Equal_Payments.rows.length;
        for (var i = 0; i < tbodyDeposit_12_Equal_Payments.rows.length; i++) {
            console.log(tbodyDeposit_12_Equal_Payments.rows[i]);
            var tr = tbodyDeposit_12_Equal_Payments.rows[i];
            if (tr.cells.length > 1) {
                if (tr.cells[0].getElementsByTagName("input").length > 0) {
                    if (tr.cells[0].getElementsByTagName("input")[0].getAttribute("name") == "memberCreateViewModel.CreateMember.deposit12monthdate") {
                        var formattedMonth = ('0' + (date.getMonth() + 1 + i)).slice(-2);
                        var displayyear = formattedYear;
                        if ((date.getMonth() + 1 + i) > 12) {
                            formattedMonth = ('0' + ((date.getMonth() + 1 + i) - 12)).slice(-2);
                            displayyear = formattedYear + 1;
                        }
                        var dateString = formattedMonth + '-' + formattedDate + '-' + displayyear;
                        tr.cells[0].getElementsByTagName("input")[0].value = dateString;
                    }
                }
                if (tr.cells[1].getElementsByTagName("input").length > 0) {
                    if (tr.cells[1].getElementsByTagName("input")[0].getAttribute("name") == "memberCreateViewModel.CreateMember.deposit12monthamount") {
                        tr.cells[1].getElementsByTagName("input")[0].value = amount.toFixed(2);
                    }
                }
            }
            
        }
    }
    if ($("#Custom_Plan").prop("checked")) {
        var deposit = 0;
        if ($("#CustomPay_Deposit").prop("checked")) {
            deposit = $('#memberCreateViewModel_CreateMember_Deposit').val();
        }

        var startDate = new Date($("#memberCreateViewModel_CreateMember_CustomStartDate").val());
        console.log("startDate", startDate);
        var customtbody = document.getElementById("customtbody");
        var amount = (membershipAmount - deposit) / customtbody.rows.length;
        
        
        for (var i = 0; i < customtbody.rows.length; i++) {
            console.log(customtbody.rows[i]);
            var tr = customtbody.rows[i];
            if (tr.cells.length > 1) {
                if (tr.cells[0].getElementsByTagName("input").length > 0) {
                    if (tr.cells[0].getElementsByTagName("input")[0].getAttribute("name") == "memberCreateViewModel.CreateMember.custompaydate") {
                        var formattedMonth = ('0' + (startDate.getMonth() + 1 + i)).slice(-2);
                        var formattedYear = startDate.getFullYear();
                        var displayyear = formattedYear;
                        if ((startDate.getMonth() + 1 + i) > 12) {
                            formattedMonth = ('0' + ((startDate.getMonth() + 1 + i) - 12)).slice(-2);
                            displayyear = formattedYear + 1;
                        }
                        if ((startDate.getMonth() + 1 + i) > 24) {
                            formattedMonth = ('0' + ((startDate.getMonth() + 1 + i) - 24)).slice(-2);
                            displayyear = formattedYear + 2;
                        }
                        var formattedDate = ('0' + startDate.getDate()).slice(-2);
                        var dateString = formattedMonth + '-' + formattedDate + '-' + displayyear;
                        tr.cells[0].getElementsByTagName("input")[0].value = dateString;
                    }
                }
                if (tr.cells[1].getElementsByTagName("input").length > 0) {
                    if (tr.cells[1].getElementsByTagName("input")[0].getAttribute("name") == "memberCreateViewModel.CreateMember.custompayamount") {
                        tr.cells[1].getElementsByTagName("input")[0].value = amount.toFixed(2);
                    }
                }
            }

        }
    }
}

function validatePayment() {
    var errorMessage;
    var deposit = $('#memberCreateViewModel_CreateMember_Deposit').val();
    var membershipAmount = $('#memberCreateViewModel_CreateMember_MembershipAmount').val();
    if ($("#Pay_in_Full").prop("checked")) {
        if ($("#memberCreateViewModel_CreateMember_payinfulldate").val() == '') {
            errorMessage = "Add Pay In Full date";
        }
        if ($("#memberCreateViewModel_CreateMember_payinfullamount").val() == '' || $("#memberCreateViewModel_CreateMember_payinfullamount").val() == '0') {
            errorMessage = "Enter Pay In Full amount";
        }
        if ($("#memberCreateViewModel_CreateMember_payinfullamount").val()  != membershipAmount) {
            errorMessage = "Enter valid Pay In Full amount";
        }
    }
    if ($("#Deposit_12_Equal_Payments").prop("checked")) {


        // Combine and format date string

        var tbodyDeposit_12_Equal_Payments = document.getElementById("tbodyDeposit_12_Equal_Payments");
        var amount = (membershipAmount - deposit) / tbodyDeposit_12_Equal_Payments.rows.length;
        var enterAmount = 0;
        for (var i = 0; i < tbodyDeposit_12_Equal_Payments.rows.length; i++) {
            console.log(tbodyDeposit_12_Equal_Payments.rows[i]);
            var tr = tbodyDeposit_12_Equal_Payments.rows[i];
            if (tr.cells.length > 1) {
                if (tr.cells[0].getElementsByTagName("input").length > 0) {
                    if (tr.cells[0].getElementsByTagName("input")[0].getAttribute("name") == "memberCreateViewModel.CreateMember.deposit12monthdate") {
                        if (tr.cells[0].getElementsByTagName("input")[0].value == '') {
                            errorMessage = "Enter Deposit + 12 Equal Payment date";
                        }
                    }
                }
                if (tr.cells[1].getElementsByTagName("input").length > 0) {
                    if (tr.cells[1].getElementsByTagName("input")[0].getAttribute("name") == "memberCreateViewModel.CreateMember.deposit12monthamount") {
                        
                        if (tr.cells[1].getElementsByTagName("input")[0].value == '') {
                            errorMessage = "Enter Deposit + 12 Equal Payment Amount";
                        }
                        else {
                            enterAmount = enterAmount + parseFloat(tr.cells[1].getElementsByTagName("input")[0].value);
                        }
                    }
                }
            }

        }
        if (enterAmount.toFixed(0) != (membershipAmount - deposit).toFixed(0)) {
            errorMessage = "Total Deposit + 12 Equal Payment Amount Not Equal Member Amount ";
        }
    }
    if ($("#Custom_Plan").prop("checked")) {
        var deposit = 0;
        var enterAmount = 0;
        if ($("#CustomPay_Deposit").prop("checked")) {
            deposit = $('#memberCreateViewModel_CreateMember_Deposit').val();
        }
        var customtbody = document.getElementById("customtbody");
        var amount = (membershipAmount - deposit) / customtbody.rows.length;
        for (var i = 0; i < customtbody.rows.length; i++) {
            console.log(customtbody.rows[i]);
            var tr = customtbody.rows[i];
            if (tr.cells.length > 1) {
                if (tr.cells[0].getElementsByTagName("input").length > 0) {
                    if (tr.cells[0].getElementsByTagName("input")[0].getAttribute("name") == "memberCreateViewModel.CreateMember.custompaydate") {
                        if (tr.cells[0].getElementsByTagName("input")[0].value == '') {
                            errorMessage = "Enter Custom Plan date";
                        }
                    }
                }
                if (tr.cells[1].getElementsByTagName("input").length > 0) {
                    if (tr.cells[1].getElementsByTagName("input")[0].getAttribute("name") == "memberCreateViewModel.CreateMember.custompayamount") {
                        if (tr.cells[1].getElementsByTagName("input")[0].value == '') {
                            errorMessage = "Enter Custom Plan Amount";
                        }
                        else {
                            enterAmount = enterAmount + parseFloat(tr.cells[1].getElementsByTagName("input")[0].value);
                        }
                    }
                }
            }

        }
        
        if ($("#CustomPay_Deposit").prop("checked")) {
            if (enterAmount.toFixed(0) != (membershipAmount - deposit).toFixed(0)) {
                errorMessage = "Total Custom Pay Amount Not Equal Member Amount ";
            }
        } else {
            if (enterAmount.toFixed(0) != (membershipAmount).toFixed(0)) {
                errorMessage = "Total Custom Pay Amount Not Equal Member Amount ";
            }
        }
    }
    return errorMessage;
}

function manageDisplayPaymentTab() {
    //$('#Pay_in_Full-info-tab').css('display', 'none');
    //$('#Deposit_12_Equal_Payments-info-tab').css('display', 'none');
    $('#Custom_Plan-info-tab').css('display', 'none');
    //$('#Pay_in_Full-info').css('display', 'none');
    //$('#Deposit_12_Equal_Payments-info').css('display', 'none');
    $('#Custom_Plan-info').css('display', 'none');
    //if ($("#Pay_in_Full").prop("checked")) {

    //    $('#Pay_in_Full-info-tab').css('display', '');
    //    $('#Pay_in_Full-info').css('display', '');
        
    //}
    //if ($("#Deposit_12_Equal_Payments").prop("checked")) {

    //    $('#Deposit_12_Equal_Payments-info-tab').css('display', '');
    //    $('#Deposit_12_Equal_Payments-info').css('display', '');
        
    //}
    if ($("#Custom_Plan").prop("checked")) {

        $('#Custom_Plan-info-tab').css('display', '');
        $('#Custom_Plan-info').css('display', '');

    }
    var checkedValue = [];
    $('input:checkbox.paymentType').each(function () {
        var sThisVal = (this.checked ? $(this).val() : "");
        if (this.checked) {
            checkedValue.push($(this).attr("id"));
        }
        
    });
    console.log('checkedValue', checkedValue);
    $('.nav-link.active').removeClass("active");
    $('.tab-pane.active').removeClass("active");
    $('.tab-pane.show').removeClass("show");
    if (checkedValue.length > 0) {
        $("#paymentmode-tab-body").css("display", "");
        $("#paymentmode-tab-header").css("display", "");
        $("#" + checkedValue[0] + "-info").addClass("show active");
        $("#" + checkedValue[0] + "-info-tab").addClass("active");
    }
    else {
        $("#paymentmode-tab-body").css("display", "none");
        $("#paymentmode-tab-header").css("display", "none");
    }

}