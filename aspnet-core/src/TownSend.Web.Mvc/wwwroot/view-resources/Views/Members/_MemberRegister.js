﻿(function ($) {

    console.log('abp.services', abp.services)
    var _memberService = abp.services.app.member;
    var _$form = $('form[name=MemberRegisterForm]');
    $.validator.addMethod('regex', function (value, element, param) {
        return this.optional(element) ||
            value.match(typeof param == 'string' ? new RegExp(param) : param);
    },
        //'Please enter a value in the correct format.');
        //'Password is not strong');
        //'Password must contain at least one letter, at least one number, and be longer than six charaters.'
        'Your password must be have at least 8 characters long, 1 uppercase, 1 lowercase character, 1 number and 1 special character.'
    );



    //_$form.validate({
    //    rules: {
    //        Password: {
    //            required: true,
    //            //regex:'/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[#$%&]).*$/',
    //            //regex:'^(?:([A-Z])*([a-z])*(\d)*(\W)*){8,12}$',
    //            //regex: '/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[#$%&]).*$/',
    //            //regex: '^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$',
    //            //regex : '^[0-9]*$'
    //            //regex : '^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{4,8}$'
    //            //regex : '^(?=.*\d).{4,8}$'
    //            regex: '^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,50}$'
    //        },
    //        ConfirmPassword: {
    //            required: true,
    //            equalTo: Password
    //        }
    //    },
    //    messages: {
    //        Password: {
    //            required: "The Field is Required",
    //            regex:'Enter valid password'

    //        },
    //        ConfirmPassword: {
    //            required: "The Field is Required",
    //            equalTo: "Password and Confirm Password Does Not Match"
    //        }
    //    }

    //});

    function save() {
        $("#PhoneNumber").inputmask("remove");
        if (!_$form.valid()) {
            return;
        }

        var member = serialize(_$form);//_$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
        //role.grantedPermissions = [];
        //var _$permissionCheckboxes = $("input[name='permission']:checked:visible");
        //if (_$permissionCheckboxes) {
        //    for (var permissionIndex = 0; permissionIndex < _$permissionCheckboxes.length; permissionIndex++) {
        //        var _$permissionCheckbox = $(_$permissionCheckboxes[permissionIndex]);
        //        role.grantedPermissions.push(_$permissionCheckbox.val());
        //    }
        //}
        
        //abp.ui.setBusy(_$form);
        _memberService.memberRegister(member).done(function () {
            showMessage('Success', 'Update Successfully');
            $('#new-user-step-1').css('display', 'none');
            $('#new-user-step-2').css('display', '');
            $('#listep2').addClass('active');
            $('#title').html('Review & Sign Your Agreement');
            setTimeout(function () {
                $.growl.notice({ title: 'Success', message: 'Update Successfully', ttl: 3000 });
            }, 100)
            //_$modal.modal('hide');
            //window.location = abp.appPath + 'Members';
            //location.reload(true); //reload page to see edited role!
        }).always(function () {
            //abp.ui.clearBusy(_$modal);
        });

        //Task-1 Changes
        
        //Task-1 Changes
    }

    //Handle save button click
    _$form.find('#SubmitForm').click(function (e) {
        //e.preventDefault();
        //save();
    });

    $('#AgreeBtn').click(function (e) {
        e.preventDefault();

        var _$formagreement = $('form[name=MemberAgreement]');

        var memberAgreement = serialize(_$formagreement);

        _memberService.memberAgreement(memberAgreement).done(function () {
            showMessage('Success', 'Update Successfully');
            $('#new-user-step-1').css('display', 'none');
            $('#new-user-step-2').css('display', 'none');
            $('#listep2').addClass('active');
            $('#listep3').addClass('active');
            $('#title').html('Select Your Payment Plan');
            setTimeout(function () {
                $.growl.notice({ title: 'Success', message: 'Signed Successfully', ttl: 3000 });
            }, 100)
            //_$modal.modal('hide');
            //window.location = abp.appPath + 'Members';
            //location.reload(true); //reload page to see edited role!
        }).always(function () {
            //abp.ui.clearBusy(_$modal);
        });
    });

    //Handle enter key
    _$form.find('input').on('keypress', function (e) {
        if (e.which === 13) {
            e.preventDefault();
            save();
        }
    });

    $("#ConfirmAgreement").click(function () {
        if (this.checked) {
            $("#AgreeBtn").attr('disabled', false);
        } else {
            $("#AgreeBtn").attr('disabled', true);
        }
    })

    //$.AdminBSB.input.activate(_$form);

    //_$modal.on('shown.bs.modal', function () {
    //    _$form.find('input[type=text]:first').focus();
    //});

    

})(jQuery);

function toggleConfirm(button) {
    let buttons = $(button).parent().children("button");
    if ($(button).is(buttons.first())) {
        $(button).hide();
        $(buttons[1]).show();
        $(buttons.last()).show();
        $(".info").find('input[type=text]:first').focus();
    }
    else if ($(button).is(buttons.last())) {
        $(buttons[1]).hide();
        $(buttons.last()).hide();
        $(buttons.first()).show();

    }
    $(".info").prop("disabled", function (i, v) { return !v; });
    $(".dropdown-toggle").toggleClass("disabled");
}


//temporary added

var serialize = function (_$form) {
    //serialize to array
    var data = $(_$form).serializeArray();

    //add also disabled items
    $(':disabled[name]', this).each(function () {
        data.push({ name: this.name, value: $(this).val() });
    });

    //map to object
    var obj = {};
    data.map(function (x) { obj[x.name] = x.value; });

    return obj;
};

$(document).ready(function () {
    if ($("#PhoneNumber").val() == "0")
        $("#PhoneNumber").val("");
    $("#PhoneNumber").inputmask("(999) 999-9999");
    $("#PhoneNumber").inputmask({ removeMaskOnSubmit: true });
});

//$(window).on('load', function () {
//    $("#PhoneNumber").inputmask("9999999999");
//})
