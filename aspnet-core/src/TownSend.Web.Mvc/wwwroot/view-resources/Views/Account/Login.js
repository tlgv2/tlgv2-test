﻿$(function () {
    $('#ReturnUrlHash').val(location.hash);

    var $loginForm = $('#LoginForm');

    $loginForm.validate({
        highlight: function (input) {
            $(input).parents('.form-group').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-group').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    $loginForm.submit(function (e) {
        e.preventDefault();

        if (!$loginForm.valid()) {
            return;            
        }

        abp.ui.setBusy
        (
            $('#LoginArea'),
            abp.ajax
            ({
                contentType: 'application/x-www-form-urlencoded',
                url: $loginForm.attr('action'),                
                data: $loginForm.serialize(),               
            }).done(function (data) {
                abp.notify.success('Created new person with id = ' + data);
            }).fail(function (error) {
                          grecaptcha.reset();
            })
        );
        
    });

    $('a.social-login-link').click(function () {
        var $a = $(this);
        var $form = $a.closest('form');
        $form.find('input[name=provider]').val($a.attr('data-provider'));
        $form.submit();
    });

    $loginForm.find('input[type=text]:first-child').focus();
});
