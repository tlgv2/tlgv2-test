﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TownSend.Web.Views.Shared.Components.Navigation
{
    public class Navigation 
    {
        public Navigation() { }

        public Navigation(int id, int navParentId, string pageLink, string navIcon, string navName,List<Navigation> childNav)
        {
            Id = id;
            NavParentId = navParentId;
            PageLink = pageLink;
            NavIcon = navIcon;
            NavName = navName;
            childNavigation = childNav;
        }
        public int Id { get; set; }
        public int NavParentId { get; set; }

        public string PageLink { get; set; }

        public string NavIcon { get; set; }
        public string NavName { get; set; }

        public List<Navigation> childNavigation { get; set; }
    }
}
