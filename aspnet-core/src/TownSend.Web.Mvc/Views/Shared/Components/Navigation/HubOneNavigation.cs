﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TownSend.Web.Views.Shared.Components.Navigation
{
    public class HubOneNavigation
    {
        private readonly NavMenuProcess _navMenuProcess=null;

        public HubOneNavigation(NavMenuProcess navMenuProcess)
        {
            _navMenuProcess = navMenuProcess;

            IList<Navigation> FinalNavMenuList = new List<Navigation>();
            IList<Navigation> TempNavMenuList = new List<Navigation>();

            // Get List of User's Menu Items
            TempNavMenuList = _navMenuProcess.GetNavigations();

            foreach (var n in TempNavMenuList)
            {
                List<Navigation> childNav = TempNavMenuList.Where(x => x.NavParentId == x.Id).ToList();

                // Add any sub-navs to this child nav
                foreach (var nn in childNav)
                {
                    List<Navigation> childChildNav = TempNavMenuList.Where(x=>x.NavParentId == x.Id).ToList();
                    nn.childNavigation = childChildNav;
                }

                n.childNavigation = childNav;

                if (n.NavParentId == 0)
                {
                    FinalNavMenuList.Add(n);
                }
            }

            NavMenuList = FinalNavMenuList;
        }
        public IList<Navigation> NavMenuList { get; set; }
    }

    public class NavMenuProcess
    {
        public List<Navigation> GetNavigations()
        {
            var navList = new List<Navigation>();
            navList.Add(new Navigation(1,0,"Partners/Index","fa-handshake","Partners",null));
            navList.Add(new Navigation(2, 0, "Members/Index", "fa-user", "Members", null));
            navList.Add(new Navigation(3, 0, "Metting/Index", "fa-calendar-alt", "Meetings", null));
            navList.Add(new Navigation(4,0,"Team/Index","fa-users","Teams",null));
            navList.Add(new Navigation(5, 4, "Team/Index", null, "Coaching", null));
            navList.Add(new Navigation(6, 4, "Team/Index", null, "TLP Teams", null));

            return navList;
        }
    }
}
