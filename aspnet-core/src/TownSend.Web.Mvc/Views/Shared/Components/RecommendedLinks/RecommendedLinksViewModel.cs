﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Partners.Dto;

namespace TownSend.Web.Views.Shared.Components.RecommendedLink
{
    public class RecommendedLinksViewModel
    {
        public GetRecommendedLinksOutput GetRecommendedLinksOutput { get; set; }
    }
}
