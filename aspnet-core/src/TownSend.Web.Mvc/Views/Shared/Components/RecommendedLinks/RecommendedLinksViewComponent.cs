﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Partners;

namespace TownSend.Web.Views.Shared.Components.RecommendedLink
{
    public class RecommendedLinksViewComponent : TownSendViewComponent
    {
        private IPartnerAppService _partnerAppService;

        public RecommendedLinksViewComponent(IPartnerAppService  partnerAppService)
        {
            _partnerAppService = partnerAppService;
        }        

        public async Task<IViewComponentResult> InvokeAsync(int Id)
        {
            var model = new RecommendedLinksViewModel
            {
                GetRecommendedLinksOutput = await _partnerAppService.GetRecommendedLinksByPartnerId(new EntityDto(Id))
            };


            return View(model);

        }




    }
}
