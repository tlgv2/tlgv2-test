﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Partners.Dto;
using TownSend.ProfileNotes.Dto;

namespace TownSend.Web.Views.Shared.Components.ProfileNote
{
    public class ProfileNoteViewModel
    {
        //public GetProfileNoteOutput GetProfileNoteOutput { get; set; }
        public IReadOnlyList<ProfileNoteListDto> ProfileNotes { get; set; }
        public EntityType Type { get; set; }
        public long MapId { get; set; }
    }
}
