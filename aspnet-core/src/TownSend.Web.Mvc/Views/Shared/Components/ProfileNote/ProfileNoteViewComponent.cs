﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using TownSend.Partners;
using TownSend.ProfileNotes;
using TownSend.ProfileNotes.Dto;

namespace TownSend.Web.Views.Shared.Components.ProfileNote
{
    public class ProfileNoteViewComponent : TownSendViewComponent
    {
        private readonly IProfileNoteAppService _profileNoteAppService;

        public ProfileNoteViewComponent(IProfileNoteAppService profileNoteAppService)
        {
            _profileNoteAppService = profileNoteAppService;
        }
        public async Task<IViewComponentResult> InvokeAsync(int id, EntityType type)
        {
            var etype = new EntityTypeMapDto() { MapId = id, Type = type };
            var notes = await _profileNoteAppService.GetNotes(etype);
          
            var viewModel = new ProfileNoteViewModel() {ProfileNotes=notes.Items,Type=etype.Type,MapId=id };
            return View(viewModel);
        }
    }
}
