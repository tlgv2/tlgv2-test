﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Documents.Dto;
using TownSend.Partners.Dto;

namespace TownSend.Web.Views.Shared.Components.Document
{
    public class DocumentViewModel1
    {
        public int PartnerId { get; set; }
        //public IReadOnlyList<DocumentListDtoP> Documents { get; set; }
    }

    public class DocumentViewModel
    {
        public IReadOnlyList<DocumentListDto> Documents { get; set; }
        public EntityType Type { get; set; }
        public long MapId { get; set; }
    }
}
