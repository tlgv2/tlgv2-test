﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using TownSend.Documents;
using TownSend.Documents.Dto;
using TownSend.Partners;
using TownSend.Partners.Dto;
using TownSend.Web.Models.Partners;

namespace TownSend.Web.Views.Shared.Components.Document
{
    public class DocumentViewComponent : TownSendViewComponent
    {
        private readonly IDocumentAppService _documentAppService;
        public DocumentViewComponent(IDocumentAppService documentAppService)
        {
            _documentAppService = documentAppService;
        }
        public async Task<IViewComponentResult> InvokeAsync(int id,EntityType type)
        {
            var etype = new EntityTypeMapDto() {MapId=id,Type=type };
            var documents = await _documentAppService.GetDocuments(etype);
            var viewmodel = new DocumentViewModel();
            viewmodel.Documents = documents.Items;
            viewmodel.Type = etype.Type;
            viewmodel.MapId = id;
            //var documents = await _partnerAppService.GetDocumentsAsync(new GetDocumentInput() { PartnerId = Id });
            //var viewmodel = new DocumentViewModel
            //{
            //    //PartnerId = model.Partner.Id,
            //    Documents = documents.Items
            //};

            return View(viewmodel);
        }
    }
}
