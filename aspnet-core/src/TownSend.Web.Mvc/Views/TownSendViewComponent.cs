﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace TownSend.Web.Views
{
    public abstract class TownSendViewComponent : AbpViewComponent
    {
        protected TownSendViewComponent()
        {
            LocalizationSourceName = TownSendConsts.LocalizationSourceName;
        }
    }
}
