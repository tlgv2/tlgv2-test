﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace TownSend.Web.Views
{
    public abstract class TownSendRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected TownSendRazorPage()
        {
            LocalizationSourceName = TownSendConsts.LocalizationSourceName;
        }
    }
}
