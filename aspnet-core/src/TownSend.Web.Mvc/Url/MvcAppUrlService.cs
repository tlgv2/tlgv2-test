﻿using Abp.MultiTenancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Url;

namespace TownSend.Web.Url
{
    public class MvcAppUrlService : AppUrlServiceBase
    {
        public override string EmailActivationRoute => "Account/EmailConfirmation";        
        public override string PasswordResetRoute => "Account/ResetPassword";
        public override string ScholarshipInviteEmailRoute => "Scholarship/Questionnaire";

        public override string ScholarshipApprovalEmailRoute => "Scholarship/ScholarshipDetail";

        public override string MemberInviteEmailRoute => "Members/MemberRegisterStep1";
        public override string EmailCallBackRoute => "Account/EmailCallBackURL";

        public override string ChangeEmailRoute => "Account/ChangeEmail";


        public override string ForgotPasswordRoute => "Account/ForgotPassword";

        public MvcAppUrlService(
                IWebUrlService webUrlService,
                ITenantCache tenantCache
            ) : base(
                webUrlService,
                tenantCache
            )
        {

        }
    }
}
