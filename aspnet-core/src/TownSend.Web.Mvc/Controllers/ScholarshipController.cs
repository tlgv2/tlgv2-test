﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Runtime.Validation;
using Abp.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using TownSend.Controllers;
using TownSend.Identity;
using TownSend.Scholarships;
using TownSend.Scholarships.Dto;
using TownSend.Teams;
using TownSend.Web.Models.Scholarships;
using static TownSend.Scholarships.ScholarshipManager;

namespace TownSend.Web.Controllers
{
    [DisableValidation]
    [AbpMvcAuthorize("Pages.Scholarships")]
    public class ScholarshipController : TownSendControllerBase
    {
        #region Field
        private readonly ITeamAppService _teamAppService;
        private readonly IScholarshipAppService _scholarshipAppService;
        private readonly ScholarshipManager _scholarshipManager;
        private readonly SignInManager _signInManager;
        #endregion

        #region Ctor
        public ScholarshipController(ITeamAppService teamAppService,
                                        IScholarshipAppService scholarshipAppService,
                                        ScholarshipManager scholarshipManager,
                                        SignInManager signInManager)
        {
            _teamAppService = teamAppService;
            _scholarshipAppService = scholarshipAppService;
            _scholarshipManager = scholarshipManager;
            _signInManager = signInManager;
        }
        #endregion

        #region Methods
        /// <summary>
        /// This Method Give Scholarship Registration Page To Fill Form And Send Request
        /// Affected Entities
        /// Scholarship
        /// Member
        /// User
        /// Invite
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Register()
        {
            ScholarshipSignupViewModel model = new ScholarshipSignupViewModel();
            var teams = await _teamAppService.GetAll();
            model.TeamList = teams;            
           
            return View(model);
        }

        /// <summary>
        /// This Method is Post Method Of Register Page This Method Take Value From Member And Fill Into Scholarship Details Table
        /// Affected Table
        /// Member
        /// Scholarship
        /// Invite
        /// User
        /// ScholarshipStatus
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(ScholarshipSignupViewModel model)
        {
            try
            {
                model.TeamList = await _teamAppService.GetAll();

                if (!ModelState.IsValid)
                {
                    //return page 
                    ModelState.AddModelError(string.Empty,"Please Refresh The page");
                    return View(model);
                }

                var result = await _scholarshipAppService.CreateAsync(model.ScholarshipRegister);

                if (!result.IsSuccess)
                {
                    model.ErrorMessage = result.ErrorMessage;
                    return View(model);
                }

                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.ScholarshipIvitationSent;
                return RedirectToAction("Register");
            }
            catch (Exception ex)
            {
                throw ex;
                //throw new UserFriendlyException(ex.Message);
            }
        }
        /// <summary>
        /// This Method Display Scholarship Details Page And Update First Page Of Scholarship Details
        /// Affected Entities
        /// Scholarship
        /// Member
        /// Scholarship Status
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AbpMvcAuthorize("Pages.Scholarships.List")]
        public async Task<IActionResult> UpdateScholarshipInfo(ScholarshipDetailsViewModel model)
        {
            try
            {
                ModelState.Remove("ScholarshipDetails.Questionnaire.GrowthDetails");
                ModelState.Remove("ScholarshipDetails.Questionnaire.AchivementsDetails");
                ModelState.Remove("ScholarshipDetails.Questionnaire.LifeExperienceDetails");
                ModelState.Remove("ScholarshipDetails.Questionnaire.FutureGoalDetails");
                ModelState.Remove("ScholarshipDetails.Questionnaire.FirstTimeinTLP");
                ModelState.Remove("ScholarshipDetails.Questionnaire.FirstTimeApply");
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.EmailAddress");
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.FirstName");
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.LastName");
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.TeamId");

                if (ModelState.IsValid)
                {
                    ScholarshipInfoEditDto dto = new ScholarshipInfoEditDto
                    {
                        ScholarshipId = model.ScholarshipDetails.Questionnaire.ScholarshipId,
                        FirstName = model.ScholarshipDetails.MemberFirstName,
                        LastName = model.ScholarshipDetails.MemberLastName,
                        PhoneNumber = model.ScholarshipDetails.MemberPhoneNumber,
                        ScholarshipAmount = model.ScholarshipDetails.ScholarshipRegister.ScholarshipAmount
                    };

                    await _scholarshipAppService.UpdateScholarshipInfo(dto);

                    TempData["Type"] = Constant.Success;
                    TempData["Message"] = Constant.RecordsUpdatedMessage;
                }
                else
                {
                    TempData["Type"] = Constant.Error;
                    TempData["Message"] = Constant.ModelstateNotValid;
                }
            }
            catch (Exception ex)
            {
                TempData["Type"] = Constant.Error;
                TempData["Message"] = Constant.SomethingwentWrong;
            }
            TempData["scholarshiptab"] = "ScholarshipInfo";
            return RedirectToAction("ScholarshipDetail", new { model.ScholarshipDetails.Questionnaire.ScholarshipId });
        }
        /// <summary>
        /// This Method Display Scholarship Details Page And Update Second Page Of Scholarship Questionnaire
        /// Affected Entities
        /// Scholarship
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //[AbpMvcAuthorize("Pages.Scholarships.List")]
        public async Task<IActionResult> UpdateQuestionnaire(ScholarshipDetailsViewModel model)
        {
            try
            {                
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.EmailAddress");
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.FirstName");
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.LastName");
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.TeamId");
                ModelState.Remove("ScholarshipDetails.ScholarshipRegister.ScholarshipAmount");
                ModelState.Remove("ScholarshipDetails.MemberLastName");
                ModelState.Remove("ScholarshipDetails.MemberFirstName");
                ModelState.Remove("ScholarshipDetails.Questionnaire.GrowthDetails");
                
                
                if (ModelState.IsValid)
                {
                    QuestionnaireDto dto = new QuestionnaireDto
                    {
                        ScholarshipId = model.ScholarshipDetails.Questionnaire.ScholarshipId,
                        FirstTimeApply = model.ScholarshipDetails.Questionnaire.FirstTimeApply,
                        FirstTimeinTLP = model.ScholarshipDetails.Questionnaire.FirstTimeinTLP,
                        GrowthDetails = model.ScholarshipDetails.Questionnaire.GrowthDetails,
                        AchivementsDetails = model.ScholarshipDetails.Questionnaire.AchivementsDetails,
                        LifeExperienceDetails = model.ScholarshipDetails.Questionnaire.LifeExperienceDetails,
                        FutureGoalDetails = model.ScholarshipDetails.Questionnaire.FutureGoalDetails
                    };

                    await _scholarshipAppService.UpdateQuestionnaire(dto);

                    TempData["Type"] = Constant.Success;
                    TempData["Message"] = Constant.RecordsUpdatedMessage;
                }
                else
                {
                    TempData["Type"] = Constant.Error;
                    TempData["Message"] = Constant.ModelstateNotValid;
                }
            }
            catch (Exception ex)
            {
                TempData["Type"] = Constant.Error;
                TempData["Message"] = Constant.SomethingwentWrong;
            }
            TempData["scholarshiptab"] = "ScholarshipQuestionnaireResult";
            return RedirectToAction("ScholarshipDetail", new { model.ScholarshipDetails.Questionnaire.ScholarshipId });
        }


        /// <summary>
        /// This Method Is Called On When Member is Fill Questionnaire Form Of Scholarship Invite 
        /// Affected Entities
        /// Scholarship 
        /// Member
        /// </summary>
        /// <param name="scholarshipId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> Questionnaire(int scholarshipId)
        {
            //remove session
            //await _signInManager.SignOutAsync();

            QuestionnaireViewModel model = new QuestionnaireViewModel();

            //check weather current request is valid or not 
            var result = _scholarshipManager.ValidateSchorlarshipById(scholarshipId);
            if (result.IsValid)
            {
                model.Questionnaire.ScholarshipId = result.ScholarshipId;
                model.YesNoList = new SelectList(this.BindYesNoList());
                return View(model);
            }
            else
            {
                var returnResult = ProcessBadStatus(result);
                if (returnResult != null)
                {
                    return returnResult;
                }

                return LocalRedirect("/Home/AccessDenied");
            }
        }
        /// <summary>
        /// THis Method Is Display Questionnaire Form For Member Fill on Scholarship Register  
        /// / Affected Entities
        /// Scholarship 
        /// Member
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Questionnaire(QuestionnaireViewModel model)
        {
            int scholarshipId = 0;
            if (model != null && model.Questionnaire != null)
            {
                scholarshipId = model.Questionnaire.ScholarshipId;
            }

            //check weather current request is valid or not 
            var result = _scholarshipManager.ValidateSchorlarshipById(scholarshipId);
            if (!result.IsValid)
            {
                var returnResult = ProcessBadStatus(result);
                if (returnResult != null)
                {
                    return returnResult;
                }

                return LocalRedirect("/Home/AccessDenied");
            }

            if (!ModelState.IsValid)
            {
                model.YesNoList = new SelectList(this.BindYesNoList());
                return View(model);
            }

            //scholarship data model valid
            await _scholarshipAppService.UpdateAsync(model.Questionnaire);

            //at the end
            return RedirectToAction("QuestionnaireCompleted");
        }
        /// <summary>
        /// This Method Is Display Completed Message When Member Fill All Details Of Scholarship Register Form
        /// Affected Entities
        /// Scholarship
        /// ScholarshipStatus
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public IActionResult QuestionnaireCompleted()
        {
            return View();
        }
        /// <summary>
        /// This Method Display Scholarship Details Page
        /// </summary>
        /// <param name="scholarshipId"></param>
        /// <returns></returns>

        //[AbpMvcAuthorize("Pages.Scholarships.List")]
        public async Task<IActionResult> ScholarshipDetail(int scholarshipId)
        {
            var scholarship = await _scholarshipAppService.GetScholarshipAsync(scholarshipId);
            if(scholarship==null)
            {
                return LocalRedirect("/Home/AccessDenied");
            }
            var model = new ScholarshipDetailsViewModel() { ScholarshipDetails = scholarship };
            model.YesNoList = new SelectList(this.BindYesNoList());
            return View(model);
        }
        /// <summary>
        /// This Method Is Update Scholarship Status From Scholarship Details Page and Give Replay To Member
        /// Affected Entities
        /// Scholarship
        /// ScholarshipStatus
        /// </summary>
        /// <param name="scholarshipId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<IActionResult> UpdateScholarshipStatus(int scholarshipId, int status)
        {
            await _scholarshipAppService.UpdateScholarshipStatus(scholarshipId, (ScholarshipStatusEnum)status);
            TempData["Type"] = Constant.Success;

            if (status == (int)ScholarshipStatusEnum.ApprovedSendInvite)
            {
                TempData["Message"] = Constant.ScholarshipApprovedMessage;
            }

            if (status == (int)ScholarshipStatusEnum.DeniedSendInvite)
            {
                TempData["Message"] = Constant.ScholarshipDeniedMessage;
            }

            if (status == (int)ScholarshipStatusEnum.Cancelled)
            {
                TempData["Message"] = Constant.ScholarshipCancelMessage;
            }
            return Json(new { Result = "OK" });
        }
        /// <summary>
        /// This Method Is Resend Updated Quesionnaire To Admin And Send Mail
        /// </summary>
        /// <param name="scholarshipId"></param>
        /// <returns></returns>
        public async Task<IActionResult> ResendQuestionnaire(int scholarshipId)
        {
            bool result = await _scholarshipAppService.GetScholarshipToResend(scholarshipId);
            if (result)
            {
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.ScholarshipResendMessage;
            }
            else
            {
                TempData["Type"] = Constant.Error;
                TempData["Message"] = Constant.SomethingwentWrong;
            }

            return RedirectToAction("ScholarshipDetail", new { scholarshipId });
        }


        /// <summary>
        /// This Method Is Display List Of Scholarship In Scholarship List Page
        /// </summary>
        /// <param name="scholarshipListViewModel"></param>
        /// <returns></returns>
        //[AbpMvcAuthorize("Pages.Scholarships.List")]
        public async Task<IActionResult> List(ScholarshipListViewModel scholarshipListViewModel)
        {
            ScholarshipListViewModel model = new ScholarshipListViewModel();
            DateTime? StartDate = scholarshipListViewModel.StartDate;
            DateTime? EndDate = scholarshipListViewModel.EndDate;
            List<int> TeamIds = scholarshipListViewModel.TeamIds;
            List<ScholarshipStatusEnum> StatusIds = scholarshipListViewModel.StatusIds;
            int[] TeamIdArray = TeamIds == null ? new int[0] : TeamIds.ToArray();
            ScholarshipStatusEnum[] StatusIdArray = StatusIds == null ? new ScholarshipStatusEnum[0] : StatusIds.ToArray(); 
            model.Scholarships = await _scholarshipAppService.GetAllScholarships(StartDate, EndDate, TeamIdArray, StatusIdArray);
            model.TeamList = await _teamAppService.GetAll();
            model.ScholarshipStatusList = await _scholarshipAppService.GetScholarshipstatusForDD();
            model.StartDate = scholarshipListViewModel.StartDate;
            model.EndDate = scholarshipListViewModel.EndDate;
            model.TeamIds = scholarshipListViewModel.TeamIds;
            model.StatusIds = scholarshipListViewModel.StatusIds;
            return View(model);
        }

        /// <summary>
        /// This Method Is Redirect to Scholarship List Page with ScholarshipInfo tab
        /// </summary>
        /// <param name="ScholarshipId"></param>
        /// <returns></returns>
        public async Task<IActionResult> ScholarshipInfoRedirctDetail(int ScholarshipId)
        {
            TempData["scholarshiptab"] = "ScholarshipInfo";
            return RedirectToAction("ScholarshipDetail", "Scholarship", new { ScholarshipId });
        }

        /// <summary>
        /// This Method Is Redirect to Scholarship List Page with ScholarshipQuestionnaire tab
        /// </summary>
        /// <param name="ScholarshipId"></param>
        /// <returns></returns>
        public async Task<IActionResult> ScholarshipQuestionnaireRedirctDetail(int ScholarshipId)
        {
            TempData["scholarshiptab"] = "ScholarshipQuestionnaireResult";
            return RedirectToAction("ScholarshipDetail", "Scholarship", new { ScholarshipId });
        }
        #endregion

        #region Utilities

        public List<SelectListItem> BindYesNoList()
        {
            var data = new List<SelectListItem> {
                            new SelectListItem { Text="Yes", Value="1" },
                            new SelectListItem { Text="No", Value="0" },
                        };
            return data;
        }

        private IActionResult ProcessBadStatus(ScholarshipValidityResult validity)
        {
            if (validity != null)
            {
                switch (validity.CurrentStatus)
                {
                    case ScholarshipStatusEnum.ApprovedSendInvite:
                        return ProcessAndLogBadStatus(validity.CurrentStatus, "success", ScholarshipConsts.Heading.ApprovedSendInvite, ScholarshipConsts.Message.ApprovedSendInvite, "fa fas fa-graduation-cap");
                    case ScholarshipStatusEnum.Cancelled:
                        return ProcessAndLogBadStatus(validity.CurrentStatus, "secondary", ScholarshipConsts.Heading.Cancelled, ScholarshipConsts.Message.Cancelled, "fa fas fa-graduation-cap");
                    case ScholarshipStatusEnum.DeniedSendInvite:
                        return ProcessAndLogBadStatus(validity.CurrentStatus, "secondary", ScholarshipConsts.Heading.DeniedSendInvite, ScholarshipConsts.Message.DeniedSendInvite, "fa fas fa-graduation-cap");
                    case ScholarshipStatusEnum.PendingApproval:
                        return ProcessAndLogBadStatus(validity.CurrentStatus, "primary", ScholarshipConsts.Heading.PendingApproval, ScholarshipConsts.Message.PendingApproval, "fa fas fa-graduation-cap");
                }
            }

            return null;
        }

        private IActionResult ProcessAndLogBadStatus(ScholarshipStatusEnum status, string alertType, string heading, string message, string icon)
        {
            //_logger.LogInformation($"Scholarship questionnaire Guid [{guid}] has a status of [{status}] and will redirect to a {alertType} message page with the following heading [{heading}] and message [{message}].");
            return RedirectToAction("ErrorMessage", "Home", new { alertType, message, heading, icon });
        }



        #endregion



    }
}