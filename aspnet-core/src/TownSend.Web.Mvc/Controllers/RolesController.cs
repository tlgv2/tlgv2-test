using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using TownSend.Authorization;
using TownSend.Controllers;
using TownSend.Roles;
using TownSend.Roles.Dto;
using TownSend.Web.Models.Roles;
using Microsoft.AspNetCore.Routing;

namespace TownSend.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Roles)]
    public class RolesController : TownSendControllerBase
    {
        private readonly IRoleAppService _roleAppService;

        public RolesController(IRoleAppService roleAppService)
        {
            _roleAppService = roleAppService;
        }

        public async Task<IActionResult> Index()
        {
            var roles = (await _roleAppService.GetRolesAsync(new GetRolesInput())).Items;
            var permissions = (await _roleAppService.GetAllPermissions()).Items;
            var model = new RoleListViewModel
            {
                Roles = roles,
                Permissions = permissions
            };

            return View(model);
        }

        public async Task<ActionResult> EditRoleModal(int roleId)
        {
            var output = await _roleAppService.GetRoleForEdit(new EntityDto(roleId));
            var model = ObjectMapper.Map<EditRoleModalViewModel>(output);

            return View("_EditRoleModal", model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateRoleAsync(EditRoleModalViewModel input)
        {
            var roleDto = new RoleDto() { Id = input.Role.Id, Description = input.Role.Description, Name = input.Role.Name, DisplayName = input.Role.DisplayName, GrantedPermissions = input.GrantedPermissionNames };
            await _roleAppService.UpdateAsync(roleDto);
            return RedirectToAction("Index");
        }
    }
}
