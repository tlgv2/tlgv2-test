﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using TownSend.Authorization;
using TownSend.Controllers;
using TownSend.Users;
using TownSend.Web.Models.Users;
using TownSend.Users.Dto;
using Microsoft.AspNetCore.Routing;
using TownSend.Authorization.Users;
using Abp.Runtime.Session;
using Abp.Domain.Uow;
using TownSend.Roles;
using System.Collections.Generic;
using System.Linq;
using Abp.Runtime.Validation;

namespace TownSend.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_AdminUses)]
    [DisableValidation]
    public class UsersController : TownSendControllerBase 
    {
        private readonly IUserAppService _userAppService;
        private readonly UserManager _userManager;
        private readonly IAbpSession _abpSession;
        private readonly LogInManager _logInManager;
        private readonly IUserEmailer _userEmailer;
        private readonly IRoleAppService _roleAppService;

        public UsersController(IUserAppService userAppService,
                               UserManager userManager,
                               IAbpSession abpSession,
                               LogInManager logInManager,
                               IUserEmailer userEmailer,
                               IRoleAppService roleAppService)
        {
            _userAppService = userAppService;
            _userManager = userManager;
            _abpSession = abpSession;
            _logInManager = logInManager;
            _userEmailer = userEmailer;
            _roleAppService = roleAppService;
        }

        public async Task<ActionResult> Index(int[] UserType, UserStatusListDto[] StatusIds)
        {
            //List<int> FilterRoleIdsList = FilterRoleIds.ToList();
            //int[] RoleIdArray = FilterRoleIdsList == null ? new int[0] : FilterRoleIdsList.ToArray();
            ////var users = (await _userAppService.GetAllAsync(new PagedUserResultRequestDto {MaxResultCount = int.MaxValue})).Items; // Paging not implemented yet

            var users = (await _userAppService.GetAllDeletedData(UserType,StatusIds)).Items;
            var roles = (await _userAppService.GetRoles()).Items;
            var roleIds = new List<int>();
            //var roletype = await _userAppService.GetRoles();
            var model = new UserListViewModel
            {
                Users = users,
                Roles = roles
            };

            //Filter
            model.UserType = UserType;
            //model.StatusIds = StatusIds;
            return View(model);

        }

        public async Task<ActionResult> EditUserModal(int userId)
        {
            using (UnitOfWorkManager.Current.DisableFilter(AbpDataFilters.SoftDelete))
            {
                var user = await _userAppService.GetAsync(new EntityDto<long>(userId));
                var roles = (await _userAppService.GetRoles()).Items;
                var model = new EditUserModalViewModel
                {
                    User = user,
                    Roles = roles
                };
                return View("EditUserModal", model);
            }


        }
        public async Task<IActionResult> UpdateUser(UserDto input)
        {
            await _userAppService.UpdateAsync(input);
            return RedirectToAction("EditUserModal", new RouteValueDictionary(
                new { controller = "Users", action = "EditUserModal", userId = input.Id }));
            //return RedirectToAction("Index");
        }
        public async Task<IActionResult> DeleteUser([FromBody]int userId)
        {
            await _userAppService.DeleteAsync(new EntityDto<long>(userId));
            return Json(true);
            //return RedirectToAction("Index");
        }
        public async Task<IActionResult> UnDeleteUser(int userId)
        {
            await _userAppService.UnDelete(new EntityDto<long>(userId));

            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> ChangeUserPassword(ResetPasswordDto input)
        {
            if(ModelState.IsValid)
            {

            }
            //await _userAppService.ConfirmPasswordAsync(input);
            //await _userManager.ChangePasswordAsync(input);
            //long userId = _abpSession.UserId.Value;
            var user = await _userManager.GetUserByIdAsync(input.UserId);
           await _userManager.ChangePasswordAsync(user,input.NewPassword);
            await _userEmailer.SendPassword(user,input.NewPassword);
            return RedirectToAction("EditUserModal", new RouteValueDictionary(new { controller = "Users", action = "EditUserModal", userId = input.UserId }));
        }
    }
}
