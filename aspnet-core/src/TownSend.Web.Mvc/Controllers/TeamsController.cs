﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Runtime.Validation;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using TownSend.Authorization;
using TownSend.Blob;
using TownSend.BlobUtilitys;
using TownSend.Common;
using TownSend.Common.Dto;
using TownSend.Controllers;
using TownSend.Documents;
using TownSend.Documents.Dto;
using TownSend.ExportToFile;
using TownSend.Partners;
using TownSend.Teams;
using TownSend.Teams.Dto;
using TownSend.Utilities;
using TownSend.Utilities.ExcelExport;
using TownSend.Utilities.ExcelReport;
using TownSend.Web.Models.Teams;

namespace TownSend.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Teams)]
    [DisableValidation]
    public class TeamsController : TownSendControllerBase
    {
        private readonly ITeamAppService _teamAppService;
        private readonly ICommonAppService _commonAppService;
        private readonly IPartnerAppService _partnerAppService;
        private readonly IDocumentAppService _documentAppService;
        private readonly IListExcelExporter _listExcelExporter;

        public TeamsController(
                                ITeamAppService teamAppService,
                                ICommonAppService commonAppService,
                                IPartnerAppService partnerAppService,
                                IDocumentAppService documentAppService,
                                IListExcelExporter listExcelExporter)
        {
            _teamAppService = teamAppService;
            _commonAppService = commonAppService;
            _partnerAppService = partnerAppService;
            _documentAppService = documentAppService;
            _listExcelExporter = listExcelExporter;

        }
        /// <summary>
        /// This Method Is Display TeamList
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <param name="FilterPartnerIds"></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(DateTime? StartDate, DateTime? EndDate, int[] FilterPartnerIds)
        {
            List<int> FilterPartnerIdsList = FilterPartnerIds.ToList();
            int[] PartnerIdArray = FilterPartnerIdsList == null ? new int[0] : FilterPartnerIdsList.ToArray();

            var allTeams = (await _teamAppService.GetAllTeamList(StartDate, EndDate, PartnerIdArray)).Items;
            var partners = (await _commonAppService.GetAllPartner());
            var partnerIds = new List<int>();
            var model = new TeamListViewModel() { Teams = allTeams };
            if (IsGranted("Pages.Partners.Create"))
            {

                model.Partners = partners;
                model.PartnerIds = partnerIds;
            }
            else
            {
                var userPartner = partners.Where(x => x.UserId == AbpSession.UserId).FirstOrDefault();
                model.Partners = partners.Where(x => x.Id != userPartner.Id).ToList();
                model.PartnerIds = partnerIds;
                model.DefaultPartnerId = userPartner.Id;
                model.DefaultPartnerFullName = userPartner.FullName;

            }
            model.StateList = await _commonAppService.GetAll();
            model.FilterPartnerList = (await _partnerAppService.GetAll()).Items.Select(r => new PartnersListForTeam { Id = r.Id, FullName = r.FullName }).ToList();
            model.StartDate = StartDate;
            model.EndDate = EndDate;
            model.FilterPartnerIds = FilterPartnerIdsList;

            ViewBag.Partners = (await _partnerAppService.GetAll()).Items.Select(r => new SelectListItem { Value = r.Id.ToString(), Text = r.FullName }).ToList();
            return View(model);
        }

        /// <summary>
        /// This Method Is Create Team And Also Add Partner Into Team 
        /// Affected Entities
        /// Team
        /// Partner
        /// TeamPartnerMap
        /// </summary>
        /// <param name="model"></param>
        /// <param name="DefaultPartnerId"></param>
        /// <returns></returns>
        public async Task<IActionResult> CreateTeam(TeamListViewModel model, int DefaultPartnerId)
        {
            if (DefaultPartnerId != 0)
            {
                model.CreateTeam.PartnerIds.Add(DefaultPartnerId);
            }
            IFormatProvider culture = new CultureInfo("en-US", true);
            //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy/MM/dd", culture);
            model.CreateTeam.StartDate = DateTime.ParseExact(model.CreateTeam.StringStartDate, "MM-dd-yyyy", culture);

            var stateList = (await _commonAppService.GetAll()).Items;
            ViewBag.SelectedListOfState = stateList;

            await _teamAppService.CreateAsync(model.CreateTeam);
            TempData["Type"] = Constant.Success;
            TempData["Message"] = Constant.addTeam;
            return RedirectToAction("Index");
        }

        /// <summary>
        /// This Method Is Update Team On Received Input And Insert Into Teams
        /// Affected Entities
        /// Team
        /// TeamPartnerMapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateTeam(EditTeamModelViewModel model)
        {
            if (ModelState.IsValid)
            {
                var input = ObjectMapper.Map<TeamDto>(model.Team);
                await _teamAppService.UpdateAsync(input);
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.updateTeam;
                return RedirectToAction("Editteam", new RouteValueDictionary(new { controller = "Teams", action = "EditTeam", Id = input.Id }));

            }
            return View();

        }

        /// <summary>
        /// This Method Is Display Team_Details 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<IActionResult> EditTeam(int Id)
        {
            var teams = await _teamAppService.GetTeamForEdit(new EntityDto(Id));
            if (teams.Team == null)
            {
                return LocalRedirect("/Home/AccessDenied");
            }
            var model = ObjectMapper.Map<EditTeamModelViewModel>(teams);
            List<int> partnerIds = new List<int>();
            var partnerList = new List<PartnersListForTeam>();
            var stateList = await _commonAppService.GetAll();
            model.StateList = stateList;

            if (IsGranted("Pages.Partners.Create"))
            {
                partnerList = (await _commonAppService.GetAllPartner()).ToList();
            }
            else
            {
                var partner = await _commonAppService.GetAllPartner();
                partnerList = (await _commonAppService.GetAllPartner()).Where(x => x.UserId != AbpSession.UserId).ToList();
                model.DefaultPartnerId = partner.Where(x => x.UserId == AbpSession.UserId).Select(y => y.Id).FirstOrDefault();
            }
            if (teams != null && teams.PartnerList != null && teams.PartnerList.Any())
            {
                foreach (var partner in teams.PartnerList)
                {
                    partnerList.RemoveAll(c => Convert.ToInt32(c.Id) == partner.PartnerId);
                }
            }

            if (partnerList != null && partnerList.Any())
            {
                ViewBag.Partners = partnerList.Select(r => new SelectListItem { Value = r.Id.ToString(), Text = r.FullName });
            }
            else
            {
                ViewBag.Partners = new List<SelectListItem>();
            }

            return View(model);
        }

        /// <summary>
        /// This Method Add Documents And Notes In Edit_Team Page
        /// Affected Entities
        /// Team 
        /// Document
        /// Notes
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> UploadFile()
        {
            int i;
            CreateDocumentDto input = new CreateDocumentDto();
            for (i = 0; i < Request.Form.Files.Count; i++)
            {

                string teamId = Request.Form["MapId"];
                var tId = Convert.ToInt32(teamId);
                string et = Request.Form["Type"];
                string VisibleDateFrom = Request.Form["VisibleDateFrom"];
                Boolean Restrict = Convert.ToBoolean(Request.Form["Restrict"]);
                string VisibleDateTo = Request.Form["VisibleDateTo"];
                input.Restrict = Restrict;
                var team = await _teamAppService.GetTeamForEdit(new EntityDto(tId));
                string filePath = "";
                input.MapId = Convert.ToInt32(teamId);
                input.Name = Request.Form.Files[i].FileName;
                input.Size = Convert.ToInt32(Request.Form.Files[i].Length);
                input.Description = Request.Form.Files[i].FileName;
                input.Type = EntityType.Teams;

                if (input.Restrict == null || input.Restrict == false)
                {
                    input.VisibleDateFrom = null;
                    input.VisibleDateTo = null;
                }
                else
                {
                    IFormatProvider culture = new CultureInfo("en-US", true);
                    input.VisibleDateFrom = DateTime.ParseExact(VisibleDateFrom, "MM-dd-yyyy", culture);
                    input.VisibleDateTo = DateTime.ParseExact(VisibleDateTo, "MM-dd-yyyy", culture);
                }
                if (input.Size > 0)
                {
                    using (var stream = Request.Form.Files[i].OpenReadStream())
                    {
                        MemoryStream memoryStream = new MemoryStream();
                        stream.CopyTo(memoryStream);
                        memoryStream.Position = 0;
                        filePath = string.Format("{0}/{1}/{2}/{3}", "Teams", team.Team.TeamName + " " + team.Team.Id, DateTime.Now.ToString("yyyy-MM-dd"), input.Name);
                        input.DocumentPath = filePath;
                        var upload = await BlobUtility.CopyToBlob(filePath, "tlgdocuments", input.Name, memoryStream);
                        if (upload == true)
                        {
                            await _documentAppService.CreateDocument(input);
                        }
                    }
                }
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.documentAdded;
                TempData["teamtab"] = "Documents";
                //return RedirectToAction("EditPartner", new RouteValueDictionary(new { controller = "Partners", action = "EditPartner", Id = input.PartnerId }));
            }
            return input.MapId.ToString();
        }

        /// <summary>
        /// This Method Is Called When Partner Is Add Co-Partner And Also When Partner Is Change Percentage OF Partners
        /// Affected Entities
        /// Teams
        /// TeamPartnersMapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdatePartners(EditTeamModelViewModel model)
        {
            if (model != null && model.PartnerList != null && model.PartnerList.Any())
            {
                decimal totalPercentage = 0;
                foreach (var partner in model.PartnerList)
                {
                    totalPercentage += Convert.ToDecimal(partner.PartnerPercentage);
                }

                if (totalPercentage != 100)
                {
                    TempData["Type"] = Constant.Error;
                    TempData["Message"] = Constant.PartnerPercentageValidation;
                    return RedirectToAction("EditTeam", new { id = model.PartnerList.Select(x => x.TeamId).FirstOrDefault() });
                }
            }

            int teamId = await _teamAppService.UpdatePartnerForTeamAsync(model.PartnerList);
            if (teamId > 0)
            {
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.RecordsUpdatedMessage;
                return RedirectToAction("EditTeam", new { id = teamId });
            }
            else
            {
                TempData["Type"] = Constant.Error;
                TempData["Message"] = Constant.SomethingwentWrong;
                return RedirectToAction("EditTeam", new { id = model.Team.Id });
            }
        }

        /// <summary>
        /// This Method Is Called When In Edit_Team Section Click On Add-Partner And Add Co-Partners In Teams
        /// Affected Entities
        /// Teams
        /// TeamPartnerMapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AbpMvcAuthorize(PermissionNames.Pages_Teams_Edit)]
        public async Task<IActionResult> AddPartners(EditTeamModelViewModel model)
        {
            try
            {
                TeamPartnerMapDto input = new TeamPartnerMapDto
                {
                    PartnerIds = model.PartnerIds,
                    TeamId = model.TeamId
                };

                await _teamAppService.CreateTeamPartnerMappingAsync(input);
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.RecordsUpdatedMessage;
            }
            catch
            {
                TempData["Type"] = Constant.Error;
                TempData["Message"] = Constant.SomethingwentWrong;
            }

            return RedirectToAction("EditTeam", new { id = model.TeamId });
        }
        /// <summary>
        /// This Method Is Remove Partners From Team And Also Remove From TeamPartnerMap
        /// Affected Entities
        /// TeamPartnerMapping
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IActionResult> RemoveTeamPartnerMapping([FromBody]TeamPartnerEditDto model)
        {
            try
            {
                await _teamAppService.RemoveTeamPartnerMappingAsync(model.TeamId, model.PartnerId);
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.RecordsUpdatedMessage;
            }
            catch
            {
                TempData["Type"] = Constant.Error;
                TempData["Message"] = Constant.SomethingwentWrong;
            }

            return Json(true);
        }
        public async Task<IActionResult> GetExcelReport(DateTime? StartDate, DateTime? EndDate, int[] FilterPartnerIds)
        {
            List<int> FilterPartnerIdsList = FilterPartnerIds.ToList();
            int[] PartnerIdArray = FilterPartnerIdsList == null ? new int[0] : FilterPartnerIdsList.ToArray();

            var allTeams = (await _teamAppService.GetAllTeamList(StartDate, EndDate, PartnerIdArray)).Items;
            var fileGuid = Guid.NewGuid().ToString();
            var fileName = $"TeamListExcel_{fileGuid}.xlsx";
            var file = await _listExcelExporter.TeamListExportToFile(allTeams, fileName);
            var filePath = Path.Combine(file.FileToken);
            if (!System.IO.File.Exists(filePath))
            {
                throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
            }
            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            var stream = new MemoryStream(fileBytes);
            await BlobUtility.CopyToBlob(file.FileName, "comm", file.FileName, stream);
            System.IO.File.Delete(filePath);
            var result = new ExcelResult(stream, file.FileName);
            Thread.Sleep(1000);
            HttpContext.Response.Headers.Add("Set-Cookie", "fileDownload=true; path =/");
            HttpContext.Response.Headers.Add("Cache-Control", "no-cache, no-store, must-revalidate");
            return File(fileBytes, result.ContentType, result.FileName);
            //return File(fileBytes, file.FileType, file.FileName);
        }

    }
}