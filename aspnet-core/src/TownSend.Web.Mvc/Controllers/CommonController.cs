﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using TownSend.Authorization.Users;
using TownSend.BlobUtilitys;
using TownSend.Common.Dto;
using TownSend.Controllers;
using TownSend.Documents;
using TownSend.Documents.Dto;
using TownSend.Identity;
using TownSend.Partners.Dto;
using TownSend.Payments;
using TownSend.ProfileNotes;
using TownSend.ProfileNotes.Dto;
using TownSend.Web.Models;

namespace TownSend.Web.Controllers
{
    public class CommonController : TownSendControllerBase
    {
        private IProfileNoteAppService _profileNoteAppService;
        private UserManager _userManager;
        private IDocumentAppService _documentAppService;
        private readonly SignInManager _signInManager;
        private readonly IPaymentAppService _paymentAppService;
            
        public CommonController(
                                IProfileNoteAppService profileNoteAppService,
                                UserManager userManager,
                                IDocumentAppService documentAppService,
                                SignInManager signInManager,
                                IPaymentAppService paymentAppService
                                )
        {
            _profileNoteAppService = profileNoteAppService;
            _userManager = userManager;
            _documentAppService = documentAppService;
            _signInManager = signInManager;
            _paymentAppService = paymentAppService;
        }        

        
        public async Task<IActionResult> TestCreateCustomerAccount()
        {
            _paymentAppService.CreateCustomerInQuickbooks();
            return View();
        }

        public async Task<IActionResult> CreateProfileNote(CreateProfileNoteDto input)
        {
            
            await _profileNoteAppService.CreateProfileNote(input);
            if(input.Type==EntityType.Partners)
            {
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.noteAdded;
                TempData["partnertab"] = "Notes";
                return RedirectToAction("EditPartner", new RouteValueDictionary(
                new { controller = "Partners", action = "EditPartner", Id = input.MapId }));                
            }
            else if(input.Type==EntityType.Teams)
            {
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.noteAdded;
                TempData["teamtab"] = "Notes";
                return RedirectToAction("EditTeam", new RouteValueDictionary(
               new { controller = "Teams", action = "EditTeam", Id = input.MapId }));
            }
            else if(input.Type==EntityType.Members)
            {
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.noteAdded;
                TempData["membertab"] = "Notes";
                return RedirectToAction("EditMember", new RouteValueDictionary(new { controller = "Members", action = "EditMember", Id = input.MapId }));
            }
            
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> UpdateDocument(DocumentDto input)
        {
            if (input.Restrict == null)
            {
                input.VisibleDateFrom = null;
                input.VisibleDateTo = null;
            }
            else
            {
                IFormatProvider culture = new CultureInfo("en-US", true);
                input.VisibleDateFrom = DateTime.ParseExact(input.StringVisibleDateFrom, "MM-dd-yyyy", culture);
                input.VisibleDateTo = DateTime.ParseExact(input.StringVisibleDateTo, "MM-dd-yyyy", culture);
            }

            await _documentAppService.UpdateDocumentAsync(input);
            TempData["Type"] = Constant.Success;
            TempData["Message"] = Constant.documentUpdate;
            if (EntityType.Partners == input.Type)
            {
                TempData["partnertab"] = "Documents";
                return RedirectToAction("EditPartner", new RouteValueDictionary(new { controller = "Partners", action = "EditPartner", Id = input.MapId }));
            }
            else if (EntityType.Teams == input.Type)
            {
                TempData["teamtab"] = "Documents";
                return RedirectToAction("EditTeam", new RouteValueDictionary(new { controller = "Teams", action = "EditTeam", Id = input.MapId }));
            }
            else if(EntityType.Members==input.Type)
            {
                TempData["membertab"] = "Documents";
                return RedirectToAction("EditMember", new RouteValueDictionary(new { controller = "Members", action = "EditMember", Id = input.MapId }));
            }
        
            return View("Index", "Error");
        }
        [HttpPost]
        public async Task<IActionResult> DeleteDocument([FromBody]DocumentDeleteDto documentDeleteDto)
        {
            
            EntityType entityType = (EntityType)Enum.Parse(typeof(EntityType), documentDeleteDto.entityType);  // Animal.Dog
            await _documentAppService.DeleteDocumentAsync(documentDeleteDto.DocumentId,entityType);
            if (EntityType.Partners== entityType)
            {
                TempData["partnertab"] = "Documents";
            }
            if (EntityType.Teams == entityType)
            {
                TempData["teamtab"] = "Documents";
            }
            if(EntityType.Members==entityType)
            {
                TempData["membertab"] = "Documents";
            }
            TempData["Type"] = Constant.Success;
            TempData["Message"] = Constant.documentRemoved;
            return Json(true);
        }
        public async Task<IActionResult> DownLoadDocument(int id,EntityType type,int mapId)
        { 
            //CheckPermission();
            var data = await _documentAppService.GetDocumentForEditAsync(id);
            if (data == null)
                return View("~/Views/Shared/ErrorMessage.cshtml", new ErrorMessageModel() { Message = "Requested document did not found", AlertType = "warning" });



            string filePath = data.DocumentPath;
            string fileName = data.Name;



            var fileExist = BlobUtility.FileInBlob(filePath, "tlgdocuments", fileName);



            if (fileExist.Result)
            {
                var fileResult = BlobUtility.StreamFromBlob(filePath, "tlgdocuments", fileName);
                var file = fileResult.Result;
                file.Stream.Position = 0;
                Thread.Sleep(1000);
                HttpContext.Response.Headers.Add("Set-Cookie", "fileDownload=true; path =/");
                HttpContext.Response.Headers.Add("Cache-Control", "no-cache, no-store, must-revalidate");
                return File(file.Stream, file.ContentType, file.FileName);
            }



            return View("~/Views/Shared/ErrorMessage.cshtml", new ErrorMessageModel() { Message = "Requested document did not found", AlertType = "warning" });
        }
        //[HttpPost]
        public async Task<JsonResult> CheckEmail(string Email)
        {
            var users = await _userManager.FindByEmailAsync(Email);
           
            if (users == null)
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }

        }

    }
}