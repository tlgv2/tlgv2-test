﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TownSend.Common;
using TownSend.Controllers;
using TownSend.Members;
using TownSend.Members.Dto;
using TownSend.Identity;
using TownSend.Web.Models.Members;
using Abp.UI;
using TownSend.Documents;
using System.IO;
using System.Globalization;
using TownSend.BlobUtilitys;
using TownSend.Documents.Dto;
using Abp.Runtime.Validation;
using TownSend.Authorization.Users;
using Microsoft.AspNetCore.Routing;
using TownSend.Payments;

namespace TownSend.Web.Controllers
{    
    [DisableValidation]
    public class MembersController : TownSendControllerBase
    {
        private readonly IMemberAppService _memberAppService;
        private readonly ICommonAppService _commonAppService;
        private readonly IDocumentAppService _documentAppService;
        private readonly UserManager _userManager;
        private readonly SignInManager _signInManager;
        public MembersController(IMemberAppService memberAppService,
                                 ICommonAppService commonAppService,
                                 IDocumentAppService documentAppService,
                                 UserManager userManager)
        {
            _memberAppService = memberAppService;
            _commonAppService = commonAppService;
            _documentAppService = documentAppService;
            _userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            MemberCreateViewModel membercreatemodel = new MemberCreateViewModel();
            var teams = await _commonAppService.GetAllTeams();
            membercreatemodel.TeamList = teams;
            membercreatemodel.CreateMember.Pay_in_Full = true;

            var members = (await _memberAppService.GetAll()).Items;
            var model = new MemberListViewModel
            {
                Members = members,
                memberCreateViewModel = membercreatemodel

            };
            //TeamList For Members
            var teamList = (await _commonAppService.GetAllTeams()).Items;
            ViewBag.listOfTeams = teamList;

            return View(model);
        }
        /// <summary>
        /// This Method is Post Method Of Create Member This Method Take Value From Member And Fill Into Member Table
        /// Affected Table
        /// Member
        /// Invite
        /// User
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateMember(MemberListViewModel model)
        {
            try
            {
                var teams = await _commonAppService.GetAllTeams();
                model.memberCreateViewModel.TeamList = teams;

                if (!ModelState.IsValid)
                {
                    //return page                    
                    return View(model);
                }

                var result = await _memberAppService.CreateAsync(model.memberCreateViewModel.CreateMember);

                if (!result.IsSuccess)
                {
                    model.memberCreateViewModel.ErrorMessage = result.ErrorMessage;
                    return View("Index", model);
                }

                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.memberIvitationSent;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Member Data For Edit
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<IActionResult> EditMember(int Id)
        {
            try
            {

                var Member = await _memberAppService.GetMemberForEdit(new EntityDto(Id));
                EditMemberModalViewModel model = new EditMemberModalViewModel();
                model.MemberEdit = Member;

                // var model = ObjectMapper.Map<EditMemberModalViewModel>(Member);
                var stateList = await _commonAppService.GetAll();
                model.StateList = stateList;

                var teamList = await _commonAppService.GetAllTeams();
                model.TeamList = teamList;
                model.TeamMemberships =await _memberAppService.TeamMembershipsAsync(Id);
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// member data update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditMember(EditMemberModalViewModel model)
        {            
            try
            {
                var teams = await _commonAppService.GetAllTeams();
                model.TeamList = teams;

                if (!ModelState.IsValid)
                {
                    //return page                    
                    return View(model);
                }

                var result = await _memberAppService.UpdateAsync(model.MemberEdit);

                if (!result.IsSuccess)
                {
                    model.ErrorMessage = result.ErrorMessage;
                    return View(model);
                }

                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.updateMember;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> MemberRegisterStep1(int Id)
        {
            try
            {

                var Member = await _memberAppService.GetMemberForRegister(new EntityDto(Id));
                RegisterMemberViewModel model = new RegisterMemberViewModel();
                model.MemberRegister = Member;

                // var model = ObjectMapper.Map<EditMemberModalViewModel>(Member);
                var stateList = await _commonAppService.GetAll();
                model.StateList = stateList;

                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //await _signInManager.SignOutAsync();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MemberRegisterStep1(RegisterMemberViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    //return page                    
                    return View(model);
                }
                var member = await _memberAppService.GetMemberForEdit(new EntityDto(model.MemberRegister.Id));
                var user = await _userManager.GetUserByIdAsync(member.UserId);
                await _userManager.ChangePasswordAsync(user, model.MemberRegister.Password);
                var result = await _memberAppService.MemberRegisterAsync(model.MemberRegister);
                if (!result.IsSuccess)
                {
                    model.ErrorMessage = result.ErrorMessage;
                    //model.MemberRegister.Email =
                    return View(model);
                }
                if (member.Email != model.MemberRegister.Email)
                {



                }

                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.updateMember;
                return RedirectToAction("MemberRegisterStep2", new RouteValueDictionary(new { controller = "Members", action = "MemberRegisterStep2", Id = model.MemberRegister.Id }));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> MemberRegisterStep2(int Id)
        {
            try
            {

                var Member = await _memberAppService.GetMemberForRegister(new EntityDto(Id));
                RegisterMemberViewModel model = new RegisterMemberViewModel();
                model.MemberRegister = Member;

                // var model = ObjectMapper.Map<EditMemberModalViewModel>(Member);
                var stateList = await _commonAppService.GetAll();
                model.StateList = stateList;

                var teamList = (await _commonAppService.GetAllTeams()).Items;
                ViewBag.listOfTeams = teamList;
                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> MemberRegisterStep3(int Id)
        {
            try
            {

                var Member = await _memberAppService.GetMemberForRegister(new EntityDto(Id));
                RegisterMemberViewModel model = new RegisterMemberViewModel();
                model.MemberRegister = Member;

                // var model = ObjectMapper.Map<EditMemberModalViewModel>(Member);
                var stateList = await _commonAppService.GetAll();
                model.StateList = stateList;

                return View(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //await _signInManager.SignOutAsync();

        }
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> MemberRegisterStep4(int userID, int inviteID, int productID, string inviteToken, PaymentTerm? paymentTerm)
        {
            try
            {
                var result = ValidateAndInitialize(userID, inviteID, productID, inviteToken, paymentTerm);

                //var Member = await _memberAppService.GetMemberForRegister(new EntityDto(Id));
                //RegisterMemberViewModel model = new RegisterMemberViewModel();
                //model.MemberRegister = Member;

                //// var model = ObjectMapper.Map<EditMemberModalViewModel>(Member);
                //var stateList = await _commonAppService.GetAll();
                //model.StateList = stateList;

                return View(new RegisterMemberViewModel());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //await _signInManager.SignOutAsync();

        }

        private IActionResult ValidateAndInitialize(int userID, int inviteID, int productID, string inviteToken, PaymentTerm? paymentTerm)
        {
            //if (userID == 0 || inviteID == 0 || productID == 0 || string.IsNullOrEmpty(inviteToken) || !paymentTerm.HasValue)
            //{
            //    return this.RedirectError("Error!", "Invalid member invitation parameters.");
            //}

            //var tokenResult = _memberInviteProcess.ValidateMemberInvite(inviteID, productID, inviteToken, Data.HubOneStatus.COACH_Invite_PendingContract);
            //if (!tokenResult.IsValid)
            //    return this.RedirectWarning("Invalid Token!", tokenResult.ErrorMessage);

            //Contact = tokenResult.ExtraResults;

            //Pricing = _memberInviteProcess.GetPricing(productID, inviteID, paymentTerm.Value);

            //Product = _productProcess.GetProduct(productID);

            //ChangePaymentTermUrl = Url.Page("./Step3MemberInvitationPaymentOption", new { userID, inviteID, productID, inviteToken });

            return null;
        }

        public IActionResult MemberInvitationComplete()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> UploadFile()
        {
            int i;
            CreateDocumentDto input = new CreateDocumentDto();
            for (i = 0; i < Request.Form.Files.Count; i++)
            {

                string memberId = Request.Form["MapId"];
                string entityType = Request.Form["Type"];
                string VisibleDateFrom = Request.Form["VisibleDateFrom"];
                Boolean Restrict = Convert.ToBoolean(Request.Form["Restrict"]);
                string VisibleDateTo = Request.Form["VisibleDateTo"];
                input.Restrict = Restrict;
                var member = _memberAppService.GetAsync(Convert.ToInt32(memberId)).Result;

                string filePath = "";
                input.MapId = Convert.ToInt32(memberId);
                input.Name = Request.Form.Files[i].FileName;
                input.Size = Convert.ToInt32(Request.Form.Files[i].Length);
                input.Description = Request.Form.Files[i].FileName;
                input.Type = EntityType.Members;

                if (input.Restrict == null || input.Restrict == false)
                {
                    input.VisibleDateFrom = null;
                    input.VisibleDateTo = null;
                }
                else
                {
                    IFormatProvider culture = new CultureInfo("en-US", true);
                    input.VisibleDateFrom = DateTime.ParseExact(VisibleDateFrom, "MM-dd-yyyy", culture);
                    input.VisibleDateTo = DateTime.ParseExact(VisibleDateTo, "MM-dd-yyyy", culture);
                }
                if (input.Size > 0)
                {
                    using (var stream = Request.Form.Files[i].OpenReadStream())
                    {
                        MemoryStream memoryStream = new MemoryStream();
                        stream.CopyTo(memoryStream);
                        memoryStream.Position = 0;
                        //filePath = string.Concat(CoreEntityConfig.MajorEntity, "/", partner.FirstName, "/", DateTime.Now.ToString("yyyy-MM-dd"), "/", fileName);
                        filePath = string.Format("{0}/{1}/{2}/{3}", "Member", member.FirstName, DateTime.Now.ToString("yyyy-MM-dd"), input.Name);
                        input.DocumentPath = filePath;
                        var upload = await BlobUtility.CopyToBlob(filePath, "tlgdocuments", input.Name, memoryStream);
                        if (upload == true)
                        {
                            await _documentAppService.CreateDocument(input);
                        }
                    }
                }
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.documentAdded;
                TempData["membertab"] = "Documents";
                //return RedirectToAction("EditPartner", new RouteValueDictionary(new { controller = "Partners", action = "EditPartner", Id = input.PartnerId }));
            }
            return input.MapId.ToString();

        }
    }
}