﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TownSend.Common;
using TownSend.Controllers;
using TownSend.Partners;
using TownSend.Partners.Dto;
using TownSend.Web.Models;
using TownSend.Web.Models.Partners;
using TownSend.Web.Models.Roles;
using TownSend.Documents;
using System.IO;
using TownSend.BlobUtilitys;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;
using System.Threading;
using System.Web.Helpers;
using TownSend.Documents.Dto;
using Abp.UI;
using Abp.Web.Mvc.Models;
using Abp.Web.Models;
using System.Globalization;
using Abp.AspNetCore.Mvc.Authorization;
using TownSend.Authorization;
using TownSend;
using Microsoft.AspNetCore.Mvc.Rendering;
using Abp.Runtime.Validation;
using TownSend.Authorization.Users;
using TownSend.Web.Models.Common;

namespace TownSend.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Partners)]
    [DisableValidation]
    public class PartnersController : TownSendControllerBase
    {
        #region Fields
        private readonly IPartnerAppService _partnerAppService;
        private readonly ICommonAppService _commonAppService;
        private readonly IRepository<Document, long> _documentRepository;
        private readonly IDocumentAppService _documentAppService;
        private readonly IErrorInfoBuilder _errorInfoBuilder;
        public UserManager UserManager { get; set; }
        #endregion


        #region Ctor
        public PartnersController(IPartnerAppService partnerAppService, 
                                    ICommonAppService commonAppService,
                                    IRepository<Document, long> documentRepository,
                                    IErrorInfoBuilder errorInfoBuilder,
                                    IDocumentAppService documentAppService)
        {
            _partnerAppService = partnerAppService;
            _commonAppService = commonAppService;
            _documentRepository = documentRepository;
            _documentAppService = documentAppService;
            _errorInfoBuilder = errorInfoBuilder;
        }
        #endregion

        #region Partner
        /// <summary>
        /// This Method Display All Partner in View Index Page
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            var partners = (await _partnerAppService.GetAll()).Items;
            var model = new PartnerListViewModel
            {
                Partners = partners
            };

            // State List from CommonAppService
            var stateList = (await _commonAppService.GetAll()).Items;
            ViewBag.ListOfState = stateList;
            return View(model);
        }
        /// <summary>
        /// This Method Create Partner
        ///  Affected Entities
        ///     Partners
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ///
        [HttpPost]
        public async Task<IActionResult> CreatePartner(PartnerListViewModel input)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var partners = (await _partnerAppService.GetAll()).Items;
                    var model = new PartnerListViewModel
                    {
                        Partners = partners,
                    };
                    return View("Index",model);
                }
                          
                await _partnerAppService.CreateAsync(input.CreatePartner);
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.addPartner;
                return RedirectToAction("Index");

            }
            catch(Exception ex)
            {
                //var error = new ErrorViewModel1() {ErrorInfo=new ErrorInfo("Our apologies, something has gone wrong"), Exception=ex };
                //return View("~/Views/Shared/Error.cshtml",error);
                throw new UserFriendlyException(ex.Message);
            }          
       }
        /// <summary>
        /// This Method Update Partner Details
        ///  Affected Entities
        ///     Partners
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdatePartner(EditPartnerModalViewModel model)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    var input = ObjectMapper.Map<PartnerDto>(model.Partner);
                    await _partnerAppService.UpdateAsync(input);
                    //return RedirectToAction("EditPartner");
                    TempData["Type"] = Constant.Success;
                    TempData["Message"] = Constant.updatePartner;
                    return RedirectToAction("EditPartner", new RouteValueDictionary(
                        new { controller = "Partners", action = "EditPartner", Id = input.Id }));
                }                
                return View();
            }
             
            catch(Exception Ex)
            {
                var error = new ErrorViewModel1() { ErrorInfo = new ErrorInfo("Our apologies, something has gone wrong"), Exception = Ex };
                return View("~/Views/Shared/Error.cshtml", error);
            }
           
        }

        /// <summary>
        /// This Method Display Partner Details Page For View Partner Info And Edit Partner Info 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public async Task<IActionResult> EditPartner(int Id)
        {
            var partner = await _partnerAppService.GetPartnerForEdit(new EntityDto(Id));
            if (partner == null)
            {
                var errormessage = new ErrorMessageModel() { Message = "Partner Is Not Found", AlertType = "warning" };
                return View("~/Views/Shared/ErrorMessage.cshtml", errormessage);
            }
            var model = ObjectMapper.Map<EditPartnerModalViewModel>(partner);
            if (model.Partner.ProfilePathGet != null && model.Partner.ProfilePathGet != "")
            {
                var result = await BlobUtility.GetProfileFromBlob(partner.Partner.ProfilePathGet, "profile");
                model.Partner.ProfilePathSet = result.Uri.OriginalString;
            }

            var links = await _partnerAppService.GetRecommendedLinksByPartnerId(new EntityDto(Id));            
            model.GetGetRecommendedLinks = links;
            // State List from CommonAppService
                var stateList = (await _commonAppService.GetAll()).Items;
            ViewBag.SelectedListOfState = stateList;

            return View(model);
        }
        /// <summary>
        /// This Method is UploadProfile Image in Partner Profile
        /// </summary>
        /// <param name="ProfileImage"></param>
        /// <param name="PartnerId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UploadProfile(IFormFile ProfileImage, int PartnerId)
        {
            var partner = await _partnerAppService.GetAsync(PartnerId);

            string filePath = "";
            string fileName = ProfileImage.FileName;
            long fileLength = ProfileImage.Length;
            if (fileLength > 0)
            {
                using (var stream = ProfileImage.OpenReadStream())
                {
                    MemoryStream memoryStream = new MemoryStream();
                    stream.CopyTo(memoryStream);
                    memoryStream.Position = 0;
                    filePath = string.Format("{0}/{1}/{2}/{3}", "Partner", partner.FirstName, DateTime.Now.ToString("yyyy-MM-dd"), fileName);

                    var upload = await BlobUtility.UploadProfile(filePath, "profile", fileName, memoryStream);
                    if (upload == true)
                    {
                        partner.ProfilePath = filePath;
                        await _partnerAppService.UpdateAsync(partner);
                    }
                }
            }

            return RedirectToAction("EditPartner", new RouteValueDictionary(new { controller = "Partners", action = "EditPartner", Id = PartnerId }));
        }
        /// <summary>
        /// This Method Display Message After Partner Successfully Confirm His Email
        /// </summary>
        /// <returns></returns>
        public IActionResult PartnerInvitationComplete()
        {
            return View();
        }
        /// <summary>
        /// This Method Delete Partner From PartnerListing
        /// </summary>
        /// <param name="partnerId"></param>
        /// <returns></returns>
        public IActionResult DeletePartner([FromBody]int partnerId)
        {
            _partnerAppService.Delete(new EntityDto(partnerId));
            TempData["Type"] = Constant.Success;
            TempData["Message"] = Constant.deletePartner;
            return Json(true);
        }

        #endregion

        #region Recommended Link
        /// <summary>
        /// This Method Create RecommendedLinks For Partner
        ///  Affected Entities
        ///    Partners
        ///    PartnerRecommendedLink
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateRecommendedLink(CreatePartnerRecommenedLinksDto input)
        {
            await _partnerAppService.CreateLinks(input);
            TempData["Type"] = Constant.Success;
            TempData["Message"] = Constant.linkAdded;
            return RedirectToAction("EditPartner", new RouteValueDictionary(
                new { controller = "Partners", action = "EditPartner", Id = input.PartnerId }));
        }
        /// <summary>
        ///  This Method Remove RecommenendLinks From Partner
        ///  Affected Entities
        ///    Partners
        ///    PartnerRecommendedLink
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="PartnerId"></param>
        /// <returns></returns>
        public async Task<IActionResult> DeleteLink(int Id, int PartnerId)
        {
            await _partnerAppService.DeleteLinks(new EntityDto(Id));
            TempData["Type"] = Constant.Success;
            TempData["Message"] = Constant.linkRemoved;
            return RedirectToAction("EditPartner", new RouteValueDictionary(
                new { controller = "Partners", action = "EditPartner", Id = PartnerId }));
        }

        #endregion

        #region Documents
        /// <summary>
        ///This Method Used For Upload All Type Files in Document With Visible Date 
        ///Affected Entities
        ///Doucment
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<string> UploadFile()
        {
            int i;
            CreateDocumentDto input = new CreateDocumentDto();
            for (i = 0; i < Request.Form.Files.Count; i++)
            {

                string PartnerId = Request.Form["MapId"];
                string et = Request.Form["Type"];
                string VisibleDateFrom = Request.Form["VisibleDateFrom"];
                Boolean Restrict = Convert.ToBoolean(Request.Form["Restrict"]);
                string VisibleDateTo = Request.Form["VisibleDateTo"];
                input.Restrict = Restrict;
                Task<PartnerDto> partnerDto = _partnerAppService.GetAsync(Convert.ToInt32(PartnerId));

                PartnerDto partner = partnerDto.Result;
                string filePath = "";
                input.MapId = Convert.ToInt32(PartnerId);
                input.Name = Request.Form.Files[i].FileName;
                input.Size = Convert.ToInt32(Request.Form.Files[i].Length);
                input.Description = Request.Form.Files[i].FileName;
                input.Type = EntityType.Partners;

                if (input.Restrict == null || input.Restrict == false)
                {
                    input.VisibleDateFrom = null;
                    input.VisibleDateTo = null;
                }
                else
                {
                    IFormatProvider culture = new CultureInfo("en-US", true);                  
                    input.VisibleDateFrom = DateTime.ParseExact(VisibleDateFrom, "MM-dd-yyyy", culture);
                    input.VisibleDateTo = DateTime.ParseExact(VisibleDateTo, "MM-dd-yyyy", culture);
                }
                if (input.Size > 0)
                {
                    using (var stream = Request.Form.Files[i].OpenReadStream())
                    {
                        MemoryStream memoryStream = new MemoryStream();
                        stream.CopyTo(memoryStream);
                        memoryStream.Position = 0;
                        filePath = string.Format("{0}/{1}/{2}/{3}", "Partner", partner.Id, DateTime.Now.ToString("yyyy-MM-dd"), input.Name);
                        input.DocumentPath = filePath;
                        var upload = await BlobUtility.CopyToBlob(filePath, "tlgdocuments", input.Name, memoryStream);
                        if (upload == true)
                        {
                            await _documentAppService.CreateDocument(input);
                        }
                    }
                }
                TempData["Type"] = Constant.Success;
                TempData["Message"] = Constant.documentAdded;
                TempData["partnertab"] = "Documents";
                //return RedirectToAction("EditPartner", new RouteValueDictionary(new { controller = "Partners", action = "EditPartner", Id = input.PartnerId }));
            }
            return input.MapId.ToString();
        }
        /// <summary>
        /// This Method Display Page For Create Document And Take Input For That Doucment
        /// Affected Entities
        /// Doucment
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateDocument(CreateDocumentDto input)
        {
            var partnerId = Convert.ToInt32(input.MapId);
            var partner = await _partnerAppService.GetAsync(partnerId);
            string filePath = "";
            input.Name = input.DocumentFile.FileName;
            input.Size = Convert.ToInt32(input.DocumentFile.Length);
            input.Description = input.DocumentFile.FileName;
            
            
            if(input.Restrict == null)
            {
                input.VisibleDateFrom= null;
                input.VisibleDateTo = null;
            }
            if (input.Size > 0)
            {
                using (var stream = input.DocumentFile.OpenReadStream())
                {
                    MemoryStream memoryStream = new MemoryStream();
                    stream.CopyTo(memoryStream);
                    memoryStream.Position = 0;
                    //filePath = string.Concat(CoreEntityConfig.MajorEntity, "/", partner.FirstName, "/", DateTime.Now.ToString("yyyy-MM-dd"), "/", fileName);
                    filePath = string.Format("{0}/{1}/{2}/{3}", "Partner", partner.Id, DateTime.Now.ToString("yyyy-MM-dd"), input.Name);
                    input.DocumentPath = filePath;
                    var upload = await BlobUtility.CopyToBlob(filePath, "tlgdocuments", input.Name, memoryStream);
                    if (upload == true)
                    {
                        await _documentAppService.CreateDocument(input);                                       
                    }
                }
            }
            TempData["Type"] = Constant.Success;
            TempData["Message"] = Constant.documentAdded;
            
            return RedirectToAction("EditPartner", new RouteValueDictionary(new { controller = "Partners", action = "EditPartner", Id = partnerId }));
        }
        /// <summary>
        /// This Method Is Used For Download Document From DocumentListing Page
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> DownLoadDocument(int id)
        {
            //CheckPermission();
            var data = await _documentRepository.GetAsync(id);
            if(data == null)
                return View("~/Views/Shared/ErrorMessage.cshtml", new ErrorMessageModel() { Message = "Requested document did not found", AlertType = "warning" });

            string filePath = data.DocumentPath;
            string fileName = data.Name;

            var fileExist = BlobUtility.FileInBlob(filePath, "tlgdocuments", fileName);

            if (fileExist.Result)
            {
                var fileResult = BlobUtility.StreamFromBlob(filePath, "tlgdocuments", fileName);
                var file = fileResult.Result;
                file.Stream.Position = 0;
                //return StreamToJQueryFileDownloadResult(file);
                Thread.Sleep(1000);
                HttpContext.Response.Headers.Add("Set-Cookie", "fileDownload=true; path =/");
                HttpContext.Response.Headers.Add("Cache-Control", "no-cache, no-store, must-revalidate");
                return File(file.Stream, file.ContentType, file.FileName);
            }

            return View("~/Views/Shared/ErrorMessage.cshtml", new ErrorMessageModel() { Message = "Requested document did not found", AlertType = "warning" });
        }
        #endregion

        #region CheckMail
        /// <summary>
        /// This Method Check Email Address Of Partners
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult> CheckEmail([FromBody]string Email)
        {
            var partners = await _partnerAppService.GetAll();
            var checkEmail = (from partner in partners.Items
                             where partner.Email == Email
                             select partner).FirstOrDefault();
            var checkuser = await UserManager.FindByEmailAsync(Email);
            if(checkEmail==null && checkuser==null)
            {
                return Json(true);
            }
            else
            {
                return Json(false);
            }
            
        }
        #endregion
        
    }
}