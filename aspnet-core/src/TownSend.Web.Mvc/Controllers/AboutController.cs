﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using TownSend.Controllers;

namespace TownSend.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : TownSendControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
