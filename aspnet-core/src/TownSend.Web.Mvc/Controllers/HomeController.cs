﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using TownSend.Controllers;
using Microsoft.AspNetCore.Authorization;
using TownSend.Web.Models;

namespace TownSend.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : TownSendControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult AccessDenied()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ErrorMessage(string alertType, string message, string heading, string icon)
        {
            ErrorMessageModel model = new ErrorMessageModel
            {
                AlertType = alertType,
                Message = message,
                Heading = heading,
                Icon = icon
            };

            return View(model);
        }

    }
}
