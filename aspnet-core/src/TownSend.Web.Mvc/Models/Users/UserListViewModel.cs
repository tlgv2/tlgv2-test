using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using TownSend.Roles.Dto;
using TownSend.Users.Dto;

namespace TownSend.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
        public List<string> RoleName { get; set; }
        public SelectList StatusList { get; set; }
        public int[] UserType { get; set; }
        public int[] Status { get; set; }
        public UserStatusListDto[] StatusIds { get; set; }
        //public List<RoleListForUser> FilterRoleList { get; set; }

        //public List<int> FilterRoleIds { get; set; }
    }
}
