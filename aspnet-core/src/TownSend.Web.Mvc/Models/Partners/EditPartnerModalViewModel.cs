﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.Collections.Generic;
using TownSend.Partners;
using TownSend.Partners.Dto;
using TownSend.Roles.Dto;
using TownSend.Web.Models.Common;

namespace TownSend.Web.Models.Partners
{
    [AutoMapFrom(typeof(GetPartnerForEditOutput))]
    public class EditPartnerModalViewModel : GetPartnerForEditOutput
    {
        public GetRecommendedLinksOutput GetGetRecommendedLinks { get; set; }
        public ProfileNoteViewModel ProfileNoteViewModel { get; set; }
    }

    public class ProfileNoteViewModel
    {
        public ProfileNoteViewModel()
        {
            ProfileNotes = new List<ProfileNoteDTO>();
        }

        public List<ProfileNoteDTO> ProfileNotes { get; set; }
        
    }
}
