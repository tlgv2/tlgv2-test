﻿using System.Collections.Generic;
using TownSend.Partners.Dto;

namespace TownSend.Web.Models.Partners
{
    public class PartnerListViewModel
    {
        public IReadOnlyList<PartnerListDto> Partners { get; set; }
       
        public CreatePartnerDto CreatePartner { get; set; }
    }
}
