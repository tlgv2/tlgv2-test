﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TownSend.Web.Models.Partners
{
    public class CreateDocumentModel
    {
        public int PatnerId { get; set; }
        public string Path { get; set; }

        public IFormFile Document { get; set; }
    }
}
