﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Members.Dto;
using TownSend.Common.Dto;
using Abp.Application.Services.Dto;

namespace TownSend.Web.Models.Members
{
    public class EditMemberModalViewModel
    {
        public EditMemberModalViewModel()
        {
            ErrorMessage = new List<string>();
            MemberEdit = new MemberEditDto();
        }
        public MemberEditDto MemberEdit { get; set; }
        public ListResultDto<TeamsListDto> TeamList { get; set; }

        public ListResultDto<StateListDto> StateList { get; set; }
        public List<string> ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }

        public List<TeamMembershipDto> TeamMemberships { get; set; }
    }
}
