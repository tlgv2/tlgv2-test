﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Members.Dto;
using TownSend.Common.Dto;
using Abp.Application.Services.Dto;

namespace TownSend.Web.Models.Members
{
    public class RegisterMemberViewModel
    {
        public RegisterMemberViewModel()
        {
            ErrorMessage = new List<string>();
            MemberRegister = new MemberRegisterDto();
        }
        public InvitationPaymentDto InvitationPaymentDto { get; set; }
        public MemberRegisterDto MemberRegister { get; set; }
        public ListResultDto<StateListDto> StateList { get; set; }
        public List<string> ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }
    }
}
