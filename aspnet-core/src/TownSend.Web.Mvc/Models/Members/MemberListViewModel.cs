﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Members.Dto;

namespace TownSend.Web.Models.Members
{
    public class MemberListViewModel
    {
        public IReadOnlyList<MemberListDto> Members { get; set; }
        public MemberCreateViewModel memberCreateViewModel { get; set; }
    }
}
