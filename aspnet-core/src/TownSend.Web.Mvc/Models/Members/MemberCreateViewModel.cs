﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Members.Dto;
using TownSend.Common.Dto;

namespace TownSend.Web.Models.Members
{
    public class MemberCreateViewModel
    {
        public MemberCreateViewModel()
        {
            ErrorMessage = new List<string>();
            CreateMember = new CreateMemberDto();
        }
        public CreateMemberDto CreateMember { get; set; }
        public ListResultDto<TeamsListDto> TeamList { get; set; }
        public List<string> ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }
    }
}
