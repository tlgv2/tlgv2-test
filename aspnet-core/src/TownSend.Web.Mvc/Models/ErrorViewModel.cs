﻿using Abp.Web.Mvc.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TownSend.Web.Models
{
    public class ErrorViewModel1 : ErrorViewModel
    {
        [BindProperty]
        public string AlertType { get; set; } = "warning";

        [BindProperty]
        public string Heading { get; set; } = "Warning!";

        [BindProperty]
        public string Icon { get; set; } = "fa-warning";
    }
}
