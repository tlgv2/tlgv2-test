﻿using System.Collections.Generic;
using TownSend.Roles.Dto;

namespace TownSend.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}