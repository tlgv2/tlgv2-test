﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using TownSend.Partners.Dto;
using TownSend.Teams;
using TownSend.Teams.Dto;

namespace TownSend.Web.Models.Teams
{
    public class TeamListViewModel
    {
        public IReadOnlyList<TeamListDto> Teams { get; set; }

        public List<PartnersListForTeam> Partners { get; set; }
        public ListResultDto<StateListDto> StateList { get; set; }
        public List<int> PartnerIds { get; set; }

        public int DefaultPartnerId { get; set; }
        public string DefaultPartnerFullName { get; set; }

        [Range(1.0, double.MaxValue, ErrorMessage = "Team Price must be greater than 0")]
        public decimal TeamPrice { get; set; }
        [Required]
        [Range(1.0, double.MaxValue,ErrorMessage ="Deposit must be greater than 0")]
        public decimal Deposit { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public string TeamName { get; set; }        
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public CreateTeamDto CreateTeam { get; set; }

        public List<PartnersListForTeam> FilterPartnerList { get; set; }

        public List<int> FilterPartnerIds { get; set; }
    }
}
