﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Common.Dto;
using TownSend.Teams.Dto;

namespace TownSend.Web.Models.Teams
{
    [AutoMapFrom(typeof(GetTeamForEditOutput))]
    public class EditTeamModelViewModel : GetTeamForEditOutput
    {
        public EditTeamModelViewModel()
        {
            PartnerIds = new List<int>();
        }
        public int DefaultPartnerId { get; set; }
        [Range(1.0, double.MaxValue, ErrorMessage = "Deposit must be greater than 0")]
        public decimal? TeamPrice { get; set; }
        [Range(1.0, double.MaxValue, ErrorMessage = "Deposit must be greater than 0")]
        public decimal? Deposit { get; set; }

        public List<TeamPartnerEditDto> PartnerList { get; set; }
        public ListResultDto<StateListDto> StateList { get; set; }
        public List<int> PartnerIds { get; set; }
        public int TeamId { get; set; }
    }
}
