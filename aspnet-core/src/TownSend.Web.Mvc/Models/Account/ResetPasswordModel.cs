﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Authorization.Accounts.Dto;
using TownSend.Users.Dto;

namespace TownSend.Web.Models.Account
{
    public class ResetPasswordModel 
    {
        public EmailConfirmationViewModel EmailConfirmation { get; set; }

        public string NewPassword { get; set; }
    }
}
