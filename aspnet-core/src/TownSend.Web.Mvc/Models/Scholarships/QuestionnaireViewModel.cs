﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Scholarships.Dto;

namespace TownSend.Web.Models.Scholarships
{
    public class QuestionnaireViewModel
    {
        public QuestionnaireViewModel()
        {
            Questionnaire = new QuestionnaireDto();
        }
        public QuestionnaireDto Questionnaire { get; set; }
        public SelectList YesNoList { get; set; }

    }
}
