﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Scholarships;
using TownSend.Scholarships.Dto;
using TownSend.Teams.Dto;

namespace TownSend.Web.Models.Scholarships
{
    public class ScholarshipListViewModel
    {
        public ScholarshipListViewModel()
        {
            Scholarships = new List<ScholarshipListDto>();
        }

        public ListResultDto<TeamListDto> TeamList { get; set; }
        public ListResultDto<ScholarshipStatusListDto> ScholarshipStatusList { get; set; }
        public SelectList StatusList { get; set; }

        public List<int> TeamIds { get; set; }

        public List<ScholarshipStatusEnum> StatusIds { get; set; }

        public List<ScholarshipListDto> Scholarships { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
