﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using TownSend.Scholarships;
using TownSend.Scholarships.Dto;
using TownSend.Teams.Dto;

namespace TownSend.Web.Models.Scholarships
{
    public class ScholarshipSignupViewModel
    {
        public ScholarshipSignupViewModel()
        {
            ErrorMessage = new List<string>();
            ScholarshipRegister = new ScholarshipRegisterDto();
        }
        public ScholarshipRegisterDto ScholarshipRegister { get; set; }
        public ListResultDto<TeamListDto> TeamList { get; set; }
        public List<string> ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }
    }
}
