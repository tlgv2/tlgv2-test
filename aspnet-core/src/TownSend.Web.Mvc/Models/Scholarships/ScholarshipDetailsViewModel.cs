﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TownSend.Scholarships.Dto;
using TownSend.Teams.Dto;

namespace TownSend.Web.Models.Scholarships
{
    public class ScholarshipDetailsViewModel
    {
        public ScholarshipDetailsViewModel()
        {
            ScholarshipDetails = new ScholarshipOutputDto();
            ErrorMessage = new List<string>();
            TeamList = new ListResultDto<TeamListDto>();
        }

        public ScholarshipOutputDto ScholarshipDetails { get; set; }
        public SelectList YesNoList { get; set; }
        public ListResultDto<TeamListDto> TeamList { get; set; }
        public List<string> ErrorMessage { get; set; }
        public bool IsSuccess { get; set; }

        
    }
}
