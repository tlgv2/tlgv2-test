﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TownSend.Web.Models
{
    public class ErrorMessageModel
    {
        public string AlertType { get; set; } 

        public string Heading { get; set; } 

        public string Icon { get; set; } 

        public string Message { get; set; }
    }
}
