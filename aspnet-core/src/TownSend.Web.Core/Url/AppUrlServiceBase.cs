﻿using Abp.Dependency;
using Abp.Extensions;
using Abp.MultiTenancy;
using System;
using System.Collections.Generic;
using System.Text;

namespace TownSend.Url
{
    public abstract class AppUrlServiceBase : IAppUrlService, ITransientDependency
    {
        public abstract string EmailActivationRoute { get; }

        public abstract string PasswordResetRoute { get; }

        public abstract string ScholarshipInviteEmailRoute { get; }

        public abstract string ScholarshipApprovalEmailRoute { get; }

        public abstract string EmailCallBackRoute { get; }

        public abstract string MemberInviteEmailRoute { get; }
        public abstract string ChangeEmailRoute { get; }

        public abstract string ForgotPasswordRoute { get; }

        protected readonly IWebUrlService WebUrlService;
        protected readonly ITenantCache TenantCache;

        protected AppUrlServiceBase(
            IWebUrlService webUrlService, 
            ITenantCache tenantCache)
        {
            WebUrlService = webUrlService;
            TenantCache = tenantCache;
        }

        #region Methods takes tenantID
        public string CreateEmailActivationUrlFormat(int? tenantId)
        {
            return CreateEmailActivationUrlFormat(GetTenancyName(tenantId));
        }

        public string CreatePasswordResetUrlFormat(int? tenantId)
        {
            return CreatePasswordResetUrlFormat(GetTenancyName(tenantId));
        }

        public string CreateScholarshipInviteEmailUrlFormat(int? tenantId)
        {
            return CreateScholarshipInviteEmailUrlFormat(GetTenancyName(tenantId));
        }

        public string CreateScholarshipApprovalEmailUrlFormat(int? tenantId)
        {
            return CreateScholarshipApprovalEmailUrlFormat(GetTenancyName(tenantId));
        }

        public string CreateMemberInviteEmailUrlFormat(int? tenantId)
        {
            return CreateMemberInviteEmailUrlFormat(GetTenancyName(tenantId));
        }

        public string ChangeEmailUrlFormat(int? tenantId)
        {
            return ChangeEmailUrlFormat(GetTenancyName(tenantId));
        }

        public string EmailCallBackURL(string returnUrl)
        {
            return WebUrlService.GetSiteRootAddress().EnsureEndsWith('/') + returnUrl;
        }

        public string ResetPasswordUrlFormat(int? tenantId)
        {
            return ResetPasswordUrlFormat(GetTenancyName(tenantId));
        }

        #endregion


        #region Methods take tenantName






        public string CreateEmailActivationUrlFormat(string tenancyName)
        {

            //var activationLink = WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + EmailCallBackRoute + "?returnUrl=" + EmailActivationRoute + "?userId={userId}&inviteId={inviteId}&confirmationCode={confirmationCode}";
            var activationLink = WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + EmailCallBackRoute + "?returnUrl=" + EmailActivationRoute + "?userId={userId}%26inviteId={inviteId}%26confirmationCode={confirmationCode}"; //%26 for &
            //var activationLink = "http://localhost:62114/" + EmailActivationRoute + "?userId={userId}&confirmationCode={confirmationCode}";

            //if (tenancyName != null)
            //{
            //    activationLink = activationLink + "&tenantId={tenantId}";
            //}

            return activationLink;
        }

        public string CreatePasswordResetUrlFormat(string tenancyName)
        {
            var resetLink = WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + PasswordResetRoute + "?userId={userId}&resetCode={resetCode}";
            //var resetLink = "http://localhost:62114/" + PasswordResetRoute + "?userId={userId}&resetCode={resetCode}";

            if (tenancyName != null)
            {
                resetLink = resetLink + "&tenantId={tenantId}";
            }

            return resetLink;
        }

        public string CreateScholarshipInviteEmailUrlFormat(string tenancyName)
        {

            return WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + EmailCallBackRoute + "?returnUrl=" + ScholarshipInviteEmailRoute + "?scholarshipId={scholarshipId}";
        }

        public string CreateScholarshipApprovalEmailUrlFormat(string tenancyName)
        {

            return WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + ScholarshipApprovalEmailRoute + "?scholarshipId={scholarshipId}";
        }

        public string CreateMemberInviteEmailUrlFormat(string tenancyName)
        {

            return WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + MemberInviteEmailRoute + "?memberId={memberId}";
        }

        public string ChangeEmailUrlFormat(string tenancyName)
        {
            //var activationLink = WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + EmailActivationRoute + "?userId={userId}&inviteId={inviteId}&confirmationCode={confirmationCode}";
            var changeEmail = WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + ChangeEmailRoute + "?userId={userId}&inviteId={inviteId}&confirmationCode={confirmationCode}";
            return changeEmail;
        }

        public string ResetPasswordUrlFormat(string tenancyName)
        {
            var forgotpasswordlink = WebUrlService.GetSiteRootAddress(tenancyName).EnsureEndsWith('/') + ForgotPasswordRoute + "?userId={userId}&inviteId={inviteId}&resetCode={resetCode}";
            return forgotpasswordlink;
        }

        #endregion



        #region Utility


        private string GetTenancyName(int? tenantId)
        {
            return tenantId.HasValue ? TenantCache.Get(tenantId.Value).TenancyName : null;
        }
        #endregion
    }
}
