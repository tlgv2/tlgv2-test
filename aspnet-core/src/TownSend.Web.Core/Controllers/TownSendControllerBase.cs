using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace TownSend.Controllers
{
    public abstract class TownSendControllerBase: AbpController
    {
        protected TownSendControllerBase()
        {
            LocalizationSourceName = TownSendConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
