﻿using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TownSend.Payments.Dto;
using TownSend.Payments.Quickbooks;

namespace TownSend.Payments
{
    public class TownsendPaymentProcesses
    {
        #region  Fields
        private readonly IPaymentAppService _paymentAppService;
        #endregion

        #region Ctor

        public TownsendPaymentProcesses(IPaymentAppService paymentAppService)
        {
            _paymentAppService = paymentAppService;
        }

        #endregion

        #region Methods

        #region Customer related Methods
        /// <summary>
        /// This is not a generic process.
        /// The purpose of this method is to create a customer in the 
        /// QB API and apply business and API logic to the HubOneCustomer
        /// onject to make sure it's "clean" for the push to the API.
        /// There are several disqualifying factors for the customer
        /// in terms of info, they're outlined in the validating summaries
        /// in this method.
        /// </summary>
        /// <returns></returns>
        public HubOnePaymentModuleResponse CreateCustomerInQuickbooks(HubOneCustomer hubOneCustomer)
        {
            //validate business rules
            var tlgValErrors = TLGInfoValidation(hubOneCustomer);

            //check if the customer exists
            var notADupe = CheckIfCustomerEmailAlreadyInSystem(hubOneCustomer);

            //bring both dictionaries together
            var allErrors = notADupe.ResponseValues.Union(tlgValErrors).ToDictionary(d => d.Key, d => d.Value);

            //if the customer doesn't exist and the rules are followed
            if (allErrors.Count == 0)
            {

                //then create the customer
                var result = CallAPIToCreateCustomer(hubOneCustomer);

                //Bind API and Value data together into a single list
                var resultInfo = result.GetAPIDataAndResponses();

                //QBCustID
                resultInfo.Add("HubOneCustomerID", hubOneCustomer.HubOneCustomerID);

                resultInfo.Add("APIName", "Quickbooks");
                resultInfo.Add("APICallDesc", "CreateCustomer");
                resultInfo.Add("APIEndpoint", "test");

                //Get id for status change
                LogAPI(resultInfo);

                //when creation is complete, bind the HubOneCustomerID to the QuickBooks CustomerId
                BindQuickbooksIDToHubOneCustomerID(resultInfo, HubOneAPIName.QuickbooksAccounting);

                //return a response of success
                return _paymentAppService.BuildResponse(resultInfo, result.ResponseMessage, true);

            }
            else
            {
                return _paymentAppService.BuildResponse(allErrors, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);//or resturn a fail response
            }

        }

        public HubOnePaymentModuleResponse TryCreateCustomerOrRelinkCustomerToQuickBooksID(HubOneCustomer hubOneCustomer)
        {
            //var paymentModule = GetPaymentModule();

            //validate business rules
            var tlgValErrors = TLGInfoValidation(hubOneCustomer);

            if (tlgValErrors.Count == 0)
            {
                var result = _paymentAppService.CheckCustomerIsUnique(hubOneCustomer);

                if (result.ResponseValues.Count == 0)
                {
                    Dictionary<string, string> errDict = new Dictionary<string, string>();
                    errDict.Add("", "Error connecting to Quickbooks API. Please try again later.");
                    return _paymentAppService.BuildResponse(errDict, "Error connecting to Quickbooks API.Please try again later.", false);
                }

                //does customer exists?
                if (!string.IsNullOrEmpty(result.ResponseValues["QueryResponse"].Replace("{}", "")))
                {
                    var response = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result.ResponseValues["QueryResponse"]);
                    if (response.Customer.HasValues)
                    {
                        var qbCustID = response.Customer.First.Id;

                        //create param to re-bind.
                        var transObj = new Dictionary<string, string>();
                        transObj.Add("HubOneCustomerID", hubOneCustomer.HubOneCustomerID);
                        transObj.Add("QBCustID", qbCustID.ToString());

                        //add missing bind the HubOneCustomerID to the QuickBooks CustomerId
                        BindQuickbooksIDToHubOneCustomerID(transObj, HubOneAPIName.QuickbooksAccounting);

                        //Bind API and Value data together into a single list
                        var resultInfo = result.GetAPIDataAndResponses();

                        //QBCustID
                        resultInfo.Add("HubOneCustomerID", hubOneCustomer.HubOneCustomerID);

                        resultInfo.Add("APIName", "Quickbooks");
                        resultInfo.Add("APICallDesc", "TryCreateCustomerOrRelinkCustomerToQuickBooksID");
                        resultInfo.Add("APIEndpoint", "test");

                        //Get id for status change
                        LogAPI(resultInfo);

                        //return a response of success
                        return _paymentAppService.BuildResponse(resultInfo, result.ResponseMessage, true);

                    }
                }
                else //doesn't exists.
                {
                    return CreateCustomerInQuickbooks(hubOneCustomer);
                }
            }

            return _paymentAppService.BuildResponse(tlgValErrors, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);
        }

        /// <summary>
        /// This method returns the ID of a customer in our system from an external system
        /// <para>HubOneCustomerID is needed</para>
        /// <para>Name of target API is needed</para>
        /// </summary>
        /// <param name="hubCustApiInfo"></param>
        /// <returns></returns>
        private string GetCustomerAPIID(HubOneCustomerAPIInformation hubCustApiInfo)
        {
            //TODO: DB CALL TO GET customer api id
            //var result = Get<HubOneCustomerAPIInformation, HubOneApiIdentifier>("[EXTD_Coach].[spAPIIdentiferForCustomer_Read]", hubCustApiInfo);
            var result = new HubOneApiIdentifier();

            if (result != null)
                return result.APIIdentifier;
            else
                return null;
        }

        /// <summary>
        /// This isn't just an api call.
        /// This method also contains sub-logic verification to confirm the results
        /// </summary>
        /// <param name="hubOneCustomer"></param>
        /// <returns></returns>
        public HubOnePaymentModuleResponse CheckIfCustomerEmailAlreadyInSystem(HubOneCustomer hubOneCustomer)
        {
            //var paymentModule = GetPaymentModule();

            //returns result after call of api
            var result = _paymentAppService.CheckCustomerIsUnique(hubOneCustomer);

            //sublogic for dupe check
            if (string.IsNullOrEmpty(result.ResponseValues["QueryResponse"].Replace("{}", "")))
                return new HubOnePaymentModuleResponse()
                {
                    IsSuccessful = true,
                    ResponseMessage = "Your Customer is not a duplicate.",
                    ResponseValues = new Dictionary<string, string>()
                };

            return result;
        }

        /// <summary>Checks if customer is already bound to quick books identifier.</summary>
        /// <param name="customerID">The customer identifier.</param>
        /// <returns>true when bound</returns>
        public bool CheckIfCustomerIsAlreadyBoundToQuickBooksID(int customerID)
        {
            var cusApiInfo = new HubOneCustomerAPIInformation()
            {
                APIName = HubOneAPIName.QuickbooksAccounting,
                CustomerID = customerID
            };

            var apiIdForHubCust = GetCustomerAPIID(cusApiInfo);

            return !string.IsNullOrEmpty(apiIdForHubCust);
        }


        #endregion

        #region Credit Cards related Methods

        /// <summary>
        /// Adds a credit card to a customer account.
        /// Then it take the token and binds the credit card using the token
        /// The Request to the API is censored in the logging to our database.
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="cusApiInfo"></param>
        /// <returns></returns>
        /// 
        [UnitOfWork]
        public HubOnePaymentModuleResponse AddCreditCardToCustomerAccount(HubOneCreditCard creditCard, HubOneCustomerAPIInformation cusApiInfo)
        {
            HubOnePaymentModuleResponse retVal = null;

            ////this bit of code needs an isolated transaction because it registers a payment method in QB and also in TLG.
            ////once it it in QB, it should always be saved in TLG too.
            //using (var unitOfWork = base.UnitOfWorkFactory.Instance.NewUnitOfWork())
            //{
                //Get Quickbooks Customer Id from our system using the CustomerID and API Name
                var apiIdForHubCust = GetCustomerAPIID(cusApiInfo);

                if (apiIdForHubCust == null)
                    return _paymentAppService.BuildResponse(new Dictionary<string, string>() { { "CustomerAPILinkMissing", "Could not retrieve Customer API ID link." } }, ValidatorValues.IsEmpty.ToString(), false);

                //Validate basic info
                var errList = ValidateCreditCardDetails(creditCard, apiIdForHubCust);

                if (errList.Count == 0)
                {
                    //once we confirm no errors, we bind the QB identifier for the customer
                    creditCard.CustAPIIdentifer = apiIdForHubCust;

                    //We call the API to save the credit card to the customer
                    var result = CallAPIToAddCreditCard(creditCard);

                    // bind the two dictionaries for saving to the DB
                    var responseInfo = result.GetAPIDataAndResponses();

                    //Get the PaymentMethodTypeID using the name
                    var paymentMethodTypeId = GetPaymentMethodType(HubOnePaymentMethodType.COACH_CreditCard);

                    //Helps us identify that the type of things we're saving is a CC
                    responseInfo.Add("PaymentMethodTypeID", paymentMethodTypeId);


                    responseInfo.Add("CustomerID", cusApiInfo.CustomerID.ToString());


                    //Credit card owner is VERY important. Because in TLG you can have someone ELSE pay for you. So indicating this will be important.
                    responseInfo.Add("IsOwner", creditCard.IsOwner.ToString());

                    //Get id for status change
                    int paymentMethodId = SaveAddCreditCardResponseInfo(responseInfo);
                    responseInfo.Add("PaymentMethodID", paymentMethodId.ToString());
                    //Bind id for status change
                    creditCard.PaymentMethodID = paymentMethodId;

                    //set status based on results
                    SaveSetCreditCardStatus(paymentMethodId, responseInfo["status"]);

                    retVal = _paymentAppService.BuildResponse(responseInfo, result.ResponseMessage, true);
                }
                else
                {
                    retVal = _paymentAppService.BuildResponse(errList, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);
                }
            //}

            return retVal;
        }
               
        public int TryGetExistingCreditCard(HubOneCreditCard creditCard, int customerId)
        {
            //TODO:DBCALL
            //var paymentMethod = Get<ExistingPaymentMethodRead, ExistingPaymentMethodRead>("[EXTD_Coach].[spPaymentMethod_ReadExisting]", new ExistingPaymentMethodRead()
            //{
            //    CustomerID = customerId,
            //    CardNumber = creditCard.CardNumber,
            //    ExpMonth = creditCard.ExpirationMonth,
            //    ExpYear = creditCard.ExpirationYear
            //});

            var paymentMethod = new ExistingPaymentMethodRead();
            if (paymentMethod != null)
            {
                return paymentMethod.PaymentMethodId;
            }
            else
            {
                return -1;
            }

        }


        #endregion

        #region Bank Account related Methods
        public HubOnePaymentModuleResponse AddBankAccountToCustomerAccount(HubOneBankAccount bankAccount, HubOneCustomerAPIInformation cusApiInfo)
        {
            //Get Quickbooks Customer Id from our system using the CustomerID and API Name
            var apiIdForHubCust = GetCustomerAPIID(cusApiInfo);

            if (apiIdForHubCust == null)
                return _paymentAppService.BuildResponse(new Dictionary<string, string>() { { "CustomerAPILinkMissing", "Could not retrieve Customer API ID link." } }, ValidatorValues.IsEmpty.ToString(), false);

            //Validate basic info
            var errList = ValidateBankAccountDetails(bankAccount, apiIdForHubCust);

            if (errList.Count == 0)
            {
                //once we confirm no errors, we bind the QB identifier for the customer
                bankAccount.CustAPIIdentifer = apiIdForHubCust;

                //We call the API to save the credit card to the customer
                var result = CallAPIToAddBankAccount(bankAccount);

                // bind the two dictionaries for saving to the DB
                var responseInfo = result.GetAPIDataAndResponses();

                //Ge the PaymentMethodTypeID using the name
                var paymentMethodTypeId = GetPaymentMethodType(HubOnePaymentMethodType.COACH_BankAccount);

                //Helps us identify that the type of things we're saving is a CC
                responseInfo.Add("PaymentMethodTypeID", paymentMethodTypeId);

                responseInfo.Add("CustomerID", cusApiInfo.CustomerID.ToString());

                //Get id for status change
                int paymentMethodId = SaveAddBankAccountResponseInfo(responseInfo); //TODO: now! 

                responseInfo.Add("PaymentMethodID", paymentMethodId.ToString());
                //Bind id for status change
                bankAccount.PaymentMethodID = paymentMethodId;

                //set status based on results
                SaveSetBankAccountStatus(paymentMethodId, responseInfo["status"]);

                return _paymentAppService.BuildResponse(responseInfo, result.ResponseMessage, true);

            }
            else
            {

                return _paymentAppService.BuildResponse(errList, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);

            }

        }



        #endregion

        #region Echecks related methods
        public HubOnePaymentModuleResponse UpdateECheckStatus(HubOneECheck eCheck)
        {

            var errList = ValidateECheckForStatusUpdate(eCheck); // TODO DONE

            if (errList.Count == 0)
            {
                var result = CallApiGetECheck(eCheck);

                // bind the two dictionaries for saving to the DB
                var responseInfo = result.GetAPIDataAndResponses();

                //Bind Invoice ID for save
                responseInfo.Add(nameof(eCheck.PaymentID), eCheck.PaymentID.ToString());

                responseInfo.Add("EngineMessage", "ENG");

                //Get id for status change
                int paymentDetailId = SavePaymentDetailFromResponse(responseInfo);

                //Bind id for status change
                eCheck.PaymentDetailID = paymentDetailId;

                //set status based on results
                var ecStatusResult = SetDebitStatus(eCheck.PaymentDetailID, responseInfo["status"]);

                //Business Domain level status
                SetPaymentDisposition(Convert.ToInt32(eCheck.PaymentID), paymentDetailId, ecStatusResult);

                return _paymentAppService.BuildResponse(responseInfo, result.ResponseMessage, true);

            }
            else
            {

                return _paymentAppService.BuildResponse(errList, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);

            }

        }

        

        public HubOnePaymentModuleResponse CheckECheckStatus(HubOneECheck eCheck)
        {

            var errList = ValidateECheckForStatusUpdate(eCheck);

            var eCheckStatusResult = CallApiGetECheck(eCheck);

            var responseInfo = eCheckStatusResult.GetAPIDataAndResponses();

            responseInfo.Add("ExternalCode", eCheck.APIPaymentID);
            responseInfo.Add("Description", eCheck.Description);

            //Bind PaymentID for PaymentDetail
            responseInfo.Add(nameof(eCheck.PaymentID), eCheck.PaymentID.ToString());

            //Might need ot change? We'll see
            if (eCheckStatusResult.IsSuccessful)
            {
                string status = responseInfo["status"];
                responseInfo["status"] = string.Concat(status, "_VERIFY");
            }

            //Get id for status change
            int paymentDetailId = SavePaymentValidationFromResponse(responseInfo);

            return _paymentAppService.BuildResponse(responseInfo, eCheckStatusResult.ResponseMessage, eCheckStatusResult.IsSuccessful);

        }

        /// <summary>
        /// Debits a bank account of a certain amount.
        /// Required fields:
        /// <para>- Bank Accoutn ID from QB</para>
        /// <para>- Description for payment</para>
        /// <para>- Check Amount (Must be greater than 0.01 and less than 99999.99)</para>
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="charge"></param>
        /// <returns></returns>
        public HubOnePaymentModuleResponse DebitBankAccount(HubOneECheck eCheck, HubOneBankAccount bankAccount)
        {
            var errList = ValidateDebitDetails(bankAccount, eCheck);


            if (errList.Count == 0)
            {
                var result = CallApiForDebit(bankAccount, eCheck);

                // bind the two dictionaries for saving to the DB
                var responseInfo = result.GetAPIDataAndResponses();

                //Bind Invoice ID for save
                responseInfo.Add(nameof(eCheck.PaymentID), eCheck.PaymentID.ToString());

                //Get id for status change
                int paymentDetailId = SavePaymentDetailFromResponse(responseInfo);

                //Bind id for status change
                eCheck.PaymentDetailID = paymentDetailId;

                //set status based on results
                var ecStatusResult = SetDebitStatus(eCheck.PaymentDetailID, responseInfo["status"]);

                //Business Domain level status
                SetPaymentDisposition(Convert.ToInt32(eCheck.PaymentID), paymentDetailId, ecStatusResult);

                return _paymentAppService.BuildResponse(responseInfo, result.ResponseMessage, true);

            }
            else
            {

                return _paymentAppService.BuildResponse(errList, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);

            }

        }

        /// <summary>
        /// Issue eCheck/Debit void.
        /// Requires
        /// <para>Description - Required for Accounting team</para>
        /// <para>APIPayment ID - This is the Quicbooks ID for the eCheck</para>
        /// <para>The Amount needs to be more than zero</para>
        /// <para>The RefundAmount needs to be the same as the Amount</para>
        /// <para>Refund needs to be more than one dollar</para>
        /// <para>Amount need to not be zero</para>
        /// </summary>
        /// <param name="echeck"></param>
        /// <returns></returns>
        public HubOnePaymentModuleResponse IssueECheckVoid(HubOneECheck echeck)
        {
            //https://developer.intuit.com/app/developer/qbpayments/docs/api/resources/all-entities/echecks#void-or-refund-an-echeck
            /*
             Void or refund an ECheck
             Full or partial refund an existing ECheck transaction. Refund requests made on the same day as the 
             associated debit request may result in the transaction being voided. Refunds cannot be issued 
             unless the original debit has succeeded (this process typically takes approximately three business days).
             */

            bool completedTransaction = false;
            string message = ValidatorValues.InformationMissingDidNotCallAPI.ToString();


            //Do I have my basic info?
            var errList = new Dictionary<string, string>(); //ValidateECheckVoidDetails(echeck);

            //Does the debit exist?
            var resultStCk = CheckECheckStatus(echeck);


            if (!resultStCk.IsSuccessful)
            {
                errList.Add("ExistsInAPI", "false");
                message = "There is no transaction with that identifier.";
            }
            else
            {

                if (!resultStCk.ResponseValues["status"].Contains("REFUNDED") && !resultStCk.ResponseValues["status"].Contains("VOIDED"))
                {


                    //If it does exist, has it been completed?
                    if (resultStCk.ResponseValues["status"].Contains("SUCCEEDED") || resultStCk.ResponseValues["status"].Contains("PENDING"))
                    {
                        completedTransaction = true;
                    }
                    else
                    {
                        errList.Add("Transaction", "NotCompleted");
                        message = "Transaction must be completed before a refund.";
                    }

                }
                else
                {
                    errList.Add("Transaction", "AlreadyRefunded");
                    message = "This transaction has already been refunded.";
                }

            }

            ////once we confirm no error and that the transaction already exists then we proceed with the refund
            if (errList.Count == 0 && completedTransaction)
            {
                var result = CallApiForECheckVoid(echeck);

                // bind the two dictionaries for saving to the DB
                var responseInfo = result.GetAPIDataAndResponses();

                //Bind PaymentID for PaymentDetail
                responseInfo.Add(nameof(echeck.PaymentID), echeck.PaymentID.ToString());

                //Get id for status change
                int paymentDetailId = SavePaymentDetailFromResponse(responseInfo); //Save Payment Detail

                //Bind PaymentDetailID for Payment
                responseInfo.Add("PaymentDetailID", paymentDetailId.ToString());

                //set payment detail status based on result. API level status
                var payDetStatus = SetDebitVoidStatus(paymentDetailId, responseInfo["status"]);

                //Default processing for loggin purpose
                SetPaymentDisposition(echeck.PaymentID, paymentDetailId, PaymentChargeStatus.RefundInProgress);

                //Business domain level status
                SetPaymentDisposition(echeck.PaymentID, paymentDetailId, payDetStatus);

                return _paymentAppService.BuildResponse(responseInfo, result.ResponseMessage, true);

            }
            else
            {

                return _paymentAppService.BuildResponse(errList, message, false);

            }
        }

        private int SavePaymentDetailFromResponse(Dictionary<string, string> transObj)
        {
            int paymentDetailID = 0;
            bool hasNoErrorMessage = true;
            bool hasNoErrorDetailMessage = true;
            var paymentStatus = "";

            if (transObj.ContainsKey("ErrorMessage"))
                hasNoErrorMessage = string.IsNullOrEmpty(transObj["ErrorMessage"]);

            if (transObj.ContainsKey("ErrorMessage"))
                hasNoErrorDetailMessage = string.IsNullOrEmpty(transObj["ErrorDetail"]);

            if (!transObj.ContainsKey("ErrorMessage"))
                paymentStatus = transObj["status"];

            if (transObj.ContainsKey("EngineMessage"))
                paymentStatus = string.Concat(transObj["status"], "_", transObj["EngineMessage"]);


            if (!hasNoErrorMessage || !hasNoErrorDetailMessage)
            {
                transObj.Add("status", "FAILED");

                //TODO:DBCALL
                //paymentDetailID = Create<PaymentDetailInsert, int>("[EXTD_Coach].[spPaymentDetail_Insert]", new PaymentDetailInsert()
                //{
                //    PaymentID = Convert.ToInt32(transObj["PaymentID"]),
                //    PaymentAmount = 00.00m,
                //    PaymentStatus = paymentStatus,
                //    CallInternalCode = transObj["InternalCode"],
                //    CallRequest = transObj["RawRequest"],
                //    CallResponse = transObj["RawResponse"],
                //    CallException = transObj["ErrorDetail"],
                //    CallWarning = transObj["ErrorMessage"],
                //    ExternalCode01 = "",
                //    ExternalCode02 = "",
                //    CreationDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")

                //}, x => x.PaymentDetailID);

            }
            else
            {
                //TODO:DBCALL
                //paymentDetailID = Create<PaymentDetailInsert, int>("[EXTD_Coach].[spPaymentDetail_Insert]", new PaymentDetailInsert()
                //{
                //    PaymentID = Convert.ToInt32(transObj["PaymentID"]),
                //    PaymentAmount = Convert.ToDecimal(transObj["amount"]),
                //    PaymentStatus = paymentStatus,
                //    CallInternalCode = transObj["InternalCode"],
                //    CallRequest = transObj["RawRequest"],
                //    CallResponse = transObj["RawResponse"],
                //    CallException = "",
                //    CallWarning = "",
                //    ExternalCode01 = transObj["id"],
                //    ExternalCode02 = "",
                //    CreationDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")

                //}, x => x.PaymentDetailID);
            }

            return paymentDetailID;

        }
        #endregion

        #region Manual Checks

        public int AddManualCheckPaymentMethod(HubOneManualCheck manualCheck)
        {
            int recordId = 0;

            var paymentMethodInsert = new PaymentMethodInsert();
            paymentMethodInsert.PaymentMethodTypeID = GetPaymentMethodType(HubOnePaymentMethodType.COACH_Check);
            paymentMethodInsert.AccountType = "Manual Check";
            paymentMethodInsert.CustomerID = manualCheck.HubOneCustomerID;
            paymentMethodInsert.OwnerName = manualCheck.SignatorName;
            paymentMethodInsert.AccountNumber = "";
            paymentMethodInsert.AccountRoutingNumber = "";
            paymentMethodInsert.Token = "";
            paymentMethodInsert.ExpirationMonth = "";
            paymentMethodInsert.ExpirationYear = "";
            paymentMethodInsert.IsOwner = "";
            paymentMethodInsert.CallInternalCode = "";
            paymentMethodInsert.CallRequest = "";
            paymentMethodInsert.CallResponse = "";
            paymentMethodInsert.CallException = "";
            paymentMethodInsert.CallWarning = "";
            paymentMethodInsert.ExternalCode = "";

            //TODO:DBCALL
            //recordId = Create<PaymentMethodInsert, int>("[EXTD_Coach].[spPaymentMethod_Insert]", paymentMethodInsert, x => x.PaymentMethodID);

            return recordId;
        }

        public void ManualCheckProcessing(string action, int paymentId)
        {

            if (paymentId == 0)
                throw new ArgumentNullException(nameof(paymentId));

            var hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Declined_Other;

            switch (action)
            {
                case "COACH_Payment_Process":
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Processing;
                    break;
                case "COACH_Payment_Cancel":
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Cancelled;
                    break;
                case "COACH_Payment_Complete":
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Completed;
                    break;
                case "COACH_Payment_Decline":
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Declined_Other;
                    break;
                case "COACH_Payment_Credit":
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Processing;
                    break;
                case "COACH_Credit_Complete":
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Completed;
                    break;
                case "COACH_Credit_Decline":
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Declined_Other;
                    break;
                case "COACH_Credit_Cancel":
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Cancelled;
                    break;
                default:
                    break;
            }

            UpdatePaymentDispostion(paymentId, hubOnePayDisp);
        }

        #endregion

        #region Charges related Methods
        private HubOnePaymentModuleResponse CheckChargeStatus(HubOneCharge charge)
        {

            //if Yes, then check the status of the charge.
            var chargeStatusResult = CalAPIToCheckChargeStatus(charge);

            // bind the two dictionaries for saving to the DB
            var responseInfo = chargeStatusResult.GetAPIDataAndResponses();

            responseInfo.Add("ExternalCode", charge.APIPaymentID);
            responseInfo.Add("Description", charge.Description);

            //Bind PaymentID for PaymentDetail
            responseInfo.Add(nameof(charge.PaymentID), charge.PaymentID.ToString());

            if (chargeStatusResult.IsSuccessful)
            {
                string status = responseInfo["status"];
                responseInfo["status"] = string.Concat(status, "_VERIFY");
            }

            //Get id for status change
            int paymentDetailId = SavePaymentValidationFromResponse(responseInfo);

            return _paymentAppService.BuildResponse(responseInfo, chargeStatusResult.ResponseMessage, chargeStatusResult.IsSuccessful);

        }

        /// <summary>
        ///  We do a charge void.
        ///  The way Quickbooks handles this functionality is as follows:
        ///  A void is simply a full refund of the id of the transaction you've sent.
        ///  We do NOT do partial refunds, in in effect all refunds are voids
        ///  for the purposes of refunds.
        ///  <para>Description cannot be empty, you need a reason for voiding</para>
        ///  <para>API Payment ID to be refunded (this is the id Quickbooks gives us for the transaction.</para>
        ///  <para>Refund amount must be above 01.00m</para>
        ///  <para>Refund must contain a payment ID</para>
        ///  <para>charge.Amount  == charge.RefundAmount</para>
        /// </summary>
        /// <param name="chargeToVoid"></param>
        /// <returns></returns>
        public HubOnePaymentModuleResponse IssueChargeVoid(HubOneCharge chargeToVoid)
        {
            bool completedTransaction = false;
            string message = ValidatorValues.InformationMissingDidNotCallAPI.ToString();

            //Do I have my basic info?
            var errList = ValidateChargeVoidDetails(chargeToVoid);


            //Does the charge exist?
            var resultStCk = CheckChargeStatus(chargeToVoid);


            if (!resultStCk.IsSuccessful)
            {
                errList.Add("ExistsInAPI", "false");
                message = "There is no transaction with that identifier.";
            }
            else
            {

                if (!resultStCk.ResponseValues["status"].Contains("REFUNDED") || !resultStCk.ResponseValues["status"].Contains("VOID"))
                {


                    //If it does exist, has it been completed?
                    if (resultStCk.ResponseValues["status"].Contains("CAPTURED"))
                    {
                        completedTransaction = true;
                    }
                    else
                    {
                        errList.Add("Transaction", "NotCompleted");
                        message = "Transaction must be completed before a refund.";
                    }

                }
                else
                {
                    errList.Add("Transaction", "AlreadyRefunded");
                    message = "This transaction has already been refunded.";
                }

            }

            //once we confirm no error and that the transaction already exists then we proceed with the refund
            if (errList.Count == 0 && completedTransaction)
            {
                var result = CallApiForChargeVoid(chargeToVoid);

                // bind the two dictionaries for saving to the DB
                var responseInfo = result.GetAPIDataAndResponses();

                //Bind PaymentID for PaymentDetail
                responseInfo.Add(nameof(chargeToVoid.PaymentID), chargeToVoid.PaymentID.ToString());

                //Get id for status change
                int paymentDetailId = SavePaymentDetailFromResponse(responseInfo); //Save Payment Detail

                //Bind PaymentDetailID for Payment
                responseInfo.Add("PaymentDetailID", paymentDetailId.ToString());

                //set payment detail status based on result. API level status
                var payDetStatus = SetPaymenDetailtStatus(paymentDetailId, responseInfo["status"]);

                //Default processing for loggin purpose
                SetPaymentDisposition(chargeToVoid.PaymentID, paymentDetailId, PaymentChargeStatus.RefundInProgress);

                //Business domain level status
                SetPaymentDisposition(chargeToVoid.PaymentID, paymentDetailId, payDetStatus);

                return _paymentAppService.BuildResponse(responseInfo, result.ResponseMessage, true);

            }
            else
            {

                return _paymentAppService.BuildResponse(errList, message, false);

            }

        }

        

        /// <summary>
        /// <para>- CreditCard.CardID must be present</para>
        /// <para>- Charge.Amount must be more than 0</para>
        /// <para>- Charge.Currency must be "USD"</para>
        /// <para>- Charge.PaymentID must be a number and must be present</para>
        /// <para>- Charge.PaymentContext.IsEcomm must be true</para>
        /// </summary>
        /// <param name="creditCard"></param>
        /// <param name="charge"></param>
        /// <returns></returns>
        public HubOnePaymentModuleResponse ChargeCreditCard(HubOneCreditCard creditCard, HubOneCharge charge)
        {

            //the card checking logic is in the business process because this logic is bound
            //more closely to Quickbooks than it is to the payment module.
            //QB provides us with an ID for a credit card after binding the credit card
            //to a customer. We NEED this id for a charge.
            var errList = ValidateChargeDetails(creditCard, charge);

            //If we have no errors
            if (errList.Count == 0)
            {
                //Have paymentId

                var result = CallApiForCharge(creditCard, charge);

                // bind the two dictionaries for saving to the DB
                var responseInfo = result.GetAPIDataAndResponses();

                //Bind PaymentID for PaymentDetail
                responseInfo.Add(nameof(charge.PaymentID), charge.PaymentID.ToString());

                //Get id for status change
                int paymentDetailId = SavePaymentDetailFromResponse(responseInfo); //Save Payment Detail

                //Bind PaymentDetailID for Payment
                responseInfo.Add("PaymentDetailID", paymentDetailId.ToString());

                //set payment detail status based on result. API level status
                var payDetStatus = SetPaymenDetailtStatus(paymentDetailId, responseInfo["status"]);

                //Default processing for loggin purpose
                SetPaymentDisposition(charge.PaymentID, paymentDetailId, PaymentChargeStatus.Authorized);

                //Business domain level status
                SetPaymentDisposition(charge.PaymentID, paymentDetailId, payDetStatus);

                return _paymentAppService.BuildResponse(responseInfo, result.ResponseMessage, true);

            }
            else
            {

                return _paymentAppService.BuildResponse(errList, ValidatorValues.InformationMissingDidNotCallAPI.ToString(), false);

            }
        }

        
        
        #endregion

        #endregion

        #region Utilities
        #region Customers
        /// <summary>
        /// This information MUST be present for TLG
        /// <para>- Address</para>
        /// <para>- Phone </para>
        /// <para>- First Name, LastName</para>
        /// <para>- Email</para>
        /// </summary>
        /// <param name="huboneCustomer"></param>
        /// <returns></returns>
        private Dictionary<string, string> TLGInfoValidation(HubOneCustomer huboneCustomer)
        {
            //** Townsend Specific **//
            var failDic = new Dictionary<string, string>();


            //Location info
            if (string.IsNullOrEmpty(huboneCustomer.City))
                failDic.Add(nameof(huboneCustomer.City), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.CivicNumber))
                failDic.Add(nameof(huboneCustomer.CivicNumber), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.StreetName))
                failDic.Add(nameof(huboneCustomer.StreetName), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.Country))
                failDic.Add(nameof(huboneCustomer.Country), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.PostalCode))
                failDic.Add(nameof(huboneCustomer.PostalCode), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.CountrySubDivisionCode))
                failDic.Add(nameof(huboneCustomer.CountrySubDivisionCode), ValidatorValues.IsEmpty.ToString());

            //Communication info
            if (string.IsNullOrEmpty(huboneCustomer.PrimaryEmailAddress))
                failDic.Add(nameof(huboneCustomer.PrimaryEmailAddress), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.PrimaryPhone))
                failDic.Add(nameof(huboneCustomer.PrimaryPhone), ValidatorValues.IsEmpty.ToString());

            //Personal Info
            if (string.IsNullOrEmpty(huboneCustomer.FamilyName))
                failDic.Add(nameof(huboneCustomer.FamilyName), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.GivenName))
                failDic.Add(nameof(huboneCustomer.GivenName), ValidatorValues.IsEmpty.ToString());

            if (string.IsNullOrEmpty(huboneCustomer.HubOneCustomerID))
                failDic.Add(nameof(huboneCustomer.HubOneCustomerID), ValidatorValues.IsEmpty.ToString());


            return failDic;


        }

        private HubOnePaymentModuleResponse CallAPIToCreateCustomer(HubOneCustomer hubOneCustomer)
        {
            //var paymentModule = GetPaymentModule();

            var response = _paymentAppService.CreateCustomer(hubOneCustomer);

            return response;

        }

        private void BindQuickbooksIDToHubOneCustomerID(Dictionary<string, string> transObj, HubOneAPIName apiName)
        {
            //TODO:DBCALL
            //Create("[EXTD_Coach].[spAPIIdentiferForCustomer_Insert]", new CustomerAPIIdentiferInsert()
            //{
            //    CustomerID = transObj["HubOneCustomerID"],
            //    APIName = apiName,
            //    APIIdentifier = transObj["QBCustID"]
            //});
        }

        public int RebindCustomerCreditCard(string customerId, string cardNumber)
        {
            //var paymentModule = GetPaymentModule();

            HubOneCustomerAPIInformation custAPIInfo = new HubOneCustomerAPIInformation();
            custAPIInfo.CustomerID = Int32.Parse(customerId);
            custAPIInfo.APIName = HubOneAPIName.QuickbooksAccounting;

            HubOneCustomer cust = new HubOneCustomer();
            cust.HubOneCustomerID = customerId;
            cust.APIReferenceID = GetCustomerAPIID(custAPIInfo);

            //returns result after call of api
            var result = _paymentAppService.GetCustomerPaymentMethods(cust, "CREDIT_CARD", cardNumber);

            // bind the two dictionaries for saving to the DB
            var responseInfo = result.GetAPIDataAndResponses();

            //Get the PaymentMethodTypeID using the name
            var paymentMethodTypeId = GetPaymentMethodType(HubOnePaymentMethodType.COACH_CreditCard);

            //Helps us identify that the type of things we're saving is a CC
            responseInfo.Add("PaymentMethodTypeID", paymentMethodTypeId);

            responseInfo.Add("IsOwner", true.ToString());

            responseInfo.Add("CustomerID", custAPIInfo.CustomerID.ToString());

            responseInfo.Add("status", "SUCCEEDED");

            //TODO:DBCALL
            var paymentMethodId = 0;
            //var paymentMethodId = Create<PaymentMethodInsert, int>("[EXTD_Coach].[spPaymentMethod_Insert]", new PaymentMethodInsert()
            //{
            //    PaymentMethodTypeID = responseInfo["PaymentMethodTypeID"],
            //    CustomerID = responseInfo["CustomerID"],
            //    OwnerName = responseInfo["cardholderName"],
            //    AccountType = responseInfo["cardType"],
            //    AccountNumber = responseInfo["cardNumber"],
            //    AccountRoutingNumber = "", //Credit Cards don't have routing numbers
            //    Token = "",
            //    CallInternalCode = responseInfo["InternalCode"],
            //    CallRequest = responseInfo["RawRequest"],
            //    CallResponse = responseInfo["RawResponse"],
            //    CallException = "",
            //    CallWarning = "",
            //    ExternalCode = responseInfo["cardId"],
            //    ExpirationMonth = responseInfo["expireMonth"],
            //    ExpirationYear = responseInfo["expireYear"],
            //    IsOwner = responseInfo["IsOwner"]

            //}, x => x.PaymentMethodID);

            //set status based on results
            SaveSetCreditCardStatus(paymentMethodId, responseInfo["status"]);

            return paymentMethodId;

            //return result;
        }
        #endregion

        #region Credit Cards
        private HubOnePaymentModuleResponse CallAPIToAddCreditCard(HubOneCreditCard creditCard)
        {
            //var paymentModule = GetPaymentModule();

            var response = _paymentAppService.AddCreditCard(creditCard);

            return response;

        }

        private static HubOneCreditCard BindCard(PaymentToProcess payment)
        {
            // Create the CreditCard object
            var creditCard = new HubOneCreditCard();
            creditCard.CardID = payment.QuickbooksPaymentMethodId;
            return creditCard;
        }

        private Dictionary<string, string> ValidateCreditCardDetails(HubOneCreditCard creditCard, string qbCusID)
        {

            var errorDictionary = new Dictionary<string, string>();

            //https://developer.intuit.com/app/developer/qbpayments/docs/api/resources/all-entities/cards#create-a-card
            //Requirements for a QB adding a credit card

            //QB Customer ID
            if (string.IsNullOrEmpty(qbCusID))
                errorDictionary.Add(nameof(qbCusID), ValidatorValues.IsEmpty.ToString());

            //card number
            if (string.IsNullOrEmpty(creditCard.CardNumber))
                errorDictionary.Add(nameof(creditCard.CardNumber), "IsEmptyOrIsNotANumber");

            //exp Month
            if (string.IsNullOrEmpty(creditCard.ExpirationMonth))
                errorDictionary.Add(nameof(creditCard.ExpirationMonth), "IsEmptyOrIsNotANumberOrNotAMonth");

            //exp year
            if (string.IsNullOrEmpty(creditCard.ExpirationYear))
                errorDictionary.Add(nameof(creditCard.ExpirationYear), "IsEmptyOrIsNotANumber");

            //cvc
            if (string.IsNullOrEmpty(creditCard.Cvc))
                errorDictionary.Add(nameof(creditCard.CardNumber), "IsEmptyOrIsNotANumber");


            return errorDictionary;

        }

        private int SaveAddCreditCardResponseInfo(Dictionary<string, string> transObj)
        {
            int recordId = 0;
            bool hasNoErrorMessage = true;
            bool hasNoErrorDetailMessage = true;

            if (transObj.ContainsKey("ErrorMessage"))
                hasNoErrorMessage = string.IsNullOrEmpty(transObj["ErrorMessage"]);

            if (transObj.ContainsKey("ErrorMessage"))
                hasNoErrorDetailMessage = string.IsNullOrEmpty(transObj["ErrorDetail"]);


            if (!hasNoErrorMessage || !hasNoErrorDetailMessage)
            {
                transObj.Add("status", "FAILED");

                //TODO:DBCAll
                //recordId = Create<PaymentMethodInsert, int>("[EXTD_Coach].[spPaymentMethod_Insert]", new PaymentMethodInsert()
                //{
                //    PaymentMethodTypeID = transObj["PaymentMethodTypeID"],
                //    CustomerID = transObj["CustomerID"],
                //    OwnerName = "",
                //    AccountType = "",
                //    AccountNumber = "",
                //    AccountRoutingNumber = "", //Credit Cards don't have routing numbers
                //    CallInternalCode = transObj["InternalCode"],
                //    CallRequest = transObj["RawRequest"],
                //    CallResponse = transObj["RawResponse"],
                //    CallException = transObj["ErrorDetail"],
                //    CallWarning = transObj["ErrorMessage"],
                //    Token = "",
                //    ExternalCode = "",
                //    ExpirationMonth = "",
                //    ExpirationYear = "",
                //    IsOwner = transObj["IsOwner"]
                //    //CreatedOn = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")

                //}, x => x.PaymentMethodID);

            }
            else
            {
                transObj.Add("status", "SUCCEEDED");
                //TODO:DBCALL
                //recordId = Create<PaymentMethodInsert, int>("[EXTD_Coach].[spPaymentMethod_Insert]", new PaymentMethodInsert()
                //{
                //    PaymentMethodTypeID = transObj["PaymentMethodTypeID"],
                //    CustomerID = transObj["CustomerID"],
                //    OwnerName = transObj["cardholderName"],
                //    AccountType = transObj["cardType"],
                //    AccountNumber = transObj["cardNumber"],
                //    AccountRoutingNumber = "", //Credit Cards don't have routing numbers
                //    Token = "",
                //    CallInternalCode = transObj["InternalCode"],
                //    CallRequest = transObj["RawRequest"],
                //    CallResponse = transObj["RawResponse"],
                //    CallException = "",
                //    CallWarning = "",
                //    ExternalCode = transObj["cardId"],
                //    ExpirationMonth = transObj["expireMonth"],
                //    ExpirationYear = transObj["expireYear"],
                //    IsOwner = transObj["IsOwner"]

                //}, x => x.PaymentMethodID);
            }

            return recordId;

        }

        #endregion

        #region Bank Account
        private Dictionary<string, string> ValidateBankAccountDetails(HubOneBankAccount bankAccount, string qbCusID)
        {

            var errorDictionary = new Dictionary<string, string>();

            //https://developer.intuit.com/app/developer/qbpayments/docs/api/resources/all-entities/bankaccounts#create-a-bank-account
            //Requirements for a QB adding a credit card

            //QB Customer ID
            if (string.IsNullOrEmpty(qbCusID))
                errorDictionary.Add(nameof(qbCusID), ValidatorValues.IsEmpty.ToString());

            //Name is required
            if (string.IsNullOrEmpty(bankAccount.AccountHolderName))
                errorDictionary.Add(nameof(bankAccount.AccountHolderName), ValidatorValues.IsEmpty.ToString());

            //accountNumber
            if (string.IsNullOrEmpty(bankAccount.AccountNumber))
                errorDictionary.Add(nameof(bankAccount.AccountNumber), ValidatorValues.IsEmpty.ToString());

            //phone
            if (string.IsNullOrEmpty(bankAccount.AccountHolderPhone))
                errorDictionary.Add(nameof(bankAccount.AccountHolderPhone), ValidatorValues.IsEmpty.ToString());

            //routing number
            if (string.IsNullOrEmpty(bankAccount.RoutingNumber))
                errorDictionary.Add(nameof(bankAccount.RoutingNumber), ValidatorValues.IsEmpty.ToString());

            return errorDictionary;

        }

        public int TryGetExistingBankAccount(HubOneBankAccount bankAccount, int customerId)
        {
            //TODO:DBCALL
            //var paymentMethod = Get<ExistingPaymentMethodRead, ExistingPaymentMethodRead>("[EXTD_Coach].[spPaymentMethod_ReadExisting]", new ExistingPaymentMethodRead()
            //{
            //    CustomerID = customerId,
            //    AccountNumber = bankAccount.AccountNumber,
            //    RoutingNumber = bankAccount.RoutingNumber
            //});

            var paymentMethod = new ExistingPaymentMethodRead();
            if (paymentMethod != null)
            {
                return paymentMethod.PaymentMethodId;
            }
            else
            {
                return -1;
            }
        }

        private int SaveAddBankAccountResponseInfo(Dictionary<string, string> transObj)
        {
            int recordId = 0;
            bool hasNoErrorMessage = true;
            bool hasNoErrorDetailMessage = true;

            if (transObj.ContainsKey("ErrorMessage"))
                hasNoErrorMessage = string.IsNullOrEmpty(transObj["ErrorMessage"]);

            if (transObj.ContainsKey("ErrorDetail"))
                hasNoErrorDetailMessage = string.IsNullOrEmpty(transObj["ErrorDetail"]);


            if (!hasNoErrorMessage || !hasNoErrorDetailMessage)
            {
                transObj.Add("status", "FAILED");

                //TODO:DBCALL
                //recordId = Create<PaymentMethodInsert, int>("[EXTD_Coach].[spPaymentMethod_Insert]", new PaymentMethodInsert()
                //{
                //    PaymentMethodTypeID = transObj["PaymentMethodTypeID"],
                //    CustomerID = transObj["CustomerID"],
                //    OwnerName = "",
                //    AccountType = "",
                //    AccountNumber = "",
                //    AccountRoutingNumber = "", //Credit Cards don't have routing numbers
                //    CallInternalCode = transObj["InternalCode"],
                //    CallRequest = transObj["RawRequest"],
                //    CallResponse = transObj["RawResponse"],
                //    CallException = transObj["ErrorDetail"],
                //    CallWarning = transObj["ErrorMessage"],
                //    Token = "",
                //    ExternalCode = "",
                //    ExpirationMonth = "",
                //    ExpirationYear = "",
                //    IsOwner = ""
                //    //CreatedOn = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")

                //}, x => x.PaymentMethodID);

            }
            else
            {
                transObj.Add("status", "SUCCEEDED");

                //TODO:DBCALL
                //    recordId = Create<PaymentMethodInsert, int>("[EXTD_Coach].[spPaymentMethod_Insert]", new PaymentMethodInsert()
                //    {
                //        PaymentMethodTypeID = transObj["PaymentMethodTypeID"],
                //        CustomerID = transObj["CustomerID"],
                //        OwnerName = transObj["cardholderName"],
                //        AccountType = transObj["accountType"],
                //        AccountNumber = transObj["bankAccountNumber"],
                //        AccountRoutingNumber = transObj["routingNumber"],
                //        Token = "",
                //        CallInternalCode = transObj["InternalCode"],
                //        CallRequest = transObj["RawRequest"],
                //        CallResponse = transObj["RawResponse"],
                //        CallException = "",
                //        CallWarning = "",
                //        ExternalCode = transObj["bankAccountId"],
                //        ExpirationMonth = "",
                //        ExpirationYear = "",
                //        IsOwner = ""

                //    }, x => x.PaymentMethodID);
            }

            return recordId;

        }

        private HubOnePaymentModuleResponse CallAPIToAddBankAccount(HubOneBankAccount bankAccount)
        {
            return _paymentAppService.AddBankAccount(bankAccount);
        }
        #endregion

        #region Charges

        private HubOnePaymentModuleResponse CalAPIToCheckChargeStatus(HubOneCharge charge)
        {
            return _paymentAppService.GetCharge(charge);
        }

        private void SaveSetCreditCardStatus(int paymentMethodId, string status)
        {
            var statusResult = CreditCardStatus.Failed;

            switch (status)
            {
                case "SUCCEEDED":
                    statusResult = CreditCardStatus.Completed;
                    break;
                case "FAILED":
                    statusResult = CreditCardStatus.Failed;
                    break;

            }

            //TODO:DBCALL
            //_recordStatusProcess.SetRecordStatus
            //(
            //    Data.HubOneContext.COACH_CreditCardProcessing,
            //    paymentMethodId,
            //    statusResult
            //);
        }

        /// <summary>
        /// Statuses for Credit Card
        /// <para>AUTHORIZED</para>
        /// <para>DECLINED</para>
        /// <para>CAPTURED</para>
        /// <para>CANCELLED</para>
        /// <para>SETTLED</para>
        /// <para>REFUNDED</para>
        /// <para>FAILED</para>
        /// </summary>
        /// <param name="paymentDetailId"></param>
        /// <param name="status"></param>
        private PaymentChargeStatus SetPaymenDetailtStatus(int paymentDetailId, string status)
        {
            var statusResult = PaymentChargeStatus.Failed;

            switch (status)
            {
                case "AUTHORIZED":
                    statusResult = PaymentChargeStatus.Authorized;
                    break;
                case "DECLINED":
                    statusResult = PaymentChargeStatus.Declined;
                    break;
                case "CAPTURED":
                    statusResult = PaymentChargeStatus.Captured;
                    break;
                case "CANCELLED":
                    statusResult = PaymentChargeStatus.Cancelled;
                    break;
                case "SETTLED":
                    statusResult = PaymentChargeStatus.Settled;
                    break;
                case "ISSUED":
                    statusResult = PaymentChargeStatus.Refunded; //PaymenChargeStatus.Issued;
                    break;
                case "REFUNDED":
                    statusResult = PaymentChargeStatus.Refunded;
                    break;
                case "FAILED":
                    statusResult = PaymentChargeStatus.Failed;
                    break;

            }

            //TODO:DBCALL
            //_recordStatusProcess.SetRecordStatus
            //(
            //    Data.HubOneContext.COACH_Process_CreditCard,
            //    paymentDetailId,
            //    statusResult
            //);

            return statusResult;

        }

        private static HubOneCharge BindCharge(PaymentToProcess payment)
        {

            // Create the Charge object
            var charge = new HubOneCharge();
            charge.PaymentID = payment.PaymentID;
            charge.Currency = "USD";
            charge.Description = payment.TeamName;

            // Round charge amount to 2 decimals, API will throw error if more than 2 decimals
            charge.Amount = Math.Round(payment.PaymentTotal, 2);
            charge.PaymentContext = new HubOnePaymentContext()
            {
                IsEcomm = true
            };
            return charge;
        }

        /// <summary>
        /// Applies API specific rules to the module object
        /// These rules specifically check for needed fields for the
        /// Quickbooks API.
        /// <para>- CreditCard.CardID must be present</para>
        /// <para>- Charge.Amount must be more than 0</para>
        /// <para>- Charge.Currency must be "USD"</para>
        /// <para>- Charge.PaymentID must be a number and must be present</para>
        /// <para>- Charge.PaymentContext.IsEcomm must be true</para>
        /// <para>- Charge.PaymentDetailID must be present and not zero</para>
        /// <para>- Charge.PaymentID must be present and not zero</para>
        /// </summary>
        /// <param name="creditCard">Credit card ID from QB should be present</param>
        /// <param name="charge">Amount above zero, "USD" as currency and ECommerce must be true</param>
        /// <returns>List of fields that were missing or erroneous in the objects that the API needs</returns>
        private Dictionary<string, string> ValidateChargeDetails(HubOneCreditCard creditCard, HubOneCharge charge)
        {

            var errorDictionary = new Dictionary<string, string>();

            //https://developer.intuit.com/app/developer/qbpayments/docs/api/resources/all-entities/charges#create-a-charge
            //Requirements for a QB charge
            if (string.IsNullOrEmpty(creditCard.CardID))
                errorDictionary.Add(nameof(creditCard.CardID), ValidatorValues.IsEmpty.ToString());


            if (charge.Amount <= 0)
                errorDictionary.Add(nameof(charge.Amount), "IsLessThanZeroOrZero");


            if (charge.Currency != "USD")
                errorDictionary.Add(nameof(charge.Currency).ToString(), "IsNotUSD");


            if (charge.PaymentContext.IsEcomm == false)
                errorDictionary.Add(nameof(charge.PaymentContext.IsEcomm), "IsECommIsFalse");

            if (charge.PaymentID == 0)
                errorDictionary.Add(nameof(charge.PaymentID), "PaymentIDMissing");


            return errorDictionary;

        }

        /// <summary>
        /// Validate the charge to make sure it has the required information.
        /// <para>-PaymendID must be present </para>
        /// <para>-PaymentDetailID must be present </para>
        /// <para>-RefundAmount must be present</para>
        /// <para>-Refund amount must be the same amount as the last payment </para>
        /// <para>-Current payyment must not have been refunded already</para>
        /// </summary>
        /// <param name="charge"></param>
        /// <returns></returns>
        private Dictionary<string, string> ValidateChargeVoidDetails(HubOneCharge charge)
        {
            var errorDictionary = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(charge.Description))
                errorDictionary.Add(nameof(charge.Description), ValidatorValues.IsEmpty.ToString());


            if (string.IsNullOrEmpty(charge.APIPaymentID))
                errorDictionary.Add(nameof(charge.PaymentDetailID), "IsNotAssigned");


            if (charge.RefundAmount < 01.00m)
                errorDictionary.Add(nameof(charge.RefundAmount), "IsLessThanZero");


            if (charge.PaymentDetailID == 0)
                errorDictionary.Add(nameof(charge.PaymentDetailID), ValidatorValues.IsEmpty.ToString());

            if (charge.RefundAmount != charge.Amount)
                errorDictionary.Add(nameof(charge.RefundAmount), "NotFullRefund");


            return errorDictionary;

        }


        private HubOnePaymentModuleResponse CallApiForCharge(HubOneCreditCard creditCard, HubOneCharge charge)
        {
            return new HubOnePaymentModuleResponse();//_paymentAppService.ChargeCreditCard(creditCard, charge);
        }

        private HubOnePaymentModuleResponse CallApiForChargeVoid(HubOneCharge charge)
        {
            return _paymentAppService.VoidCharge(charge);
        }
        #endregion

        #region EChecks

        private HubOnePaymentModuleResponse CallApiForECheckVoid(HubOneECheck echeck)
        {
            return _paymentAppService.VoidECheck(echeck);
        }

        private HubOnePaymentModuleResponse CallApiForDebit(HubOneBankAccount bankaccount, HubOneECheck eCheck)
        {
            return _paymentAppService.CreateDebit(bankaccount);//, eCheck);
        }

        private HubOnePaymentModuleResponse CallApiGetECheck(HubOneECheck eCheck)
        {
            return _paymentAppService.GetECheck(eCheck);
        }

        /// <summary>
        /// Validates the HubOneEcheck object to make sure it has the required values
        /// </summary>
        /// <param name="eCheck">the QB Id of the transaction for the ECheck</param>
        /// <returns></returns>
        private Dictionary<string, string> ValidateECheckForStatusUpdate(HubOneECheck eCheck)
        {

            var errorDictionary = new Dictionary<string, string>();

            //TLG requirement - need to insert info about the group with which this payment is for
            if (string.IsNullOrEmpty(eCheck.APIPaymentID))
                errorDictionary.Add(nameof(eCheck.APIPaymentID), ValidatorValues.IsEmpty.ToString());


            return errorDictionary;

        }

        /// <summary>
        /// Statuses for
        /// <para>AUTHORIZED</para>
        /// <para>DECLINED</para>
        /// <para>CAPTURED</para>
        /// <para>CANCELLED</para>
        /// <para>SETTLED</para>
        /// <para>REFUNDED</para>
        /// <para>FAILED</para>
        /// </summary>
        /// <param name="paymentDetailID"></param>
        /// <param name="status"></param>
        private PaymentChargeStatus SetDebitVoidStatus(int paymentDetailID, string status)
        {
            var statusResult = PaymentChargeStatus.Failed;

            switch (status)
            {
                case "AUTHORIZED":
                    statusResult = PaymentChargeStatus.Authorized;
                    break;
                case "DECLINED":
                    statusResult = PaymentChargeStatus.Declined;
                    break;
                case "CAPTURED":
                    statusResult = PaymentChargeStatus.Captured;
                    break;
                case "CANCELLED":
                    statusResult = PaymentChargeStatus.Cancelled;
                    break;
                case "SETTLED":
                    statusResult = PaymentChargeStatus.Settled;
                    break;
                case "REFUNDED":
                    statusResult = PaymentChargeStatus.Refunded;
                    break;
                case "FAILED":
                    statusResult = PaymentChargeStatus.Failed;
                    break;

            }


            //TODO:DBCALL
            //_recordStatusProcess.SetRecordStatus
            //(
            //    Data.HubOneContext.COACH_Void_ECheck,
            //    paymentDetailID,
            //    statusResult
            //);

            return statusResult;
        }

        /// <summary>
        /// Statuses for EChecks
        /// <para>AUTHORIZED</para>
        /// <para>DECLINED</para>
        /// <para>CAPTURED</para>
        /// <para>CANCELLED</para>
        /// <para>SETTLED</para>
        /// <para>REFUNDED</para>
        /// <para>FAILED</para>
        /// </summary>
        /// <param name="oaymentDetailId"></param>
        /// <param name="status"></param>
        private PaymentChargeStatus SetDebitStatus(int oaymentDetailId, string status)
        {

            var statusResult = PaymentChargeStatus.EcheckFailed;

            switch (status)
            {
                case "PENDING":
                    statusResult = PaymentChargeStatus.EcheckPending;
                    break;
                case "SUCCEEDED":
                    statusResult = PaymentChargeStatus.EcheckSucceeded;
                    break;
                case "DECLINED":
                    statusResult = PaymentChargeStatus.EcheckDeclined;
                    break;
                case "VOIDED":
                    statusResult = PaymentChargeStatus.EcheckVoided;
                    break;
                case "REFUNDED":
                    statusResult = PaymentChargeStatus.EcheckRefunded;
                    break;

            }

            //TODO:DBCALL
            //_recordStatusProcess.SetRecordStatus
            //(
            //    Data.HubOneContext.COACH_Process_BankAccount,
            //    oaymentDetailId,
            //    statusResult
            //);

            return statusResult;
        }

        private void SaveSetBankAccountStatus(int paymentMethodId, string status)
        {
            var statusResult = BankAccountStatus.Failed;

            switch (status)
            {
                case "SUCCEEDED":
                    statusResult = BankAccountStatus.Completed;
                    break;
                case "FAILED":
                    statusResult = BankAccountStatus.Failed;
                    break;

            }

            //TODO:DBCALL
            //_recordStatusProcess.SetRecordStatus
            //(
            //    Data.HubOneContext.COACH_BankAccountProcessing,
            //    paymentMethodId,
            //    statusResult
            //);
        }

        /// <summary>
        /// Applies API specific rules to the module object
        /// These rules specifically check for needed fields for the
        /// Quickbooks API.
        /// <para>- Bank Account ID from QB </para>
        /// <para>- Description to put on the check </para>
        /// <para>- Check Number</para>
        /// <para>- Check Amount GT 0.01 and LT 99999.99 </para>
        /// </summary>
        /// <param name="bankAccount"></param>
        /// <param name="eCheck"></param>
        /// <returns>List of fields that were missing or erroneous in the objects that the API needs</returns>
        private Dictionary<string, string> ValidateDebitDetails(HubOneBankAccount bankAccount, HubOneECheck eCheck)
        {
            //https://developer.intuit.com/app/developer/qbpayments/docs/api/resources/all-entities/echecks#create-a-debit
            //These rules cover both TLG and API requirements for a charge.

            var errorDictionary = new Dictionary<string, string>();

            //QB Bank Account ID
            if (string.IsNullOrEmpty(bankAccount.AccountApiIdentifier))
                errorDictionary.Add(nameof(bankAccount.AccountApiIdentifier), ValidatorValues.IsEmpty.ToString());

            //TLG requirement - need to insert info about the group with which this payment is for
            if (string.IsNullOrEmpty(eCheck.Description))
                errorDictionary.Add(nameof(eCheck.Description), ValidatorValues.IsEmpty.ToString());

            if (eCheck.PaymentID == 0)
                errorDictionary.Add(nameof(eCheck.PaymentID), ValidatorValues.IsEmpty.ToString());

            //API boundaries for a check
            if (eCheck.Amount < 0.01m || eCheck.Amount > 99999.99m)
                errorDictionary.Add(nameof(eCheck.Amount), "AmountOutOfBounds");

            return errorDictionary;

        }

        #endregion

        private string GetPaymentMethodType(HubOnePaymentMethodType paymentMethodType)
        {
            //TODO:DBCALL
            //var payMetRdObj = Get<PaymentMethodTypeRead, PaymentMethodTypeRead>("[EXTD_Coach].[spPaymentMethodType_Read_PaymentTypeName]", new PaymentMethodTypeRead()
            //{
            //    PaymentMethodTypeH1Name = paymentMethodType.ToString()

            //});

            var payMetRdObj = new PaymentMethodTypeRead();

            return payMetRdObj.PaymentMethodTypeID.ToString();

        }


        private void LogAPI(Dictionary<string, string> transObj)
        {
            bool hasNoErrorMessage = true;
            bool hasNoErrorDetailMessage = true;
            string errorDetail = "";

            if (transObj.ContainsKey("ErrorMessage"))
                hasNoErrorMessage = string.IsNullOrEmpty(transObj["ErrorMessage"]);

            if (transObj.ContainsKey("ErrorDetail"))
            {
                errorDetail = transObj["ErrorDetail"];
                hasNoErrorDetailMessage = string.IsNullOrEmpty(transObj["ErrorDetail"]);
            }

            if (!hasNoErrorMessage || !hasNoErrorDetailMessage)
            {
                transObj.Add("status", "FAILED");

                //insert logs in to log table

                //Create<APILogInsert, int>("[EXTD_Coach].[spAPILog_Insert]", new APILogInsert()
                //{
                //    CustomerID = Convert.ToInt32(transObj["HubOneCustomerID"]),
                //    APIName = transObj["APIName"],
                //    APICallDesc = transObj["APICallDesc"],
                //    APIEndpoint = transObj["APIEndpoint"],
                //    APICallStatus = transObj["status"],
                //    APICallRequest = transObj["RawRequest"],
                //    APICallResponse = transObj["RawResponse"],
                //    CallException = errorDetail,
                //    CallWarning = transObj["ErrorMessage"],
                //    CreatedOn = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")

                //});

            }
            else
            {
                transObj.Add("status", "SUCCESS");

                //insert logs in to log table
                //Create<APILogInsert, int>("[EXTD_Coach].[spAPILog_Insert]", new APILogInsert()
                //{
                //    CustomerID = Convert.ToInt32(transObj["HubOneCustomerID"]),
                //    APIName = transObj["APIName"],
                //    APICallDesc = transObj["APICallDesc"],
                //    APIEndpoint = transObj["APIEndpoint"],
                //    APICallStatus = string.Concat(transObj["status"]),
                //    APICallRequest = transObj["RawRequest"],
                //    APICallResponse = transObj["RawResponse"],
                //    CallException = "",
                //    CallWarning = "",
                //    CreatedOn = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")

                //});
            }

        }
        
        public void UpdatePaymentMethodForContract(PaymentMethodUpdateForContract paymentMethodUpdate)
        {
            //TODO:DBCALL
            //Update("[EXTD_Coach].[spPayment_UpdatePaymentMethodForContract]", paymentMethodUpdate);
        }

        private int SavePaymentValidationFromResponse(Dictionary<string, string> transObj)
        {
            int paymentDetailID = 0;
            bool hasNoErrorDetailMessage = true;

            if (transObj.ContainsKey("ErrorDetail"))
                hasNoErrorDetailMessage = false;


            if (!hasNoErrorDetailMessage)
            {
                transObj.Add("status", "FAIL_VERIFY");

                //TODO:DBCALL
                //paymentDetailID = Create<PaymentDetailInsert, int>("[EXTD_Coach].[spPaymentDetail_Insert]", new PaymentDetailInsert()
                //{
                //    PaymentID = Convert.ToInt32(transObj["PaymentID"]),
                //    PaymentAmount = 00.00m,
                //    PaymentStatus = transObj["status"],
                //    CallInternalCode = "",
                //    CallRequest = "NO_REQUEST_BODY",
                //    CallResponse = "CHARGE_IS_INVALID_OR_NOT_PRESENT",
                //    CallException = transObj["ErrorDetail"],
                //    CallWarning = "PRE_REFUND_VERIFICATION",
                //    ExternalCode01 = transObj["ExternalCode"],
                //    ExternalCode02 = "",
                //    CreationDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")

                //}, x => x.PaymentDetailID);

            }
            else
            {
                //TODO:DBCALL
                //paymentDetailID = Create<PaymentDetailInsert, int>("[EXTD_Coach].[spPaymentDetail_Insert]", new PaymentDetailInsert()
                //{
                //    PaymentID = Convert.ToInt32(transObj["PaymentID"]),
                //    PaymentAmount = Convert.ToDecimal(transObj["amount"]),
                //    PaymentStatus = transObj["status"],
                //    CallInternalCode = transObj["InternalCode"],
                //    CallRequest = transObj["RawRequest"],
                //    CallResponse = transObj["RawResponse"],
                //    CallException = "",
                //    CallWarning = "PRE_REFUND_VERIFICATION",
                //    ExternalCode01 = transObj["id"],
                //    ExternalCode02 = "",
                //    CreationDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ")

                //}, x => x.PaymentDetailID);
            }

            return paymentDetailID;

        }
        
        private void SetPaymentDisposition(int paymentId, int paymentDetailId, PaymentChargeStatus paymentDetailStatus)
        {

            //Validation
            if (paymentId == 0)
                throw new ArgumentException("PaymentId cannot be zero.");

            if (paymentDetailId == 0)
                throw new ArgumentException("PaymentDetailId cannot be zero.");


            var completionDate = DateTime.UtcNow;
            bool? flagSuccess = false;
            var hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Declined_Other;

            switch (paymentDetailStatus)
            {
                /*Charge statuses - Credit cards*/
                case PaymentChargeStatus.Authorized:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Processing;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.Declined:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Declined_Other;
                    flagSuccess = false;
                    break;
                case PaymentChargeStatus.Captured:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Completed;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.Cancelled:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Cancelled;
                    flagSuccess = false;
                    break;
                case PaymentChargeStatus.Settled:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Completed;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.Refunded:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Completed;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.Failed:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Declined_Other;
                    flagSuccess = true;
                    break;
                /*The status below is a filler call to align all processes.
                 So this is called just to change the dispostion on the payment level.
                 There is no PaymentDetail status that maps to this*/
                case PaymentChargeStatus.RefundInProgress:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Processing;
                    flagSuccess = true;
                    break;

                /*ECheck Statuses - Bank Accounts*/
                case PaymentChargeStatus.EcheckPending:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Processing;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.EcheckSucceeded:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Completed;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.EcheckDeclined:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Declined_Other;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.EcheckVoided:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Cancelled;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.EcheckRefunded:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Completed;
                    flagSuccess = true;
                    break;
                case PaymentChargeStatus.EcheckFailed:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Declined_Other;
                    flagSuccess = false;
                    break;
                /*The status bellow is a filler call to align all processes.
                 So this is called just to change the dispostion on the payment level.
                 There is no PaymentDetail status that maps to this*/
                case PaymentChargeStatus.EcheckRefundInProgress:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Credit_Processing;
                    flagSuccess = true; //TODO: I'm not sure if this flag should be this
                    break;
                default:
                    hubOnePayDisp = HubOnePaymentDisposition.COACH_Payment_Declined_Other;
                    flagSuccess = null;
                    break;

            }

            var paymentUpdate = new PaymentUpdate()
            {
                PaymentID = paymentId,
                PaymentDetailID = paymentDetailId,
                CompletionSuccess = true,
                CompletionDate = completionDate.ToString("yyyy-MM-dd"),
                FlagSuccess = flagSuccess

            };

            UpdatePayment(paymentUpdate);
            UpdatePaymentDispostion(paymentId, hubOnePayDisp);
        }

        private void UpdatePayment(PaymentUpdate paymentUpdate)
        {
            //TODO:DBCALL
            //Update("[EXTD_Coach].[spPayment_Update]", paymentUpdate);

        }

        private void UpdatePaymentDispostion(int paymentId, HubOnePaymentDisposition dispositionName)
        {

            var disUpdate = new DispositionUpdate()
            {
                PaymentID = paymentId,
                DispositionName = dispositionName.ToString()
            };


            //TODO:DBCALL
            //Run("[EXTD_Coach].[spPayment_ChangeDisposition]", disUpdate);


        }

        #endregion
    }
}
