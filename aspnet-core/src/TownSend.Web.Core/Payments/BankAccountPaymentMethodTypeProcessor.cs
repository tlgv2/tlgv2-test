﻿using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Payments.Dto;

namespace TownSend.Payments
{
    public class BankAccountPaymentMethodTypeProcessor : PaymentMethodTypeProcessor
    {
        public BankAccountPaymentMethodTypeProcessor(
            //ILogger<BankAccountPaymentMethodTypeProcessor> logger,
            //MemberContractProcess memberContractProcess,
            //MemberInvoiceProcess memberInvoiceProcess,
            //RecordStatusProcess recordStatusProcess,
            TownsendPaymentProcesses townsendPaymentProcess,
            //ProductProcess productProcess,
            PaymentMethodTypeProcessorResolver paymentMethodTypeProcessorResolver)
            : base(HubOnePaymentMethodType.COACH_BankAccount,
                 ///logger,
                  //memberContractProcess,
                  //memberInvoiceProcess,
                  //recordStatusProcess,
                  townsendPaymentProcess,
                  //productProcess,
                  paymentMethodTypeProcessorResolver)
        {
        }

        public override IPaymentType PreparePaymentAccount(dynamic parameters)
        {
            var bankAccount = new HubOneBankAccount();
            bankAccount.AccountHolderPhone = parameters.PhoneNumber;
            bankAccount.AccountHolderName = parameters.AccountOwnerName;
            bankAccount.AccountNumber = parameters.AccountNumber;
            bankAccount.RoutingNumber = parameters.RoutingNumber;

            return bankAccount;
        }

        public override bool NeedsSecondaryPaymentMethod()
        {
            if (PaymentTerm == PaymentTerm.PeriodicInstallment)
                return true;
            else
                return false;
        }

        protected override bool ProcessPayment(MemberPayment paymentToProcess)
        {
            bool isSuccess = true;

            if (paymentToProcess == null)
                throw new ArgumentNullException(nameof(paymentToProcess));

            bool useTestValue = false;
            if (PaymentAccount is HubOneBankAccount)
            {
                var account = PaymentAccount as HubOneBankAccount;
                if (account.AccountNumber == "11000000333456781" && account.RoutingNumber == "322079353")
                {
                    useTestValue = true;
                }
            }

            //TODO:Needtouncommentinanotherbranch
            //// Create the debit
            //var bankAccount = new HubOneBankAccount();
            //bankAccount.AccountApiIdentifier = paymentToProcess.CardExternalID;
            //var eCheck = new HubOneECheck();
            //eCheck.PaymentID = paymentToProcess.PaymentID;
            //eCheck.Description = paymentToProcess.TeamName;

            //if (useTestValue)
            //{
            //    //VALUE RETURNED STATUS
            //    //$1.11   PENDING
            //    //$2.22   PENDING
            //    //$3.33   DECLINED
            //    //$4.44   DECLINED
            //    //$5.55   SUCCEEDED
            //    Random r = new Random();
            //    int multiplier = r.Next(0, 6);
            //    eCheck.Amount = Math.Round(1.11m * multiplier, 2);
            //}
            //else
            //{
            //    eCheck.Amount = Math.Round(paymentToProcess.PaymentTotal, 2);
            //}

            //var successfulDebitResponse = TownsendPaymentProcess.DebitBankAccount(eCheck, bankAccount);

            //if (successfulDebitResponse.IsSuccessful)
            //{
            //    if ((successfulDebitResponse.ErrorList != null) && (successfulDebitResponse.ErrorList.Any()))
            //    {
            //        ProcessPaymentErrorHandlers.OnGenericError(successfulDebitResponse.ErrorList);
            //        isSuccess = false;
            //    }
            //}
            //else
            //{
            //    ProcessPaymentErrorHandlers.OnQuickbooksError($"Could not charge the bank account.", successfulDebitResponse.ToString(true));
            //    isSuccess = false;
            //}

            return isSuccess;
        }
    }
}
