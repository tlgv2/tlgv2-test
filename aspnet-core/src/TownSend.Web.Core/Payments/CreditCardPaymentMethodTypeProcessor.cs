﻿using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Payments.Dto;

namespace TownSend.Payments
{
    public class CreditCardPaymentMethodTypeProcessor : PaymentMethodTypeProcessor
    {
        public CreditCardPaymentMethodTypeProcessor(
            TownsendPaymentProcesses townsendPaymentProcess,
            PaymentMethodTypeProcessorResolver paymentMethodTypeProcessorResolver)
            : base(HubOnePaymentMethodType.COACH_CreditCard,
                  townsendPaymentProcess,
                  paymentMethodTypeProcessorResolver)
        {
        }

        public override IPaymentType PreparePaymentAccount(dynamic parameters)
        {
            var expiration = Convert.ToString(parameters.Expiration);
            var cardNumber = Convert.ToString(parameters.CardNumber);
            if (!string.IsNullOrEmpty(expiration) && expiration.Length > 3)
                expiration = expiration.Insert(2, "/");

            if (!string.IsNullOrEmpty(cardNumber))
                cardNumber = cardNumber.Replace(" ", string.Empty);

            var creditCard = new HubOneCreditCard();
            creditCard.NameOnCard = parameters.NameOnCard;
            creditCard.CardNumber = cardNumber;
            creditCard.ExpirationMonth = expiration.Substring(0, 2);
            creditCard.ExpirationYear = "20" + expiration.Substring(expiration.Length - 2);
            creditCard.Cvc = parameters.CVC;

            return creditCard;
        }

        protected override bool ProcessPayment(MemberPayment paymentToProcess)
        {
            bool isSuccess = true;

            if (paymentToProcess == null)
                throw new ArgumentNullException(nameof(paymentToProcess));

            var creditCard = new HubOneCreditCard();
            var charge = new HubOneCharge();

            creditCard.CardID = paymentToProcess.CardExternalID;
            charge.PaymentID = paymentToProcess.PaymentID;
            charge.Currency = "USD";
            charge.Description = paymentToProcess.TeamName;
            charge.Amount = Math.Round(paymentToProcess.PaymentTotal, 2);
            charge.PaymentContext = new HubOnePaymentContext()
            {
                IsEcomm = true,
                Mobile = false
            };

            //TODO:Needtouncommentinanotherbranch
            //var successfulChargeResponse = TownsendPaymentProcess.ChargeCreditCard(creditCard, charge);

            //if (successfulChargeResponse.IsSuccessful)
            //{
            //    if ((successfulChargeResponse.ErrorList != null) && (successfulChargeResponse.ErrorList.Any()))
            //    {
            //        ProcessPaymentErrorHandlers.OnGenericError(successfulChargeResponse.ErrorList);
            //        isSuccess = false;
            //    }

            //    var paymentDetailID = int.Parse(successfulChargeResponse.ResponseValues["PaymentDetailID"]);
            //    var status = RecordStatusProcess.GetCurrentStatus(Data.HubOneContext.COACH_Process_CreditCard, paymentDetailID);
            //    if (status.Status == Data.HubOneStatus.COACH_Charge_Declined)
            //    {
            //        ProcessPaymentErrorHandlers.OnAccountError($"Could not charge the credit card, the transaction was declined.");
            //    }
            //}
            //else
            //{
            //    ProcessPaymentErrorHandlers.OnQuickbooksError($"Could not charge the credit card.", successfulChargeResponse.ToString(true));
            //    isSuccess = false;
            //}

            return isSuccess;
        }

        public override bool NeedsSecondaryPaymentMethod()
        {
            return false;
        }
    }
}
