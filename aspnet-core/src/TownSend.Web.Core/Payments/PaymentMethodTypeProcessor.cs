﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TownSend.Members.Dto;
using TownSend.Payments.Dto;
using TownSend.Payments.Quickbooks;

namespace TownSend.Payments
{
    public delegate PaymentMethodTypeProcessor PaymentMethodTypeProcessorResolver(HubOnePaymentMethodType paymentMethodType);

    public static class PaymentMethodTypeProcessorServiceCollectionExtension
    {
        public static IServiceCollection AddPaymentMethodTypeProcessor(this IServiceCollection services)
        {
            services.AddTransient<BankAccountPaymentMethodTypeProcessor>();
            services.AddTransient<CheckPaymentMethodTypeProcessor>();
            services.AddTransient<CreditCardPaymentMethodTypeProcessor>();

            services.AddTransient<PaymentMethodTypeProcessorResolver>(serviceProvider => paymentMethodType =>
            {
                switch (paymentMethodType)
                {
                    case HubOnePaymentMethodType.COACH_BankAccount:
                        return serviceProvider.GetService<BankAccountPaymentMethodTypeProcessor>();
                    case HubOnePaymentMethodType.COACH_Check:
                        return serviceProvider.GetService<CheckPaymentMethodTypeProcessor>();
                    case HubOnePaymentMethodType.COACH_CreditCard:
                        return serviceProvider.GetService<CreditCardPaymentMethodTypeProcessor>();
                    case HubOnePaymentMethodType.COACH_Manual:
                        throw new NotImplementedException("Payment Method Type [COACH_Manual] is not supported.");
                    default:
                        throw new NotImplementedException($"Payment Method Type [{paymentMethodType}] is not supported.");
                }
            });

            return services;
        }
    }

    //public class PaymentMethodTypeProcessorFactory
    //{
    //    public static PaymentMethodTypeProcessor GetProcessor(HubOnePaymentMethodType paymentMethodType)
    //    {
    //        switch (paymentMethodType)
    //        {
    //            case HubOnePaymentMethodType.COACH_BankAccount:
    //                return new BankAccountPaymentMethodTypeProcessor();
    //            //TODO:Needtouncommentinanotherbranch
    //            //case HubOnePaymentMethodType.COACH_Check:
    //            //    return new CheckPaymentMethodTypeProcessor();
    //            case HubOnePaymentMethodType.COACH_CreditCard:
    //                return new CreditCardPaymentMethodTypeProcessor();
    //            case HubOnePaymentMethodType.COACH_Manual:
    //                throw new NotImplementedException("Payment Method Type [COACH_Manual] is not supported.");
    //            default:
    //                throw new NotImplementedException($"Payment Method Type [{paymentMethodType}] is not supported.");
    //        }
    //    }
    //}

    public class SavePaymentMethodErrorHandlers
    {
        public Action<string> OnAccountError { get; set; }
        public Action<IList<string>> OnGenericError { get; set; }
        public Action<string, string> OnQuickbooksError { get; set; }
    }

    public class ProcessPaymentErrorHandlers
    {
        public Action<string> OnAccountError { get; set; }
        public Action<IList<string>> OnGenericError { get; set; }
        public Action<string, string> OnQuickbooksError { get; set; }
    }

    public abstract class PaymentMethodTypeProcessor
    {
        //private readonly ILogger _logger;
        private readonly PaymentMethodTypeProcessorResolver _paymentMethodTypeProcessorResolver;
        //private readonly ProductProcess _productProcess;

        public PaymentMethodTypeProcessor(HubOnePaymentMethodType paymentMethodType,
            //ILogger logger,
            //MemberContractProcess memberContractProcess,
            //MemberInvoiceProcess memberInvoiceProcess,
            //RecordStatusProcess recordStatusProcess,
            TownsendPaymentProcesses townsendPaymentProcess,
            //ProductProcess productProcess,
            PaymentMethodTypeProcessorResolver paymentMethodTypeProcessorResolver)
        {
            //_logger = logger;
            PaymentMethodType = paymentMethodType;
            //MemberInvoiceProcess = memberInvoiceProcess;
            //MemberContractProcess = memberContractProcess;
            //RecordStatusProcess = recordStatusProcess;
            TownsendPaymentProcess = townsendPaymentProcess;
            //_productProcess = productProcess;
            _paymentMethodTypeProcessorResolver = paymentMethodTypeProcessorResolver;
        }

        //protected MemberContractProcess MemberContractProcess { get; }
        //protected MemberInvoiceProcess MemberInvoiceProcess { get; }
        //protected RecordStatusProcess RecordStatusProcess { get; }
        protected TownsendPaymentProcesses TownsendPaymentProcess { get; }


        protected PaymentTerm PaymentTerm
        {
            get;
            private set;
        }

        protected HubOnePaymentMethodType PaymentMethodType { get; }

        protected int CustomerID
        {
            get;
            private set;
        }

        public int PaymentMethodID
        {
            get;
            protected set;
        }

        public int SecondaryPaymentMethodID
        {
            get
            {
                return SecondaryAccountProcessor.PaymentMethodID;
                //if (NeedsSecondaryPaymentMethod())
                //    return SecondaryAccountProcessor.PaymentMethodID;
                //else
                //    return 0;
            }
        }

        protected IPaymentType PaymentAccount
        {
            get;
            private set;
        }

        protected PaymentMethodTypeProcessor SecondaryAccountProcessor
        {
            get;
            private set;
        }

        public SavePaymentMethodErrorHandlers PrimaryAccountErrorHandlers
        {
            get;
            private set;
        }

        public SavePaymentMethodErrorHandlers SecondaryAccountErrorHandlers
        {
            get;
            private set;
        }

        public ProcessPaymentErrorHandlers ProcessPaymentErrorHandlers
        {
            get;
            private set;
        }

        public PaymentMethodTypeProcessor WithCustomerID(int customerID)
        {
            CustomerID = customerID;
            return this;
        }

        public PaymentMethodTypeProcessor WithPaymentTerm(PaymentTerm paymentTerm)
        {
            PaymentTerm = paymentTerm;
            return this;
        }

        public void WithAccount(dynamic accontParameters)
        {
            PaymentAccount = PreparePaymentAccount(accontParameters);

            if (NeedsSecondaryPaymentMethod())
            {
                SecondaryAccountProcessor = _paymentMethodTypeProcessorResolver(HubOnePaymentMethodType.COACH_CreditCard);
                SecondaryAccountProcessor.WithCustomerID(CustomerID);
                SecondaryAccountProcessor.WithPaymentTerm(this.PaymentTerm);

                var expiration = Convert.ToString(accontParameters.DepositCCExpiration);
                var secondaryPaymentMethod = new HubOneCreditCard();
                secondaryPaymentMethod.NameOnCard = accontParameters.DepositCCNameOnCard;
                secondaryPaymentMethod.CardNumber = accontParameters.DepositCCCardNumber;
                secondaryPaymentMethod.ExpirationMonth = expiration.Substring(0, 2);
                secondaryPaymentMethod.ExpirationYear = "20" + expiration.Substring(expiration.Length - 2);
                secondaryPaymentMethod.Cvc = accontParameters.DepositCCCVC;
                SecondaryAccountProcessor.PaymentAccount = secondaryPaymentMethod;
            }
        }

        public PaymentMethodTypeProcessor WithPrimaryAccountErrorHandlers(SavePaymentMethodErrorHandlers errorHandlers)
        {
            PrimaryAccountErrorHandlers = errorHandlers;
            return this;
        }

        public PaymentMethodTypeProcessor WithSecondaryAccountErrorHandlers(SavePaymentMethodErrorHandlers errorHandlers)
        {
            SecondaryAccountErrorHandlers = errorHandlers;
            return this;
        }

        public PaymentMethodTypeProcessor WithProcessPaymentErrorHandlers(ProcessPaymentErrorHandlers errorHandlers)
        {
            ProcessPaymentErrorHandlers = errorHandlers;
            return this;
        }

        public bool RegisterPaymentMethod()
        {
            bool isSuccess = true;
            //in the original code, secondary was called first, so I'm leaving that logic in place.
            if (NeedsSecondaryPaymentMethod())
            {
                isSuccess = SecondaryAccountProcessor.SavePaymentMethod(SecondaryAccountErrorHandlers);
            }

            //now, save the primary
            if (isSuccess)
            {
                isSuccess = SavePaymentMethod(PrimaryAccountErrorHandlers);
            }

            return isSuccess;
        }

        //TODO:Needtouncommentinanotherbranch
        //public int CreateContractAndProcessPayments(int inviteID, PricingResult pricing, int productID, MemberInvite invite)
        //{
        //    // Create the Contract Info
        //    var contractID = CreateContractInfo(CustomerID, inviteID, pricing, PaymentMethodID, productID, invite, PaymentTerm, PaymentMethodType);

        //    var product = _productProcess.GetProduct(productID);

        //    // Create the Invoice (This also creates the payments)
        //    // If this is successful, we'll get the first payment to be processed and create the charge
        //    CreateInvoiceAndPayments(contractID, PaymentTerm, product.ProductStartDate);

        //    ProcessPayments(contractID);

        //    return contractID;
        //}

        protected abstract bool ProcessPayment(MemberPayment paymentToProcess);

        private bool ProcessPayments(int contractID)
        {
            bool isPaymentSuccess = false;
            var paymentID = 0;

            //TODO:DBCALL
            ////Get the Payment object from HubOne DB, create the charge
            //var paymentToProcess = MemberInvoiceProcess.GetMemberPayment(contractID);
            var paymentToProcess = new MemberPayment();
            
            if (paymentToProcess != null)
            {
                isPaymentSuccess = ProcessPayment(paymentToProcess);
                paymentID = paymentToProcess.PaymentID;
            }
            else
            {
                isPaymentSuccess = true;
                paymentID = -1; //yes, this is correct, the SP will find invoiced attached to the same contract with paymentid > 0... meaning all for this contract. This happens when deposit is zero.
            }

            if (NeedsSecondaryPaymentMethod() && isPaymentSuccess)
            {
                // Update remaining payments to use the secondary payment method
                PaymentMethodUpdateForContract(contractID, paymentID, SecondaryPaymentMethodID);
            }

            return isPaymentSuccess;
        }

        private int CreateContractInfo(int customerID, int inviteID, PricingResult pricing, int paymentMethodID, int productID, MemberInvite invite, PaymentTerm? paymentTerm, HubOnePaymentMethodType paymentMethodType)
        {
            var contract = new MemberContractInfo();
            contract.CreationDate = DateTime.UtcNow;
            contract.CustomerID = customerID;
            contract.InviteDiscount = pricing.Discounts.Get(DiscountType.Invite);
            contract.LumpSumDiscount = pricing.LumpSumRebate;
            contract.GenericDiscount = 0m;
            contract.InviteID = inviteID;
            contract.PaymentMethodID = paymentMethodID;
            contract.ProductDuration = pricing.InstallmentCount;
            contract.ProductID = productID;
            contract.ProductPrice = pricing.Price;
            contract.Quantity = 1;
            contract.ReferralID = null;
            contract.ScolarshipDiscount = pricing.Discounts.Get(DiscountType.Scholarship);
            contract.ScholarshipID = invite.ScholarshipID;
            contract.SubTotal = pricing.SubTotal;
            contract.ConfirmationDate = contract.CreationDate;

            //TODO:LOGGER
            //if (_logger.IsEnabled(LogLevel.Debug))
            //{
            //    var json = Newtonsoft.Json.JsonConvert.SerializeObject(contract);
            //    _logger.LogDebug($"Step 4 for Invite ID {inviteID}, creating contract with params: {json}");
            //}

            //TODO:DBCALL
            //var contractID = MemberContractProcess.CreateMemberContract(contract, paymentTerm.Value, paymentMethodType);
            var contractID = 0;

            return contractID;
        }

        private void PaymentMethodUpdateForContract(int contractID, int paymentID, int secondaryPaymentMethodId)
        {
            // Update remaining payments to use the secondary payment method
            PaymentMethodUpdateForContract paymentMethodUpdateForContract = new PaymentMethodUpdateForContract();
            paymentMethodUpdateForContract.ContractInfoID = contractID;
            paymentMethodUpdateForContract.CurrentPaymentID = paymentID;
            paymentMethodUpdateForContract.PaymentMethodID = secondaryPaymentMethodId;
            TownsendPaymentProcess.UpdatePaymentMethodForContract(paymentMethodUpdateForContract);
        }

        //private void CreateInvoiceAndPayments(int contractID, PaymentTerm? paymentTerm, DateTime? startDate)
        //{
        //    var invoice = new MemberInvoice();
        //    invoice.ContractInfoID = contractID;

        //    if (startDate.HasValue)
        //        invoice.FirstInvoiceDate = startDate.Value;

        //    if (paymentTerm == PaymentTerm.PeriodicInstallment)
        //    {
        //        //invoice.InvoiceType = InvoiceType.COACH_Periodic;
        //        invoice.InvoiceType = InvoiceType.COACH_Deposit;
        //    }
        //    else if (paymentTerm == PaymentTerm.LumpSum)
        //    {
        //        invoice.InvoiceType = InvoiceType.COACH_LumpSum;
        //    }
        //    else if (paymentTerm == PaymentTerm.SingleInstallment)
        //    {
        //        invoice.InvoiceType = InvoiceType.COACH_Deposit;
        //    }

        //    MemberInvoiceProcess.CreateMemberInvoice(invoice);
        //}

        public abstract bool NeedsSecondaryPaymentMethod();
        public abstract IPaymentType PreparePaymentAccount(dynamic parameters);

        public virtual bool SavePaymentMethod(SavePaymentMethodErrorHandlers errorHandlers)
        {
            var response = AddPaymentMethodToCustomer(PaymentAccount, CustomerID);
            //string status = response.ResponseValues["status"];
            if (response.ResponseValues.ContainsKey("ErrorDetail"))
            {
                if (!response.ResponseValues["ErrorDetail"].Contains("Card already exists") && !response.ResponseValues["ErrorDetail"].Contains("Bank account already exists"))
                    errorHandlers.OnAccountError(response.ResponseValues["ErrorDetail"]);
            }

            if (response.IsSuccessful)
            {
                PaymentMethodID = HandleCreatePaymentMethodResponse(response, PaymentAccount, CustomerID, errorHandlers.OnGenericError);
                if (PaymentMethodID == -1)
                    return false;
            }
            else
            {
                var accountType = "account";
                if (PaymentAccount is HubOneCreditCard)
                    accountType = "credit card";
                else if (PaymentAccount is HubOneBankAccount)
                    accountType = "bank account";
                else if (PaymentAccount is HubOneManualCheck)
                    accountType = "check";

                errorHandlers.OnQuickbooksError($"Could not add the {accountType} to the customer.", response.ToString(true));
            }

            return true;
        }

        //public void AddSecondaryPaymentMethod(string nameOncard, string cardNumber, string cardExpiration, string cvc,
        //    Action<string> onCardError,
        //    Action<IList<string>> onGenericCardError,
        //    Action<string, string> onQuickbooksError)
        //{
        //    _secondaryPaymentMethod = new HubOneCreditCard();
        //    _secondaryPaymentMethod.NameOnCard = nameOncard;
        //    _secondaryPaymentMethod.CardNumber = cardNumber;
        //    _secondaryPaymentMethod.ExpirationMonth = cardExpiration.Substring(0, 2);
        //    _secondaryPaymentMethod.ExpirationYear = "20" + cardExpiration.Substring(cardExpiration.Length - 2);
        //    _secondaryPaymentMethod.Cvc = cvc;
        //    _onErrorSecondaryPaymentCard = onCardError;
        //    _onGenericErrorSecondaryPaymentCard = onGenericCardError;
        //    _onQuickbooksErrorSecondaryPaymentCard = onQuickbooksError;
        //}

        //public IList<Result<string>> SecondaryPaymentMethodErrors { get; set; }

        //public bool SaveSecondaryPaymentMethod(Action<string> onAccountError, Action<IList<string>> onGenericError, Action<string, string> onQuickbooksError)
        //{
        //    var response = AddPaymentMethodToCustomer(_secondaryPaymentMethod, CustomerID);
        //    string status = response.ResponseValues["status"];
        //    if (response.ResponseValues.ContainsKey("ErrorDetail"))
        //    {
        //        onAccountError(response.ResponseValues["ErrorDetail"]);
        //    }

        //    if (response.IsSuccessful)
        //    {
        //        var secondaryPaymentMethodId = HandleCreatePaymentMethodResponse(response, _secondaryPaymentMethod, CustomerID, onGenericError);
        //        if (secondaryPaymentMethodId == -1)
        //            return false;
        //    }
        //    else
        //    {
        //        onQuickbooksError("Could not add the credit card to the customer.", response.ToString(true));
        //    }

        //    return true;
        //}

        protected HubOnePaymentModuleResponse AddPaymentMethodToCustomer(IPaymentType paymentAccount, int customerID)
        {
            HubOnePaymentModuleResponse response = null;

            HubOneCustomerAPIInformation custAPIInfo = new HubOneCustomerAPIInformation();
            custAPIInfo.CustomerID = customerID;
            custAPIInfo.APIName = HubOneAPIName.QuickbooksAccounting;

            // Use Townsend payment process to bind the payment method to the customer
            if (paymentAccount is HubOneCreditCard)
            {
                //HubOneCreditCard creditCard = new HubOneCreditCard();
                //creditCard.NameOnCard = paymentMethodInfo.NameOnCard;
                //creditCard.CardNumber = paymentMethodInfo.CardNumber;
                //creditCard.ExpirationMonth = paymentMethodInfo.Expiration.Substring(0, 2);
                //creditCard.ExpirationYear = "20" + paymentMethodInfo.Expiration.Substring(paymentMethodInfo.Expiration.Length - 2);
                //creditCard.Cvc = paymentMethodInfo.CVC;
                response = TownsendPaymentProcess.AddCreditCardToCustomerAccount((HubOneCreditCard)paymentAccount, custAPIInfo);
            }
            else if (paymentAccount is HubOneBankAccount)
            {
                //HubOneBankAccount bankAccount = new HubOneBankAccount();
                //bankAccount.AccountHolderPhone = paymentMethodInfo.PhoneNumber;
                //bankAccount.AccountHolderName = paymentMethodInfo.AccountOwnerName;
                //bankAccount.AccountNumber = paymentMethodInfo.AccountNumber;
                //bankAccount.RoutingNumber = paymentMethodInfo.RoutingNumber;

                response = TownsendPaymentProcess.AddBankAccountToCustomerAccount((HubOneBankAccount)paymentAccount, custAPIInfo);
            }
            else
            {
                throw new NotSupportedException($"Payment account of type {paymentAccount.GetType()} is not supported.");
            }

            return response;
        }

        public int HandleCreatePaymentMethodResponse(HubOnePaymentModuleResponse response, IPaymentType paymentAccount, int customerId, Action<IList<string>> onGenericCardError)
        {
            int paymentMethodId = 0;

            if ((response.ErrorList != null) && (response.ErrorList.Any()))
            {
                var errors = new List<string>();

                for (int i = 0; i < response.ErrorList.Count(); i++)
                {
                    if (response.ErrorList.Count == 1 && (response.ErrorList[i].Contains("Card already exists") || response.ErrorList[i].Contains("Bank account already exists")))
                    {
                        // If the error is that the card already exists, verify that we have the payment method id for this customer
                        if (paymentAccount is HubOneCreditCard)
                        {
                            paymentMethodId = TryGetExistingCreditCard((HubOneCreditCard)paymentAccount);
                        }
                        else if (paymentAccount is HubOneBankAccount)
                        {
                            paymentMethodId = TryGetExistingBankAccount((HubOneBankAccount)paymentAccount);
                        }

                        if (paymentMethodId == -1)
                        {
                            errors.Add("Payment Method already exists in Quickbooks but not in the HubOne system.");
                        }

                    }
                    else
                    {
                        // ValidationSummary flag will only display ModelErrors for string.empty as the key
                        errors.Add(response.ErrorList[i]);
                        if (i == response.ErrorList.Count() - 1)
                        {
                            paymentMethodId = -1;
                        }
                    }
                }

                onGenericCardError(errors);
            }

            if (paymentMethodId == 0)
            {
                paymentMethodId = Convert.ToInt32(response.ResponseValues["PaymentMethodID"]);
            }

            return paymentMethodId;
        }

        private int TryGetExistingCreditCard(HubOneCreditCard existingCC)
        {
            var paymentMethodId = TownsendPaymentProcess.TryGetExistingCreditCard(existingCC, CustomerID);

            //If TryGetExistingCreditCard does not return a card, try to pull all payment methods from QB
            if (paymentMethodId == -1)
            {
                paymentMethodId = TownsendPaymentProcess.RebindCustomerCreditCard(CustomerID.ToString(), existingCC.CardNumber);
            }


            return paymentMethodId;
        }

        private int TryGetExistingBankAccount(HubOneBankAccount existingBA)
        {
            var paymentMethodId = TownsendPaymentProcess.TryGetExistingBankAccount(existingBA, CustomerID);

            //If TryGetExistingCreditCard does not return a card, try to pull all payment methods from QB
            if (paymentMethodId == -1)
            {
                paymentMethodId = TownsendPaymentProcess.RebindCustomerCreditCard(CustomerID.ToString(), existingBA.AccountNumber);
            }


            return paymentMethodId;
        }

    }
}
