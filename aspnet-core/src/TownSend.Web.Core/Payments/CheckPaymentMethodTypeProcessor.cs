﻿using System;
using System.Collections.Generic;
using System.Text;
using TownSend.Payments.Dto;

namespace TownSend.Payments
{
    public class CheckPaymentMethodTypeProcessor : PaymentMethodTypeProcessor
    {
        public CheckPaymentMethodTypeProcessor(
            //ILogger<CheckPaymentMethodTypeProcessor> logger,
            //MemberContractProcess memberContractProcess,
            //MemberInvoiceProcess memberInvoiceProcess,
            //RecordStatusProcess recordStatusProcess,
            TownsendPaymentProcesses townsendPaymentProcess,
            //ProductProcess productProcess,
            PaymentMethodTypeProcessorResolver paymentMethodTypeProcessorResolver)
            : base(HubOnePaymentMethodType.COACH_Check,
                  //logger,
                  //memberContractProcess,
                  //memberInvoiceProcess,
                  //recordStatusProcess,
                  townsendPaymentProcess,
                  //productProcess,
                  paymentMethodTypeProcessorResolver)
        {
        }

        public override bool NeedsSecondaryPaymentMethod()
        {
            if (PaymentTerm == PaymentTerm.PeriodicInstallment)
                return true;
            else
                return false;
        }

        public override IPaymentType PreparePaymentAccount(dynamic parameters)
        {
            var manualCheck = new HubOneManualCheck();
            manualCheck.HubOneCustomerID = CustomerID.ToString();
            manualCheck.SignatorName = parameters.CheckSignatorName;

            return manualCheck;
        }

        public override bool SavePaymentMethod(SavePaymentMethodErrorHandlers errorHandlers)
        {
            PaymentMethodID = TownsendPaymentProcess.AddManualCheckPaymentMethod((HubOneManualCheck)PaymentAccount);
            if (PaymentMethodID == -1)
                return false;

            return true;
        }

        protected override bool ProcessPayment(MemberPayment paymentToProcess)
        {
            TownsendPaymentProcess.ManualCheckProcessing("COACH_Payment_Process", PaymentMethodID);

            return true;
        }
    }
}
