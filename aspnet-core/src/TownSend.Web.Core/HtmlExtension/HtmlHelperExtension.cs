﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq.Expressions;
using System.Text;

namespace TownSend.HtmlExtension
{
    public static class HtmlHelperExtension
    {
        private const string _defaultBootstrapFormControl = "form-control";
        private const string _defaultBootstrapSelectPickerStyle = "btn-info";
        private const string _defaultBootstrapSelectPickerFormControl = _defaultBootstrapFormControl + " selectpicker filter";

        private static object _defaultLabelForClass = new { @class = "custom-form-label" };
        private static object _defaultEditorForClass = new { @class = _defaultBootstrapFormControl };
        private static object _defaultEditorForAdditionalViewData = new { htmlAttributes = _defaultEditorForClass };
        private static object _defaultValidationMessageForClass = new { @Class = "text-danger" };

        public static string FormatDate(this HtmlHelper helper, DateTime dateTime)
        {
            return dateTime.ToString("{0:MM/dd/yyyy}");//Data.Constants.MvcFormats.Date);
        }

        public static IHtmlContent DefaultValidationMessageFor<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        {
            return helper.ValidationMessageFor(expression, null, _defaultValidationMessageForClass);
        }

        public static IHtmlContent DefaultEditorFor<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, dynamic data = null, string templateName = null)
        {
            object customEditorForAdditionalViewData = _defaultEditorForAdditionalViewData;

            var attributes = new ExpandoObject() as IDictionary<string, Object>;

            if (data != null || !string.IsNullOrEmpty(customClass))
            {
                if (data != null)
                {
                    var properties = data.GetType().GetProperties();
                    foreach (var property in properties)
                    {
                        var PropertyName = property.Name;

                        PropertyName = PropertyName.Replace("_", "-");

                        var PropetyValue = data.GetType().GetProperty(property.Name).GetValue(data, null);
                        attributes.Add(PropertyName, PropetyValue);
                    }
                }

                if (!string.IsNullOrEmpty(customClass))
                {
                    attributes.Add("class", string.Concat(_defaultBootstrapFormControl, " ", customClass));
                }
                else
                {
                    attributes.Add("class", _defaultBootstrapFormControl);
                }

                customEditorForAdditionalViewData = new { htmlAttributes = attributes };
            }

            if (string.IsNullOrEmpty(templateName))
            {
                return helper.EditorFor(expression, additionalViewData: customEditorForAdditionalViewData);
            }
            else
            {
                return helper.EditorFor(expression, templateName, additionalViewData: customEditorForAdditionalViewData);
            }
        }

        public static IHtmlContent DefaultLabelFor<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customLabelClass = null, bool requiredMarker = false)
        {
            var labelClass = _defaultLabelForClass;
            if (!string.IsNullOrEmpty(customLabelClass))
                labelClass = new { @class = customLabelClass };

            if (requiredMarker)
            {
                //var metaData = ExpressionMetadataProvider.FromLambdaExpression(expression, helper.ViewData, helper.MetadataProvider).Metadata;
                var metaData = helper.ViewContext.ViewData.ModelMetadata;
                //string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
                //var displayFor = metaData.DisplayName; // helpermetaData.dipl.DisplayFor(expression);
                //var labelText = displayFor.ToString();
                var labelText = metaData.DisplayName;

                if (string.IsNullOrEmpty(labelText))
                    return HtmlString.Empty;

                var label = new TagBuilder("label");
                label.Attributes.Add("for", helper.IdFor(expression));

                label.InnerHtml.Append(labelText);

                if (string.IsNullOrEmpty(customLabelClass))
                    label.AddCssClass("custom-form-label");
                else
                    label.AddCssClass(customLabelClass);

                if (metaData.IsRequired)
                    label.InnerHtml.AppendHtml("<span class=\"required\"></span>");

                return label;
            }
            else
            {
                return helper.LabelFor(expression, htmlAttributes: labelClass);
            }
        }

        //public static IHtmlContent DefaultBootstrapSelect<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string title = null, string customStyle = null, string customClass = null, dynamic data = null, bool verticalAlign = false)
        //{
        //    return DefaultBootstrapSelectOrMultiSelect(helper, expression, listItems, false, title, customStyle, customClass, data);
        //}

        //public static IHtmlContent DefaultBootstrapMultiSelect<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string title = null, string customStyle = null, string customClass = null, dynamic data = null, bool verticalAlign = false)
        //{
        //    return DefaultBootstrapSelectOrMultiSelect(helper, expression, listItems, true, title, customStyle, customClass, data);
        //}

        private static IHtmlContent DefaultBootstrapSelectOrMultiSelect<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, bool isMultiple, string title = null, string customStyle = null, string customClass = null, dynamic data = null, bool verticalAlign = false)
        {
            //var fieldName = ((MemberExpression)expression.Body).Member.Name;

            var customHtmlAttributes = new ExpandoObject() as IDictionary<string, Object>;

            if (data != null)
            {
                var properties = data.GetType().GetProperties();
                foreach (var property in properties)
                {
                    var PropertyName = property.Name;

                    PropertyName = PropertyName.Replace("_", "-");

                    var PropetyValue = data.GetType().GetProperty(property.Name).GetValue(data, null);
                    customHtmlAttributes.Add(PropertyName, PropetyValue);
                }
            }

            if (isMultiple)
                customHtmlAttributes.Add("multiple", "multiple");

            if (!string.IsNullOrEmpty(title))
                customHtmlAttributes.Add("title", title);

            customHtmlAttributes.Add("class", string.IsNullOrEmpty(customClass) ? _defaultBootstrapSelectPickerFormControl : string.Concat(_defaultBootstrapSelectPickerFormControl, " ", customClass));
            customHtmlAttributes.Add("data-style", string.IsNullOrEmpty(customStyle) ? _defaultBootstrapSelectPickerStyle : customStyle);

            return helper.DropDownListFor(expression, listItems, customHtmlAttributes);
        }

        public static IHtmlContent DefaultDropDownList<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string customClass = null)
        {
            return DefaultDropDownList(helper, expression, listItems, customClass);
        }

        public static IHtmlContent DefaultDropDownList<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string customClass, dynamic data = null)
        {
            var fieldName = ((MemberExpression)expression.Body).Member.Name;

            object customEditorForAdditionalViewData = _defaultEditorForAdditionalViewData;

            var htmlAttributes = new ExpandoObject() as IDictionary<string, Object>;

            if (data != null || !string.IsNullOrEmpty(customClass))
            {
                if (data != null)
                {
                    var properties = data.GetType().GetProperties();
                    foreach (var property in properties)
                    {
                        var PropertyName = property.Name;

                        PropertyName = PropertyName.Replace("_", "-");

                        var PropetyValue = data.GetType().GetProperty(property.Name).GetValue(data, null);
                        htmlAttributes.Add(PropertyName, PropetyValue);
                    }
                }

                if (!string.IsNullOrEmpty(customClass))
                {
                    htmlAttributes.Add("class", string.Concat(_defaultBootstrapFormControl, " ", customClass));
                }

                return helper.DropDownList(fieldName, listItems, htmlAttributes);

            }
            else
            {
                return helper.DropDownList(fieldName, listItems, _defaultEditorForAdditionalViewData);
            }
        }

        public static IHtmlContent DefaultEditorTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, bool requiredMarker = false, dynamic data = null)
        {
            var cb = new HtmlContentBuilder();
            cb.AppendHtml(DefaultLabelFor(helper, expression, requiredMarker: requiredMarker));
            cb.AppendHtml(DefaultEditorFor(helper, expression, customClass, data, null));
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultEditorDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, dynamic data = null, bool verticalAlign = false)
        {
            var cb = new HtmlContentBuilder();

            if (verticalAlign) //With Other Elements In The Row
            {
                cb.AppendHtml(@"<label class=""custom-form-label"">&nbsp;</label>");
            }

            cb.AppendHtml(DefaultEditorFor(helper, expression, customClass, data, null));
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultDropDownTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string customClass = null, bool requiredMarker = false, string customLabelClass = null, dynamic data = null)
        {
            var cb = new HtmlContentBuilder();
            cb.AppendHtml(DefaultLabelFor(helper, expression, customLabelClass, requiredMarker: requiredMarker));
            cb.AppendHtml(DefaultDropDownList(helper, expression, listItems, customClass, data));
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultDropDownDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string customClass = null, string customLabelClass = null, dynamic data = null, bool verticalAlign = false)
        {
            var cb = new HtmlContentBuilder();

            if (verticalAlign) //With Other Elements In The Row
            {
                cb.AppendHtml(@"<label class=""custom-form-label"">&nbsp;</label>");
            }

            cb.AppendHtml(DefaultDropDownList(helper, expression, listItems, customClass, data));
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultBootstrapSelectTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string title = null, string customStyle = null, string customClass = null, string customLabelClass = null, bool requiredMarker = false, dynamic data = null)
        {
            var cb = new HtmlContentBuilder();
            cb.AppendHtml(DefaultLabelFor(helper, expression, customLabelClass, requiredMarker: requiredMarker));
            cb.AppendHtml(DefaultBootstrapSelectOrMultiSelect(helper, expression, listItems, false, title, customStyle, customClass, data));
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultBootstrapMultiSelectTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string title = null, string customStyle = null, string customClass = null, string customLabelClass = null, bool requiredMarker = false, dynamic data = null)
        {
            var cb = new HtmlContentBuilder();
            cb.AppendHtml(DefaultLabelFor(helper, expression, customLabelClass, requiredMarker: requiredMarker));
            cb.AppendHtml(DefaultBootstrapSelectOrMultiSelect(helper, expression, listItems, true, title, customStyle, customClass, data));
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultBootstrapMultiSelect<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string title = null, string customStyle = null, string customClass = null, dynamic data = null, bool verticalAlign = false)
        {
            return DefaultBootstrapMultiSelectUnoOrDuo(helper, expression, listItems, false, title, customStyle, customClass, data, verticalAlign);
        }

        public static IHtmlContent DefaultBootstrapMultiSelectDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string title = null, string customStyle = null, string customClass = null, dynamic data = null, bool verticalAlign = false)
        {
            return DefaultBootstrapMultiSelectUnoOrDuo(helper, expression, listItems, true, title, customStyle, customClass, data, verticalAlign);
        }

        private static IHtmlContent DefaultBootstrapMultiSelectUnoOrDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, bool showValidation, string title = null, string customStyle = null, string customClass = null, dynamic data = null, bool verticalAlign = false)
        {
            var cb = new HtmlContentBuilder();

            if (verticalAlign) //With Other Elements In The Row
            {
                cb.AppendHtml(@"<label class=""custom-form-label"">&nbsp;</label>");
            }

            cb.AppendHtml(DefaultBootstrapSelectOrMultiSelect(helper, expression, listItems, true, title, customStyle, customClass, data, verticalAlign));

            if (showValidation)
                cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultBootstrapSelect<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string title = null, string customStyle = null, string customClass = null, string customLabelClass = null, bool requiredMarker = false, dynamic data = null, bool verticalAlign = false)
        {
            return DefaultBootstrapSelectUnoOrDuo(helper, expression, listItems, false, title, customStyle, customClass, customLabelClass, requiredMarker, data, verticalAlign);
        }

        public static IHtmlContent DefaultBootstrapSelectDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, string title = null, string customStyle = null, string customClass = null, string customLabelClass = null, bool requiredMarker = false, dynamic data = null, bool verticalAlign = false)
        {
            return DefaultBootstrapSelectUnoOrDuo(helper, expression, listItems, true, title, customStyle, customClass, customLabelClass, requiredMarker, data, verticalAlign);
        }

        private static IHtmlContent DefaultBootstrapSelectUnoOrDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> listItems, bool showValidation, string title = null, string customStyle = null, string customClass = null, string customLabelClass = null, bool requiredMarker = false, dynamic data = null, bool verticalAlign = false)
        {
            var cb = new HtmlContentBuilder();

            if (verticalAlign) //With Other Elements In The Row
            {
                if (string.IsNullOrEmpty(customLabelClass))
                    cb.AppendHtml(@"<label class=""custom-form-label"">&nbsp;</label>");
                else
                    cb.AppendHtml($@"<label class=""{customLabelClass}"">&nbsp;</label>");
            }

            cb.AppendHtml(DefaultBootstrapSelectOrMultiSelect(helper, expression, listItems, false, title, customStyle, customClass, data));

            if (showValidation)
                cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultCheckboxDuo<TModel>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, bool verticalAlign = false, bool isChecked = false, bool disabled = false)
        {
            return DefaultCheckboxDuoOrTrio(helper, expression, false, null, verticalAlign, isChecked, disabled);
        }

        public static IHtmlContent DefaultCheckboxDuo<TModel>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, string onClick, bool verticalAlign = false, bool isChecked = false, bool disabled = false)
        {
            return DefaultCheckboxDuoOrTrio(helper, expression, false, onClick, verticalAlign, isChecked, disabled);
        }

        public static IHtmlContent DefaultCheckboxTrio<TModel>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, bool verticalAlign = false, bool isChecked = false, bool disabled = false)
        {
            return DefaultCheckboxDuoOrTrio(helper, expression, true, null, verticalAlign, isChecked, disabled);
        }

        public static IHtmlContent DefaultCheckboxTrio<TModel>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, string onClick, bool verticalAlign = false, bool isChecked = false, bool disabled = false)
        {
            return DefaultCheckboxDuoOrTrio(helper, expression, true, onClick, verticalAlign, isChecked, disabled);
        }

        private static IHtmlContent DefaultCheckboxDuoOrTrio<TModel>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, bool isTrio, string onClick, bool verticalAlign = false, bool isChecked = false, bool disabled = false)
        {
            var cb = new HtmlContentBuilder();
            if (verticalAlign) //With Other Groups In The Row
            {
                cb.AppendHtml(@"<label class=""custom-form-label"">&nbsp;</label>");
            }

            cb.AppendHtml(@"<div class=""form-check"">");
            cb.AppendHtml(@"<label class=""custom-form-label form-check-label"">");

            if (isChecked && disabled)
                cb.AppendHtml(helper.CheckBoxFor(expression, htmlAttributes: new { @checked = true, @disabled = true, @class = "form-check-input" }));
            else if (isChecked && !string.IsNullOrEmpty(onClick))
                cb.AppendHtml(helper.CheckBoxFor(expression, htmlAttributes: new { @checked = true, @onclick = onClick, @class = "form-check-input" }));
            else if (isChecked)
                cb.AppendHtml(helper.CheckBoxFor(expression, htmlAttributes: new { @checked = true, @class = "form-check-input" }));
            else if (disabled)
                cb.AppendHtml(helper.CheckBoxFor(expression, htmlAttributes: new { @disabled = true, @class = "form-check-input" }));
            else if (!string.IsNullOrEmpty(onClick))
                cb.AppendHtml(helper.CheckBoxFor(expression, htmlAttributes: new { @onclick = onClick, @class = "form-check-input" }));
            else
                cb.AppendHtml(helper.CheckBoxFor(expression, htmlAttributes: new { @class = "form-check-input" }));

            if (isTrio)
                cb.AppendHtml(helper.DisplayNameFor(expression));

            cb.AppendHtml(@"</label>");
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));
            cb.AppendHtml(@"</div>");

            return cb;
        }

        public static IHtmlContent DefaultTextAreaFor<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, bool requiredMarker = false)
        {
            var cb = new HtmlContentBuilder();
            cb.AppendHtml(DefaultLabelFor(helper, expression, requiredMarker: requiredMarker));
            cb.AppendHtml(helper.TextAreaFor(expression, new { @class = "form-control description", @style = "height: 3.5rem;  min-height: 3.5rem;", @onkeyup = "autoGrow(this)" }));
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultDatePickerDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, string customLabelClass = null, string customAppendageClass = null, dynamic data = null)
        {
            return DefaultDatePickerDuoOrTrio(helper, expression, false, customClass, customLabelClass, customAppendageClass, data);
        }

        public static IHtmlContent DefaultDatePickerTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, string customLabelClass = null, string customAppendageClass = null, bool requiredMarker = false, dynamic data = null)
        {
            return DefaultDatePickerDuoOrTrio(helper, expression, true, customClass, customLabelClass, customAppendageClass, requiredMarker, data);
        }

        private static IHtmlContent DefaultDatePickerDuoOrTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, bool isTrio, string customClass = null, string customLabelClass = null, string customAppendageClass = null, bool requiredMarker = false, dynamic data = null)
        {
            var cb = new HtmlContentBuilder();

            //label
            if (isTrio)
                cb.AppendHtml(DefaultLabelFor(helper, expression, customLabelClass, requiredMarker));

            //group
            cb.AppendHtml(@"<div class=""input-group"">");

            if (string.IsNullOrEmpty(customClass))
                cb.AppendHtml(DefaultEditorFor(helper, expression, string.Concat(_defaultBootstrapFormControl, " hubone-date-picker"), data, null));
            else
                cb.AppendHtml(DefaultEditorFor(helper, expression, string.Concat(_defaultBootstrapFormControl, " hubone-date-picker ", customClass), data, null));

            cb.AppendHtml(@"<div class=""input-group-append"">");
            if (string.IsNullOrEmpty(customAppendageClass))
                cb.AppendHtml(@"<button class=""btn btn-primary"" type=""button""><i class=""fa fa-calendar-alt""></i></button>");
            else
                cb.AppendHtml($@"<button class=""btn {customAppendageClass}"" type=""button""><i class=""fa fa-calendar-alt""></i></button>");

            cb.AppendHtml(@"</div>");
            cb.AppendHtml(@"</div>");

            //validation error
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        public static IHtmlContent DefaultDateTimePickerDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, string customLabelClass = null, string customAppendageClass = null, bool isTimeOnly = false, dynamic data = null)
        {
            return DefaultDateTimePickerDuoOrTrio(helper, expression, false, customClass, customLabelClass, customAppendageClass, isTimeOnly, data);
        }

        public static IHtmlContent DefaultDateTimePickerTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, string customLabelClass = null, string customAppendageClass = null, bool requiredmarker = false, bool isTimeOnly = false, dynamic data = null)
        {
            return DefaultDateTimePickerDuoOrTrio(helper, expression, true, customClass, customLabelClass, customAppendageClass, requiredmarker, isTimeOnly, data);
        }

        private static IHtmlContent DefaultDateTimePickerDuoOrTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, bool isTrio, string customClass = null, string customLabelClass = null, string customAppendageClass = null, bool requiredmarker = false, bool isTimeOnly = false, dynamic data = null)
        {
            var cb = new HtmlContentBuilder();

            string icon = "fa fa-calendar-alt";
            if (isTimeOnly)
                icon = "fa fa-clock";

            //label
            if (isTrio)
                cb.AppendHtml(DefaultLabelFor(helper, expression, customLabelClass, requiredmarker));

            //group
            cb.AppendHtml(@"<div class=""input-group"">");

            if (string.IsNullOrEmpty(customClass))
                cb.AppendHtml(DefaultEditorFor(helper, expression, string.Concat(_defaultBootstrapFormControl, " hubone-date-time-picker", isTimeOnly ? " hubone-time-only " : string.Empty), data, null));
            else
                cb.AppendHtml(DefaultEditorFor(helper, expression, string.Concat(_defaultBootstrapFormControl, " hubone-date-time-picker ", isTimeOnly ? " hubone-time-only " : string.Empty, customClass), data, null));

            cb.AppendHtml(@"<div class=""input-group-append"">");
            if (string.IsNullOrEmpty(customAppendageClass))
                cb.AppendHtml($@"<button class=""btn btn-primary"" type=""button""><i class=""{icon}""></i></button>");
            else
                cb.AppendHtml($@"<button class=""btn {customAppendageClass}"" type=""button""><i class=""{icon}""></i></button>");

            cb.AppendHtml(@"</div>");
            cb.AppendHtml(@"</div>");

            //validation error
            cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

            return cb;
        }

        //public static IHtmlContent PhoneNumberFormat<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        //{
        //    return new HtmlString(FormatPhoneNumber(helper, expression));
        //}


        //public static IHtmlContent DefaultPhoneNumberDuo<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, string customLabelClass = null, dynamic data = null)
        //{
        //    return DefaultPhoneNumberDuoOrTrio(helper, expression, false, customClass, customLabelClass, false, data);
        //}

        //public static IHtmlContent DefaultPhoneNumberTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string customClass = null, string customLabelClass = null, bool requiredMarker = false, dynamic data = null)
        //{
        //    return DefaultPhoneNumberDuoOrTrio(helper, expression, true, customClass, customLabelClass, requiredMarker, data);
        //}

        //private static IHtmlContent DefaultPhoneNumberDuoOrTrio<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, bool isTrio, string customClass = null, string customLabelClass = null, bool requiredmarker = false, dynamic data = null)
        //{
        //    var cb = new HtmlContentBuilder();

        //    object customEditorForAdditionalViewData = _defaultEditorForAdditionalViewData;

        //    var attributes = new ExpandoObject() as IDictionary<string, Object>;

        //    if (data != null)
        //    {
        //        var properties = data.GetType().GetProperties();
        //        foreach (var property in properties)
        //        {
        //            var PropertyName = property.Name;

        //            PropertyName = PropertyName.Replace("_", "-");

        //            var PropetyValue = data.GetType().GetProperty(property.Name).GetValue(data, null);
        //            attributes.Add(PropertyName, PropetyValue);
        //        }
        //    }

        //    if (!string.IsNullOrEmpty(customClass))
        //    {
        //        attributes.Add("class", string.Concat(_defaultBootstrapFormControl, " ", customClass));
        //    }
        //    else
        //    {
        //        attributes.Add("class", _defaultBootstrapFormControl);
        //    }


        //    customEditorForAdditionalViewData = new { htmlAttributes = attributes };

        //    var formattedValue = FormatPhoneNumber(helper, expression);

        //    //label
        //    if (isTrio)
        //        cb.AppendHtml(DefaultLabelFor(helper, expression, customLabelClass, requiredmarker));

        //    //cb.AppendHtml(helper.EditorFor(expression, customEditorForAdditionalViewData));
        //    cb.AppendHtml(helper.TextBox(ExpressionHelper.GetExpressionText(expression), formattedValue, attributes));

        //    //validation error
        //    cb.AppendHtml(DefaultValidationMessageFor(helper, expression));

        //    return cb;
        //}

        //private static string FormatPhoneNumber<TModel, TProperty>(IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        //{
        //    var modelExplorer = ExpressionMetadataProvider.FromLambdaExpression(expression, helper.ViewData, helper.MetadataProvider);
        //    var model = modelExplorer.Model;

        //    string modelValue = string.Empty;
        //    if (model != null)
        //    {
        //        modelValue = model.ToString();
        //    }

        //    if (string.IsNullOrEmpty(modelValue) || modelValue.Length < 10)
        //        return string.Empty;

        //    //format the phone number and add it as the Value.
        //    var formattedValue = string.Format("{0:(###) ###-#### x}{1}", modelValue.Substring(0, 10), modelValue.Substring(10));
        //    long phoneDigits;
        //    if (!string.IsNullOrEmpty(formattedValue) && !string.IsNullOrWhiteSpace(formattedValue))
        //    {
        //        if (long.TryParse(formattedValue.ToString(), out phoneDigits))
        //        {
        //            string cleanPhoneDigits = phoneDigits.ToString();
        //            int digitCount = cleanPhoneDigits.Length;

        //            if (digitCount == 10)
        //            {
        //                formattedValue = string.Format("{0:(###) ###-####}", phoneDigits);
        //            }
        //            else if (digitCount > 10)
        //            {
        //                formattedValue = string.Format("{0:(###) ###-#### x#########}", phoneDigits);
        //            }
        //            else
        //            {
        //                formattedValue = @cleanPhoneDigits;
        //            }
        //        }
        //    }

        //    return formattedValue;
        //}

        public static IHtmlContent AddClassIf<TModel>(this IHtmlHelper<TModel> helper, bool condition, string className)
        {
            return new HtmlString(condition ? className : "");
        }

        //public static IHtmlContent NullOrEmptyNbsp<TModel, TProperty>(this IHtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        //{
        //    var modelExplorer = ExpressionMetadataProvider.FromLambdaExpression(expression, helper.ViewData, helper.MetadataProvider);
        //    var model = modelExplorer.Model;

        //    string modelValue = string.Empty;
        //    if (model != null)
        //    {
        //        modelValue = model.ToString();
        //    }

        //    return new HtmlString(string.IsNullOrEmpty(modelValue) ? "&nbsp;" : modelValue);
        //}

        public static IHtmlContent DisabledIf<TModel>(this IHtmlHelper<TModel> helper, bool condition)
        {
            return new HtmlString(condition ? "disabled=\"disabled\"" : string.Empty);
        }

        public static IHtmlContent HideIf<TModel>(this IHtmlHelper<TModel> helper, bool condition)
        {
            return new HtmlString(condition ? "style=\"display: none\"" : string.Empty);
        }
    }
}
