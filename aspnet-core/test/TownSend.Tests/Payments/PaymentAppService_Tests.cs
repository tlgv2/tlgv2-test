﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TownSend.Payments;
using Xunit;

namespace TownSend.Tests.Payments
{
    public class PaymentAppService_Tests : TownSendTestBase
    {
        private readonly IPaymentAppService _paymentAppService;

        public PaymentAppService_Tests()
        {
            _paymentAppService = Resolve<IPaymentAppService>();
        }

        [Fact]
        public async Task Should_Get_AccessToken_for_APICall()
        {
            // Act
            var output = _paymentAppService.GetAccessToken();

            //// Assert
            //var currentUser = await GetCurrentUserAsync();
            //var currentTenant = await GetCurrentTenantAsync();

            //output.User.ShouldNotBe(null);
            //output.User.Name.ShouldBe(currentUser.Name);

            //output.Tenant.ShouldNotBe(null);
            //output.Tenant.Name.ShouldBe(currentTenant.Name);
        }
    }
}
